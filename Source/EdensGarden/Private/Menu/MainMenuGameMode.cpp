// Copyright of Sock Goblins

#include "MainMenuGameMode.h"
#include "MainMenuPlayerController.h"

#include "EdensGardenFunctionLibrary.h"
#include "EdensGardenGameInstance.h"
#include "EdensGardenSaveGame.h"
#include "UserWidget.h"

#include "Kismet/GameplayStatics.h"

AMainMenuGameMode::AMainMenuGameMode()
{
	bPauseable = false;
	PlayerControllerClass = AMainMenuPlayerController::StaticClass();
}

void AMainMenuGameMode::BeginPlay()
{
	Super::BeginPlay();

	if (DefaultMenuWidget)
	{
		DisplayWidget(DefaultMenuWidget);
	}
	else
	{
		UE_LOG(LogEdensGarden, Warning, TEXT("MainMenuGameMode: Default Menu Widget is null"));
	}

	// Loading screen could potentially be visible at this point
	UEdensGardenFunctionLibrary::HideLoadingScreen();
}

void AMainMenuGameMode::DisplayWidget(TSubclassOf<UUserWidget> Widget, int32 ZOrder)
{
	// Remove existing widget
	{
		UEdensGardenFunctionLibrary::RemoveWidgetFromParent(DisplayedWidget);
		DisplayedWidget = nullptr;
	}

	// Create new widget
	if (Widget)
	{
		DisplayedWidget = CreateWidget<UUserWidget>(GetGameInstance(), Widget);
		UEdensGardenFunctionLibrary::AddWidgetToViewport(DisplayedWidget, ZOrder);
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("MainMenuGameMode: Display Widget was given invalid widget"));
	}
}

bool AMainMenuGameMode::CreateSaveGameAndStart(bool bUseEditorSaveIfPossible)
{
	// Temp mode (TODO: use async loading)
	if (DefaultGameLevelName == NAME_None)
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Unable to create new game as default game level is null"));
		return false;
	}

	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance && GameInstance->CreateNewPersistantSave(DefaultGameLevelName, bUseEditorSaveIfPossible))
	{
		UE_LOG(LogEdensGarden, Log, TEXT("New game created successfully, transitioning to map %s"), *DefaultGameLevelName.ToString());
		TransitionToMap(DefaultGameLevelName, EGGM_NEWGAME);
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Failed to create new save game data"));
		return false;
	}

	return true;
}

bool AMainMenuGameMode::LoadSaveGameAndStart(bool bUseEditorSaveIfPossible)
{
	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance && GameInstance->LoadPersistantSave(bUseEditorSaveIfPossible))
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		check(SaveData);

		UE_LOG(LogEdensGarden, Log, TEXT("Existing game loaded successfully, transitioning to map %s"), *SaveData->LevelName.ToString());
		TransitionToMap(SaveData->LevelName, EGGM_LOADGAME);
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Failed to load save game data"));
		return false;
	}

	return true;
}

void AMainMenuGameMode::TransitionToMap(const FName& MapName, const FString& Options)
{
	// We expect levels to hide the loading screen once they have finished initializing the level
	UEdensGardenFunctionLibrary::DisplayLoadingScreen(false, 5.f);

	UGameplayStatics::OpenLevel(this, MapName, true, Options);
}
