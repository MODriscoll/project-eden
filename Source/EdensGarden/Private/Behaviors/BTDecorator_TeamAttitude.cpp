// Copyright of Sock Goblins

#include "BTDecorator_TeamAttitude.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "AIController.h"

UBTDecorator_TeamAttitude::UBTDecorator_TeamAttitude()
{
	NodeName = "TeamAttitude";

	BlackboardKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTDecorator_TeamAttitude, BlackboardKey), AActor::StaticClass());
}

bool UBTDecorator_TeamAttitude::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	const UBlackboardComponent* MyBlackboard = OwnerComp.GetBlackboardComponent();
	const AAIController* MyController = OwnerComp.GetAIOwner();

	if (MyBlackboard && MyController)
	{
		UObject* KeyValue = MyBlackboard->GetValueAsObject(BlackboardKey.SelectedKeyName);
		if (KeyValue)
		{
			// TODO: If cast fails, should also check if it has a controller
			IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(KeyValue);
			if (TeamAgent)
			{
				return FGenericTeamId::GetAttitude(MyController->GetGenericTeamId(), TeamAgent->GetGenericTeamId()) == Attitude;
			}
		}
	}

	return false;
}
