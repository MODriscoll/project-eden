// Copyright of Sock Goblins

#include "BTTask_ActivateAbilitiesByTag.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

#include "AIController.h"

UBTTask_ActivateAbilitiesByTag::UBTTask_ActivateAbilitiesByTag()
{
	NodeName = "ActivateAbilitiesByTag";
	bCreateNodeInstance = true;
	bIgnoreRestartSelf = true;

	bNonBlocking = false;
	bCancelAbilitiesOnAbort = true;
}

EBTNodeResult::Type UBTTask_ActivateAbilitiesByTag::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	// We might already be executing!
	if (CachedASC)
	{
		CleanUp(OwnerComp);
	}

	AAIController* Controller = OwnerComp.GetAIOwner();
	IAbilitySystemInterface* System = Controller ? Cast<IAbilitySystemInterface>(Controller->GetPawn()) : nullptr;

	if (System)
	{
		// Ability system shouldn't be null if implementing this interface
		UAbilitySystemComponent* ASC = System->GetAbilitySystemComponent();
		if (ensure(ASC != nullptr))
		{
			ActivatedSpecs.Reset();

			TArray<FGameplayAbilitySpec*> AbilitiesToActivate;
			ASC->GetActivatableGameplayAbilitySpecsByAllMatchingTags(AbilityTags, AbilitiesToActivate);

			// Try activating abilities
			for (auto AbilitySpec : AbilitiesToActivate)
			{
				// AI should only be running on the server, so we don't care for remote activation
				if (ASC->TryActivateAbility(AbilitySpec->Handle))
				{
					ActivatedSpecs.Add(AbilitySpec->Handle);
				}
			}
		
			// Handle result
			if (bNonBlocking)
			{
				bool bSuccess = ActivatedSpecs.Num() > 0;

				CleanUp(OwnerComp);
				return bSuccess ? EBTNodeResult::Succeeded : EBTNodeResult::Failed;
			}
			else
			{
				// We should only wait if we activated an ability
				if (ActivatedSpecs.Num() > 0)
				{
					CachedASC = ASC;
					MyOwner = &OwnerComp;

					// Need to track if this abilities have finished
					AbilityFinishedHandle = CachedASC->OnAbilityEnded.AddUObject(this, &UBTTask_ActivateAbilitiesByTag::OnAbilityFinished);
					return EBTNodeResult::InProgress;
				}
				else
				{
					return EBTNodeResult::Failed;
				}
			}
		}
	}

	return EBTNodeResult::Failed;
}

void UBTTask_ActivateAbilitiesByTag::DescribeRuntimeValues(const UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTDescriptionVerbosity::Type Verbosity, TArray<FString>& Values) const
{
	Super::DescribeRuntimeValues(OwnerComp, NodeMemory, Verbosity, Values);

	if (!bNonBlocking)
	{
		Values.Add(FString::Printf(TEXT("Waiting on %i abilities"), ActivatedSpecs.Num()));

		if (Verbosity == EBTDescriptionVerbosity::Detailed && CachedASC)
		{
			TArray<FGameplayAbilitySpecHandle> HandleArray = ActivatedSpecs.Array();
			for (int32 Index = 0; Index < HandleArray.Num(); ++Index)
			{
				const FGameplayAbilitySpec* Spec = CachedASC->FindAbilitySpecFromHandle(HandleArray[Index]);
				Values.Add(FString::Printf(TEXT("Ability %i: %s"), Index, Spec ? *Spec->Ability->GetName() : TEXT("Invalid Spec")));
			}
		}
	}
}

EBTNodeResult::Type UBTTask_ActivateAbilitiesByTag::AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (CachedASC != nullptr)
	{
		if (bCancelAbilitiesOnAbort)
		{
			// Cancle abilities
			for (auto AbilityHandle : ActivatedSpecs)
			{
				CachedASC->CancelAbilityHandle(AbilityHandle);		

				// For some reason, asc is being garbage collected just after cancelling an ability
				// (This only happens when ending a PIE session though)
				if (!CachedASC)
				{
					break;
				}
			}
		}
	}

	CleanUp(OwnerComp);
	return EBTNodeResult::Aborted;
}

void UBTTask_ActivateAbilitiesByTag::CleanUp(UBehaviorTreeComponent& OwnerComp)
{
	if (CachedASC && AbilityFinishedHandle.IsValid())
	{
		CachedASC->OnAbilityEnded.Remove(AbilityFinishedHandle);
	}

	CachedASC = nullptr;
	AbilityFinishedHandle.Reset();
	ActivatedSpecs.Empty();
}

void UBTTask_ActivateAbilitiesByTag::OnAbilityFinished(const FAbilityEndedData& EndedData)
{
	if (ensure(CachedASC != nullptr))
	{
		FGameplayAbilitySpec* AbilitySpec = CachedASC->FindAbilitySpecFromHandle(EndedData.AbilitySpecHandle);
		if (AbilitySpec && ActivatedSpecs.Contains(AbilitySpec->Handle))
		{
			ActivatedSpecs.Remove(AbilitySpec->Handle);

			// Are there no more 
			if (ActivatedSpecs.Num() <= 0)
			{
				CleanUp(*MyOwner);
				FinishLatentTask(*MyOwner, EBTNodeResult::Succeeded);
			}
		}
	}
	else
	{
		CleanUp(*MyOwner);
		FinishLatentTask(*MyOwner, EBTNodeResult::Failed);
	}
}
