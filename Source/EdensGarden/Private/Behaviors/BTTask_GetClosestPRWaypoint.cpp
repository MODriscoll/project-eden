// Copyright of Sock Goblins

#include "BTTask_GetClosestPRWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "AIController.h"
#include "PatrolRouteComponent.h"
#include "GameFramework/Pawn.h"

UBTTask_GetClosestPRWaypoint::UBTTask_GetClosestPRWaypoint()
{
	NodeName = "Get Closest Patrol Waypoint";

	// Patrol route components are objects
	PatrolRouteComponentKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_GetClosestPRWaypoint, PatrolRouteComponentKey), UPatrolRouteComponent::StaticClass());

	// Waypoints are only ever vectors
	WaypointKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_GetClosestPRWaypoint, WaypointKey));
}

EBTNodeResult::Type UBTTask_GetClosestPRWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIController* Controller = OwnerComp.GetAIOwner();
	APawn* Pawn = Controller ? Controller->GetPawn() : nullptr;
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();

	if (Pawn && Blackboard)
	{
		UObject* PatrolRouteObject = Blackboard->GetValueAsObject(PatrolRouteComponentKey.SelectedKeyName);
		UPatrolRouteComponent* PatrolRouteComponent = PatrolRouteObject ? Cast<UPatrolRouteComponent>(PatrolRouteObject) : nullptr;

		if (PatrolRouteComponent)
		{
			FVector Waypoint = PatrolRouteComponent->GetWaypointClosestTo(Pawn->GetActorLocation());
			Blackboard->SetValueAsVector(WaypointKey.SelectedKeyName, Waypoint);

			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
