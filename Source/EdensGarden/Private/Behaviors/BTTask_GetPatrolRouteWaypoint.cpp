// Copyright of Sock Goblins

#include "BTTask_GetPatrolRouteWaypoint.h"
#include "BehaviorTree/BlackboardComponent.h"

#include "PatrolRouteComponent.h"

UBTTask_GetPatrolRouteWaypoint::UBTTask_GetPatrolRouteWaypoint()
{
	NodeName = "Get Next Patrol Waypoint";

	// Patrol route components are objects
	PatrolRouteComponentKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_GetPatrolRouteWaypoint, PatrolRouteComponentKey), UPatrolRouteComponent::StaticClass());

	// Waypoints are only ever vectors
	WaypointKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_GetPatrolRouteWaypoint, WaypointKey));
}

EBTNodeResult::Type UBTTask_GetPatrolRouteWaypoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	if (Blackboard)
	{
		UObject* PatrolRouteObject = Blackboard->GetValueAsObject(PatrolRouteComponentKey.SelectedKeyName);
		UPatrolRouteComponent* PatrolRouteComponent = PatrolRouteObject ? Cast<UPatrolRouteComponent>(PatrolRouteObject) : nullptr;

		if (PatrolRouteComponent)
		{
			FVector Waypoint = PatrolRouteComponent->GetNextWaypoint();
			Blackboard->SetValueAsVector(WaypointKey.SelectedKeyName, Waypoint);

			return EBTNodeResult::Succeeded;
		}
	}

	return EBTNodeResult::Failed;
}
