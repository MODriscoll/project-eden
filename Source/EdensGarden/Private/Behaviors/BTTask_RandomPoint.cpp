// Copyright of Sock Goblins

#include "BTTask_RandomPoint.h"
#include "AIController.h"
#include "GameFramework/Pawn.h"

#include "AI/Navigation/NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"

UBTTask_RandomPoint::UBTTask_RandomPoint()
{
	NodeName = "Get Random Point In NavMesh";

	// Random point can only be vector
	RandomPointKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_RandomPoint, RandomPointKey));

	// We can use actors and vectors to retrieve origins
	OriginKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_RandomPoint, OriginKey), AActor::StaticClass());
	OriginKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTTask_RandomPoint, OriginKey));

	Radius = 500.f;
	bMustBeReachable = true;
}

EBTNodeResult::Type UBTTask_RandomPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (RandomPointKey.IsNone())
	{
		return EBTNodeResult::Failed;
	}

	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	AAIController* Owner = OwnerComp.GetAIOwner();
	APawn* Pawn = Owner ? Owner->GetPawn() : nullptr;

	if (Pawn && Blackboard)
	{
		FVector Origin = GetTestOrigin(*Pawn, *Blackboard);
		
		FVector Point(Origin);
		if (bMustBeReachable)
		{
			UNavigationSystem::K2_GetRandomReachablePointInRadius(Pawn, Origin, Point, Radius, nullptr, QueryFilter);
		}
		else
		{
			UNavigationSystem::K2_GetRandomPointInNavigableRadius(Pawn, Origin, Point, Radius, nullptr, QueryFilter);
		}

		Blackboard->SetValueAsVector(RandomPointKey.SelectedKeyName, Point);
	}
	else
	{
		return EBTNodeResult::Failed;
	}

	return EBTNodeResult::Succeeded;
}

FVector UBTTask_RandomPoint::GetTestOrigin(const APawn& OwnerPawn, const UBlackboardComponent& OwnerBlackboard) const
{
	if (OriginKey.IsSet())
	{
		if (OriginKey.SelectedKeyType == UBlackboardKeyType_Object::StaticClass())
		{
			UObject* Object = OwnerBlackboard.GetValue<UBlackboardKeyType_Object>(OriginKey.GetSelectedKeyID());
			if (AActor* Actor = Cast<AActor>(Object))
			{
				return Actor->GetActorLocation();
			}
		}
		else if (OriginKey.SelectedKeyType == UBlackboardKeyType_Vector::StaticClass())
		{
			return OwnerBlackboard.GetValue<UBlackboardKeyType_Vector>(OriginKey.GetSelectedKeyID());
		}
	}

	// If we have reached this point, it means origin key wasn't set
	return OwnerPawn.GetActorLocation();
}