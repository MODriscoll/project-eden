// Copyright of Sock Goblins

#include "EdensGardenSaveGame.h"

UEdensGardenSaveGame::UEdensGardenSaveGame()
{
	Version = EEdensGardenSaveGameVersion::CurrentVersion;
	Policy = ESaveGamePolicy::AllowSave;

	TimesSaved = 0;
	LevelName = NAME_None;
	CheckpointIndex = 0;
	CheckpointName = NAME_None;
}
