// Copyright of Sock Goblins

#include "EffectVolume.h"
#include "Components/BrushComponent.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

AEffectVolume::AEffectVolume()
{
	bRemoveEffectOnExit = true;

	OnActorBeginOverlap.AddDynamic(this, &AEffectVolume::StartOverlap);
	OnActorEndOverlap.AddDynamic(this, &AEffectVolume::EndOverlap);

	UBrushComponent* Brush = GetBrushComponent();
	Brush->SetCollisionProfileName(TEXT("Trigger"));
}

void AEffectVolume::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	if (bRemoveEffectOnExit)
	{
		#if WITH_EDITOR
		// We probaly will be overlapping actors when being streamed out,
		// but we should warn just in case this effect isn't intended
		if (ActiveEffects.Num() > 0 &&
			EndPlayReason == EEndPlayReason::RemovedFromWorld)
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("Effect volume is being streamed out but is still applying effects!"), *GetName());
		}
		#endif

		for (auto Iterator = ActiveEffects.CreateIterator(); Iterator; ++Iterator)
		{
			if (Iterator->Key.IsValid())
			{
				UAbilitySystemComponent* ASC = Iterator->Value.GetOwningAbilitySystemComponent();
				if (ASC)
				{
					ASC->RemoveActiveGameplayEffect(Iterator->Value);
				}
			}
		}
	}

	Super::EndPlay(EndPlayReason);
}

void AEffectVolume::RemoveEffectFromActor(AActor* Actor)
{
	FActiveGameplayEffectHandle* Handle = ActiveEffects.Find(Actor);
	if (Handle)
	{
		UAbilitySystemComponent* ASC = Handle->GetOwningAbilitySystemComponent();
		if (ASC)
		{
			ASC->RemoveActiveGameplayEffect(*Handle);
		}

		ActiveEffects.Remove(Actor);
	}
}

void AEffectVolume::StartOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (!EffectToApply)
	{
		return;
	}

	UAbilitySystemComponent* ASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OtherActor);
	if (ASC)
	{
		// We are responsible for instigating this effect
		FGameplayEffectContextHandle Context(UAbilitySystemGlobals::Get().AllocGameplayEffectContext());
		Context.AddInstigator(this, this);

		// We are also the source of this effect
		Context.AddSourceObject(this);

		// Create and apply new effect
		FGameplayEffectSpecHandle Spec = ASC->MakeOutgoingSpec(EffectToApply, UGameplayEffect::INVALID_LEVEL, Context);
		FActiveGameplayEffectHandle Handle = ASC->ApplyGameplayEffectSpecToSelf(*Spec.Data);

		if (Handle.IsValid())
		{
			ActiveEffects.Add(OtherActor, Handle);
		}
	}
}

void AEffectVolume::EndOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	RemoveEffectFromActor(OtherActor);
}
