// Copyright of Sock Goblins

#include "Abilities/Tasks/AbilityTask_PlayMontageForEvents.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "GameplayAbilityTypes.h"

#include "Animation/AnimInstance.h"
#include "GameFramework/Character.h"

UAbilityTask_PlayMontageForEvents::UAbilityTask_PlayMontageForEvents()
{
	bOnlyReportOnce = false;
	bStopOnAbilityEnd = false;
	PlayRate = 1.f;
	RootMotionTranslationScale = 1.f;
}

void UAbilityTask_PlayMontageForEvents::OnStartBlendOut(UAnimMontage* Montage, bool bInterrupted)
{
	if (Ability && Ability->GetCurrentMontage() == PlayingMontage && Montage == PlayingMontage)
	{
		AbilitySystemComponent->ClearAnimatingAbility(Ability);

		// Reset root motion scale
		ACharacter* Character = Cast<ACharacter>(GetAvatarActor());
		if (Character)
		{
			Character->SetAnimRootMotionTranslationScale(1.f);
		}
	}

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		if (bInterrupted)
		{
			OnInteruppted.Broadcast(FGameplayTag(), FGameplayEventData());
		}
		else
		{
			OnBlendOut.Broadcast(FGameplayTag(), FGameplayEventData());
		}
	}
}

void UAbilityTask_PlayMontageForEvents::OnMontageFinished(UAnimMontage* Montage, bool bInterrupted)
{
	if (!bInterrupted && ShouldBroadcastAbilityTaskDelegates())
	{
		OnCompleted.Broadcast(FGameplayTag(), FGameplayEventData());
	}

	EndTask();
}

void UAbilityTask_PlayMontageForEvents::OnAbilityCancelled()
{
	// Always stop playing the montage on cancel
	if (TryStopPlayingMontage() && ShouldBroadcastAbilityTaskDelegates())
	{
		OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
	}
}

void UAbilityTask_PlayMontageForEvents::OnGameplayEventRecieved(const FGameplayEventData* Payload)
{
	// Check if we have already reported this event
	if (!bOnlyReportOnce ||
		(bOnlyReportOnce && !bHaveReportedEvent))
	{
		if (ShouldBroadcastAbilityTaskDelegates())
		{
			OnEvent.Broadcast(EventTag, *Payload);
			bHaveReportedEvent = true;
		}
	}
}

UAbilityTask_PlayMontageForEvents* UAbilityTask_PlayMontageForEvents::PlayMontageWhileWaitingForEvents(UGameplayAbility* OwningAbility, 
	FName TaskInstanceName,  UAnimMontage* Montage, FGameplayTag TagToListenFor, bool bOnlyReportOnce, bool bStopOnAbilityEnd, 
	float PlayRate, FName StartSection, AActor* OptionalExternalTarget, float RootMotionTranslationScale)
{
	UAbilityTask_PlayMontageForEvents* Task = NewAbilityTask<UAbilityTask_PlayMontageForEvents>(OwningAbility, TaskInstanceName);
	Task->PlayingMontage = Montage;
	Task->EventTag = TagToListenFor;
	Task->bOnlyReportOnce = bOnlyReportOnce;
	Task->bStopOnAbilityEnd = bStopOnAbilityEnd;
	Task->PlayRate = PlayRate;
	Task->StartAtSection = StartSection;
	Task->SetTargetASC(OptionalExternalTarget);
	Task->RootMotionTranslationScale = RootMotionTranslationScale;

	return Task;
}

void UAbilityTask_PlayMontageForEvents::Activate()
{
	bHaveReportedEvent = false;

	if (!Ability || !AbilitySystemComponent)
	{
		return;
	}

	// Tracks if activation has passed
	bool bSuccess = false;

	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	UAnimInstance* AnimInstance = ActorInfo->GetAnimInstance();

	// Need a valid anim instance to play a montage
	if (AnimInstance)
	{
		// Listen to targets event
		UAbilitySystemComponent* ASC = GetTargetASC();
		EventHandle = ASC->GenericGameplayEventCallbacks.FindOrAdd(EventTag).AddUObject(this, &UAbilityTask_PlayMontageForEvents::OnGameplayEventRecieved);

		// Play montage on our actual owner
		if (AbilitySystemComponent->PlayMontage(Ability, Ability->GetCurrentActivationInfo(), PlayingMontage, PlayRate, StartAtSection) > 0.f)
		{
			// Important! Playing a montage could potentially fire off a callback into game code which could kill this ability! Early out if we are  pending kill
			if (!ShouldBroadcastAbilityTaskDelegates())
			{
				return;
			}

			// Bind remaining callbacks
			{
				BlendOutHandle.BindUObject(this, &UAbilityTask_PlayMontageForEvents::OnStartBlendOut);
				AnimInstance->Montage_SetBlendingOutDelegate(BlendOutHandle, PlayingMontage);

				MontageEndHandle.BindUObject(this, &UAbilityTask_PlayMontageForEvents::OnMontageFinished);
				AnimInstance->Montage_SetEndDelegate(MontageEndHandle, PlayingMontage);

				CancelHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &UAbilityTask_PlayMontageForEvents::OnAbilityCancelled);
			}

			// Apply root motion scaling
			ACharacter* Character = Cast<ACharacter>(ActorInfo->AvatarActor.Get());
			if (Character)
			{
				Character->SetAnimRootMotionTranslationScale(RootMotionTranslationScale);
			}

			bSuccess = true;
		}
	}

	if (!bSuccess && ShouldBroadcastAbilityTaskDelegates())
	{
		OnCancelled.Broadcast(FGameplayTag(), FGameplayEventData());
	}

	SetWaitingOnAvatar();
}

void UAbilityTask_PlayMontageForEvents::ExternalCancel()
{
	OnAbilityCancelled();

	Super::ExternalCancel();
}

void UAbilityTask_PlayMontageForEvents::OnDestroy(bool bAbilityEnded)
{
	UAbilitySystemComponent* ASC = GetTargetASC();

	// Need to remove ability events
	if (ASC)
	{
		ASC->GenericGameplayEventCallbacks.FindOrAdd(EventTag).Remove(EventHandle);
	}

	// Need to remove cancelled ability delegate
	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(CancelHandle);
		if (bAbilityEnded && bStopOnAbilityEnd)
		{
			TryStopPlayingMontage();
		}
	}

	Super::OnDestroy(bAbilityEnded);
}

void UAbilityTask_PlayMontageForEvents::SetTargetASC(AActor* Target)
{
	if (Target)
	{
		ExternalTarget = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Target);
	}
	else
	{
		ExternalTarget = nullptr;
	}
}

UAbilitySystemComponent* UAbilityTask_PlayMontageForEvents::GetTargetASC() const
{
	return ExternalTarget ? ExternalTarget : AbilitySystemComponent;
}

bool UAbilityTask_PlayMontageForEvents::TryStopPlayingMontage()
{
	const FGameplayAbilityActorInfo* ActorInfo = Ability->GetCurrentActorInfo();
	UAnimInstance* AnimInstance = ActorInfo ? ActorInfo->GetAnimInstance() : nullptr;

	if (AnimInstance && AbilitySystemComponent)
	{
		// Make sure our owning ability is actually the one animating
		if (AbilitySystemComponent->GetAnimatingAbility() == Ability &&
			AbilitySystemComponent->GetCurrentMontage() == PlayingMontage)
		{
			// Unbind our callbacks
			FAnimMontageInstance* MontageInstance = AnimInstance->GetActiveInstanceForMontage(PlayingMontage);
			if (MontageInstance)
			{
				MontageInstance->OnMontageBlendingOutStarted.Unbind();
				MontageInstance->OnMontageEnded.Unbind();
			}

			AbilitySystemComponent->CurrentMontageStop();
			
			return true;
		}
	}

	return false;
}