// Copyright of Sock Goblins

#include "Abilities/Calculations/TeamBasedDamageCalculation.h"
#include "EGAttributeSet.h"

#include "GenericTeamAgentInterface.h"

struct TeamDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);

	TeamDamageStatics()
	{
		// This calculation should be used on target

		// We keep the damage we were created with
		DEFINE_ATTRIBUTE_CAPTUREDEF(UEGAttributeSet, Damage, Target, true);
	}
};

static const TeamDamageStatics& GetTeamDamageStatics()
{
	static TeamDamageStatics DmgStatics;
	return DmgStatics;
}

UTeamBasedDamageCalculation::UTeamBasedDamageCalculation()
{
	const TeamDamageStatics& DmgStatics = GetTeamDamageStatics();

	RelevantAttributesToCapture.Add(DmgStatics.DamageDef);
}

void UTeamBasedDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const TeamDamageStatics& DmgStatics = GetTeamDamageStatics();

	UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
	UAbilitySystemComponent* TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();

	AActor* SourceActor = SourceASC ? SourceASC->AvatarActor : nullptr;
	AActor* TargetActor = TargetASC ? TargetASC->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float Damage = 0.f;
	if (ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DmgStatics.DamageDef, EvaluationParameters, Damage))
	{
		// We will only ignore applying this damage if both avatars are on the same team (friends)
		// We will still apply damage if both avatars happen to be the same
		IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(SourceActor);
		if (!TeamAgent || !TargetActor || SourceActor == TargetActor ||
			TeamAgent->GetTeamAttitudeTowards(*TargetActor) != ETeamAttitude::Friendly)
		{
			OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DmgStatics.DamageProperty, EGameplayModOp::Additive, Damage));
		}
	}
}
