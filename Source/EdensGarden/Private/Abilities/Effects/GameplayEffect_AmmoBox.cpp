// Copyright of Sock Goblins

#include "Effects/GameplayEffect_AmmoBox.h"
#include "Guns/GunAttributeSet.h"

const FName UGameplayEffect_AmmoBox::AmmoDataName(TEXT("Ammo"));

UGameplayEffect_AmmoBox::UGameplayEffect_AmmoBox()
{
	FSetByCallerFloat AmmoFloat;
	AmmoFloat.DataName = AmmoDataName;

	FGameplayModifierInfo AmmoMod;
	AmmoMod.Attribute = UAmmoGunAttributeSet::GetReserveAmmoAttribute();
	AmmoMod.ModifierOp = EGameplayModOp::Additive;
	AmmoMod.ModifierMagnitude = FGameplayEffectModifierMagnitude(AmmoFloat);

	Modifiers.Add(AmmoMod);
}


