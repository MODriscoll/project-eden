// Copyright of Sock Goblins

#include "GameplayEffect_DirectDamage.h"
#include "Gameplay/EGAttributeSet.h"

const FName UGameplayEffect_DirectDamage::DamageDataName(TEXT("Damage"));

UGameplayEffect_DirectDamage::UGameplayEffect_DirectDamage()
{
	FSetByCallerFloat DamageFloat;
	DamageFloat.DataName = DamageDataName;

	FGameplayModifierInfo DamageMod;
	DamageMod.Attribute = UEGAttributeSet::GetDamageAttribute();
	DamageMod.ModifierOp = EGameplayModOp::Additive;
	DamageMod.ModifierMagnitude = FGameplayEffectModifierMagnitude(DamageFloat);

	Modifiers.Add(DamageMod);

	const FGameplayTag ForceTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Force"));
	InheritableGameplayEffectTags.AddTag(ForceTag);
}
