// Copyright of Sock Goblins

#include "PauseMenuWidget.h"
#include "GameFramework/PlayerController.h"

void UPauseMenuWidget::Unpause()
{
	APlayerController* Controller = GetOwningPlayer();
	if (Controller)
	{
		Controller->SetPause(false);
	}
}


