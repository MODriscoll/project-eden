// Copyright of Sock Goblins

#include "FadingWidget.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

UFadingWidget::UFadingWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	FadeDilation = 1.f;
	FadeMode = EFadeMode::FadeOut;

	ResetFadeValues();
}

void UFadingWidget::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	if (IsFading() && ShouldFadeOut())
	{
		FadeElapsed += InDeltaTime * FMath::Abs(FadeDilation);
		float CurTime = FadeStart + FadeElapsed;
		float DelTime = FadeStart + FadeDelay;
		float FinTime = DelTime + FadeTime;

		if (CurTime >= DelTime)
		{
			CalculateAndSetOpacity(DelTime, CurTime, FinTime);

			// Reset if we are done
			if (CurTime >= FinTime)
			{
				OnFadeFinished.ExecuteIfBound(this);
				ResetFadeValues();
			}
		}
	}
}

void UFadingWidget::ResetFadeValues()
{
	FadeStart = -1.f;
	FadeElapsed = 0.f;
	FadeTime = 0.f;
	FadeDelay = 0.f;
}

float UFadingWidget::GetUpdatedAlpha(float Alpha) const
{
	switch (FadeMode)
	{
		case EFadeMode::FadeIn:
		{
			return Alpha;
		}
		case EFadeMode::FadeOut:
		{
			return 1.f - Alpha;
		}
	}

	return 1.f;
}

void UFadingWidget::CalculateAndSetOpacity(float StartTime, float CurrentTime, float EndTime)
{
	// This is the unsafe method, as we are assuming start time is positive
	// The safe method, would be to abs start time then add it to each value (including start time)

	float Cur = FMath::Max(CurrentTime - StartTime, 0.f);
	float End = (EndTime - StartTime);

	float Alpha = 0.f;
	if (!FMath::IsNearlyZero(End))
	{
		Alpha = FMath::Clamp(Cur / End, 0.f, 1.f);
	}

	SetOpacity(GetUpdatedAlpha(Alpha));
}

void UFadingWidget::SetOpacity(float Alpha)
{
	FLinearColor Color;
	if (ColorAndOpacityDelegate.IsBound())
	{
		Color = ColorAndOpacityDelegate.Execute();
	}
	else
	{
		Color = ColorAndOpacity;
	}

	SetColorAndOpacity(Color.CopyWithNewOpacity(Alpha));
}

void UFadingWidget::StartFading(float Time, EFadeMode Mode, float Delay)
{
	APlayerController* Controller = GetOwningPlayer();
	UWorld* World = Controller ? Controller->GetWorld() : nullptr;

	if (World)
	{
		FadeStart = World->GetTimeSeconds();
		FadeTime = FMath::Abs(Time);
		FadeDelay = FMath::Abs(Delay);
		FadeMode = Mode;

		FadeElapsed = 0.f;

		SetOpacity(1.f);
	}
}

void UFadingWidget::StopFading()
{
	OnFadeFinished.ExecuteIfBound(this);
	ResetFadeValues();
}

bool UFadingWidget::IsFading(bool bConsiderDelay) const
{
	if (FadeStart != -1.f)
	{
		if (bConsiderDelay)
		{
			return true;
		}
		else
		{
			float CurTime = FadeStart + FadeElapsed;
			float DelTime = FadeStart + FadeDelay;
			return CurTime >= DelTime;
		}
	}

	return false;
}
