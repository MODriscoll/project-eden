// Copyright of Sock Goblins

#include "UMG/SpecOpHUDWidget.h"
#include "SpecOp/SpecOpCharacter.h"
#include "SpecOp/SpecOpHUD.h"

#include "Components/VerticalBox.h"

#include "FadingProgressBar.h"
#include "GunInfoWidget.h"
#include "InteractiveWidget.h"

// TODO: will need to add events for when max values (max health, max energy, etc) have changed so
// hud stays up to data correctly (maybe just have attribute send health changed event again?)

USpecOpHUDWidget::USpecOpHUDWidget(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	ProgressFadeTime = 2.f;
	ProgressShowTime = 3.f;
}

void USpecOpHUDWidget::SetOwner(ASpecOpHUD* Owner, ASpecOpCharacter* Character)
{
	MyOwner = Owner;
	MySpecOp = Character; // Only keeping this here to get max attribute values, might want to pass max with events

	if (InteractivePrompt)
	{
		InteractivePrompt->SpecOp = MySpecOp;
	}

	if (MySpecOp)
	{
		SetBarProgress(HealthBar, MySpecOp->GetHealth(), MySpecOp->GetMaxHealth(), TEXT("HealthBar"));
		SetBarProgress(BreathBar->ProgressBar, MySpecOp->GetBreath(), MySpecOp->GetMaxBreath(), TEXT("BreathBar"));
		SetBarProgress(EnergyBar->ProgressBar, MySpecOp->GetEnergy(), MySpecOp->GetMaxEnergy(), TEXT("EnergyBar"));
	}

	// We don't want this bars starting off visible
	{
		if (BreathBar)
		{
			BreathBar->OnFadeFinished.BindDynamic(this, &USpecOpHUDWidget::OnBarFinishFade);
			BreathBar->SetVisibility(ESlateVisibility::Collapsed);
		}

		if (EnergyBar)
		{
			EnergyBar->OnFadeFinished.BindDynamic(this, &USpecOpHUDWidget::OnBarFinishFade);
			EnergyBar->SetVisibility(ESlateVisibility::Collapsed);
		}
	}
}

void USpecOpHUDWidget::NotifySubtitlesSet(const FText& SubtitlesText)
{
	OnDisplaySubtitles(SubtitlesText);
}

void USpecOpHUDWidget::NotifyHealthChanged(float NewHealth, float Alpha, const FGameplayTagContainer& GameplayTags)
{
	if (MySpecOp)
	{
		SetBarProgress(HealthBar, NewHealth, MySpecOp->GetMaxHealth(), TEXT("HealthBar"));
	}

	OnHealthChanged(NewHealth, Alpha, GameplayTags);
}

void USpecOpHUDWidget::NotifyBreathChanged(float NewBreath, float Alpha, const FGameplayTagContainer& GameplayTags)
{
	if (MySpecOp)
	{
		SetBarProgressAndFade(BreathBar, NewBreath, MySpecOp->GetMaxBreath(), TEXT("BreathBar"));
	}

	OnBreathChanged(NewBreath, Alpha, GameplayTags);
}

void USpecOpHUDWidget::NotifyEnergyChanged(float NewEnergy, float Alpha, const FGameplayTagContainer& GameplayTags)
{
	if (MySpecOp)
	{
		SetBarProgressAndFade(EnergyBar, NewEnergy, MySpecOp->GetMaxEnergy(), TEXT("EnergyBar"));
	}

	OnEnergyChanged(NewEnergy, Alpha, GameplayTags);
}

void USpecOpHUDWidget::NotifyEquippedGunChanged(AGunBase* Gun)
{
	if (EquippedGunWidget)
	{
		EquippedGunWidget->SetDisplayedGun(Gun);
	}
}

void USpecOpHUDWidget::NotifyInteractiveChanged(AActor* Interactive, const FText& DisplayTitle)
{
	if (InteractivePrompt)
	{
		InteractivePrompt->OnInteractiveChanged(Interactive, DisplayTitle);
	}

	OnInteractiveChanged(Interactive, DisplayTitle);
}

void USpecOpHUDWidget::NotifyInteractionStart()
{
	if (InteractivePrompt)
	{
		InteractivePrompt->OnInteractionStart();
	}
}

void USpecOpHUDWidget::NotifyInteractionFinished(bool bWasCancelled)
{
	if (InteractivePrompt)
	{
		InteractivePrompt->OnInteractionFinished(bWasCancelled);
	}
}

void USpecOpHUDWidget::NotifyHUDTagCountUpdated(const FGameplayTag& GameplayTag, int32 Count)
{
	OnTagCountChanged(GameplayTag, Count);
}

void USpecOpHUDWidget::NotifyHUDTagEventRecieved(const FGameplayTagContainer& GameplayTags)
{
	OnTagEventRecieved(GameplayTags);
}

void USpecOpHUDWidget::NotifyHUDFailureTagRecieved(const FGameplayTag& GameplayTag)
{
	OnAbilityFailureTagRecieved(GameplayTag);
}

void USpecOpHUDWidget::NotifyCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved)
{
	OnNewCheckpointReached(CheckpointIndex, CheckpointName, bProgressSaved);
}

void USpecOpHUDWidget::SetBarProgress(UProgressBar* Bar, float Current, float Max, const FString& BarString)
{
	if (Bar)
	{
		Bar->SetPercent(Current / Max);
	}
	else
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Unable to update %s progress as bar is null"), *BarString);
	}
}

void USpecOpHUDWidget::SetBarProgressAndFade(UFadingProgressBar* Bar, float Current, float Max, const FString& BarString)
{
	if (Bar)
	{
		Bar->SetPercent(Current / Max);
		Bar->StartFading(ProgressFadeTime, EFadeMode::FadeOut, ProgressShowTime);

		Bar->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
	else
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Unable to update %s progress as bar is null"), *BarString);
	}
}

void USpecOpHUDWidget::OnBarFinishFade(UFadingWidget* FadingWidget)
{
	FadingWidget->SetVisibility(ESlateVisibility::Collapsed);
}
