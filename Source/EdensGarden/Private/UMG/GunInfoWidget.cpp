// Copyright of Sock Goblins

#include "GunInfoWidget.h"
#include "AmmoGun.h"
#include "FuelGun.h"

#include "Components/Image.h"

void UGunInfoWidget::NotifyOwnerToFadeOut()
{
	if (MasterWidget.IsValid())
	{
		MasterWidget->StartMasterFadeOut();
	}
}

void UAmmoGunInfoWidget::InitGunWidget(AGunBase* Gun)
{
	AmmoGun = Cast<AAmmoGun>(Gun);
	if (!AmmoGun)
	{
		UE_LOG(LogEGHUD, Error, TEXT("Ammo Gun Info Widget was expecting an ammo gun. Type recieved is invalid"));
		return;
	}

	UAmmoGunAttributeSet* AttributeSet = AmmoGun->GetAttributeSet();
	if (AttributeSet)
	{
		AttributeSet->OnAmmoCountChanged.AddDynamic(this, &UAmmoGunInfoWidget::InternalAmmoCountChanged);
	}
	else
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Ammo Gun Info Widget was unable to listen for "
			"ammo changes as attribute set for gun %s was null"), *AmmoGun->GetName());
	}

	OnAmmoCountUpdated(AmmoGun->GetClipCount(), AmmoGun->GetReserveAmmo());
}

void UAmmoGunInfoWidget::InternalAmmoCountChanged(float ClipCount, float ReserveAmmo)
{
	OnAmmoCountUpdated(ClipCount, ReserveAmmo);
	NotifyOwnerToFadeOut();
}

void UFuelGunInfoWidget::InitGunWidget(AGunBase* Gun)
{
	FuelGun = Cast<AFuelGun>(Gun);
	if (!FuelGun)
	{
		UE_LOG(LogEGHUD, Error, TEXT("Fuel Gun Info Widget was expecting a fuel gun. Type recieved is invalid"));
		return;
	}

	UFuelGunAttributeSet* AttributeSet = FuelGun->GetAttributeSet();
	if (AttributeSet)
	{
		AttributeSet->OnFuelCountChanged.AddDynamic(this, &UFuelGunInfoWidget::InternalFuelCountChanged);
	}
	else
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Fuel Gun Info Widget was unable to listen for "
			"fuel tank changes as attribute set for gun %s was null"), *FuelGun->GetName());
	}

	OnFuelCountUpdated(FuelGun->GetFuelCount(), FuelGun->GetMaxFuelCount());
}

void UFuelGunInfoWidget::InternalFuelCountChanged(float FuelCount, float MaxFuelCount)
{
	OnFuelCountUpdated(FuelCount, MaxFuelCount);
	NotifyOwnerToFadeOut();
}

UGunMasterWidget::UGunMasterWidget()
{
	WidgetShowTime = 5.f;
	WidgetFadeTime = 2.f;
}

void UGunMasterWidget::SetDisplayedGun(AGunBase* Gun)
{
	// Always remove existing info widget from viewport
	if (GunInfo)
	{
		GunInfo->RemoveFromParent();
		GunInfo = nullptr;
	}

	if (Gun)
	{
		const FGunWidgetData& WidgetData = Gun->GetWidgetData();

		// Update icon
		{
			if (GunIcon)
			{
				GunIcon->SetBrush(WidgetData.Icon);
				GunIcon->SetVisibility(ESlateVisibility::Visible);
			}
			else
			{
				UE_LOG(LogEGHUD, Warning, TEXT("Icon for gun %s has null"), *Gun->GetName());
			}
		}

		// Update info widget
		{
			if (WidgetData.InfoWidget)
			{
				GunInfo = CreateWidget<UGunInfoWidget>(GetOwningPlayer(), WidgetData.InfoWidget);
				if (GunInfo)
				{
					GunInfo->MasterWidget = this;
					GunInfo->InitGunWidget(Gun);

					AttachGunInfoWidgetToHUD(GunInfo);
				}
				else
				{
					UE_LOG(LogEGHUD, Warning, TEXT("Failed at creating gun info widget for gun %s"), *Gun->GetName());
				}
			}
			else
			{
				UE_LOG(LogEGHUD, Warning, TEXT("Info widget for gun %s is null"), *Gun->GetName());
			}
		}
	}
	else
	{
		// Invalidate icon so previous icon doesn't linger
		if (GunIcon)
		{
			GunIcon->SetBrush(FSlateBrush());
			GunIcon->SetVisibility(ESlateVisibility::Hidden);
		}
	}

	StartMasterFadeOut();
}

void UGunMasterWidget::StartMasterFadeOut()
{
	StartFading(WidgetFadeTime, EFadeMode::FadeOut, WidgetShowTime);
	SetVisibility(ESlateVisibility::SelfHitTestInvisible);
}
