// Copyright of Sock Goblins

#include "FadingProgressBar.h"

void UFadingProgressBar::SetPercent(float Percent)
{
	if (ProgressBar)
	{
		ProgressBar->SetPercent(Percent);
	}
}


