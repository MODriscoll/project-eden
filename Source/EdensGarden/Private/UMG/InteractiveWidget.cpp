// Copyright of Sock Goblins

#include "InteractiveWidget.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

#include "SpecOpCharacter.h"

void UInteractiveWidget::OnInteractiveChanged_Implementation(AActor* Interactive, const FText& DisplayTitle)
{
	if (Interactive && !DisplayTitle.IsEmpty())
	{
		if (TitleText)
		{
			TitleText->SetText(DisplayTitle);
		}

		SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
	else
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}

void UInteractiveWidget::OnInteractionStart_Implementation()
{
	if (TitleText)
	{
		TitleText->SetVisibility(ESlateVisibility::Collapsed);
	}

	if (InteractionProgress)
	{
		InteractionProgress->SetVisibility(ESlateVisibility::SelfHitTestInvisible);

		if (!InteractionProgress->PercentDelegate.IsBound())
		{
			InteractionProgress->PercentDelegate.BindDynamic(this, &UInteractiveWidget::GetInteractionProgress);
		}
	}
}

void UInteractiveWidget::OnInteractionFinished_Implementation(bool bWasCancelled)
{
	if (InteractionProgress)
	{
		InteractionProgress->SetVisibility(ESlateVisibility::Collapsed);
	}

	if (TitleText)
	{
		TitleText->SetVisibility(ESlateVisibility::SelfHitTestInvisible);
	}
}

float UInteractiveWidget::GetInteractionProgress()
{
	ASpecOpCharacter* Character = GetSpecOp();
	if (Character)
	{
		UInteractiveComponent* InteractComp = Character->GetInteractiveArea();
		return InteractComp->GetInteractionProgress();
	}

	return 0.f;
}

ASpecOpCharacter* UInteractiveWidget::GetSpecOp() const
{
	if (SpecOp.IsValid())
	{
		return SpecOp.Get();
	}

	return nullptr;
}
