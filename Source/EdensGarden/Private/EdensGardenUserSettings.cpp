// Copyright of Sock Goblins

#include "EdensGardenUserSettings.h"

UEdensGardenUserSettings::UEdensGardenUserSettings()
{
	SetToDefaults();
}

void UEdensGardenUserSettings::SetToDefaults()
{
	Super::SetToDefaults();

	bSubtitlesEnabled = false;
	bSavingEnabled = true;
	Sensitivity = 45.f;
	FocusedSensitivity = 30.f;
}

UEdensGardenUserSettings* UEdensGardenUserSettings::GetEdensGardenUserSettings()
{
	return Cast<UEdensGardenUserSettings>(GetGameUserSettings());
}

void UEdensGardenUserSettings::SetSubtitlesEnabled(bool bEnable)
{
	bSubtitlesEnabled = bEnable;
}

bool UEdensGardenUserSettings::IsSubtitlesEnabled() const
{
	return bSubtitlesEnabled;
}

void UEdensGardenUserSettings::SetSavingEnabled(bool bEnable)
{
	bSavingEnabled = bEnable;
}

bool UEdensGardenUserSettings::IsSavingEnabled() const
{
	return bSavingEnabled;
}

void UEdensGardenUserSettings::SetSensitivity(float InSensitivity, bool bFocused)
{
	if (bFocused)
	{
		FocusedSensitivity = InSensitivity;;
	}
	else
	{
		Sensitivity = InSensitivity;
	}
}

float UEdensGardenUserSettings::GetSensitivity(bool bFocused) const
{
	if (bFocused)
	{
		return FocusedSensitivity;
	}
	else
	{
		return Sensitivity;
	}
}

void UEdensGardenUserSettings::SetInvertYAxis(bool bInvert)
{
	bInvertYSensitivity = bInvert;
}

bool UEdensGardenUserSettings::ShouldInvertYAxis() const
{
	return bInvertYSensitivity;
}
