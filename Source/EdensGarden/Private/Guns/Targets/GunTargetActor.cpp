// Copyright of Sock Goblins

#include "Targets/GunTargetActor.h"
#include "Abilities/GameplayAbility.h"

#include "DrawDebugHelpers.h"

DEFINE_LOG_CATEGORY(LogGunTargetActor);

AGunTargetActor::AGunTargetActor()
{
	PrimaryActorTick.bCanEverTick = false;

	bDrawDebug = false;
	DebugDuration = 2.f;
}

void AGunTargetActor::InitializeTargeting(UGameplayAbility* Ability)
{
	check(Ability != nullptr);
	OwningAbility = Ability;
	OwningController = Ability->GetCurrentActorInfo()->PlayerController.Get();

	InternalInitialize();
}

void AGunTargetActor::StartTargeting()
{
	const FGameplayAbilityActorInfo* ActorInfo = OwningAbility ? OwningAbility->GetCurrentActorInfo() : nullptr;
	SourceActor = ActorInfo ? ActorInfo->AvatarActor.Get() : nullptr;

	// Stop here if we require a source actor but none has been provided
	if (!SourceActor && RequiresSourceActor())
	{
		UE_LOG(LogGunTargetActor, Warning, TEXT("Gun targeting cancelled as required source actor is null!"));

		CancelTargeting();
		return;
	}

	InternalStart();
}

void AGunTargetActor::FinishTargeting(FGameplayAbilityTargetDataHandle Handle)
{
	InternalFinish();

	if (OnTraceCompleteDelegate.IsBound())
	{
		OnTraceCompleteDelegate.Broadcast(Handle);
	}

	Destroy();
}

void AGunTargetActor::CancelTargeting()
{
	InternalCancel();

	if (OnCancelledDelegate.IsBound())
	{
		OnCancelledDelegate.Broadcast(FGameplayAbilityTargetDataHandle());
	}

	Destroy();
}

void AGunTargetActor::DrawDebugTrace(const FVector& Start, const FVector& End, FColor Color, float Thickness) const
{
	#if EG_DRAW_DEBUG
	if (bDrawDebug)
	{
		DrawDebugLine(GetWorld(), Start, End, Color, true, DebugDuration, 0, Thickness);
	}
	#endif
}
