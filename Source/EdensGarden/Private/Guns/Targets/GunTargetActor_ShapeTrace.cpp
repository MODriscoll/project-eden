// Copyright of Sock Goblins

#include "GunTargetActor_ShapeTrace.h"

#include "Engine/World.h"
#include "DrawDebugHelpers.h"

DECLARE_CYCLE_STAT(TEXT("GunTargetActor_ShapeTrace - PerformTrace"), STAT_GunTargetActor_PerformTrace, STATGROUP_GunTargetActor);

AGunTargetActor_ShapeTrace::AGunTargetActor_ShapeTrace()
{
	bIgnoreSourceActor = true;
	Offset = 0.f;
	bPerformLineOfSightTrace = true;
	bLineOfSightFromCenter = false;
	LineOfSightProfile = FCollisionProfileName(TEXT("BlockAll"));
}

void AGunTargetActor_ShapeTrace::PerformTrace(FGameplayAbilityTargetDataHandle& Handle, const FCollisionShape& Shape) const
{
	SCOPE_CYCLE_COUNTER(STAT_GunTargetActor_PerformTrace);

	UWorld* World = GetWorld();
	FGameplayAbilityTargetingLocationInfo Source = Origin;

	// First offset our origin to new location
	OffsetTargetLocation(Source, Offset);

	FTransform STransform = Source.GetTargetingTransform();
	FCollisionQueryParams QParams = GenerateQueryParams();

	#if EG_DRAW_DEBUG
	DrawDebugTrace(Origin.GetTargetingTransform().GetLocation(), STransform.GetLocation(), FColor::Blue);
	#endif

	// Origin for the line of sight test
	FVector LOSOrigin = STransform.GetLocation();
	if (bPerformLineOfSightTrace && !bLineOfSightFromCenter)
	{
		LOSOrigin = Origin.GetTargetingTransform().GetLocation();
	}

	// Perform actual trace (this will handle the line of sight test if set)
	TArray<FHitResult> Results;
	ShapeTrace(Results, World, STransform, Shape, TraceProfile.Name, QParams, Filter, &LOSOrigin);

	FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
	ActorArray->SourceLocation = Source;

	// Copy the hit results directly into the target data
	CopyActorsToArray(Results, ActorArray->TargetActorArray);

	// The handle will now manage the new actor array (via shared pointer)
	Handle.Add(ActorArray);
}

void AGunTargetActor_ShapeTrace::OffsetTargetLocation(FGameplayAbilityTargetingLocationInfo& OutLocationInfo, float Amount, EAxis::Type Axis) const
{
	FTransform Transform = OutLocationInfo.GetTargetingTransform();

	// Apply offset
	Transform.AddToTranslation(Transform.GetUnitAxis(Axis) * Amount);

	OutLocationInfo.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
	OutLocationInfo.LiteralTransform = Transform;
}

void AGunTargetActor_ShapeTrace::CopyActorsToArray(const TArray<FHitResult>& Results, TArray<TWeakObjectPtr<AActor>>& OutActors) const
{
	for (const auto& Hit : Results)
	{
		OutActors.Add(Hit.Actor);
	}
}

bool AGunTargetActor_ShapeTrace::ShapeTrace(TArray<FHitResult>& OutResults, UWorld* World, 
	const FTransform& Transform, const FCollisionShape& Shape, const FName& ProfileName, const FCollisionQueryParams& Params, 
	const FGameplayTargetDataFilterHandle& Filter, const FVector* LineOfSightOrigin) const
{
	check(World);

	FVector Origin = Transform.GetLocation();

	TArray<FOverlapResult> Overlaps;
	World->OverlapMultiByProfile(Overlaps, Origin, Transform.GetRotation(), ProfileName, Shape, Params);

	// No overlaps, don't need to continue
	if (!(Overlaps.Num() > 0))
	{
		DrawDebugShape(Transform, Shape, FColor::Magenta);
		return false;
	}

	DrawDebugShape(Transform, Shape, FColor::Emerald);

	OutResults.Reset();

	// Update our origin based on the line of sight test
	if (LineOfSightOrigin)
	{
		Origin = *LineOfSightOrigin;
	}

	// TODO: will prob have to re-work as this prim might not cover all of actor
	// We only want to process an actor once (multiple
	// of its primitives could have been overlapped)
	TSet<AActor*> HandledActors;
	for (int32 Index = 0; Index < Overlaps.Num(); ++Index)
	{
		const FOverlapResult& Result = Overlaps[Index];
		AActor* HitActor = Result.GetActor();

		// We only want to process the actor once
		if (HitActor && !HandledActors.Contains(HitActor) && Filter.FilterPassesForActor(HitActor))
		{
			FHitResult Hit;
			if (GenerateFakeHitResult(Hit, World, Origin, HitActor, Result.GetComponent(), bPerformLineOfSightTrace))
			{
				HandledActors.Add(HitActor);
				OutResults.Add(Hit);
			}
		}
	}

	return OutResults.Num() > 0;
}

bool AGunTargetActor_ShapeTrace::GenerateFakeHitResult(FHitResult& OutResult, UWorld* World, const FVector& Start, AActor* Actor, UPrimitiveComponent* Primitive, bool bTestLOS) const
{
	if (bTestLOS)
	{
		FCollisionQueryParams QParams;
		QParams.bTraceComplex = true;
		QParams.bTraceAsyncScene = true;

		if (World->LineTraceSingleByProfile(OutResult, Start, Primitive->Bounds.Origin, TEXT("BlockAll"), QParams))
		{
			if (OutResult.Actor != Actor)
			{
				DrawDebugTrace(Start, OutResult.Location, FColor::Red);
				return false;
			}
			else
			{
				DrawDebugTrace(Start, OutResult.Location, FColor::Green);
				return true;
			}
		}
	}

	OutResult = FHitResult(Actor, Primitive, Primitive->Bounds.Origin, (Start - Primitive->Bounds.Origin).GetSafeNormal());
	DrawDebugTrace(Start, OutResult.Location, FColor::Cyan);
	return true;
}

FCollisionQueryParams AGunTargetActor_ShapeTrace::GenerateQueryParams() const
{
	FCollisionQueryParams QParams;
	QParams.bReturnPhysicalMaterial = true;
	QParams.bTraceComplex = true;
	QParams.bTraceComplex = true;

	if (bIgnoreSourceActor)
	{
		QParams.AddIgnoredActor(SourceActor);
	}

	return QParams;
}

void AGunTargetActor_ShapeTrace::DrawDebugShape(const FTransform& Transform, const FCollisionShape& Shape, FColor Color, float Thickness) const
{
	#if EG_DRAW_DEBUG
	if (bDrawDebug)
	{
		switch (Shape.ShapeType)
		{
			case ECollisionShape::Box:
			{
				DrawDebugBox(GetWorld(), Transform.GetLocation(), Shape.GetExtent(), Transform.GetRotation(), Color, true, DebugDuration, 0, Thickness);
				break;
			}
			case ECollisionShape::Sphere:
			{
				DrawDebugSphere(GetWorld(), Transform.GetLocation(), Shape.GetSphereRadius(), 16, Color, true, DebugDuration, 0, Thickness);
				break;
			}
			case ECollisionShape::Capsule:
			{
				DrawDebugCapsule(GetWorld(), Transform.GetLocation(), Shape.GetCapsuleHalfHeight(), Shape.GetCapsuleRadius(), Transform.GetRotation(), Color, true, DebugDuration, 0, 2.f);
				break;
			}
			default:
				break;
		}
	}
	#endif
}

AGunTargetActor_BoxTrace::AGunTargetActor_BoxTrace()
{
	HalfExtents = FVector(100.f);
}

void AGunTargetActor_BoxTrace::InternalStart_Implementation()
{
	FCollisionShape Box = FCollisionShape::MakeBox(HalfExtents);

	// Trace the world using our box
	FGameplayAbilityTargetDataHandle Handle;
	PerformTrace(Handle, Box);

	FinishTargeting(Handle);
}

AGunTargetActor_SphereTrace::AGunTargetActor_SphereTrace()
{
	Radius = 100.f;
}

void AGunTargetActor_SphereTrace::InternalStart_Implementation()
{
	FCollisionShape Sphere = FCollisionShape::MakeSphere(Radius);

	// Trace the world using our sphere
	FGameplayAbilityTargetDataHandle Handle;
	PerformTrace(Handle, Sphere);

	FinishTargeting(Handle);
}

AGunTargetActor_CapsuleTrace::AGunTargetActor_CapsuleTrace()
{
	Radius = 50.f;
	HalfHeight = 100.f;
}

void AGunTargetActor_CapsuleTrace::InternalStart_Implementation()
{
	FCollisionShape Capsule = FCollisionShape::MakeCapsule(Radius, HalfHeight);

	// Trace the world using our capsule
	FGameplayAbilityTargetDataHandle Handle;
	PerformTrace(Handle, Capsule);

	FinishTargeting(Handle);
}
