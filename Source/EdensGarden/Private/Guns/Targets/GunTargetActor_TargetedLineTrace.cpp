// Copyright of Sock Goblins

#include "GunTargetActor_TargetedLineTrace.h"
#include "Kismet/KismetMathLibrary.h"

AGunTargetActor_TargetedLineTrace::AGunTargetActor_TargetedLineTrace()
{
	bTraceTargetDirectly = true;
	TargetingRadius = 100.f;
	TargetingPrecisionOdds = 1.f;
}

void AGunTargetActor_TargetedLineTrace::InternalInitialize_Implementation()
{
	Super::InternalInitialize_Implementation();

	TargetingRadius = FMath::Max(0.f, TargetingRadius);
	TargetingPrecisionOdds = FMath::Clamp(TargetingPrecisionOdds, 0.f, 1.f);
}

void AGunTargetActor_TargetedLineTrace::InternalStart_Implementation()
{
	// Need a valid location to target
	if (TargetLocation.ContainsNaN())
	{
		CancelTargeting();
		return;
	}

	FTransform OGTransform = Origin.GetTargetingTransform();
	FVector Start = OGTransform.GetLocation();
	FVector TraceDirection = (GetTargetPoint() - Start).GetSafeNormal();

	// Can't trace with nulled vector
	if (TraceDirection.IsNearlyZero())
	{
		TraceDirection = OGTransform.GetUnitAxis(EAxis::X);
	}

	// Apply spread before calculating target point
	FVector Target = Start + (ApplySpread(TraceDirection) * TraceRange);

	FCollisionQueryParams QParams;
	QParams.bReturnPhysicalMaterial = true;
	QParams.bTraceComplex = true;
	QParams.bTraceAsyncScene = true;

	// Ignore our source actor
	QParams.AddIgnoredActor(SourceActor);

	FGameplayAbilityTargetDataHandle Handle = TraceWorldByChannel(Start, Target, TraceChannel, QParams, Filter);
	FinishTargeting(Handle);
}

FVector AGunTargetActor_TargetedLineTrace::GetTargetPoint() const
{
	if (bTraceTargetDirectly)
	{
		return TargetLocation;
	}

	// TODO: Research randomness (this is just the prototype implementation)
	// Our odds will be used to determine how accurate we are
	float PrecisionScale = FMath::RandRange(0.f, TargetingPrecisionOdds);
	float PrecisionRadius = TargetingRadius * PrecisionScale;

	// Random direction to trace in
	FVector Direction = UKismetMathLibrary::RandomRotator().Vector();

	return TargetLocation + (Direction * PrecisionRadius);
}
