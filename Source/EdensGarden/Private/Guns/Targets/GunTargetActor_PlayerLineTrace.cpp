// Copyright of Sock Goblins

#include "GunTargetActor_PlayerLineTrace.h"
#include "Abilities/GameplayAbility.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

AGunTargetActor_PlayerLineTrace::AGunTargetActor_PlayerLineTrace()
{
	TraceMode = EPlayerLineTraceMode::ControllerThenOrigin;
	PlayerTraceTolerance = 0.6f;
	PlayerTraceChannel = ECC_Visibility;
}

void AGunTargetActor_PlayerLineTrace::InternalInitialize_Implementation()
{
	Super::InternalInitialize_Implementation();

	PlayerTraceTolerance = FMath::Clamp(PlayerTraceTolerance, -1.f, 1.f);
}

void AGunTargetActor_PlayerLineTrace::InternalStart_Implementation()
{
	UWorld* World = GetWorld();

	FTransform OGTransform = Origin.GetTargetingTransform();
	FVector Start = OGTransform.GetLocation();
	FVector Target = FVector::ZeroVector;

	FVector TraceDirection = OGTransform.GetUnitAxis(EAxis::X);

	// Query params used in all traces
	FCollisionQueryParams QParams;
	QParams.bReturnPhysicalMaterial = true;
	QParams.bTraceComplex = true;
	QParams.bTraceAsyncScene = true;

	// Ignore our source actor
	QParams.AddIgnoredActor(SourceActor);

	// Peform trace from player first if required.
	if (ShouldTraceController() && OwningController)
	{
		// Trace from players eyes and update the target
		// location to be at the first blocking hit we see
		FVector EyeLocation;
		FRotator EyeRotation;

		OwningController->GetPlayerViewPoint(EyeLocation, EyeRotation);

		// Only apply spread here if its a controller only trace
		FVector EyeDirection = EyeRotation.Vector();
		if (!ShouldTraceOrigin())
		{
			EyeDirection = ApplySpread(EyeDirection);
		}

		Target = EyeLocation + (EyeDirection * TraceRange);

		FHitResult Result;
		if (SingleLineTrace(Result, World, EyeLocation, Target, PlayerTraceChannel, QParams, Filter))
		{
			// Update the target to now be the hit point
			if (Result.bBlockingHit && (FVector::DistSquared(Start, Result.Location) <= FMath::Square(TraceRange)))
			{
				Target = Result.Location;
			}
		}

		DrawDebugTrace(EyeLocation, Target, FColor::Blue);

		// We can stop here if not tracing from origin
		if (!ShouldTraceOrigin())
		{
			// Memory handled via shared pointer
			FGameplayAbilityTargetData_SingleTargetHit* Data = new FGameplayAbilityTargetData_SingleTargetHit(Result);
			FGameplayAbilityTargetDataHandle Handle = FGameplayAbilityTargetDataHandle(Data);
			FinishTargeting(Handle);

			return;
		}

		// Need valid direction to trace from
		FVector Direction = (Target - Start).GetSafeNormal();
		if (Direction.IsZero())
		{
			Direction = EyeDirection;
		}

		// Apply spread before calculate target point
		Direction = ApplySpread(Direction);
		Target = Start + (Direction * TraceRange);

		// We don't want to be tracing behind us
		if (FVector::DotProduct(TraceDirection, (Target - Start).GetSafeNormal()) <= PlayerTraceTolerance)
		{
			// Applying spread again here, since spread should always be applied if requested
			TraceDirection = ApplySpread(TraceDirection);
			Target = Start + (TraceDirection * TraceRange);
		}
	}
	else
	{
		// Re-checking here so target is always set
		if (ShouldTraceController() && !OwningController)
		{
			UE_LOG(LogGunTargetActor, Warning, TEXT("PlayerLineTrace: Unable to perform line trace from player first as controller is null"));
		}

		// Apply spread before calculating target point
		Target = Start + (ApplySpread(TraceDirection) * TraceRange);
	}

	// If this point has been reached, we should be tracing origin

	// Get all hit targets in a target data handle
	FGameplayAbilityTargetDataHandle Handle = TraceWorldByChannel(Start, Target, TraceChannel, QParams, Filter);
	FinishTargeting(Handle);
}
