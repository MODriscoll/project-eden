// Copyright of Sock Goblins

#include "Targets/GunTargetActor_LineTrace.h"
#include "Abilities/GameplayAbility.h"

#include "Engine/World.h"
#include "GameFramework/PlayerController.h"

#include "EdensGardenFunctionLibrary.h"

AGunTargetActor_LineTrace::AGunTargetActor_LineTrace()
{
	TraceRange = 999999.f;
	MaxNumOfHits = 2;
	TraceChannel = COLLISION_WEAPON;

	bApplySpread = false;
	MinSpread = 10.f;
	MaxSpread = 20.f;
	MinVelocity = 100.f;
	MaxVelocity = 400.f;
}

void AGunTargetActor_LineTrace::InternalInitialize_Implementation()
{
	MaxNumOfHits = FMath::Max(1, MaxNumOfHits);
}

void AGunTargetActor_LineTrace::InternalStart_Implementation()
{
	FTransform OGTransform = Origin.GetTargetingTransform();
	FVector Start = OGTransform.GetLocation();
	FVector TraceDirection = OGTransform.GetUnitAxis(EAxis::X);

	// Apply spread before calculating target point
	FVector Target = Start + (ApplySpread(TraceDirection) * TraceRange);

	FCollisionQueryParams QParams;
	QParams.bReturnPhysicalMaterial = true;
	QParams.bTraceComplex = true;
	QParams.bTraceAsyncScene = true;

	// Ignore our source actor
	QParams.AddIgnoredActor(SourceActor);

	FGameplayAbilityTargetDataHandle Handle = TraceWorldByChannel(Start, Target, TraceChannel, QParams, Filter);
	FinishTargeting(Handle);
}

FGameplayAbilityTargetDataHandle AGunTargetActor_LineTrace::TraceWorldByProfile(const FVector& Start, const FVector& End, const FName& ProfileName, const FCollisionQueryParams& QParams, const FGameplayTargetDataFilterHandle& Filter) const
{
	UWorld* World = GetWorld();

	// Perform trace with data to return
	TArray<FHitResult> Results;
	MultiLineTrace(Results, World, Start, End, TraceChannel, QParams, Filter);

	if (Results.Num() > 0)
	{
		DrawDebugTrace(Start, Results.Top().Location, FColor::Green);
	}
	else
	{
		DrawDebugTrace(Start, End, FColor::Red);
	}

	// If we got too many hits, clear away the furthest results
	if (MaxNumOfHits > 0 && Results.Num() > MaxNumOfHits)
	{
		Results.SetNum(MaxNumOfHits);
	}

	FGameplayAbilityTargetDataHandle Handle = Origin.MakeTargetDataHandleFromHitResults(OwningAbility, Results);
	return Handle;
}

FGameplayAbilityTargetDataHandle AGunTargetActor_LineTrace::TraceWorldByChannel(const FVector& Start, const FVector& End, TEnumAsByte<ECollisionChannel> Channel, const FCollisionQueryParams& QParams, const FGameplayTargetDataFilterHandle& Filter) const
{
	UWorld* World = GetWorld();

	// Perform trace with data to return
	TArray<FHitResult> Results;
	MultiLineTrace(Results, World, Start, End, TraceChannel, QParams, Filter);

	if (Results.Num() > 0)
	{
		DrawDebugTrace(Start, Results.Top().Location, FColor::Green);
	}
	else
	{
		DrawDebugTrace(Start, End, FColor::Red);
	}

	// If we got too many hits, clear away the furthest results
	if (MaxNumOfHits > 0 && Results.Num() > MaxNumOfHits)
	{
		Results.SetNum(MaxNumOfHits);
	}

	// Last hit should be blocking hit
	if (Results.IsValidIndex(0))
	{		
		FHitResult& LastHit = Results.Last();
		LastHit.bBlockingHit = true;
	}

	FGameplayAbilityTargetDataHandle Handle = Origin.MakeTargetDataHandleFromHitResults(OwningAbility, Results);
	return Handle;
}

bool AGunTargetActor_LineTrace::SingleLineTrace(FHitResult& OutResult, UWorld* World, const FVector& Start, const FVector& End, const FName& ProfileName, const FCollisionQueryParams& Params, const FGameplayTargetDataFilterHandle& Filter) const
{
	check(World);

	TArray<FHitResult> Hits;
	World->LineTraceMultiByProfile(Hits, Start, End, ProfileName, Params);

	OutResult.TraceStart = Start;
	OutResult.TraceEnd = End;

	// We need to consider actors (even overlapped) might pass the filter
	for (const FHitResult& Result : Hits)
	{
		if (!Result.Actor.IsValid() || Filter.FilterPassesForActor(Result.Actor))
		{
			OutResult = Result;
			OutResult.bBlockingHit = true;
			return true;
		}
	}

	return false;
}

bool AGunTargetActor_LineTrace::MultiLineTrace(TArray<FHitResult>& OutResults, UWorld* World, const FVector& Start, const FVector& End, const FName& ProfileName, const FCollisionQueryParams& Params, const FGameplayTargetDataFilterHandle& Filter) const
{
	check(World);

	TArray<FHitResult> Hits;
	World->LineTraceMultiByProfile(Hits, Start, End, ProfileName, Params);

	// We need to consider actors might pass the filter
	OutResults.Reset();
	for (FHitResult& Result : Hits)
	{
		if (!Result.Actor.IsValid() || Filter.FilterPassesForActor(Result.Actor))
		{
			OutResults.Push(Result);
		}
	}

	return OutResults.Num() > 0;
}

bool AGunTargetActor_LineTrace::SingleLineTrace(FHitResult& OutResult, UWorld* World, const FVector& Start, const FVector& End, TEnumAsByte<ECollisionChannel> Channel, const FCollisionQueryParams& Params, const FGameplayTargetDataFilterHandle& Filter) const
{
	check(World);

	TArray<FHitResult> Hits;
	World->LineTraceMultiByChannel(Hits, Start, End, Channel, Params);

	OutResult.TraceStart = Start;
	OutResult.TraceEnd = End;

	// We need to consider actors (even overlapped) might pass the filter
	for (const FHitResult& Result : Hits)
	{
		if (!Result.Actor.IsValid() || Filter.FilterPassesForActor(Result.Actor))
		{
			OutResult = Result;
			OutResult.bBlockingHit = true;
			return true;
		}
	}

	return false;
}

bool AGunTargetActor_LineTrace::MultiLineTrace(TArray<FHitResult>& OutResults, UWorld* World, const FVector& Start, const FVector& End, TEnumAsByte<ECollisionChannel> Channel, const FCollisionQueryParams& Params, const FGameplayTargetDataFilterHandle& Filter) const
{
	check(World);

	TArray<FHitResult> Hits;
	World->LineTraceMultiByChannel(Hits, Start, End, Channel, Params);

	OutResults.Reset();

	// For some reason, the same component is returning more than one result
	// We only want one hit result per actor
	TSet<AActor*> HandledActors;
	for (FHitResult& Result : Hits)
	{
		AActor* HitActor = Result.GetActor();

		// We need to consider actors might pass the filter
		if (HitActor && !HandledActors.Contains(HitActor) &&  Filter.FilterPassesForActor(HitActor))
		{
			HandledActors.Add(HitActor);
			OutResults.Push(Result);
		}
	}

	return OutResults.Num() > 0;
}

FVector AGunTargetActor_LineTrace::ApplySpread(FVector Direction) const
{
	// No point of applying spread if we have no source actor
	if (bApplySpread && SourceActor)
	{
		return UEdensGardenFunctionLibrary::ApplySpreadToDirection(
			Direction, SourceActor->GetVelocity(),
			MinSpread, MaxSpread,
			MinVelocity, MaxVelocity);
	}
	else
	{
		return Direction;
	}
}
