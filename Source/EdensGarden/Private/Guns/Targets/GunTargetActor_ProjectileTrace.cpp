// Copyright of Sock Goblins

#include "GunTargetActor_ProjectileTrace.h"
#include "GameplayAbility.h"

#include "Kismet/GameplayStatics.h"

AGunTargetActor_ProjectileTrace::AGunTargetActor_ProjectileTrace()
{
	Speed = 500.f;
	ProjectileRadius = 0.f;
	MaxSimTime = 1.f;
	TraceChannel = ECC_WorldStatic;
	SimFrequency = 20.f;
	OverrideGravityZ = 0.f;
	bTraceComplex = false;
}

void AGunTargetActor_ProjectileTrace::InternalStart_Implementation()
{
	FTransform OGTransform = Origin.GetTargetingTransform();
	FVector Start = OGTransform.GetLocation();
	FVector Direction = OGTransform.GetUnitAxis(EAxis::X);

	FPredictProjectilePathParams Params;
	Params.StartLocation = Start;
	Params.LaunchVelocity = Direction * Speed;
	Params.bTraceWithCollision = true;
	Params.ProjectileRadius = ProjectileRadius;
	Params.MaxSimTime = MaxSimTime;
	Params.bTraceWithChannel = true;
	Params.TraceChannel = TraceChannel;
	Params.SimFrequency = SimFrequency;
	Params.OverrideGravityZ = OverrideGravityZ;

	if (SourceActor)
	{
		Params.ActorsToIgnore.Add(SourceActor);
	}

	if (bDrawDebug)
	{
		Params.DrawDebugTime = DebugDuration;
		if (DebugDuration > 0.f)
		{
			Params.DrawDebugType = EDrawDebugTrace::ForDuration;
		}
		else
		{
			Params.DrawDebugType = EDrawDebugTrace::ForOneFrame;
		}
	}

	FGameplayAbilityTargetDataHandle Handle;

	// Predict projectiles path, record result if something was hit
	FPredictProjectilePathResult Result;
	if (UGameplayStatics::PredictProjectilePath(this, Params, Result))
	{
		// Consider no targets hit if we weren't mean to hit this actor
		AActor* HitActor = Result.HitResult.GetActor();
		if (!HitActor || Filter.FilterPassesForActor(HitActor))
		{
			Handle = Origin.MakeTargetDataHandleFromHitResult(OwningAbility, Result.HitResult);
		}
	}

	FinishTargeting(Handle);
}
