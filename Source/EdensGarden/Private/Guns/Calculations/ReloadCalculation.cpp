// Copyright of Sock Goblins

#include "ReloadCalculation.h"
#include "Guns/GunAttributeSet.h"

struct ReloadStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(ClipCount);
	DECLARE_ATTRIBUTE_CAPTUREDEF(MaxClipCount);
	DECLARE_ATTRIBUTE_CAPTUREDEF(ReserveAmmo);

	ReloadStatics()
	{
		// The gun is always the target (source is guns owner)
		DEFINE_ATTRIBUTE_CAPTUREDEF(UAmmoGunAttributeSet, ClipCount, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UAmmoGunAttributeSet, MaxClipCount, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UAmmoGunAttributeSet, ReserveAmmo, Target, false);
	}
};

static const ReloadStatics& GetReloadStatics()
{
	static ReloadStatics RldStatics;
	return RldStatics;
}

UReloadCalculation::UReloadCalculation()
{
	const ReloadStatics& RldStatics = GetReloadStatics();

	RelevantAttributesToCapture.Add(RldStatics.ClipCountDef);
	RelevantAttributesToCapture.Add(RldStatics.MaxClipCountDef);
	RelevantAttributesToCapture.Add(RldStatics.ReserveAmmoDef);

	#if WITH_EDITOR
	InvalidScopedModifierAttributes.Add(RldStatics.ClipCountDef);
	#endif
}

void UReloadCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const ReloadStatics& RldStatics = GetReloadStatics();

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	// We want tags to see if source has infinite reserve
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float ClipCount = 0.f;
	float MaxClipCount = 0.f;
	float ReserveAmmo = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(RldStatics.ClipCountDef, EvaluationParameters, ClipCount);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(RldStatics.MaxClipCountDef, EvaluationParameters, MaxClipCount);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(RldStatics.ReserveAmmoDef, EvaluationParameters, ReserveAmmo);

	// Can't give any ammo if out of reserve
	if (ReserveAmmo > 0.f)
	{
		// Ammo to give, is limited by the amount of reserve ammo we have
		float AmmoToGive = FMath::Min(ReserveAmmo, MaxClipCount - ClipCount);

		// Consume ammo if gun doesn't have infinite ammo
		const FGameplayTag InfReserveTag = FGameplayTag::RequestGameplayTag(TEXT("Weapon.Ammo.Infinite"));
		if (!(SourceTags && SourceTags->HasTag(InfReserveTag)))
		{
			OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(RldStatics.ReserveAmmoProperty, EGameplayModOp::Additive, -AmmoToGive));
		}

		// Refill clip
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(RldStatics.ClipCountProperty, EGameplayModOp::Additive, AmmoToGive));
	}
}