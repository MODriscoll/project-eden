// Copyright of Sock Goblins

#include "Tasks/AbilityTask_GunTrace.h"
#include "Targets/GunTargetActor.h"

#include "AbilitySystemComponent.h"

#include "Engine/Engine.h"
#include "Engine/World.h"

void UAbilityTask_GunTrace::OnTraceFinished(const FGameplayAbilityTargetDataHandle& Data)
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		FGameplayAbilityTargetDataHandle MutableData = Data;
		TraceFinished.Broadcast(MutableData);
	}

	EndTask();
}

void UAbilityTask_GunTrace::OnTraceCancelled(const FGameplayAbilityTargetDataHandle& Data)
{
	if (ShouldBroadcastAbilityTaskDelegates())
	{
		Cancelled.Broadcast(FGameplayAbilityTargetDataHandle());
	}

	EndTask();
}

UAbilityTask_GunTrace* UAbilityTask_GunTrace::GunTrace(UGameplayAbility* OwningAbility, FName TaskInstanceName, TSubclassOf<AGunTargetActor> Class)
{
	UAbilityTask_GunTrace* MyTask = NewAbilityTask<UAbilityTask_GunTrace>(OwningAbility, TaskInstanceName);
	MyTask->TargetClass = Class;
	return MyTask;
}

void UAbilityTask_GunTrace::Activate()
{
	if (Ability && !TargetClass)
	{
		EndTask();
	}
}

bool UAbilityTask_GunTrace::BeginSpawningActor(UGameplayAbility* OwningAbility, TSubclassOf<AGunTargetActor> Class, AGunTargetActor*& SpawnedActor)
{
	SpawnedActor = nullptr;

	if (Ability)
	{
		if (TargetClass)
		{
			UWorld* World = GEngine->GetWorldFromContextObject(OwningAbility, EGetWorldErrorMode::LogAndReturnNull);
			if (World)
			{
				SpawnedActor = World->SpawnActorDeferred<AGunTargetActor>(*TargetClass, FTransform::Identity, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
			}
		}

		if (SpawnedActor)
		{
			// Move to an init func?
			{
				TargetActor = SpawnedActor;

				TargetActor->OwningController = Ability->GetCurrentActorInfo()->PlayerController.Get();
				TargetActor->OnTraceCompleteDelegate.AddUObject(this, &UAbilityTask_GunTrace::OnTraceFinished);
				TargetActor->OnCancelledDelegate.AddUObject(this, &UAbilityTask_GunTrace::OnTraceCancelled);
			}
		}
	}

	return SpawnedActor != nullptr;
}

void UAbilityTask_GunTrace::FinishSpawningActor(UGameplayAbility* OwningAbility, AGunTargetActor* SpawnedActor)
{
	if (SpawnedActor && !SpawnedActor->IsPendingKill())
	{
		check(TargetActor == SpawnedActor);

		// TODO: Use avatar transform instead?
		const FTransform SpawnTransform = AbilitySystemComponent->GetOwner()->GetTransform();
		SpawnedActor->FinishSpawning(SpawnTransform);

		SpawnedActor->InitializeTargeting(OwningAbility);
		SpawnedActor->StartTargeting();
	}
}

void UAbilityTask_GunTrace::OnDestroy(bool AbilityEnded)
{
	// Make sure our target actor is destroyed
	if (TargetActor)
	{
		TargetActor->Destroy();
	}

	Super::OnDestroy(AbilityEnded);
}
