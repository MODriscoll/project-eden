// Copyright of Sock Goblins

#include "FuelGun.h"
#include "AbilitySystemComponent.h"

AFuelGun::AFuelGun()
{
	PrimaryActorTick.bCanEverTick = true;

	AttributeSet = CreateDefaultSubobject<UFuelGunAttributeSet>(TEXT("AttributeSet"));
	AttributeSet->OnFuelCountChanged.AddDynamic(this, &AFuelGun::OnFuelCountChanged);
}

void AFuelGun::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame() && AttributeSet)
	{
		if (Ar.IsSaving())
		{
			FFuelGunSaveData SaveData;
			SaveData.FuelCount = GetFuelCount();

			Ar << SaveData;
		}

		if (Ar.IsLoading())
		{
			FFuelGunSaveData SaveData;
			Ar << SaveData;

			AttributeSet->SetFuelCount(SaveData.FuelCount);
		}
	}
}

void AFuelGun::InitAttributeDefaults(const UDataTable* DataTable)
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		ASC->InitStats(UFuelGunAttributeSet::StaticClass(), DataTable);
	}
}

float AFuelGun::GetFuelCount() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetFuelCount();
	}
	else
	{
		return 0.f;
	}
}

float AFuelGun::GetMaxFuelCount() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetMaxFuelCount();
	}
	else
	{
		return 0.f;
	}
}

#if EG_GAMEPLAY_DEBUGGER
void AFuelGun::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}Fuel Count: %s%.2f {white}/ Max Fuel Count: {green}%.2f\n");

	float Fuel = GetFuelCount();
	float MaxFuel = GetMaxFuelCount();

	float FRatio = Fuel / MaxFuel;

	String.Append(FString::Printf(*Formatting,
		FRatio <= 0.1f ? TEXT("{red}") : (FRatio <= 0.4f ? *FString("{yellow}") : *FString("{green}")), Fuel, MaxFuel));
}
#endif

void AFuelGun::OnFuelCountChanged(float CurrentFuel, float MaxFuel)
{
	OnFuelUpdated(CurrentFuel, MaxFuel);
}
