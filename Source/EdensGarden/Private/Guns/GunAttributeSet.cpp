// Copyright of Sock Goblins

#include "GunAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

#include "AbilitySystemComponent.h"

UAmmoGunAttributeSet::UAmmoGunAttributeSet()
{
	ClipCount = FGameplayAttributeData(30.f);
	MaxClipCount = FGameplayAttributeData(30.f);
	ReserveAmmo = FGameplayAttributeData(300.f);
	MaxReserveAmmo = FGameplayAttributeData(300.f);
}

float UAmmoGunAttributeSet::GetTotalAmmo() const
{
	return GetClipCount() + GetReserveAmmo();
}

void UAmmoGunAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetClipCountAttribute() ||
		Data.EvaluatedData.Attribute == GetReserveAmmoAttribute())
	{
		OnAmmoCountChanged.Broadcast(GetClipCount(), GetReserveAmmo());
	}
}

float UAmmoGunAttributeSet::GetMaxTotalAmmo() const
{
	return GetMaxClipCount() + GetMaxReserveAmmo();
}

UFuelGunAttributeSet::UFuelGunAttributeSet()
{
	FuelCount = FGameplayAttributeData(500.f);
	MaxFuelCount = FGameplayAttributeData(500.f);
	FuelConsumptionRate = FGameplayAttributeData(0.f);
	FuelRegenRate = FGameplayAttributeData(50.f);
}

void UFuelGunAttributeSet::Tick(float DeltaTime)
{
	const UAbilitySystemComponent* ASC = GetOwningAbilitySystemComponent();

	float CurFuel = GetFuelCount();
	float MaxFuel = GetMaxFuelCount();

	float ConsumeRate = GetFuelConsumptionRate();
	if (ConsumeRate > 0.f && CurFuel > 0.f)
	{
		// Don't consume fuel if we have inf ammo
		const FGameplayTag InfFuelTag = FGameplayTag::RequestGameplayTag(TEXT("Weapon.Ammo.Infinite"));
		if (!(ASC && ASC->HasMatchingGameplayTag(InfFuelTag)))
		{
			float AmountToConsume = ConsumeRate * DeltaTime;
			float NewFuel = FMath::Max(CurFuel - AmountToConsume, 0.f);

			SetFuelCount(NewFuel);

			// Notify listeners
			OnFuelCountChanged.Broadcast(NewFuel, MaxFuel);		
				
			// We don't regenerate fuel if consuming it
			return;
		}
	}
	
	float RegenRate = GetFuelRegenRate();
	if (RegenRate > 0.f && CurFuel < MaxFuel)
	{
		// Might not be able to consume fuel at this point
		const FGameplayTag CantRegenerateFuel = FGameplayTag::RequestGameplayTag(TEXT("Weapon.Ammo.Disabled.FuelRegen"));
		if (!(ASC && ASC->HasMatchingGameplayTag(CantRegenerateFuel)))
		{
			float AmountToGive = RegenRate * DeltaTime;
			float NewFuel = FMath::Min(CurFuel + AmountToGive, MaxFuel);

			SetFuelCount(NewFuel);

			// Notify listeners
			OnFuelCountChanged.Broadcast(NewFuel, MaxFuel);
		}
	}
}

bool UFuelGunAttributeSet::ShouldTick() const
{
	return true;
}

void UFuelGunAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	if (Data.EvaluatedData.Attribute == GetFuelCountAttribute() ||
		Data.EvaluatedData.Attribute == GetMaxFuelCountAttribute())
	{
		OnFuelCountChanged.Broadcast(GetFuelCount(), GetMaxFuelCount());
	}
}

float UFuelGunAttributeSet::GetTankFullPercentage() const
{
	return GetFuelCount() / GetMaxFuelCount();
}
