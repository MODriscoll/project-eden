// Copyright of Sock Goblins

#include "Gun.h"
#include "GunAttributeSet.h"
#include "SpecOp/SpecOpCharacter.h"
#include "SpecOp/SpecOpAbilitySystemComponent.h"
#include "SpecOp/SpecOpAnimInstance.h"

AGunBase::AGunBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	bCanCrouchWhileEquipped = true;
	bCanSprintWhileEquipped = true;
	bCanRollWhileEquipped = true;
	SwitchState = ESwitchGunState::Holstered;
	bPhysicallyEquipped = false;

	AbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystem"));
}

void AGunBase::OnAddToInventory(ASpecOpCharacter* SpecOp)
{
	Super::OnAddToInventory(SpecOp);

	UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
	EquipAbilityHandle = GiveAbilityIfValid(OwnerASC, EquipAbility);
	HolsterAbilityHandle = GiveAbilityIfValid(OwnerASC, HolsterAbility);
}

void AGunBase::OnRemoveFromInventory()
{
	// Our owner will need to equip something else if dropping us
	if (!IsHolstered())
	{
		OnHolsterSkipped();
	}

	UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
	OwnerASC->ClearAbility(EquipAbilityHandle);
	OwnerASC->ClearAbility(HolsterAbilityHandle);

	Super::OnRemoveFromInventory();
}

void AGunBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (AbilitySystem && AttributeDefaults)
	{
		InitAttributeDefaults(AttributeDefaults);
	}
}

void AGunBase::BeginPlay()
{
	Super::BeginPlay();

	if (AbilitySystem)
	{
		AbilitySystem->InitAbilityActorInfo(this, this);
	}
}

#if EG_GAMEPLAY_DEBUGGER
void AGunBase::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}Equip Status = {Green}%s\n"
		"{white}Physically Equipped = {Green}%s\n");

	FString SwitchStateString;
	switch (SwitchState)
	{
		case ESwitchGunState::Equipped:				{ SwitchStateString = TEXT("Equipped"); break; }
		case ESwitchGunState::Holstered:			{ SwitchStateString = TEXT("Holstered"); break; }
		case ESwitchGunState::PendingEquip:			{ SwitchStateString = TEXT("Pending Equip"); break; }
		case ESwitchGunState::PendingHolster:		{ SwitchStateString = TEXT("Pending Holster"); break; }
	}

	String.Append(FString::Printf(*Formatting,
		*SwitchStateString,
		bPhysicallyEquipped ? TEXT("True") : TEXT("False")));
}
#endif

void AGunBase::CleanupWeapon()
{
	check(SpecOpOwner.IsValid());

	// Need to remove all abilities and effects we gave our owner
	if (!IsHolstered())
	{
		UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
		check(OwnerASC);
		ClearActions(OwnerASC);

		if (EquippedEffectHandle.IsValid())
		{
			OwnerASC->RemoveActiveGameplayEffect(EquippedEffectHandle);
		}
	}
}

void AGunBase::GiveSelfTo(ASpecOpCharacter* SpecOp)
{
	SpecOp->GiveAndEquipGun(this, true);
}

void AGunBase::RemoveSelfFrom(ASpecOpCharacter* SpecOp)
{
	SpecOp->DropGun(this);
}

void AGunBase::OnEquipStart()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should only ever be called if holstered
		ensure(SwitchState == ESwitchGunState::Holstered);
		#endif

		// Prevents weapons shadow when owner is using helmet torch
		USkeletalMeshComponent* Mesh = GetMesh();
		Mesh->bSelfShadowOnly = true;

		// Have spec op start using this guns animations
		USpecOpAnimInstance* AnimInstance = SpecOpOwner->GetSpecOpAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->SetGunAnimationSet(AnimationSet);
		}

		SwitchState = ESwitchGunState::PendingEquip;

		OnEquipStartByOwner();
	}
}

void AGunBase::OnEquipFinished()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Equip could have been skipped, meaning we can still be holstered at this point
		ensure(SwitchState == ESwitchGunState::Holstered || SwitchState == ESwitchGunState::PendingEquip);
		#endif

		UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
		check(OwnerASC);
		SetupActions(OwnerASC);

		// Equip effect, this could apply speed modifications
		if (EquippedEffect)
		{
			// Temp fix to switch gun problem
			if (!(EquippedEffectHandle.IsValid() && OwnerASC->GetActiveGameplayEffect(EquippedEffectHandle) != nullptr))
			{
				EquippedEffectHandle = AbilitySystem->ApplyGameplayEffectToTarget(EquippedEffect.GetDefaultObject(), OwnerASC);
			}
		}

		// We should be physically equipped by this point,
		// force our owner to equip us if not the case
		if (!bPhysicallyEquipped && !SpecOpOwner->IsDead()) // Prob want to move the IsDead check elsewhere
		{
			SpecOpOwner->PhysicallyEquipWeapon();
		}

		// We might be skipping the equip action, meaning the animation set hasn't been changed
		if (SwitchState == ESwitchGunState::Holstered)
		{
			USpecOpAnimInstance* AnimInstance = SpecOpOwner->GetSpecOpAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->SetGunAnimationSet(AnimationSet);
			}
		}

		SwitchState = ESwitchGunState::Equipped;
		OnEquippedByOwner();
	}
}

void AGunBase::OnEquipSkipped()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should only ever be called if holstered
		ensure(SwitchState == ESwitchGunState::Holstered);
		#endif

		OnEquipFinished();
	}
}

void AGunBase::OnEquipCancelled()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should have been pending equip if cancelled
		ensure(SwitchState == ESwitchGunState::PendingEquip);
		#endif

		SwitchState = ESwitchGunState::Holstered;

		// We might have been physically equipped
		if (bPhysicallyEquipped)
		{
			SpecOpOwner->PhysicallyHolsterWeapon();
		}
	}
}

void AGunBase::OnHolsterStart()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should only ever be called if equipped
		ensure(SwitchState == ESwitchGunState::Equipped);
		#endif

		SwitchState = ESwitchGunState::PendingHolster;

		OnHolsterStartByOwner();
	}
}

void AGunBase::OnHolsterFinished()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Holster could have been skipped, meaning we can still be equipped at this point
		ensure(SwitchState == ESwitchGunState::Equipped || SwitchState == ESwitchGunState::PendingHolster);
		#endif

		// Allow normal shadowing from lights
		USkeletalMeshComponent* Mesh = GetMesh();
		Mesh->bSelfShadowOnly = false;

		UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
		check(OwnerASC);
		ClearActions(OwnerASC);

		// Remove effect we applied to owner
		if (EquippedEffectHandle.IsValid())
		{
			OwnerASC->RemoveActiveGameplayEffect(EquippedEffectHandle);
		}

		// We should be physically holstered by this point,
		// force our owner to holster us if not the case
		if (bPhysicallyEquipped)
		{
			SpecOpOwner->PhysicallyHolsterWeapon();
		}

		SwitchState = ESwitchGunState::Holstered;
		OnHolsteredByOwner();
	}
}

void AGunBase::OnHolsterSkipped()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should only ever be called if equipped
		ensure(SwitchState == ESwitchGunState::Equipped);
		#endif

		OnHolsterFinished();
	}
}

void AGunBase::OnHolsterCancelled()
{
	if (IsInInventory())
	{
		#if WITH_EDITOR
		// Should have been pending holster if cancelled
		ensure(SwitchState == ESwitchGunState::PendingHolster);
		#endif

		SwitchState = ESwitchGunState::Equipped;

		// We might have already be physically holstered
		if (!bPhysicallyEquipped)
		{
			SpecOpOwner->PhysicallyEquipWeapon();
		}
	}
}

void AGunBase::OnPhysicallyEquipped()
{
	if (IsInInventory())
	{
		if (!bPhysicallyEquipped)
		{
			bPhysicallyEquipped = true;
			AttachWeaponToOwner(EquipSocket);

			OnPhysicallyEquippedByOwner();
		}
	}
}

void AGunBase::OnPhysicallyHolstered()
{
	if (IsInInventory())
	{
		if (bPhysicallyEquipped)
		{
			bPhysicallyEquipped = false;
			AttachWeaponToOwner(HolsterSocket);

			// Temp fix to switch gun bug
			UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
			check(OwnerASC);
			ClearActions(OwnerASC);

			OnPhysicallyHolsteredByOwner();
		}
	}
}

void AGunBase::SetupActions(UAbilitySystemComponent* OwnerASC)
{
	PrimaryActionHandle = GiveAbilityIfValid(OwnerASC, PrimaryAction, static_cast<int32>(EEdensGardenInputID::PrimaryAction));
	SecondaryActionHandle = GiveAbilityIfValid(OwnerASC, SecondaryAction, static_cast<int32>(EEdensGardenInputID::SecondaryAction));
}

void AGunBase::ClearActions(UAbilitySystemComponent* OwnerASC)
{
	OwnerASC->ClearAbility(PrimaryActionHandle);
	OwnerASC->ClearAbility(SecondaryActionHandle);
}