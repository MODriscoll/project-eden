// Copyright of Sock Goblins

#include "AmmoGun.h"
#include "AbilitySystemComponent.h"

AAmmoGun::AAmmoGun()
{
	AttributeSet = CreateDefaultSubobject<UAmmoGunAttributeSet>(TEXT("AttributeSet"));
	AttributeSet->OnAmmoCountChanged.AddDynamic(this, &AAmmoGun::OnAmmoCountChanged);
}

void AAmmoGun::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame() && AttributeSet)
	{
		if (Ar.IsSaving())
		{
			FAmmoGunSaveData SaveData;
			SaveData.ClipCount = GetClipCount();
			SaveData.ReserveAmmo = GetReserveAmmo();

			Ar << SaveData;
		}
		
		if (Ar.IsLoading())
		{
			FAmmoGunSaveData SaveData;
			Ar << SaveData;

			AttributeSet->SetClipCount(SaveData.ClipCount);
			AttributeSet->SetReserveAmmo(SaveData.ReserveAmmo);
		}
	}
}

void AAmmoGun::InitAttributeDefaults(const UDataTable* DataTable)
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		ASC->InitStats(UAmmoGunAttributeSet::StaticClass(), DataTable);
	}
}

void AAmmoGun::SetupActions(UAbilitySystemComponent* OwnerASC)
{
	Super::SetupActions(OwnerASC);

	ReloadActionHandle = GiveAbilityIfValid(OwnerASC, ReloadAction, static_cast<int32>(EEdensGardenInputID::Reload));
}

void AAmmoGun::ClearActions(UAbilitySystemComponent* OwnerASC)
{
	Super::ClearActions(OwnerASC);

	OwnerASC->ClearAbility(ReloadActionHandle);
}

float AAmmoGun::GetClipCount() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetClipCount();
	}
	else
	{
		return 0.0f;
	}
}

float AAmmoGun::GetMaxClipCount() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetMaxClipCount();
	}
	else
	{
		return 0.0f;
	}
}

float AAmmoGun::GetReserveAmmo() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetReserveAmmo();
	}
	else
	{
		return 0.0f;
	}
}

float AAmmoGun::GetMaxReserveAmmo() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetMaxReserveAmmo();
	}
	else
	{
		return 0.0f;
	}
}

float AAmmoGun::GetTotalAmmo() const
{
	return GetClipCount() + GetReserveAmmo();
}

float AAmmoGun::GetMaxTotalAmmo() const
{
	return GetMaxClipCount() + GetMaxReserveAmmo();
}

#if EG_GAMEPLAY_DEBUGGER
void AAmmoGun::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}Clip Count: %s%.2f {white}/ Max Clip Count: {green}%.2f\n"
		"{white}Reserve Ammo: %s%.2f {white}/ Max Reserve Ammo: {green}%.2f\n");

	float Clip = GetClipCount();
	float MaxClip = GetMaxClipCount();
	float Reserve = GetReserveAmmo();
	float MaxReserve = GetMaxReserveAmmo();

	float CRatio = Clip / MaxClip;
	float RRatio = Reserve / MaxReserve;

	String.Append(FString::Printf(*Formatting,
		CRatio <= 0.1f ? TEXT("{red}") : (CRatio <= 0.4f ? *FString("{yellow}") : *FString("{green}")), Clip, MaxClip,
		RRatio <= 0.1f ? TEXT("{red}") : (RRatio <= 0.4f ? *FString("{yellow}") : *FString("{green}")), Reserve, MaxReserve));
}
#endif

float AAmmoGun::GetAmmoNeededToRefill(float MaxAmmoToConsume) const
{
	// We will also consider the that reserve ammo could also hold ammo
	// that will refill the current clip and still clamp at max reserve
	return FMath::Min(MaxAmmoToConsume, GetMaxTotalAmmo() - GetTotalAmmo());
}

void AAmmoGun::OnAmmoCountChanged(float Clip, float Reserve)
{
	OnAmmoUpdated(Clip, Reserve);
}
