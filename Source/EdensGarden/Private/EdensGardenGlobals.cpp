// Copyright of Sock Goblins

#include "EdensGardenGlobals.h"
#include "EdensGardenModule.h"
#include "EdensGarden.h"
#include "ModuleManager.h"

UEdensGardenGlobals::UEdensGardenGlobals()
{
	MovingEnergyRegenThreshold = 0.1f;
	WeaponDamageMultiplier = 1.f;
}

UEdensGardenGlobals& UEdensGardenGlobals::Get()
{
	FEdensGardenModule& Module = FModuleManager::LoadModuleChecked<FEdensGardenModule>("EdensGarden");
	return *Module.GetGlobalData();
}

void UEdensGardenGlobals::Initialize()
{
	LoadTags();

	UE_LOG(LogEdensGarden, Log, TEXT("Edens Garden Globals - Data Initialized"));
}

void UEdensGardenGlobals::LoadTags()
{
	CharacterFallingTag = FGameplayTag::RequestGameplayTag(TEXT("Character.State.Falling"));
	CharacterStunnedTag = FGameplayTag::RequestGameplayTag(TEXT("Character.State.Stunned"));
	CharacterDeadTag = FGameplayTag::RequestGameplayTag(TEXT("Character.State.Dead"));

	InvincibilityTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Invincible"));
}
