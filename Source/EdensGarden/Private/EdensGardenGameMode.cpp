// Copyright of Sock Goblins

#include "EdensGardenGameMode.h"
#include "EdensGardenGameInstance.h"
#include "EdensGardenGameState.h"
#include "EdensGardenLoadingScreenModule.h"
#include "EdensGardenSaveGame.h"
#include "EdensGardenUserSettings.h"

#include "Camera/CameraComponent.h"
#include "Components/MeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "CheckpointStart.h"
#include "SpecOpCharacter.h"
#include "SpecOpPlayerController.h"

#include "EngineUtils.h"
#include "TimerManager.h"
#include "Engine/PlayerStartPIE.h"
#include "Kismet/GameplayStatics.h"

AEdensGardenGameMode::AEdensGardenGameMode()
{
	DefaultPawnClass = ASpecOpCharacter::StaticClass();
	PlayerControllerClass = ASpecOpPlayerController::StaticClass();
	GameStateClass = AEdensGardenGameState::StaticClass();

	HighlighCustomColorParameter = TEXT("CustomColor");
}

void AEdensGardenGameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	FString UpdatedOptions = Options;

	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		// We might be starting at the game directly (e.g. Play In Editor)
		if (!GameInstance->IsActiveSaveGameValid())
		{
			GameInstance->CreateSaveGame(TEXT("TransientSave"), ESaveGamePolicy::OnlyUpdate);

			// Still write level name and checkpoint as the save could be changed to write on disk
			{
				UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
				check(SaveData);

				SaveData->LevelName = FName(*UGameplayStatics::GetCurrentLevelName(this));
				SaveData->CheckpointIndex = 0;
				SaveData->CheckpointName = NAME_None;
			}

			UpdatedOptions = FString(EGGM_NEWGAME);
		}
	}

	Super::InitGame(MapName, UpdatedOptions, ErrorMessage);
}

void AEdensGardenGameMode::StartPlay()
{
	Super::StartPlay();

	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.HideLoadingScreen();
}

void AEdensGardenGameMode::SetPlayerDefaults(APawn* PlayerPawn)
{
	Super::SetPlayerDefaults(PlayerPawn);

	// We attach the highlight material to player, as 
	// we might have multiple post processing volumes
	{
		UCameraComponent* CameraComp = PlayerPawn->FindComponentByClass<UCameraComponent>();
		if (CameraComp)
		{
			UMaterialInstanceDynamic* HighlightMat = GetHighlightMaterial();
			if (HighlightMat)
			{
				CameraComp->AddOrUpdateBlendable(HighlightMat);
			}
		}
	}

	// Requires spec op class in order to load defaults
	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(PlayerPawn);
	if (!ensure(SpecOp))
	{
		return;
	}

	SpecOp->OnCharacterDeath.AddDynamic(this, &AEdensGardenGameMode::OnPlayerCharacterDeath);

	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		// Should write to disk if saving anything (as we could be handling level transition)
		bool bWriteToDisk = false;

		// Save initial attributes if required
		{
			bool bSavePlayer = UGameplayStatics::HasOption(OptionsString, EGGM_OPTION_SAVEPLAYER);
			if (bSavePlayer)
			{
				GameInstance->GenerateSpecOpSaveData(SpecOp);
				bWriteToDisk = true;
			}
		}

		// Save default game world
		{
			bool bSaveWorld = UGameplayStatics::HasOption(OptionsString, EGGM_OPTION_SAVEWORLD);
			if (bSaveWorld)
			{
				GameInstance->SaveGameWorldSaveData(GetWorld(), true);
				bWriteToDisk = true;
			}
		}

		if (bWriteToDisk)
		{
			GameInstance->WriteSaveGame();
		}
	}
}

AActor* AEdensGardenGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	UWorld* World = GetWorld();

	// Find potential PIE player start (using play from here while in editor)
	{
		for (TActorIterator<APlayerStartPIE> It(World); It; ++It)
		{
			// TODO: prob want to check if we can fit here
			return *It;
		}
	}

	UClass* PawnClass = GetDefaultPawnClassForController(Player);
	APawn* PawnToFit = PawnClass ? PawnClass->GetDefaultObject<APawn>() : nullptr;

	// Find checkpoint from active save game
	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		check(SaveData);

		ACheckpointStart* BestCheckpoint = nullptr;

		UWorld* World = GetWorld();
		for (TActorIterator<ACheckpointStart> It(World); It; ++It)
		{
			ACheckpointStart* Checkpoint = *It;
			if (Checkpoint && Checkpoint->CheckpointIndex == SaveData->CheckpointIndex)
			{
				// Checkpoint might have been specified
				if (SaveData->CheckpointName != NAME_None && SaveData->CheckpointName != Checkpoint->PlayerStartTag)
				{
					continue;
				}

				FVector PointLocation = Checkpoint->GetActorLocation();
				FRotator PointRotation = Checkpoint->GetActorRotation();				

				// We should be able to fit at this spot
				if (!World->FindTeleportSpot(PawnToFit, PointLocation, PointRotation))
				{
					UE_LOG(LogEdensGarden, Error, TEXT("Unable to place player as saved checkpoint (Name = %s) "
						"as there is no free space (Enroaching Geometry)"), *Checkpoint->PlayerStartTag.ToString());
					continue;
				}
				
				BestCheckpoint = Checkpoint;
			}
		}

		if (BestCheckpoint)
		{
			return BestCheckpoint;
		}

		UE_LOG(LogEdensGarden, Warning, TEXT("Unable to find checkpoint for active save game object"));
	}

	// Find first player start as last resort
	for (TActorIterator<APlayerStart> It(World); It; ++It)
	{
		// TODO: prob want to check if we can fit here
		return *It;
	}

	return nullptr;
}

void AEdensGardenGameMode::Reset()
{
	Super::Reset();

	// Stop all highlighting
	{
		if (FocusedPingedActor.Actor.IsValid())
		{
			RestoreHighlightedMeshes(FocusedPingedActor.Meshes);
			FocusedPingedActor.Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

			FocusedPingedActor.Actor.Reset();
			FocusedPingedActor.Meshes.Empty();
		}

		FTimerManager& TimerManager = GetWorldTimerManager();

		TArray<FHighlightedActor> PingedActors;
		DurationPingedActors.GenerateValueArray(PingedActors);
		for (FHighlightedActor& Ping : PingedActors)
		{
			if (Ping.Actor.IsValid())
			{
				RestoreHighlightedMeshes(Ping.Meshes);
				Ping.Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

				TimerManager.ClearTimer(Ping.TimerHandle_PingDuration);
			}
		}

		DurationPingedActors.Empty();
	}
}

void AEdensGardenGameMode::RestartAtCheckpoint()
{
	FString LevelName;
	UWorld* World = GetWorld();

	// Try travelling to saved games level (since we might not be saving progress)
	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance && GameInstance->IsActiveSaveGameValid())
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		check(SaveData);

		if (!SaveData->LevelName.IsNone())
		{
			LevelName = SaveData->LevelName.ToString();
		}
	}

	// Fallback to the current levels name
	if (LevelName.IsEmpty())
	{
		FString LevelName = World->GetMapName();
		LevelName.RemoveFromStart(World->StreamingLevelsPrefix);
	}

	UE_LOG(LogEdensGarden, Log, TEXT("Restarting at checkpoint, Level Name = %s"), *LevelName);

	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.DisplayLoadingScreen(false, 5.f);

	UGameplayStatics::OpenLevel(World, FName(*LevelName), true, EGGM_RESTART_CHECKPOINT);
}

void AEdensGardenGameMode::TransitionToNextLevel(FName LevelName)
{
	// Might not be allowed to record progress (move this part to game instance as well)
	//UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	//if (!UserSettings || UserSettings->IsSavingEnabled())
	{
		// Update level progression here, so once map is loaded the updated data is saved
		UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
		if (GameInstance)
		{
			// TODO: Move this to game instance
			UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
			check(SaveData);

			SaveData->LevelName = LevelName;
			SaveData->CheckpointIndex = 0;
			SaveData->CheckpointName = NAME_None;

			UE_LOG(LogEdensGarden, Log, TEXT("Save data level updated. Current level = %s"), *LevelName.ToString());
		}
	}

	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.DisplayLoadingScreen(false, 5.f);

	UGameplayStatics::OpenLevel(GetWorld(), LevelName, true, EGGM_TRANSITION);
}

void AEdensGardenGameMode::TransitionToMainMenu(FName LevelName)
{
	UEdensGardenGameInstance* GameInstance = Cast<UEdensGardenGameInstance>(GetGameInstance());
	if (GameInstance)
	{
		GameInstance->DestroySaveGame();
	}

	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.DisplayLoadingScreen(false, 5.f);

	UGameplayStatics::OpenLevel(GetWorld(), LevelName);
}

void AEdensGardenGameMode::OnPlayerCharacterDeath(AEGCharacter* DeadCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	bPauseable = false;
}

void AEdensGardenGameMode::HighlightActor(AActor* Actor, const FLinearColor& Color)
{
	if (HighlightMaterialInstance)
	{
		// Awlays invalidate current pinged actor, even if its the same.
		// There could potentially be new meshes that the actor owns
		if (FocusedPingedActor.Actor.IsValid())
		{
			RestoreHighlightedMeshes(FocusedPingedActor.Meshes);
			FocusedPingedActor.Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

			FocusedPingedActor.Actor.Reset();
			FocusedPingedActor.Meshes.Empty();
		}

		if (Actor)
		{
			// Need to cancel the ping for this actor, as we don't want the timer to revert the highlight
			FHighlightedActor* PingedActor = DurationPingedActors.Find(Actor->GetUniqueID());
			if (PingedActor)
			{
				FTimerManager& TimerManager = GetWorldTimerManager();
				TimerManager.ClearTimer(PingedActor->TimerHandle_PingDuration);

				RestoreHighlightedMeshes(PingedActor->Meshes);

				DurationPingedActors.Remove(Actor->GetUniqueID());
			}

			if (!Actor->IsPendingKill())
			{
				HighlightActor(Actor, HIGHLIGHT_COLOR_CUSTOM, -1.f, FocusedPingedActor);
				HighlightMaterialInstance->SetVectorParameterValue(HighlighCustomColorParameter, Color);
			}
		}
	}
}

void AEdensGardenGameMode::PingActor(AActor* Actor, EHighlightColor Color, float Duration)
{
	if (Duration <= 0.f)
	{
		return;
	}

	if (HighlightMaterialInstance &&
		Actor && !Actor->IsPendingKill())
	{
		// No longer indefinitie
		if (FocusedPingedActor.Actor == Actor)
		{
			// There could possibly be new meshes
			RestoreHighlightedMeshes(FocusedPingedActor.Meshes);
			
			FocusedPingedActor.Actor = nullptr;
			FocusedPingedActor.Meshes.Empty();
		}

		// This actor might already be pinged
		FHighlightedActor* PingedActor = DurationPingedActors.Find(Actor->GetUniqueID());
		if (PingedActor)
		{
			// There could possibly be new meshes
			RestoreHighlightedMeshes(PingedActor->Meshes);

			FTimerManager& TimerManager = GetWorldTimerManager();
			TimerManager.ClearTimer(PingedActor->TimerHandle_PingDuration);

			// Re-Highlight this actor
			HighlightActor(Actor, (int32)Color, Duration, *PingedActor);
		}
		else
		{
			FHighlightedActor& NewPingedActor = DurationPingedActors.Add(Actor->GetUniqueID());
			HighlightActor(Actor, (int32)Color, Duration, NewPingedActor);
		}
	}
}

void AEdensGardenGameMode::StopHighlightingActor(AActor* Actor)
{
	if (Actor)
	{
		// Drop indef actor
		if (FocusedPingedActor.Actor == Actor)
		{
			RestoreHighlightedMeshes(FocusedPingedActor.Meshes);
			FocusedPingedActor.Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

			FocusedPingedActor.Actor.Reset();
			FocusedPingedActor.Meshes.Empty();
		}
		else
		{
			// Drop duration ping
			FHighlightedActor* PingedActor = DurationPingedActors.Find(Actor->GetUniqueID());
			if (PingedActor)
			{
				RestoreHighlightedMeshes(PingedActor->Meshes);
				
				FTimerManager& TimerManager = GetWorldTimerManager();
				TimerManager.ClearTimer(PingedActor->TimerHandle_PingDuration);

				Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

				DurationPingedActors.Remove(Actor->GetUniqueID());
			}
		}
	}
}

UMaterialInstanceDynamic* AEdensGardenGameMode::GetHighlightMaterial()
{
	if (!HighlightMaterialInstance)
	{
		if (HighlightMaterialTemplate)
		{
			HighlightMaterialInstance = UMaterialInstanceDynamic::Create(HighlightMaterialTemplate, this);
		}
		else
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("Unable to create Highlight material as template was null!"));
		}
	}

	return HighlightMaterialInstance;
}

void AEdensGardenGameMode::HighlightActor(AActor* Actor, int32 StencilValue, float Duration, FHighlightedActor& OutHighlightActor)
{
	check(Actor);

	OutHighlightActor.Actor = Actor;
	OutHighlightActor.Meshes.Empty();

	// We only highlight meshes
	TArray<UActorComponent*> MeshComponents = Actor->GetComponentsByClass(UMeshComponent::StaticClass());
	for (UActorComponent* Component : MeshComponents)
	{
		UMeshComponent* Mesh = CastChecked<UMeshComponent>(Component);

		Mesh->SetRenderCustomDepth(true);
		Mesh->SetCustomDepthStencilValue(StencilValue);

		OutHighlightActor.Meshes.Add(Mesh);
	}

	// Need to know when this actor is destroyed, so we don't have any invalid reference
	Actor->OnDestroyed.AddUniqueDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);

	if (Duration > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();

		FTimerDelegate Delegate;
		Delegate.BindUFunction(this, TEXT("OnActorPingExpired"), Actor->GetUniqueID());
		TimerManager.SetTimer(OutHighlightActor.TimerHandle_PingDuration, Delegate, Duration, false);
	}
}

void AEdensGardenGameMode::RestoreHighlightedMeshes(TArray<TWeakObjectPtr<UMeshComponent>>& Meshes)
{
	for (auto& Mesh : Meshes)
	{
		if (Mesh.IsValid())
		{
			Mesh->SetRenderCustomDepth(false);
			Mesh->SetCustomDepthStencilValue(0);
		}
	}
}

void AEdensGardenGameMode::OnActorPingExpired(uint32 UniqueID)
{
	// This function should only be called for pinged actors
	FHighlightedActor* PingedActor = DurationPingedActors.Find(UniqueID);
	if (PingedActor)
	{
		if (PingedActor->Actor.IsValid())
		{
			// IDs can be reused when an actor is destroyed, we
			// should still be referencing the same actor though
			check(PingedActor->Actor->GetUniqueID() == UniqueID);
			RestoreHighlightedMeshes(PingedActor->Meshes);

			PingedActor->Actor->OnDestroyed.RemoveDynamic(this, &AEdensGardenGameMode::OnPingedActorDestroyed);
		}

		DurationPingedActors.Remove(UniqueID);
	}
}

void AEdensGardenGameMode::OnPingedActorDestroyed(AActor* DestroyedActor)
{
	FHighlightedActor* PingedActor = DurationPingedActors.Find(DestroyedActor->GetUniqueID());
	if (PingedActor)
	{
		// The meshes could potentially be detached (unlikely though)
		RestoreHighlightedMeshes(PingedActor->Meshes);

		// No longer need callback
		{
			FTimerManager& TimerManager = GetWorldTimerManager();
			TimerManager.ClearTimer(PingedActor->TimerHandle_PingDuration);
		}

		DurationPingedActors.Remove(DestroyedActor->GetUniqueID());
	}
	else
	{
		// Should be the indef highlighted actor
		ensure(FocusedPingedActor.Actor == DestroyedActor);

		// The meshes could potentially be detached (unlikely though)
		RestoreHighlightedMeshes(FocusedPingedActor.Meshes);
	}
}
