// Copyright of Sock Goblins

#include "EdensGardenAssetManager.h"
#include "EdensGarden.h"
#include "Engine/Engine.h"

#include "MonsterCharacter.h"
#include "SpecOpInventory.h"
#include "Weapon.h"

const FPrimaryAssetType UEdensGardenAssetManager::WeaponType = TEXT("Weapon");
const FPrimaryAssetType UEdensGardenAssetManager::MonsterType = TEXT("Monster");
const FPrimaryAssetType UEdensGardenAssetManager::SpecOpInventoryType = TEXT("SpecOpInventory");

UEdensGardenAssetManager::UEdensGardenAssetManager()
{
}

UEdensGardenAssetManager& UEdensGardenAssetManager::Get()
{
	UEdensGardenAssetManager* AssetManager = Cast<UEdensGardenAssetManager>(GEngine->AssetManager);
	if (AssetManager)
	{
		return *AssetManager;
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Failed to get Edens Garden Asset Manager!"));
		return *NewObject<UEdensGardenAssetManager>();
	}
}

TSubclassOf<AWeaponBase> UEdensGardenAssetManager::LoadWeapon(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure)
{
	// Class might already exist
	TSubclassOf<AWeaponBase> ExistingClass = GetPrimaryAssetObjectClass<AWeaponBase>(PrimaryAssetId);
	if (ExistingClass)
	{
		return ExistingClass;
	}
	else
	{
		// Try finding it manually
		FSoftObjectPath WeaponPath = GetPrimaryAssetPath(PrimaryAssetId);

		// Get weapon synchronously
		UClass* WeaponClass = Cast<UClass>(WeaponPath.TryLoad());

		if (bEnsure && !WeaponClass)
		{
			UE_LOG(LogEdensGarden, Error, TEXT("Failed to load class for Weapon. Asset ID = %s"), *PrimaryAssetId.ToString());
		}

		return WeaponClass;
	}
}

TSubclassOf<AMonsterCharacter> UEdensGardenAssetManager::LoadMonster(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure)
{
	// Class might already exist
	TSubclassOf<AMonsterCharacter> ExistingClass = GetPrimaryAssetObjectClass<AMonsterCharacter>(PrimaryAssetId);
	if (ExistingClass)
	{
		return ExistingClass;
	}
	else
	{
		// Try finding it manually
		FSoftObjectPath MonsterPath = GetPrimaryAssetPath(PrimaryAssetId);

		// Get monster synchronously
		UClass* MonsterClass = Cast<UClass>(MonsterPath.TryLoad());

		if (bEnsure && !MonsterClass)
		{
			UE_LOG(LogEdensGarden, Error, TEXT("Failed to load class for Monster. Asset ID = %s"), *PrimaryAssetId.ToString());
		}

		return MonsterClass;
	}
}

USpecOpInventory* UEdensGardenAssetManager::LoadSpecOpInventory(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure)
{
	FSoftObjectPath InventoryPath = GetPrimaryAssetPath(PrimaryAssetId);

	// Get inventory synchronously
	USpecOpInventory* InventoryData = Cast<USpecOpInventory>(InventoryPath.TryLoad());

	if (bEnsure && !InventoryData)
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Failed to load spec op inventory. Asset ID = %s"), *PrimaryAssetId.ToString());
	}

	return InventoryData;
}
