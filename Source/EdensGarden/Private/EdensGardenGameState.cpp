// Copyright of Sock Goblins

#include "EdensGardenGameState.h"
#include "EdensGardenGameInstance.h"
#include "EdensGardenGameMode.h"
#include "EdensGardenLevelScriptActor.h"
#include "EdensGardenSaveGame.h"
#include "EdensGardenUserSettings.h"

#include "Gun.h"
#include "MeleeWeapon.h"
#include "SpecOpCharacter.h"
#include "SpecOpAbilitySystemComponent.h"

#include "CheckpointStart.h"
#include "EngineUtils.h"
#include "MemoryReader.h"
#include "MemoryWriter.h"
#include "Engine/Engine.h"
#include "GameFramework/WorldSettings.h"

#include "TimerManager.h"

void AEdensGardenGameState::HandleBeginPlay()
{
	bReplicatedHasBegunPlay = true;

	AWorldSettings* WorldSettings = GetWorldSettings();
	check(WorldSettings);

	InitializeSaveGame();

	WorldSettings->NotifyBeginPlay();
	WorldSettings->NotifyMatchStarted();
}

void AEdensGardenGameState::InitializeSaveGame()
{
	UWorld* World = GetWorld();
	UEdensGardenGameInstance* GameInstance = World->GetGameInstance<UEdensGardenGameInstance>();
	if (GameInstance)
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		check(SaveData);

		// Find spec op to apply save game to (should only be one player controller with one spec op pawn)
		ASpecOpCharacter* SpecOp = nullptr;
		{
			APlayerController* Controller = World->GetFirstPlayerController();
			SpecOp = Controller ? Cast<ASpecOpCharacter>(Controller->GetPawn()) : nullptr;
		}

		// Load world state
		if (SpecOp)
		{
			GameInstance->LoadSpecOpSaveData(SpecOp);
		}

		GameInstance->LoadGameWorldSaveData(World);
	}
}

void AEdensGardenGameState::NotifyCheckpointReached(int32 Index, FName Name)
{
	UWorld* World = GetWorld();
	UEdensGardenGameInstance* GameInstance = World->GetGameInstance<UEdensGardenGameInstance>();
	if (GameInstance)
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		check(SaveData);

		// TODO: Make a more secure way for determining if this checkpoint should update save
		// For now, just use the checkpoint index as a determinator
		// TODO: Move actual check to game instance
		if (Index > SaveData->CheckpointIndex || (Index == SaveData->CheckpointIndex &&
			(Name != NAME_None && Name != SaveData->CheckpointName)))
		{
			bool bProgressSaved = false;

			// We might not be allowed to save (we don't want to overwrite progress)
			UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
			if (!UserSettings || UserSettings->IsSavingEnabled())
			{
				SaveData->CheckpointIndex = Index;
				SaveData->CheckpointName = Name;

				UE_LOG(LogEdensGarden, Log, TEXT("New checkpoint reached! Checkpoint Name = %s, Index = %i"), *Name.ToString(), Index);

				// Get players character to save
				ASpecOpCharacter* SpecOp = nullptr;
				{
					APlayerController* Controller = World->GetFirstPlayerController();
					SpecOp = Controller ? Cast<ASpecOpCharacter>(Controller->GetPawn()) : nullptr;
				}

				if (SpecOp)
				{
					GameInstance->SaveSpecOpSaveData(SpecOp);
				}

				GameInstance->SaveGameWorldSaveData(World, false, &RecordedDestroyedActors);

				// Save this game to disk now in-case application closes unexpectedly	
				bProgressSaved = GameInstance->WriteSaveGame();
			}

			// Notify listeners
			OnNewCheckpointReached.Broadcast(Index, Name, bProgressSaved);
		}
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Unable to verify checkpoint as game instance is invalid"));
	}
}

void AEdensGardenGameState::NotifyLevelEndReached(FName NextLevelName)
{
	UWorld* World = GetWorld();
	UEdensGardenGameInstance* GameInstance = World->GetGameInstance<UEdensGardenGameInstance>();
	if (GameInstance)
	{
		ASpecOpCharacter* SpecOp = nullptr;
		{
			APlayerController* Controller = World->GetFirstPlayerController();
			SpecOp = Controller ? Cast<ASpecOpCharacter>(Controller->GetPawn()) : nullptr;
		}

		if (SpecOp)
		{
			GameInstance->SaveSpecOpSaveData(SpecOp);
		}
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Unable to save player data as game instance is invalid"));
	}

	AEdensGardenGameMode* GameMode = Cast<AEdensGardenGameMode>(AuthorityGameMode);
	if (GameMode)
	{
		GameMode->TransitionToNextLevel(NextLevelName);
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Unable to transition to next level as game mode is invalid"));
	}
}

void AEdensGardenGameState::NotifyGameEndReached(FName MenuLevelName)
{
	AEdensGardenGameMode* GameMode = Cast<AEdensGardenGameMode>(AuthorityGameMode);
	if (GameMode)
	{
		GameMode->TransitionToMainMenu(MenuLevelName);
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Unable to return to main menu as game mode is invalid"));
	}
}

void AEdensGardenGameState::SaveActorDestroyed(AActor* Actor)
{
	if (!Actor || Actor->IsA(ALevelScriptActor::StaticClass()))
	{
		return;
	}

	RecordedDestroyedActors.Add(FName(*Actor->GetName()));
}


