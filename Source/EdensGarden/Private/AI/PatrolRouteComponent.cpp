// Copyright of Sock Goblins

#include "PatrolRouteComponent.h"

UPatrolRouteComponent::UPatrolRouteComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	RouteDirection = EPatrolRouteDirection::Forward;
	bRouteLoops = true;

	CurrentWaypoint = 0;
	Waypoints.Add(FVector::ZeroVector);

	#if WITH_EDITORONLY_DATA
	WaypointColor = FColor::Red;
	SelectedWaypointColor = FColor::Green;
	RouteColor = FColor::Magenta;
	SelectedRouteColor = FColor::Blue;
	#endif
}

#if WITH_EDITOR
void UPatrolRouteComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	
	// We should always have at least one waypoint
	if (Waypoints.Num() <= 0)
	{
		AddWaypoint(FVector::ZeroVector);
	}

	CurrentWaypoint = FMath::Clamp(CurrentWaypoint, 0, Waypoints.Num() - 1);
}
#endif

FVector UPatrolRouteComponent::GetNextWaypoint()
{
	FVector Waypoint(ForceInit);

	FTransform Transform = GetComponentTransform();

	int32 Index = GetNextWaypointIndex();
	if (Index != INDEX_NONE)
	{
		// Should have recieved a valid index, need to check to be safe
		if (ensure(Waypoints.IsValidIndex(Index)))
		{
			Waypoint = Transform.TransformPosition(Waypoints[Index]);
		}
		else
		{
			UE_LOG(LogEdensGarden, Error, TEXT("PatrolRouteComponent: Was expecting CalculateNextWaypointIndex "
				"to return valid index, but instead recieved index %i"), Index);
		}
	}

	return Waypoint;
}

FVector UPatrolRouteComponent::GetWaypointClosestTo(const FVector& Location)
{
	FVector Point(ForceInit);

	if (Waypoints.Num() > 0)
	{
		FTransform Transform = GetComponentTransform();
		FVector Relative = Transform.TransformPosition(Location);

		int32 ClosestIndex = 0;
		float ClosestDistance = FVector::DistSquared(Waypoints[0], Relative);

		for (int32 Index = 1; Index < Waypoints.Num(); ++Index)
		{
			float Distance = FVector::DistSquared(Waypoints[Index], Relative);
			if (Distance < ClosestDistance)
			{
				ClosestIndex = Index;
				ClosestDistance = Distance;
			}
		}

		CurrentWaypoint = ClosestIndex;
		Point = Transform.InverseTransformPosition(Waypoints[CurrentWaypoint]);
	}

	return Point;
}

int32 UPatrolRouteComponent::AddWaypoint(const FVector& Location)
{
	int32 NewIndex = Waypoints.Add(Location);
	if (CurrentWaypoint == INDEX_NONE)
	{
		GetNextWaypointIndex();
	}

	return NewIndex;
}

int32 UPatrolRouteComponent::AddWorldspaceWaypoint(const FVector& Location)
{
	FTransform Transform = GetComponentTransform();
	return AddWaypoint(Transform.InverseTransformPosition(Location));
}

int32 UPatrolRouteComponent::InsertWaypoint(const FVector& Location, int32 Index)
{
	if (ensure(Index >= 0 && Index <= Waypoints.Num()))
	{
		int32 NewIndex = Waypoints.Insert(Location, Index);
		if (CurrentWaypoint == INDEX_NONE)
		{
			GetNextWaypointIndex();
		}

		return NewIndex;
	}
	else
	{
		UE_LOG(LogEdensGarden, Error, TEXT("PatrolRouteComponent: Was unable to insert "
			"location as index is not within range! Index = %i"), Index);
	}

	return INDEX_NONE;
}

int32 UPatrolRouteComponent::InsertWorldspaceWaypoint(const FVector& Location, int32 Index)
{
	FTransform Transform = GetComponentTransform();
	return InsertWaypoint(Transform.InverseTransformPosition(Location), Index);
}

void UPatrolRouteComponent::RemoveWaypoint(int32 Index)
{
	// We always need to keep at least one waypoint
	if (Waypoints.Num() > 1 && Waypoints.IsValidIndex(Index))
	{
		// Need to update our current waypoint as we might be clearing it
		if (CurrentWaypoint == Index)
		{
			// Decrement by one, we need to loop by using new size
			CurrentWaypoint = (CurrentWaypoint - 1) % (Waypoints.Num() - 1);
		}

		Waypoints.RemoveAt(Index);
	}
}

void UPatrolRouteComponent::SetWaypointLocation(const FVector& Location, int32 Index)
{
	if (Waypoints.IsValidIndex(Index))
	{
		Waypoints[Index] = Location;
	}
}

void UPatrolRouteComponent::OffsetWaypointLocation(const FVector& Offset, int32 Index)
{
	if (Waypoints.IsValidIndex(Index))
	{
		Waypoints[Index] += Offset;
	}
}

FVector UPatrolRouteComponent::GetCurrentWaypoint() const
{
	return GetWaypointAt(CurrentWaypoint);
}

FVector UPatrolRouteComponent::GetWaypointAt(int32 Index) const
{
	if (Waypoints.IsValidIndex(Index))
	{
		FTransform Transform = GetComponentTransform();
		return Transform.TransformPosition(Waypoints[Index]);
	}

	return FVector::ZeroVector;
}

int32 UPatrolRouteComponent::CalculateNextWaypointIndex_Implementation()
{
	int32 Count = Waypoints.Num();
	if (Count <= 0)
	{
		UE_LOG(LogEdensGarden, Error, TEXT("PatrolRouteComponent: Failed to calculate waypoint as no waypoints are set!"));
		return INDEX_NONE;
	}

	// Might not be following route yet
	if (CurrentWaypoint == INDEX_NONE)
	{
		return 0;
	}

	// Are we at the end of the route

	// Iterate based on direction
	int32 Iterate = (RouteDirection == EPatrolRouteDirection::Forward) ? 1 : -1;
	int32 NewIndex = (CurrentWaypoint + Iterate) % Count;

	// Need to wrap back if at end of route
	if (NewIndex < 0)
	{
		if (bRouteLoops)
		{
			return Count - 1;
		}
		else
		{
			// We have to go forward to follow the route correctly
			RouteDirection = EPatrolRouteDirection::Forward;
			return FMath::Min(1, Count);
		}
	}
	else if (NewIndex >= Count)
	{
		if (bRouteLoops)
		{
			return 0;
		}
		else
		{
			// We have to go backwards to follow the route correctly
			RouteDirection = EPatrolRouteDirection::Backward;
			return FMath::Min(0, Count - 2);
		}
	}

	return NewIndex;
}

int32 UPatrolRouteComponent::GetNextWaypointIndex()
{
	CurrentWaypoint = CalculateNextWaypointIndex();
	return CurrentWaypoint;
}
