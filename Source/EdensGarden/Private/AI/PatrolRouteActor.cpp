// Copyright of Sock Goblins

#include "PatrolRouteActor.h"
#include "Components/BillboardComponent.h"

#include "Engine/Texture2D.h"
#include "UObject/ConstructorHelpers.h"

APatrolRouteActor::APatrolRouteActor()
{
	PrimaryActorTick.bCanEverTick = false;

	PatrolRouteComponent = CreateDefaultSubobject<UPatrolRouteComponent>(TEXT("Patrol Route"));
	SetRootComponent(PatrolRouteComponent);

	#if WITH_EDITORONLY_DATA
	BillboardComponent = CreateEditorOnlyDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	BillboardComponent->SetupAttachment(PatrolRouteComponent);

	// Default values for billboard
	{
		BillboardComponent->bIsEditorOnly = true;

		static ConstructorHelpers::FObjectFinder<UTexture2D> WaypointTexture(TEXT("/Engine/EditorResources/Waypoint.Waypoint"));
		if (WaypointTexture.Succeeded())
		{
			BillboardComponent->SetSprite(WaypointTexture.Object);
		}
	}
	#endif
}
