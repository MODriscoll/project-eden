// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "EdensGardenSettings.generated.h"

/** Settings for builds of Edens Garden (Includes some variables unique to editor builds) */
UCLASS(config = EdensGarden)
class EDENSGARDEN_API UEdensGardenSettings : public UDeveloperSettings
{
	GENERATED_BODY()

public:

	UEdensGardenSettings();

public:

	/** Get settings */
	static UEdensGardenSettings* Get();

public:

	/** The name of slot to save to by default */
	UPROPERTY(EditAnywhere, config, Category = Saving, meta = (DisplayName = "Game Save Name"))
	FString GameSaveSlot;

	/** If active games can be saved (to disk) while in editor */
	UPROPERTY(EditAnywhere, config, AdvancedDisplay, Category = Saving, meta = (DisplayName="Save In Editor"))
	uint8 bCanSaveInEditor : 1;

	/** The custom slot name to use when saving in editor (If invalid, GameSaveSlot will be used) */
	UPROPERTY(EditAnywhere, config, AdvancedDisplay, Category = Saving, meta = (DisplayName = "Editor Game Save Name", EditCondition = bCanSaveInEditor))
	FString EditorSaveSlot;
};

