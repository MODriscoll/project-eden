// Copyright of Sock Goblins

#include "EGCharacter.h"
#include "EGCharacterAttributeSet.h"
#include "EGCharacterMovementComponent.h"
#include "EdensGardenGlobals.h"
#include "EdensGardenFunctionLibrary.h"

#include "AbilitySystemComponent.h"
#include "AIController.h"
#include "Components/AudioComponent.h"
#include "Components/CapsuleComponent.h"

#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionSystem.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Hearing.h"
#include "Perception/AISense_Damage.h"
#include "Perception/AISense_Touch.h"

#include "Effects/GameplayEffect_DirectDamage.h"

DEFINE_LOG_CATEGORY(LogEGCharacter);

const FName AEGCharacter::AbilitySystemComponentName(TEXT("AbilitySystem"));
const FName AEGCharacter::AttributeSetName(TEXT("Attributes"));
const FGenericTeamId AEGCharacter::DefaultTeamId;

AEGCharacter::AEGCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<UEGCharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	AbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>(AbilitySystemComponentName);
	AttributeSet = CreateDefaultSubobject<UEGCharacterAttributeSet>(AttributeSetName);

	PerceptionSource = CreateDefaultSubobject<UAIPerceptionStimuliSourceComponent>(TEXT("PerceptionSource"));

	CharacterVoice = CreateDefaultSubobject<UAudioComponent>(TEXT("CharacterVoice"));
	CharacterVoice->SetupAttachment(GetCapsuleComponent());
	CharacterVoice->bIsUISound = false;
	CharacterVoice->bIsMusic = false;
	CharacterVoice->bAlwaysPlay = true;

	CharacterVoice->OnAudioFinished.AddDynamic(this, &AEGCharacter::OnSpeechFinished);

	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->NavAgentProps.bCanJump = false;
	MovementComponent->bIgnoreBaseRotation = true;

	// We want to tick after movement component ticks, as we
	// can record any hit events, then save them for next tick
	{
		AddTickPrerequisiteComponent(MovementComponent);
	}

	UCapsuleComponent* Capsule = GetCapsuleComponent();
	Capsule->OnComponentHit.AddDynamic(this, &AEGCharacter::OnCharacterHit);

	IdentityTags.AddTag(FGameplayTag::RequestGameplayTag(TEXT("Character")));
	bStopVoiceOnDeath = true;
	bIsDead = false;
	bIsRagdoll = false;
	bIsStunned = false;

	TeamId = EEdensGardenTeamID::NoTeam;

	InheritYawRotationSpeed = 0.f;
	CurrentVoicePriority = INDEX_NONE;
}

void AEGCharacter::Falling()
{
	const auto& Globals = UEdensGardenGlobals::Get();
	AbilitySystem->AddLooseGameplayTag(Globals.CharacterFallingTag);
}

void AEGCharacter::Landed(const FHitResult& Hit)
{
	const auto& Globals = UEdensGardenGlobals::Get();
	AbilitySystem->RemoveLooseGameplayTag(Globals.CharacterFallingTag);
}

void AEGCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	if (AbilitySystem)
	{
		AbilitySystem->RefreshAbilityActorInfo();
		ApplyDefaultAbilitiesAndEffects();
	}
}

void AEGCharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	if (IsStunned())
	{
		return;
	}

	if (bUseControllerRotationPitch || bUseControllerRotationYaw || bUseControllerRotationRoll)
	{
		const FRotator CurrentRotation = GetActorRotation();

		if (!bUseControllerRotationPitch)
		{
			NewControlRotation.Pitch = CurrentRotation.Pitch;
		}

		if (!bUseControllerRotationYaw)
		{
			NewControlRotation.Yaw = CurrentRotation.Yaw;
		}
		else
		{
			if (DeltaTime > 0.f && InheritYawRotationSpeed > 0.f)
			{
				FRotator CharRot(0.f, CurrentRotation.Yaw, 0.f);
				FRotator ContRot(0.f, NewControlRotation.Yaw, 0.f);

				NewControlRotation.Yaw = FMath::RInterpTo(CurrentRotation, NewControlRotation, DeltaTime, InheritYawRotationSpeed).Yaw;
			}
		}

		if (!bUseControllerRotationRoll)
		{
			NewControlRotation.Roll = CurrentRotation.Roll;
		}

		SetActorRotation(NewControlRotation);
	}
}

void AEGCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// We are our own avatar
	if (AbilitySystem)
	{
		AbilitySystem->InitAbilityActorInfo(this, this);

		// Initializing here so attributes are defaulted before begin play
		if (AttributeDefaults)
		{
			InitializeAttributeDefaults();
		}
	}

	// Bind touch events
	UCapsuleComponent* Capsule = GetCapsuleComponent();
	if (Capsule)
	{
		Capsule->OnComponentHit.AddUniqueDynamic(this, &AEGCharacter::OnCharacterHit);
	}
}

void AEGCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	HitActorsLastTick = MoveTemp(HitActorsThisTick);

	#if (WITH_EDITORONLY_DATA && WITH_EDITOR)
	if (bIsStunned)
	{
		float TimeSinceStart = GetWorld()->GetTimeSeconds() - StunStartTime;

		// Ten seconds is probaly too long for a stun
		if (TimeSinceStart > 10.f)
		{
			UE_LOG(LogEGCharacter, Warning, TEXT("Character %s has been stunned for longer than 10 seconds! Forcing stun to finish"), *GetName());
			StopStun();
		}
	}
	#endif
}

void AEGCharacter::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	// We want to unregister from perception system, this should
	// update stimulus that any perceptions components might have on us
	PerceptionSource->UnregisterFromPerceptionSystem();

	Super::EndPlay(EndPlayReason);
}

void AEGCharacter::FellOutOfWorld(const UDamageType& DamageType)
{
	if (!bIsDead)
	{
		Kill();
	}
}

#if EG_GAMEPLAY_DEBUGGER
void AEGCharacter::GetDebugString(FString& String) const
{
	const FString Formatting = TEXT(
		"{white}Identity Tags: {green}%s\n"
		"{white}Health: %s%.2f {white}/ Max Health: {green}%.2f\n"
		"{white}Speed Modifier: {green}%.2f\n"
		"{white}Defense Multiplier: {green}%.2f\n"
		"{white}Is Dead = {green}%s{white}, Is Ragdoll = {green}%s{white}, Is Stunned = {green}%s\n\n");

	float Health = GetHealth();
	float MaxHealth = GetMaxHealth();

	float Ratio = Health / MaxHealth;
	
	String.Append(FString::Printf(*Formatting,
		*IdentityTags.ToStringSimple(),
		Ratio <= 0.1f ? TEXT("{red}") : (Ratio <= 0.4f ? TEXT("{yellow}") : TEXT("{green}")), Health,
		MaxHealth, GetSpeedModifier(), GetDefenseMultiplier(),
		bIsDead ? TEXT("True") : TEXT("False"), 
		bIsRagdoll ? TEXT("True") : TEXT("False"),
		bIsStunned ? TEXT("True") : TEXT("False")));
}
#endif

void AEGCharacter::GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const
{
	TagContainer.AppendTags(IdentityTags);
	if (AbilitySystem)
	{
		AbilitySystem->GetOwnedGameplayTags(TagContainer);
	}
}

void AEGCharacter::ApplyDefaultAbilitiesAndEffects()
{
	for (const TSubclassOf<UGameplayAbility>& AbilityTemplate : PassiveAbilities)
	{
		UGameplayAbility* Ability = AbilityTemplate.GetDefaultObject();
		if (Ability)
		{
			// We are the source of this ability
			AbilitySystem->GiveAbility(FGameplayAbilitySpec(Ability, 0, INDEX_NONE, this));
		}
	}

	for (const TSubclassOf<UGameplayEffect>& Effect : PassiveEffects)
	{
		if (Effect)
		{
			// Set us as the source of this effect
			FGameplayEffectContextHandle Context = AbilitySystem->MakeEffectContext();
			Context.AddSourceObject(this);

			FGameplayEffectSpecHandle Spec = AbilitySystem->MakeOutgoingSpec(Effect, UGameplayEffect::INVALID_LEVEL, Context);
			AbilitySystem->ApplyGameplayEffectSpecToSelf(*Spec.Data);
		}
	}
}

void AEGCharacter::RemoveDefaultAbilitiesAndEffects()
{
	// We need to first record what abilities we are removing.
	// Can't do it one go as removing will invalidate the array
	TArray<FGameplayAbilitySpecHandle> PassiveSpecs;

	// Need to run through all abilities we can activate. As they could potentially
	// be a duplicate ability not made by use which we probaly don't want to touch
	const TArray<FGameplayAbilitySpec>& AbilitySpecs = AbilitySystem->GetActivatableAbilities();
	for (const FGameplayAbilitySpec& Spec : AbilitySpecs)
	{
		if (Spec.SourceObject == this && PassiveAbilities.Contains(Spec.Ability->GetClass()))
		{
			PassiveSpecs.Add(Spec.Handle);
		}
	}

	// We can now safely remove all our passive abilities
	for (const FGameplayAbilitySpecHandle& Spec : PassiveSpecs)
	{
		AbilitySystem->ClearAbility(Spec);
	}

	// Remove effects sourced by us (there might be effects made somewhere else)
	FGameplayEffectQuery Query;
	Query.EffectSource = this;

	AbilitySystem->RemoveActiveEffects(Query);
}

void AEGCharacter::Kill()
{
	if (!AbilitySystem)
	{
		return;
	}

	if (!IsDead())
	{
		UE_LOG(LogEGCharacter, Log, TEXT("Forcefully killing character %s"), *GetName());

		// Should always be succesfull, check to be safe
		FGameplayEffectSpecHandle SpecHandle = AbilitySystem->MakeOutgoingSpec(
			UGameplayEffect_DirectDamage::StaticClass(), UGameplayEffect::INVALID_LEVEL, AbilitySystem->MakeEffectContext());
		if (SpecHandle.IsValid())
		{
			// Damage us by what ever health we have left
			SpecHandle.Data->SetSetByCallerMagnitude(UGameplayEffect_DirectDamage::DamageDataName, GetHealth());
			AbilitySystem->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
		}
	}
}

void AEGCharacter::SetGodEnabled(bool bEnable)
{
	if (!AbilitySystem)
	{
		return;
	}

	// Adding and removing instead of setting, this will allow
	// multiple sources to enable god and disbable it later without
	// accidently disabling another sources god mode

	const FGameplayTag InvicibilityTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Invincible"));
	if (bEnable)
	{
		AbilitySystem->AddLooseGameplayTag(InvicibilityTag);
	}
	else
	{
		AbilitySystem->RemoveLooseGameplayTag(InvicibilityTag);
	}
}

float AEGCharacter::GetHealth() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetHealth();
	}
	else
	{
		return 0.f;
	}
}

float AEGCharacter::GetMaxHealth() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetMaxHealth();
	}
	else
	{
		return 0.f;
	}
}

float AEGCharacter::GetSpeedModifier() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetSpeedModifier();
	}
	else
	{
		return 0.f;
	}
}

float AEGCharacter::GetDefenseMultiplier() const
{
	if (AttributeSet)
	{
		return AttributeSet->GetDefenseMultiplier();
	}
	else
	{
		return 0.f;
	}
}

void AEGCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	DisableMovementAndCollision();

	// Cancel any effects and abilities that might be active
	if (AbilitySystem)
	{
		AbilitySystem->RemoveActiveEffects(FGameplayEffectQuery());
		AbilitySystem->CancelAbilities();
	}

	// AI should no longer pick up this character
	if (PerceptionSource)
	{
		PerceptionSource->UnregisterFromPerceptionSystem();
	}
}

void AEGCharacter::NotifyHealthChanged(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags)
{
	if (NewHealth >= GetMaxHealth())
	{
		OnHealthFullyRestored();
	}

	OnHealthChanged(NewHealth, Delta, GameplayTags);
	OnHealthChanged_Implementation(NewHealth, Delta, GameplayTags);

	OnCharacterHealthChanged.Broadcast(this, NewHealth, Delta, GameplayTags);
}

void AEGCharacter::NotifyDamaged(float Damage, float NewHealth, bool bKillingBlow, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	if (bKillingBlow && !bIsDead)
	{
		ensureMsgf(NewHealth <= 0.f, TEXT("Notified as killing blow but health is greater than zero!"));

		bIsDead = true;

		const auto& Globals = UEdensGardenGlobals::Get();
		AbilitySystem->AddLooseGameplayTag(Globals.CharacterDeadTag);
		
		// We cancel the voice before executing events, so any event that wants to play a voice doesn't get cancelled 
		if (bStopVoiceOnDeath)
		{
			if (CharacterVoice)
			{
				CharacterVoice->Stop();
			}
		}

		// Notify ability system
		{
			const FGameplayTag DeathEvent = FGameplayTag::RequestGameplayTag(TEXT("Event.Death"));

			FGameplayEventData Payload;
			Payload.EventTag = DeathEvent;
			Payload.Target = this;
			Payload.Instigator = DamageInstigator;
			Payload.InstigatorTags.AppendTags(GameplayTags);
			Payload.OptionalObject = Source;
			Payload.EventMagnitude = Damage;

			if (bValidHit)
			{
				// Memeory managed via shared pointer
				Payload.TargetData.Add(new FGameplayAbilityTargetData_SingleTargetHit(Hit));
			}

			FScopedPredictionWindow(AbilitySystem, true);
			AbilitySystem->HandleGameplayEvent(DeathEvent, &Payload);

			// Execute any gameplay cues
			{
				FGameplayCueParameters Parameters;
				Parameters.OriginalTag = DeathEvent;
				Parameters.RawMagnitude = Damage;
				Parameters.Instigator = DamageInstigator;
				Parameters.EffectCauser = Source;

				if (bValidHit)
				{
					Parameters.Normal = Hit.Normal;
					Parameters.PhysicalMaterial = Hit.PhysMaterial;
				}

				const FGameplayTag RootDeathTag = FGameplayTag::RequestGameplayTag(TEXT("Death"), false);
				FGameplayTagContainer DeathTags = GameplayTags.Filter(RootDeathTag.GetSingleTagContainer());

				if (DeathTags.IsEmpty())
				{
					ExecuteGameplayCuesFromTable(DeathEvent, Parameters);
				}
				else
				{
					for (const FGameplayTag& Tag : DeathTags)
					{
						ExecuteGameplayCuesFromTable(Tag, Parameters);
					}
				}
			}
		}

		OnDeath(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);
		OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

		OnCharacterDeath.Broadcast(this, Damage, DamageInstigator, Source, GameplayTags);
	}
	else
	{
		// Notify ability system
		{
			const FGameplayTag DamageEvent = FGameplayTag::RequestGameplayTag(TEXT("Event.Damaged"));

			FGameplayEventData Payload;
			Payload.EventTag = DamageEvent;
			Payload.Target = this;
			Payload.Instigator = DamageInstigator;
			Payload.InstigatorTags.AppendTags(GameplayTags);
			Payload.OptionalObject = Source;
			Payload.EventMagnitude = Damage;

			if (bValidHit)
			{
				// Memeory managed via shared pointer
				Payload.TargetData.Add(new FGameplayAbilityTargetData_SingleTargetHit(Hit));
			}

			FScopedPredictionWindow(AbilitySystem, true);
			AbilitySystem->HandleGameplayEvent(DamageEvent, &Payload);

			// Execute any gameplay cues
			{
				FGameplayCueParameters Parameters;
				Parameters.OriginalTag = DamageEvent;
				Parameters.RawMagnitude = Damage;
				Parameters.Instigator = DamageInstigator;
				Parameters.EffectCauser = Source;
				
				if (bValidHit)
				{
					Parameters.Normal = Hit.Normal;
					Parameters.PhysicalMaterial = Hit.PhysMaterial;
				}

				const FGameplayTag RootDamageTag = FGameplayTag::RequestGameplayTag(TEXT("Damage"));
				FGameplayTagContainer DamageTags = GameplayTags.Filter(RootDamageTag.GetSingleTagContainer());

				if (DamageTags.IsEmpty())
				{
					ExecuteGameplayCuesFromTable(DamageEvent, Parameters);
				}
				else
				{
					for (const FGameplayTag& Tag : DamageTags)
					{
						ExecuteGameplayCuesFromTable(Tag, Parameters);
					}
				}
			}
		}

		OnDamaged(Damage, NewHealth, bValidHit, Hit, DamageInstigator, Source, GameplayTags);
		OnDamage_Implementation(Damage, NewHealth, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

		OnCharacterDamaged.Broadcast(this, Damage, DamageInstigator, Source, GameplayTags);
	}

	AEGCharacter* OtherCharacter = Cast<AEGCharacter>(DamageInstigator);
	if (OtherCharacter)
	{
		OtherCharacter->OnInstigatedDamage(this, Damage, GameplayTags);
	}

	FVector DamageSource = Source ? Source->GetActorLocation() : GetActorLocation();
	FVector HitLocation = bValidHit ? Hit.Location : GetActorLocation();

	// TODO: Either keep this and check for DOT here, or send using custom damage function for controllers to listen for
	UAISense_Damage::ReportDamageEvent(this, this, DamageInstigator, Damage, DamageSource, HitLocation);
}

void AEGCharacter::NotifySpeedModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags)
{
	OnSpeedModifierChanged(NewModifier, Delta, GameplayTags);
	OnSpeedModifierChanged_Implementation(NewModifier, Delta, GameplayTags);
}

void AEGCharacter::InitializeAttributeDefaults()
{
	AbilitySystem->InitStats(UEGCharacterAttributeSet::StaticClass(), AttributeDefaults);
}

bool AEGCharacter::EnterRagdoll()
{
	if (bIsRagdoll)
	{
		return false;
	}

	if (!bIsDead)
	{
		UE_LOG(LogEGCharacter, Warning, TEXT("Character %s is entering ragdoll while alive!"), *GetName());
	}

	USkeletalMeshComponent* Mesh = GetMesh();

	RelTransformBeforeRagdoll = Mesh->GetRelativeTransform();

	// Some abilities may be using montages which they should be listening to via callbacks.
	// This should notify them and hopefully cancel the ability via the montage callbacks
	StopAnimMontage();

	Mesh->SetCollisionProfileName(TEXT("Ragdoll"));
	Mesh->SetAllBodiesSimulatePhysics(true);
	Mesh->SetAllBodiesPhysicsBlendWeight(1.f);
	Mesh->SetAnimationMode(EAnimationMode::AnimationCustomMode);

	bIsRagdoll = true;

	// Let derived types handle entering ragdoll if required
	OnRagdoll();

	// Notify listeners
	OnRagdollStateChanged.Broadcast(this, true);

	return true;
}

bool AEGCharacter::EnterRagdollAndApplyImpulse(FVector Impulse, FName Bone)
{
	if (!EnterRagdoll())
	{
		return false;
	}

	USkeletalMeshComponent* Mesh = GetMesh();
	Mesh->AddImpulseToAllBodiesBelow(Impulse, Bone, true);

	return true;
}

void AEGCharacter::ExitRagdoll()
{
	if (bIsRagdoll)
	{
		USkeletalMeshComponent* Mesh = GetMesh();

		Mesh->SetCollisionProfileName(TEXT("CharacterMesh"));
		Mesh->SetAllBodiesSimulatePhysics(false);
		Mesh->SetAllBodiesPhysicsBlendWeight(0.f);
		Mesh->SetAnimationMode(EAnimationMode::AnimationBlueprint);

		Mesh->AttachToComponent(GetCapsuleComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		Mesh->SetRelativeTransform(RelTransformBeforeRagdoll);

		RelTransformBeforeRagdoll = FTransform::Identity;

		bIsRagdoll = false;

		// Allow derived types to revert back to normal state
		OnRagdollStop();

		// Notify listeners
		OnRagdollStateChanged.Broadcast(this, false);
	}
}

void AEGCharacter::EnableMovementAndCollision()
{
	AController* Controller = GetController();
	if (Controller)
	{
		Controller->SetIgnoreMoveInput(false);
	}

	// Restore collision
	UCapsuleComponent* Capsule = GetCapsuleComponent();
	{
		// There is potential of derived types having modified the default collision presets
		AEGCharacter* DefaultEGCharacter = GetClass()->GetDefaultObject<AEGCharacter>();
		if (DefaultEGCharacter)
		{
			UCapsuleComponent* DefaultCapsule = Cast<UCapsuleComponent>(DefaultEGCharacter->GetDefaultSubobjectByName(ACharacter::CapsuleComponentName));
			if (ensure(DefaultCapsule))
			{
				Capsule->SetCollisionProfileName(DefaultCapsule->GetCollisionProfileName());
				return;
			}
		}

		// Default
		Capsule->SetCollisionProfileName(TEXT("Pawn"));
	}
}

void AEGCharacter::DisableMovementAndCollision()
{
	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->StopActiveMovement();

	AController* Controller = GetController();
	if (Controller)
	{
		Controller->SetIgnoreMoveInput(true);
	}

	UCapsuleComponent* Capsule = GetCapsuleComponent();
	Capsule->SetCollisionProfileName(DISABLEDCHARACTER_PROFILE);
}

void AEGCharacter::SetGenericTeamId(const FGenericTeamId& TeamID)
{
	int32 Id = TeamID.GetId();
	if (Id < static_cast<uint8>(EEdensGardenTeamID::Count) || Id == static_cast<uint8>(EEdensGardenTeamID::NoTeam))
	{
		EEdensGardenTeamID NewTeamId = static_cast<EEdensGardenTeamID>(Id);
		if (NewTeamId != TeamId)
		{
			TeamId = NewTeamId;

			// Reset this actors perception (there is not direct re-register function)
			if (PerceptionSource && !IsDead())
			{
				PerceptionSource->UnregisterFromPerceptionSystem();
				PerceptionSource->RegisterWithPerceptionSystem();

				// Reset controllers perception of other actors
				AAIController* AIController = Cast<AAIController>(GetController());
				if (AIController && AIController->GetPerceptionComponent() != nullptr)
				{
					UAIPerceptionComponent* PerceptionComponent = AIController->GetPerceptionComponent();
					PerceptionComponent->ForgetAll();
				}
			}
		}		
	}
	else
	{
		UE_LOG(LogEGCharacter, Warning, TEXT("TeamID given to character %s is out of range. ID = %i"), *GetName(), Id);
	}
}

FGenericTeamId AEGCharacter::GetGenericTeamId() const
{
	return FGenericTeamId(static_cast<uint8>(TeamId));
}

void AEGCharacter::OnCharacterHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor)
	{
		if (!HitActorsLastTick.Contains(OtherActor))
		{
			FAITouchEvent Touch;
			Touch.Location = OtherActor->GetActorLocation();//Hit.Location;
			Touch.OtherActor = OtherActor;
			Touch.TouchReceiver = GetController();

			// Report this event
			UAIPerceptionSystem* PerceptionSystem = UAIPerceptionSystem::GetCurrent(*GetWorld());
			if (PerceptionSystem)
			{
				PerceptionSystem->OnEvent(Touch);
			}

			OnCharacterInitialHit(OtherActor, NormalImpulse, Hit);
		}

		HitActorsThisTick.Add(OtherActor);
	}
}

void AEGCharacter::Stun(const FVector& Source, float Power, FGameplayTag InstigatorTag)
{
	FVector Forward = GetActorForwardVector();
	FVector SourceDir = GetActorLocation() - Source;

	Stun(Source, SourceDir.GetSafeNormal2D() * Power, InstigatorTag);
}

void AEGCharacter::StunWithImpulse(const FVector& Source, const FVector& Impulse, FGameplayTag InstigatorTag)
{
	Stun(Source, Impulse, InstigatorTag);
}

void AEGCharacter::Stun(const FVector& Source, const FVector& Impulse, FGameplayTag InstigatorTag)
{
	if (CanBeStunned() && ShouldBeStunnedBy(InstigatorTag))
	{
		if (!Impulse.ContainsNaN())
		{ 
			// Push us back a bit from character stunning us
			UCharacterMovementComponent* MovementComp = GetCharacterMovement();
			MovementComp->AddImpulse(Impulse, true);
		}

		// Notify ability system
		{
			const FGameplayTag StunnedEvent = FGameplayTag::RequestGameplayTag(TEXT("Event.Stunned"));

			FGameplayEventData Payload;
			Payload.EventTag = InstigatorTag;
			Payload.Target = this;

			// Calculate the direction we were hit from (relative to way we are facing)
			{
				FVector Forward = GetActorForwardVector();
				FVector ImpulseDir = Impulse.GetSafeNormal2D();

				float Cosine = Forward.CosineAngle2D(ImpulseDir);
				FVector Right = FVector::CrossProduct(Forward, ImpulseDir);
				Payload.EventMagnitude = FMath::Acos(Cosine) * FMath::Sign(Right.Z);
			}

			FScopedPredictionWindow(AbilitySystem, true);
			AbilitySystem->HandleGameplayEvent(StunnedEvent, &Payload);

			// Execute any gameplay cues
			{
				FGameplayCueParameters Parameters;
				Parameters.Location = Source;

				ExecuteGameplayCuesFromTable(InstigatorTag.IsValid() ? InstigatorTag : StunnedEvent, Parameters);
			}
		}

		bool bPreviousStunState = bIsStunned;
		bIsStunned = true;

		// We only want to perform these on initial stun
		if (!bPreviousStunState)
		{
			const auto& Globals = UEdensGardenGlobals::Get();
			AbilitySystem->AddLooseGameplayTag(Globals.CharacterStunnedTag);

			OnCharacterStunned(Source, InstigatorTag);
		}

		#if WITH_EDITORONLY_DATA
		StunStartTime = GetWorld()->GetTimeSeconds();
		#endif
	}
}

void AEGCharacter::StopStun()
{
	if (bIsStunned)
	{
		bIsStunned = false;

		const auto& Globals = UEdensGardenGlobals::Get();
		AbilitySystem->RemoveLooseGameplayTag(Globals.CharacterStunnedTag);

		OnCharacterRecoveredFromStun();
	}
}

bool AEGCharacter::CanBeStunned() const
{
	return !(bIsDead || bIsRagdoll);
}

bool AEGCharacter::ShouldBeStunnedBy_Implementation(const FGameplayTag& InstigatorTag)
{
	if (InstigatorTag.IsValid())
	{
		return !InstigatorTag.MatchesAny(StunImmunityTags);
	}

	return true;
}

void AEGCharacter::ExecuteGameplayCuesFromTable(const FGameplayTag& Tag, const FGameplayCueParameters& Parameters)
{
	if (TaggedGameplayCues)
	{
		UEdensGardenFunctionLibrary::ExecuteGameplayCuesFromTable(AbilitySystem, Tag, TaggedGameplayCues, Parameters);
	}
}

bool AEGCharacter::Talk(const FCharacterSpeechRequest& SpeechRequest)
{
	if (!CharacterVoice)
	{
		return false;
	}

	if (SpeechRequest.Sound)
	{
		int32 RequestPriority = FMath::Max(0, SpeechRequest.Priority);
		if (RequestPriority > CurrentVoicePriority || (RequestPriority == CurrentVoicePriority && SpeechRequest.bOverride))
		{
			CharacterVoice->SetSound(SpeechRequest.Sound);
			CharacterVoice->Play();

			// Set priority last, as stopping potentially current sound may reset priority
			CurrentVoicePriority = RequestPriority;

			return true;
		}
	}
	else
	{
		UE_LOG(LogEGCharacter, Warning, TEXT("Unable to accept speech request for character %s as sound was null"), *GetName());
	}

	return false;
}

bool AEGCharacter::TalkDontRepeat(const FCharacterSpeechRequest& SpeechRequest)
{
	if (CharacterVoice && CharacterVoice->Sound != SpeechRequest.Sound)
	{
		return Talk(SpeechRequest);
	}

	return false;
}

void AEGCharacter::StopTalking(int32 CancelPriority)
{
	if (CancelPriority <= -1 || CancelPriority >= CurrentVoicePriority)
	{
		// Avoid having callback trigger if already finished
		if (CharacterVoice->IsPlaying())
		{
			CharacterVoice->SetSound(nullptr);
		}
	}
}

void AEGCharacter::StopTalkingSound(USoundBase* Sound)
{
	// Avoid having callback trigger if already finished
	if (CharacterVoice->Sound == Sound && CharacterVoice->IsPlaying())
	{
		CharacterVoice->SetSound(nullptr);
	}
}

void AEGCharacter::OnSpeechFinished()
{
	CurrentVoicePriority = INDEX_NONE;
}

