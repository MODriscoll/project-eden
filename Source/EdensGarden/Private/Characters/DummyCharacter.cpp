// Copyright of Sock Goblins

#include "DummyCharacter.h"
#include "EGCharacterAIController.h"
#include "EGCharacterAttributeSet.h"
#include "AbilitySystemComponent.h"

#include "Engine.h"
#include "TimerManager.h"

#include "AIController.h"
#include "BrainComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AIPerceptionStimuliSourceComponent.h"

ADummyCharacter::ADummyCharacter()
{
	AIControllerClass = AEGCharacterAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag(TEXT("Character.ID.Test")));
	RespawnTime = 5.f;
}

void ADummyCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& Tags)
{
	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, Tags);

	if (bValidHit)
	{
		EnterRagdollAndApplyImpulse(Hit.ImpactNormal * -500.f, Hit.BoneName);
	}
	else
	{
		EnterRagdoll();
	}

	UAIPerceptionStimuliSourceComponent* PerSource = GetPerceptionSource();
	if (PerSource)
	{
		PerSource->UnregisterFromPerceptionSystem();
	}

	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.SetTimer(TimerHandle_Respawn, this, &ADummyCharacter::OnRespawn, RespawnTime, false);
}

void ADummyCharacter::OnRespawn()
{
	UEGCharacterAttributeSet* Attributes = GetAttributeSet();
	Attributes->SetHealth(GetMaxHealth());

	// Stand up again (Make respawn function (or call reset))
 	bIsDead = false;
	ExitRagdoll();
	EnableMovementAndCollision();

	// Remove any active effects that were applied to us (need something like this in base character)
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	ASC->RemoveActiveEffects(FGameplayEffectQuery());

	// Inform controller (if any) to resume logic
	if (AAIController* Controller = Cast<AAIController>(GetController()))
	{
		if (Controller->PerceptionComponent)
		{
			//Controller->PerceptionComponent->RegisterComponent();
			//Controller->PerceptionComponent->Activate();
		}

		if (Controller->BrainComponent)
		{
			Controller->BrainComponent->RestartLogic();
		}
	}

	// Need to register with perception system again
	UAIPerceptionStimuliSourceComponent* Source = GetPerceptionSource();
	if (Source)
	{
		Source->RegisterWithPerceptionSystem();
	}
}
