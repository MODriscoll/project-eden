// Copyright of Sock Goblins

#include "EGCharacterGameplayAbility.h"
#include "AbilitySystemComponent.h"

#include "Animation/AnimMontage.h"
#include "Animation/AnimInstance.h"

UEGCharacterGameplayAbility::UEGCharacterGameplayAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
}

FGameplayEffectContextHandle UEGCharacterGameplayAbility::MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo) const
{
	check(ActorInfo);
	FGameplayEffectContextHandle Context= Super::MakeEffectContext(Handle, ActorInfo);

	AActor* AdditionalSource = GetAdditionalEffectContextSource();
	if (AdditionalSource)
	{
		Context.AddInstigator(ActorInfo->OwnerActor.Get(), AdditionalSource);
	}

	return Context;
}

bool UEGCharacterGameplayAbility::CheckCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo * ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	bool bResult = Super::CheckCooldown(Handle, ActorInfo, OptionalRelevantTags);
	if (!bResult)
	{
		if (OptionalRelevantTags && CheckCooldownFailTag.IsValid())
		{
			OptionalRelevantTags->AddTag(CheckCooldownFailTag);
		}
	}

	return bResult;
}

bool UEGCharacterGameplayAbility::CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	bool bResult = Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags);
	if (!bResult)
	{
		if (OptionalRelevantTags && CheckCostFailTag.IsValid())
		{
			OptionalRelevantTags->AddTag(CheckCostFailTag);
		}
	}

	return bResult;
}

void UEGCharacterGameplayAbility::PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate)
{
	Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate);

	UAbilitySystemComponent* ASC = ActorInfo ? ActorInfo->AbilitySystemComponent.Get() : nullptr;
	if (IsInstantiated() && ASC)
	{
		TagEventHandle = ASC->RegisterGenericGameplayTagEvent().AddUObject(this, &UEGCharacterGameplayAbility::OnGenericTagEvent);
	}
}

void UEGCharacterGameplayAbility::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	UAbilitySystemComponent* ASC = ActorInfo ? ActorInfo->AbilitySystemComponent.Get() : nullptr;
	if (IsInstantiated() && ASC)
	{
		ASC->RegisterGenericGameplayTagEvent().Remove(TagEventHandle);
	}
}

void UEGCharacterGameplayAbility::SetMontagePlayRate(float PlayRate)
{
	check(CurrentActorInfo);

	if (FMath::IsNearlyZero(PlayRate))
	{
		ABILITY_LOG(Warning, TEXT("Ability %s is setting montage play rate to zero! Aborting request"), *GetName());
		return;
	}

	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (ASC && CurrentMontage)
	{
		// Should only set play rate if this is our montage
		if (ASC->IsAnimatingAbility(this))
		{
			UAnimInstance* AnimInstance = CurrentActorInfo->GetAnimInstance();
			if (AnimInstance)
			{
				FAnimMontageInstance* MontageInstance = AnimInstance->GetActiveInstanceForMontage(CurrentMontage);
				if (MontageInstance)
				{
					MontageInstance->SetPlayRate(PlayRate);
				}
			}
		}
	}
}

FName UEGCharacterGameplayAbility::GetMontageSectionName(int32 SectionIndex)
{
	check(CurrentActorInfo);

	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	if (ASC && CurrentMontage)
	{
		// Only get the name if montage is from our ability
		if (ASC->IsAnimatingAbility(this))
		{
			return CurrentMontage->GetSectionName(SectionIndex);
		}
	}

	return NAME_None;
}

void UEGCharacterGameplayAbility::OnGenericTagEvent(const FGameplayTag Tag, int32 Count)
{
	if (ListenForTags.HasTag(Tag))
	{
		OnTagEvent(Tag, Count > 0);
	}
}
