// Copyright of Sock Goblins

#include "EGCharacterMovementComponent.h"
#include "EGCharacter.h"

bool UEGCharacterMovementComponent::CanStartPathFollowing() const
{
	bool bCanStart = Super::CanStartPathFollowing();
	if (EGCharacterOwner)
	{
		bCanStart = bCanStart && !EGCharacterOwner->IsStunned();
	}

	return bCanStart;
}

void UEGCharacterMovementComponent::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	Super::SetUpdatedComponent(NewUpdatedComponent);

	EGCharacterOwner = Cast<AEGCharacter>(CharacterOwner);
}

float UEGCharacterMovementComponent::GetMaxSpeed() const
{
	if (!EGCharacterOwner)
	{
		return Super::GetMaxSpeed();
	}

	float Speed = 0.f;

	// Modes to ignore: Falling, Flying
	switch (MovementMode)
	{
		case MOVE_Walking:
		case MOVE_NavWalking:
		{
			Speed = IsCrouching() ? MaxWalkSpeedCrouched : MaxWalkSpeed;
			break;
		}
		case MOVE_Falling:
		{
			return MaxWalkSpeed;
		}
		case MOVE_Swimming:
		{
			Speed = MaxWalkSpeed;
			break;
		}
		case MOVE_Flying:
		{
			return MaxFlySpeed;
		}
		case MOVE_Custom:
		{
			Speed = MaxCustomMovementSpeed;
			break;
		}
		case MOVE_None:
		default:
		{
			return 0.f;
		}
	}

	// Apply speed modification
	ApplyModifierToSpeed_LogIfNearlyZero(Speed, EGCharacterOwner->GetSpeedModifier(), TEXT("General Speed Modifier"));

	return Speed;
}

void UEGCharacterMovementComponent::PostLoad()
{
	Super::PostLoad();

	EGCharacterOwner = Cast<AEGCharacter>(CharacterOwner);
}

void UEGCharacterMovementComponent::ApplyModifierToSpeed_LogIfNearlyZero(float& Speed, float Modifier, const FString& ModifierString) const
{
	if (FMath::IsNearlyZero(Modifier, 0.01f))
	{
		UE_LOG(LogEGCharacter, Warning, TEXT("Speed Modifer to apply is nearly zero! Modifier = %s, Value = %f"), *ModifierString, Modifier);
	}

	Speed *= Modifier;
}


