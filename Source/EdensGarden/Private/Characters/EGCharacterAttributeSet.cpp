// Copyright of Sock Goblins

#include "EGCharacterAttributeSet.h"
#include "EGCharacter.h"

#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

UEGCharacterAttributeSet::UEGCharacterAttributeSet()
{
	SpeedModifier = FGameplayAttributeData(1.f);
}

void UEGCharacterAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	// Not calling super, as we will handle all previous attributes
	//Super::PostGameplayEffectExecute(Data);

	// Context and tags of effect
	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	const FGameplayTagContainer& Tags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Source of this effect
	UAbilitySystemComponent* SourceASC = Context.GetOriginalInstigatorAbilitySystemComponent();

	float Delta = Data.EvaluatedData.ModifierOp == EGameplayModOp::Additive ? Data.EvaluatedData.Magnitude : 0.f;

	// Get target of effect
	AActor* TargetActor = nullptr;
	AController* TargetController = nullptr;
	AEGCharacter* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetController = Data.Target.AbilityActorInfo->PlayerController.Get();
		TargetCharacter = Cast<AEGCharacter>(TargetActor);
	}

	if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		float DamageApplied = GetDamage();

		// Damage should only negate health
		if (TargetCharacter && TargetCharacter->WantsDamageNotification() && DamageApplied > 0)
		{
			// Apply the damage
			float CurHealth = GetHealth();
			CurHealth = FMath::Clamp(CurHealth - DamageApplied, 0.f, GetMaxHealth());
			SetHealth(CurHealth);

			// Get source of effect
			AActor* SourceActor = nullptr;
			APawn* SourcePawn = nullptr;
			AController* SourceController = nullptr;
			if (SourceASC && SourceASC->AbilityActorInfo.IsValid() && SourceASC->AbilityActorInfo->AvatarActor.IsValid())
			{
				SourceActor = SourceASC->AbilityActorInfo->AvatarActor.Get();
				SourceController = SourceASC->AbilityActorInfo->PlayerController.Get();

				if (SourceController)
				{
					SourcePawn = SourceController->GetPawn();
				}
				else if (SourceActor)
				{
					SourcePawn = Cast<APawn>(SourceActor);
					if (!SourcePawn)
					{
						SourcePawn = Cast<APawn>(SourceActor->GetOwner());
					}
				}
			}

			// If we have a causer for the effect, use it instead
			// (Could be a gun or a projectile)
			if (Context.GetEffectCauser())
			{
				SourceActor = Context.GetEffectCauser();
			}

			// Get the hit result of the damage being applied
			bool bValidHit = false;
			FHitResult HitResult;
			if (Context.GetHitResult())
			{
				bValidHit = true;
				HitResult = *Context.GetHitResult();
			}

			if (TargetCharacter)
			{
				// Notify the target has been damaged
				TargetCharacter->NotifyDamaged(DamageApplied, CurHealth, CurHealth <= 0.f, bValidHit, HitResult, SourcePawn, SourceActor, Tags);

				// Notify health has been changed (Delta is simply negated damage)
				TargetCharacter->NotifyHealthChanged(GetHealth(), -DamageApplied, Tags);
			}

			OnDamaged.Broadcast(DamageApplied, CurHealth <= 0.f, SourcePawn, SourceActor, Tags, bValidHit, HitResult);
		}

		// Consume the damage
		SetDamage(0.f);
	}
	else if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		float CurHealth = GetHealth();
		CurHealth = FMath::Clamp(CurHealth, 0.f, GetMaxHealth());
		SetHealth(CurHealth);

		if (TargetCharacter)
		{
			TargetCharacter->NotifyHealthChanged(CurHealth, Delta, Tags);
		}

		OnHealthChanged.Broadcast(CurHealth, Tags);
	}
	else if (Data.EvaluatedData.Attribute == GetSpeedModifierAttribute())
	{
		if (TargetCharacter)
		{
			TargetCharacter->NotifySpeedModifierChanged(GetSpeedModifier(), Delta, Tags);
		}
	}
}
