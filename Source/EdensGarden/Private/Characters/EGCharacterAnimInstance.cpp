// Copyright of Sock Goblins

#include "EGCharacterAnimInstance.h"
#include "EGCharacter.h"

#include "Kismet/KismetMathLibrary.h"

#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

UEGCharacterAnimInstance::UEGCharacterAnimInstance()
{
	MaxOffsetAngle = 90.f;
	ResetOffsetsAngle = 135.f;
	OffsetInterpSpeed = 5.f;
	OffsetResetInterpSpeed = 10.f;

	SpeedRatio = 0.f;
	Direction = 0.f;
	YawOffset = 0.f;
	PitchOffset = 0.f;

	bIsFalling = false;
	bIsCrouching = false;
	bIsDead = false;
	bIsRagdoll = false;
}

void UEGCharacterAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	const AEGCharacter* Character = Cast<AEGCharacter>(TryGetPawnOwner());
	if (Character)
	{
		UpdateEGCharacter(Character, DeltaTime);
	}
}

void UEGCharacterAnimInstance::UpdateEGCharacter(const AEGCharacter* Character, float DeltaTime)
{
	check(Character);

	// Update the state flags first, as some calculations might change based on our state
	UpdateStateFlags(Character, DeltaTime);

	// Common values used for animations
	SpeedRatio = CalculateSpeedRatio(Character, DeltaTime);
	Direction = CalculateDirectionAngle(Character, DeltaTime);

	CalculateYawPitchOffset(Character, DeltaTime, ResetOffsetsAngle);
}

float UEGCharacterAnimInstance::CalculateSpeedRatio(const AEGCharacter* Character, float DeltaTime)
{
	UPawnMovementComponent* MovementComp = Character->GetMovementComponent();

	if (MovementComp)
	{
		float VelocitySquared = MovementComp->Velocity.SizeSquared();
		float MaxSpeedSquared = FMath::Square(MovementComp->GetMaxSpeed());

		if (FMath::IsNearlyZero(MaxSpeedSquared))
		{
			return 0.f;
		}

		return VelocitySquared / MaxSpeedSquared;
	}
	else
	{
		return 0.f;
	}
}

float UEGCharacterAnimInstance::CalculateDirectionAngle(const AEGCharacter* Character, float DeltaTime)
{
	return CalculateDirection(Character->GetVelocity(), Character->GetActorRotation());
}

void UEGCharacterAnimInstance::CalculateYawPitchOffset(const AEGCharacter* Character, float DeltaTime, float ResetAt)
{
	FRotator CharRot = Character->GetActorRotation();
	FRotator ContRot = Character->GetControlRotation();

	FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(ContRot, CharRot);

	FRotator Current(PitchOffset, YawOffset, 0.f);

	// We want to clear the offset if controller is offset by too much
	if (bIsFalling || FMath::Abs(Delta.Yaw) >= ResetAt)
	{
		FRotator NewOffset = FMath::RInterpTo(Current, FRotator::ZeroRotator, DeltaTime, OffsetResetInterpSpeed);
		YawOffset = NewOffset.Yaw;
		PitchOffset = NewOffset.Pitch;
	}
	else
	{
		// Interp to target offset
		FRotator NewOffset = FMath::RInterpTo(Current, Delta, DeltaTime, OffsetInterpSpeed);
		YawOffset = ClampOffsetAngle(NewOffset.Yaw, MaxOffsetAngle);
		PitchOffset = ClampOffsetAngle(NewOffset.Pitch, MaxOffsetAngle);
	}
}

void UEGCharacterAnimInstance::UpdateStateFlags(const AEGCharacter* Character, float DeltaTime)
{
	UCharacterMovementComponent* MovementComp = Character->GetCharacterMovement();

	if (MovementComp)
	{
		bIsFalling = MovementComp->IsFalling();
		bIsCrouching = MovementComp->IsCrouching();

		bIsDead = Character->IsDead();
		bIsRagdoll = Character->IsRagdolling();
	}
}

float UEGCharacterAnimInstance::ClampOffsetAngle(float Angle, float ClampAngle) const
{
	return FMath::Clamp(Angle, -ClampAngle, ClampAngle);
}