// Copyright of Sock Goblins

#include "EGCharacterAIController.h"
#include "EGCharacter.h"

#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Hearing.h"

DEFINE_LOG_CATEGORY(LogEGCharacterAI);

const FName AEGCharacterAIController::PerceptionComponentName(TEXT("Perception"));

AEGCharacterAIController::AEGCharacterAIController(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(PerceptionComponentName);
	PerceptionComponent->bAutoRegister = false;	// We want to register after possesion, so we can set our Team ID

	PerceptionComponent->OnTargetPerceptionUpdated.AddDynamic(this, &AEGCharacterAIController::OnActorsPerceptionUpdated);

	// By default, we only use the sight perception
	{
		UAISenseConfig_Sight* SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("SightConfig"));
		SightConfig->SightRadius = 800.f;
		SightConfig->LoseSightRadius = 1500.f;
		SightConfig->PeripheralVisionAngleDegrees = 70.f;
		SightConfig->SetMaxAge(15.f);
		SightConfig->DetectionByAffiliation.bDetectFriendlies = false;
		SightConfig->DetectionByAffiliation.bDetectNeutrals = false;
		SightConfig->DetectionByAffiliation.bDetectEnemies = true;

		PerceptionComponent->ConfigureSense(*SightConfig);

		PerceptionComponent->SetDominantSense(SightConfig->GetSenseImplementation());
	}

	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTree"));
	BrainComponent = BehaviorTreeComponent;

	Blackboard = CreateDefaultSubobject<UBlackboardComponent>(TEXT("Blackboard"));

	bStopAILogicOnUnposses = true;
	bWantsPlayerState = false;
	bSetControlRotationFromPawnOrientation = true;
}

void AEGCharacterAIController::SetGenericTeamId(const FGenericTeamId& TeamID)
{
	if (PossessedCharacter.IsValid())
	{
		PossessedCharacter->SetGenericTeamId(TeamID);
	}
	else
	{
		IGenericTeamAgentInterface* PawnAgent = Cast<IGenericTeamAgentInterface>(GetPawn());
		if (PawnAgent)
		{
			PawnAgent->SetGenericTeamId(TeamID);
		}	
	}

	Super::SetGenericTeamId(TeamID);
}

FGenericTeamId AEGCharacterAIController::GetGenericTeamId() const
{
	if (PossessedCharacter.IsValid())
	{
		return PossessedCharacter->GetGenericTeamId();
	}
	else
	{
		IGenericTeamAgentInterface* PawnAgent = Cast<IGenericTeamAgentInterface>(GetPawn());
		if (PawnAgent)
		{
			return PawnAgent->GetGenericTeamId();
		}
		else
		{
			return Super::GetGenericTeamId();
		}
	}
}

void AEGCharacterAIController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	PossessedCharacter = Cast<AEGCharacter>(Pawn);
	if (PossessedCharacter.IsValid())
	{
		Super::SetGenericTeamId(PossessedCharacter->GetGenericTeamId());

		/** Bind callbacks to character so we stay up to date */
		PossessedCharacter->OnCharacterDamaged.AddDynamic(this, &AEGCharacterAIController::OnPossessedCharacterDamaged);
		PossessedCharacter->OnCharacterDeath.AddDynamic(this, &AEGCharacterAIController::OnPossessedCharacterDeath);
	}
	else
	{
		// Try to at least capture our team
		IGenericTeamAgentInterface* PawnAgent = Cast<IGenericTeamAgentInterface>(GetPawn());
		if (PawnAgent)
		{
			Super::SetGenericTeamId(PawnAgent->GetGenericTeamId());
		}
	}

	// Register perception now, so we use our characters team id
	PerceptionComponent->RegisterComponent();

	// Start running behaviors
	if (BehaviorTree)
	{
		RunBehaviorTree(BehaviorTree);
	}
	else
	{
		UE_LOG(LogEGCharacterAI, Warning, TEXT("Behavior tree for controller %s is null!"), *GetName());
	}
}

void AEGCharacterAIController::UnPossess()
{
	Super::UnPossess();

	if (PossessedCharacter.IsValid())
	{
		PossessedCharacter->OnCharacterDamaged.RemoveDynamic(this, &AEGCharacterAIController::OnPossessedCharacterDamaged);
		PossessedCharacter->OnCharacterDeath.RemoveDynamic(this, &AEGCharacterAIController::OnPossessedCharacterDeath);
	}

	PossessedCharacter.Reset();
}

void AEGCharacterAIController::Tick(float DeltaTime)
{
	// We want to skip the AI controller update
	AController::Tick(DeltaTime);

	// By default, we don't want character to rotate when stunned
	bool bUpdatePawn = true;
	if (PossessedCharacter.IsValid() && PossessedCharacter->IsStunned())
	{
		bUpdatePawn = false;
	}

	UpdateControlRotation(DeltaTime, bUpdatePawn);
}

void AEGCharacterAIController::OnActorsPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (Actor)
	{
		// Sight perception
		if (Stimulus.Type == UAISense::GetSenseID<UAISense_Sight>())
		{
			// Sight does not call expired (which sucks)

			// Distinguish between actor being seen for first time or just losing sight
			if (Stimulus.IsActive())
			{
				NotifyActorFirstSight(Actor, Stimulus);
			}
			else
			{
				NotifyActorLostSight(Actor, Stimulus);
			}

			return;
		}
		
		// Hearing perception
		if (Stimulus.Type == UAISense::GetSenseID<UAISense_Hearing>())
		{
			if (Stimulus.IsExpired())
			{
				NotifyActorHearingExpired(Actor, Stimulus);
			}
			else
			{
				// For some reason, the hearing range doesn't actually limit the range we can hear things
				/*UAISenseConfig_Hearing* HearingConfig = static_cast<UAISenseConfig_Hearing*>(PerceptionComponent->GetSenseConfig(UAISense::GetSenseID<UAISense_Hearing>()));
				if (Stimulus.Strength <= HearingConfig->HearingRange &&
					FVector::DistSquared(Stimulus.ReceiverLocation, Stimulus.StimulusLocation) <= FMath::Square(HearingConfig->HearingRange))
				{*/
					NotifyActorHeard(Actor, Stimulus);
				//}
			}

			return;
		}
	}
}

void AEGCharacterAIController::NotifyActorFirstSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just seen %s"), *GetName(), *Actor->GetName());
}

void AEGCharacterAIController::NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just lost sight of %s"), *GetName(), *Actor->GetName());
}

void AEGCharacterAIController::NotifyActorHeard_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just heard %s"), *GetName(), *Actor->GetName());
}

void AEGCharacterAIController::NotifyActorHearingExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just forgotten about hearing %s"), *GetName(), *Actor->GetName());
}

void AEGCharacterAIController::NotifyCharacterDamaged_Implementation(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	if (DamageInstigator)
	{
		//UE_LOG(LogEGCharacterAI, Log, TEXT("%s was damaged by %s"), *GetName(), *DamageInstigator->GetName());
	}
}

void AEGCharacterAIController::NotifyCharacterDeath_Implementation(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	// This should stop the running behavior tree
	BrainComponent->StopLogic(FString("Dead"));
	//PerceptionComponent->UnregisterComponent();
}

void AEGCharacterAIController::OnPossessedCharacterDamaged(AEGCharacter* OurCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	check(PossessedCharacter == OurCharacter);

	NotifyCharacterDamaged(Damage, DamageInstigator, DamageSource, GameplayTags);
}

void AEGCharacterAIController::OnPossessedCharacterDeath(AEGCharacter* OurCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	check(PossessedCharacter == OurCharacter);

	NotifyCharacterDeath(Damage, DamageInstigator, DamageSource, GameplayTags);
}
