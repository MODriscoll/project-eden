// Copyright of Sock Goblins

#include "SteeringForceMovementComponent.h"
#include "Characters/EGCharacter.h"

#include "Components/PrimitiveComponent.h"

#include "DrawDebugHelpers.h"
#include "Engine/World.h"

DECLARE_CYCLE_STAT(TEXT("SteeringForces - Wander"), STAT_CalculateWanderForce, STATGROUP_SteeringForces);
DECLARE_CYCLE_STAT(TEXT("SteeringForces - Avoid"), STAT_CalculateAvoidForce, STATGROUP_SteeringForces);
DECLARE_CYCLE_STAT(TEXT("SteeringForces - Seek"), STAT_CalculateSeekForce, STATGROUP_SteeringForces);

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)

static TAutoConsoleVariable<int32> CVarVisualizeSteeringForces(
	TEXT("eg.VisualizeSteeringForces"),
	0,
	TEXT("Visualizes all steering forces calculated by the steering force movement component.\n")
	TEXT("This can be expensive if lots of characters are using steering at the same time!\n")
	TEXT("0: Disabled, 1: Enabled"),
	ECVF_Cheat);

#endif

const uint8 USteeringForceMovementComponent::SteeringForceMode = 0;

USteeringForceMovementComponent::USteeringForceMovementComponent()
{
	SteeringFlags |= (1 << static_cast<int32>(ESteeringForces::Wander));
	SteeringFlags |= (1 << static_cast<int32>(ESteeringForces::Avoid));

	WanderAreaRadius = 200.f;
	WanderAreaDistance = 200.f;
	WanderAngle = 90.f;
	PrevWanderAngle = 0.f;

	MaxAvoidDistance = 300.f;
	MaxAvoidForce = 500.f;
	AvoidProfile = FCollisionProfileName(TEXT("BlockAll"));

	SeekTarget = nullptr;
	bProjectSeekForceOntoFloor = true;
	
	WanderSteerWeight = 1.f;
	AvoidSteerWeight = 1.f;
	SeekSteerWeight = 1.f;
}

void USteeringForceMovementComponent::PhysCustom(float DeltaTime, int32 Interations)
{
	FVector SteeringForce = FVector::ZeroVector;

	// Wander force
	if (DoFlagsHaveForce(ESteeringForces::Wander))
	{
		SteeringForce += CalculateWanderForce(DeltaTime);
	}

	// Avoid force
	if (DoFlagsHaveForce(ESteeringForces::Avoid))
	{
		SteeringForce += CalculateAvoidForce(DeltaTime);
	}

	// Seek force
	if (DoFlagsHaveForce(ESteeringForces::Seek))
	{
		SteeringForce += CalculateSeekForce(DeltaTime);
	}

	// Finally apply input forces
	AddInputVector(SteeringForce.GetSafeNormal2D());

	// Let default walking handle the rest
	PhysWalking(DeltaTime, Interations);
}

bool USteeringForceMovementComponent::IsMovingOnGround() const
{
	return Super::IsMovingOnGround() && (MovementMode == MOVE_Custom && CustomMovementMode == SteeringForceMode);
}

void USteeringForceMovementComponent::SetSteeringForceEnabled(ESteeringForces Force, bool bEnable)
{
	if (bEnable)
	{
		SteeringFlags |= (1 << static_cast<int32>(Force));
	}
	else
	{
		SteeringFlags &= ~(1 << static_cast<int32>(Force));
	}
}

void USteeringForceMovementComponent::SetSteeringForces(int32 Forces)
{
	SteeringFlags = Forces;
}

void USteeringForceMovementComponent::SetSeekTarget(AActor* Actor, bool bEnable)
{
	SeekTarget = Actor;
	SetSteeringForceEnabled(ESteeringForces::Seek, bEnable && SeekTarget != nullptr);
}

FVector USteeringForceMovementComponent::CalculateWanderForce(float DeltaTime)
{
	SCOPE_CYCLE_COUNTER(STAT_CalculateWanderForce);

	// Center of circle (relative to character)
	FVector CircleCenter = Velocity;
	CircleCenter = CircleCenter.GetSafeNormal() * WanderAreaDistance;

	// Displacement between radius
	FVector Displacement(0.f, -WanderAreaDistance, 0.f);
	Displacement.X = FMath::Cos(PrevWanderAngle) * WanderAreaRadius;
	Displacement.Y = FMath::Sin(PrevWanderAngle) * WanderAreaRadius;

	// Skew the wandering angle a bit to slowly change direction
	float Radians = FMath::DegreesToRadians(WanderAngle);
	PrevWanderAngle += FMath::RandRange(0.f, 1.f) * Radians - Radians * 0.5f;

	// Our final target to head towards
	FVector Final = CircleCenter + Displacement;

	#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	int32 VisualFlag = CVarVisualizeSteeringForces.GetValueOnGameThread();
	if (VisualFlag > 0)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FVector CharLocation = CharacterOwner->GetActorLocation();
			FVector CircLocation = CharLocation + CircleCenter;
			FVector FinlLocation = CircLocation + Displacement;

			// Calculate matrix for drawing wander area
			FMatrix CircleMatrix = FTransform(FRotator(90.f, 0.f, 0.f), CircLocation).ToMatrixNoScale();

			// Wander area
			DrawDebug2DDonut(World, CircleMatrix, WanderAreaRadius, WanderAreaRadius, 24, FColor::Green, false, -1.f, 0, 5.f);

			// Wander direction and distance
			DrawDebugDirectionalArrow(World, CharLocation, CircLocation, 200.f, FColor::Purple, false, -1.f, 0, 5.f);

			// New displaced direction
			DrawDebugDirectionalArrow(World, CircLocation, FinlLocation, 200.f, FColor::Red, false, -1.f, 0, 5.f);

			// Final calculated force
			DrawDebugDirectionalArrow(World, CharLocation, FinlLocation, 200.f, FColor::Blue, false, -1.f, 0, 5.f);
		}
	}
	#endif

	return Final * WanderSteerWeight;
}

FVector USteeringForceMovementComponent::CalculateAvoidForce(float DeltaTime)
{
	SCOPE_CYCLE_COUNTER(STAT_CalculateAvoidForce);

	FVector Force = FVector::ZeroVector;

	float MaxSpeed = GetMaxSpeed();
	float VelSquared = GetVelocitySquaredIncludingFloor();

	FVector TraceDirection = Velocity.GetSafeNormal2D();
	
	// Can't check out in front of us if dynamic length turns out as zero
	// We also need a safe direction vector to use
	if (FMath::IsNearlyZero(VelSquared, 1.f))
	{
		// We still want to check as something may be coming towards us
		VelSquared = FMath::Square(FMath::Min(UpdatedPrimitive->Bounds.SphereRadius, MaxSpeed));

		// Just use the forward vector to be safe
		TraceDirection = UpdatedPrimitive->GetForwardVector().GetSafeNormal2D();
	}

	// Calculate the length of 'sight'. We try to avoid things we can see
	float DynamicLength = VelSquared / FMath::Square(MaxSpeed);
	float SightLength = MaxAvoidDistance * DynamicLength;

	// TODO: cast from just above the feet and just above the head
	// cast just above feet to avoid hitting the ground and just above
	// the head so there is some gap for cielings
	// TODO: also cast to left and right by adjustable angle

	FVector Origin = CharacterOwner->GetActorLocation();
	FVector End = Origin + (TraceDirection.GetSafeNormal2D() * SightLength);
	
	FCollisionQueryParams QParams;
	QParams.bTraceComplex = true;
	QParams.AddIgnoredActor(CharacterOwner);

	UWorld* World = GetWorld();
	if (World)
	{
		FHitResult Result;
		bool bHit = World->LineTraceSingleByProfile(Result, Origin, End, AvoidProfile.Name, QParams);

		if (bHit)
		{
			UPrimitiveComponent* HitPrimitive = Result.GetComponent();

			// We want to move away from the hit location relative to the hit primitive
			FVector Avoidance = Result.Location - HitPrimitive->Bounds.Origin;
			Force += Avoidance.GetSafeNormal() * MaxAvoidForce;
		}

		#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		// TODO: Would want to draw each trace and avoidance force applied from it
		int32 VisualFlag = CVarVisualizeSteeringForces.GetValueOnGameThread();
		if (VisualFlag > 0)
		{
			if (bHit)
			{
				// Trace performed
				DrawDebugDirectionalArrow(World, Origin, Result.Location, 200.f, FColor::Cyan, false, -1.f, 0, 5.f);
				
				// Avoidance from hit obstacle
				DrawDebugDirectionalArrow(World, Result.Location, Result.Location + Force, 200.f, FColor::Magenta, false, -1.f, 0, 5.f);
			}
			else
			{
				// Trace performed
				DrawDebugDirectionalArrow(World, Origin, End, 200.f, FColor::Cyan, false, -1.f, 0, 5.f);
			}
		}
		#endif
	}

	return Force * AvoidSteerWeight;
}

float USteeringForceMovementComponent::GetVelocitySquaredIncludingFloor() const
{
	float VelSquared = Velocity.SizeSquared2D();

	if (IsFalling())
	{
		return VelSquared;
	}
	else
	{
		// The floor we are standing on might be travelling faster than we are
		// (it could be a moving platform which we are standing still on)
		if (IsMovingOnGround() && CurrentFloor.IsWalkableFloor())
		{
			UPrimitiveComponent* PrimitiveFloor = CurrentFloor.HitResult.GetComponent();
			float FloorVelSquared = PrimitiveFloor->ComponentVelocity.SizeSquared2D();

			return FMath::Max(VelSquared, FloorVelSquared);
		}
		else
		{
			return VelSquared;
		}
	}
}

FVector USteeringForceMovementComponent::CalculateSeekForce(float DeltaTime)
{
	if (!SeekTarget)
	{
		return FVector::ZeroVector;
	}

	SCOPE_CYCLE_COUNTER(STAT_CalculateSeekForce);

	FVector TargetLocation = SeekTarget->GetActorLocation();
	FVector CurrentLocation = CharacterOwner->GetActorLocation();

	FVector DesiredVelocity = (TargetLocation - CurrentLocation);
	FVector Force = DesiredVelocity - Velocity;

	// Move along the floor instead of directly towards target
	if (bProjectSeekForceOntoFloor && CurrentFloor.IsWalkableFloor())
	{
		Force = FVector::VectorPlaneProject(Force, CurrentFloor.HitResult.Normal);
	}

	#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	int32 VisualFlag = CVarVisualizeSteeringForces.GetValueOnGameThread();
	if (VisualFlag > 0)
	{
		UWorld* World = GetWorld();
		if (World)
		{
			FVector SteeringOrigin = CurrentLocation + Velocity;

			DrawDebugLine(World, CurrentLocation, SteeringOrigin, FColor::Turquoise, false, -1.f, 0, 5.f);
			DrawDebugLine(World, CurrentLocation, CurrentLocation + DesiredVelocity, FColor::Orange, false, -1.f, 0, 5.f);
			DrawDebugLine(World, SteeringOrigin, SteeringOrigin + Force, FColor::Silver, false, -1.f, 0, 5.f);
		}
	}
	#endif

	return Force * SeekSteerWeight;
}

