// Copyright of Sock Goblins

#include "EdensGardenGameInstance.h"
#include "EdensGardenLevelScriptActor.h"
#include "EdensGardenLoadingScreenModule.h"
#include "EdensGardenSettings.h"
#include "EdensGardenUserSettings.h"

#include "SpecOpAttributeSet.h"
#include "SpecOpCharacter.h"
#include "SpecOpInventory.h"
#include "SpecOpAbilitySystemComponent.h"

#include "Gun.h"
#include "MeleeWeapon.h"

#include "AbilitySystemGlobals.h"
#include "GenericTeamAgentInterface.h"

#include "EngineUtils.h"
#include "MemoryWriter.h"
#include "MemoryReader.h"
#include "TimerManager.h"
#include "Engine/Engine.h"
#include "Kismet/GameplayStatics.h"

DECLARE_CYCLE_STAT(TEXT("GameInstance - SaveSpecOp"), STAT_SaveSpecOp, STATGROUP_EdensGarden);
DECLARE_CYCLE_STAT(TEXT("GameInstance - LoadSpecOp"), STAT_LoadSpecOp, STATGROUP_EdensGarden);
DECLARE_CYCLE_STAT(TEXT("GameInstance - SaveGameWorld"), STAT_SaveGameWorld, STATGROUP_EdensGarden);
DECLARE_CYCLE_STAT(TEXT("GameInstance - LoadGameWorld"), STAT_LoadGameWorld, STATGROUP_EdensGarden);

#define STRING_TO_NAME(String) FName(*String)

UEdensGardenGameInstance::UEdensGardenGameInstance()
{
	ActiveSaveGame = nullptr;
	ActiveSaveSlot = FString();
	bIsSavingEnabled = true;
}

void UEdensGardenGameInstance::Init()
{
	Super::Init();

	// Attitude solver
	FGenericTeamId::SetAttitudeSolver(DetermineTeamAttitude);

	// Ability system
	UAbilitySystemGlobals& ASG = UAbilitySystemGlobals::Get();
	if (!ASG.IsAbilitySystemGlobalsInitialized())
	{
		ASG.InitGlobalData();
	}
}

bool UEdensGardenGameInstance::CreateSaveGame(const FString& Slot, ESaveGamePolicy Policy)
{
	ClearSaveGame();

	ActiveSaveGame = Cast<UEdensGardenSaveGame>(UGameplayStatics::CreateSaveGameObject(UEdensGardenSaveGame::StaticClass()));
	ActiveSaveSlot = Slot;

	// Init Data
	{
		ActiveSaveGame->Policy = Policy;
	}

	return true;
}

bool UEdensGardenGameInstance::LoadSaveGame(const FString& Slot, ESaveGamePolicy Policy)
{
	ClearSaveGame();

	if (UGameplayStatics::DoesSaveGameExist(Slot, 0))
	{
		ActiveSaveGame = Cast<UEdensGardenSaveGame>(UGameplayStatics::LoadGameFromSlot(Slot, 0));
		if (ActiveSaveGame)
		{
			ActiveSaveSlot = Slot;
			ActiveSaveGame->Policy = Policy;
			return true;
		}
		else
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("SaveGame exists at slot %s, but is not correct type"), *Slot);
		}
	}

	return false;
}

bool UEdensGardenGameInstance::LoadOrCreateSaveGame(const FString& Slot, ESaveGamePolicy Policy)
{
	if (!LoadSaveGame(Slot, Policy))
	{
		return CreateSaveGame(Slot, Policy);
	}

	return false;
}

bool UEdensGardenGameInstance::WriteSaveGame(bool bIncrementCount)
{
	if (ShouldWriteSaveGame())
	{
		// Record times saved
		int32 Addition = (int32)bIncrementCount;
		ActiveSaveGame->TimesSaved += Addition;

		if (UGameplayStatics::SaveGameToSlot(ActiveSaveGame, ActiveSaveSlot, 0))
		{
			UE_LOG(LogEdensGarden, Log, TEXT("Active Save Game successfully written to disk. Slot = %s"), *ActiveSaveSlot);
			return true;
		}
		else
		{
			// Revert potential times saved if save failed
			ActiveSaveGame -= Addition;
		}
	}

	return false;
}

void UEdensGardenGameInstance::ClearSaveGame()
{
	ActiveSaveGame = nullptr;
	ActiveSaveSlot.Empty();
}

void UEdensGardenGameInstance::DestroySaveGame(bool bClearMemory)
{
	if (IsActiveSaveGameValid())
	{
		if (UGameplayStatics::DeleteGameInSlot(ActiveSaveSlot, 0))
		{
			UE_LOG(LogEdensGarden, Log, TEXT("Active Save game successfully deleted from disk. Slot = %s"), *ActiveSaveSlot);
		}
	}

	if (bClearMemory)
	{
		ClearSaveGame();
	}
}

bool UEdensGardenGameInstance::CreateNewPersistantSave(const FName& DefaultLevelName, bool bUseEditorSaveIfPossible)
{
	// Need a valid name in order to create level
	if (DefaultLevelName == NAME_None)
	{
		return false;
	}

	FString SaveSlot = GetPersistantSaveSlotString(bUseEditorSaveIfPossible);
	if (UGameplayStatics::DeleteGameInSlot(SaveSlot, 0))
	{
		UE_LOG(LogEdensGarden, Log, TEXT("Existing save game %s has been deleted"), *SaveSlot);
	}

	if (CreateSaveGame(SaveSlot , ESaveGamePolicy::AllowSave))
	{
		ActiveSaveGame->LevelName = DefaultLevelName;

		// Initially write this save to disk (this will technically count
		// as an actual save as we merely want to acknowldge its creation
		WriteSaveGame(false);
		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::LoadPersistantSave(bool bUseEditorSaveIfPossible)
{
	FString SaveSlot = GetPersistantSaveSlotString(bUseEditorSaveIfPossible);
	if (LoadSaveGame(SaveSlot, ESaveGamePolicy::AllowSave))
	{
		UE_LOG(LogEdensGarden, Log, TEXT("Existing save game %s has been loaded"), *SaveSlot);
		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::DoesPersistantSaveExist(bool bUseEditorSaveIfPossible) const
{
	FString SaveSlot = GetPersistantSaveSlotString(bUseEditorSaveIfPossible);
	return UGameplayStatics::DoesSaveGameExist(SaveSlot, 0);
}

FString UEdensGardenGameInstance::GetPersistantSaveSlotString(bool bEditorSlot) const
{
	UEdensGardenSettings* Settings = UEdensGardenSettings::Get();
	if (Settings)
	{
		#if WITH_EDITOR
		// If using editor slot, make sure it also has a valid slot name
		const FString& SlotName = (bEditorSlot && !Settings->EditorSaveSlot.IsEmpty()) ? Settings->EditorSaveSlot : Settings->GameSaveSlot;
		#else
		const FString& SlotName = Settings->GameSaveSlot;
		#endif

		if (SlotName.IsEmpty())
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("Game Save Slot in settings is Invalid"));
		}
		else
		{
			return SlotName;
		}
	}

	return FString(TEXT("Default"));
}

bool UEdensGardenGameInstance::ShouldWriteSaveGame() const
{
	#if WITH_EDITOR
	// Editor might disallow saving
	UEdensGardenSettings* Settings = UEdensGardenSettings::Get();
	if (Settings && !Settings->bCanSaveInEditor)
	{
		return false;
	}
	#endif

	// User might not want to have progress saved
	UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	if (UserSettings && !UserSettings->IsSavingEnabled())
	{
		return false;
	}

	if (bIsSavingEnabled)
	{
		if (ActiveSaveGame && ActiveSaveGame->Policy == ESaveGamePolicy::AllowSave)
		{
			return true;
		}
	}

	return false;
}

bool UEdensGardenGameInstance::GenerateSpecOpSaveData(ASpecOpCharacter* SpecOp)
{
	if (!SpecOp)
	{
		return false;
	}

	if (IsActiveSaveGameValid())
	{
		SCOPE_CYCLE_COUNTER(STAT_SaveSpecOp);

		UWorld* World = SpecOp->GetWorld();
		if (!World)
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("Failed to generate spec op save data. World was null"));
			return false;
		}

		WriteSpecOpAttributesToSaveData(SpecOp);
		
		if (!GenerateAndWriteInventoryToSaveData(World))
		{
			UE_LOG(LogEdensGarden, Warning, TEXT("Failed to generate default inventory for spec op"));
		}

		// TODO: powers

		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::SaveSpecOpSaveData(ASpecOpCharacter* SpecOp)
{
	if (!SpecOp)
	{
		return false;
	}

	if (IsActiveSaveGameValid())
	{
		SCOPE_CYCLE_COUNTER(STAT_SaveSpecOp);

		WriteSpecOpAttributesToSaveData(SpecOp);
		
		FSpecOpInventory Inventory;
		Inventory.Guns = SpecOp->GetAllGuns();
		Inventory.GunInventorySize = SpecOp->GetMaxNumOfGuns();
		Inventory.EquippedGunIndex = SpecOp->GetEquippedGunIndex();
		Inventory.MeleeWeapon = SpecOp->GetMeleeWeapon();

		WriteInventoryToSaveData(Inventory);
		FSpecOpSaveData& PlayerData = ActiveSaveGame->PlayerData;

		// TODO: Powers

		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::LoadSpecOpSaveData(ASpecOpCharacter* SpecOp)
{
	if (!SpecOp)
	{
		return false;
	}

	if (IsActiveSaveGameValid())
	{
		SCOPE_CYCLE_COUNTER(STAT_LoadSpecOp);

		CloneSpecOpAttributes(SpecOp);
		CloneSpecOpInventory(SpecOp);

		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::SaveGameWorldSaveData(UWorld* World, bool bResetDestroyed, const TArray<FName>* DestroyedActors)
{
	if (!World)
	{
		return false;
	}

	if (IsActiveSaveGameValid())
	{
		SCOPE_CYCLE_COUNTER(STAT_SaveGameWorld);

		FDestroyedActorsSaveData& DestroyData = ActiveSaveGame->DestroyedActorsData;

		// Reset all recorded destroyed actors if requested (transition to new level)
		if (bResetDestroyed)
		{
			DestroyData.Names.Empty();
		}

		// Record (add) any actors that need to be destroyed next level load
		if (DestroyedActors)
		{
			const TArray<FName>& DestroyedActorsRef = *DestroyedActors;		
			for (const FName& Name : DestroyedActorsRef)
			{
				DestroyData.Names.Add(Name);
			}
		}

		TArray<FActorSaveData>& SavedActors = ActiveSaveGame->GameWorldData;
		SavedActors.Empty();

		// Save all save game actors
		for (TActorIterator<AActor> It(World); It; ++It)
		{
			AActor* Actor = *It;
			ISaveGameInterface* SaveGame = Cast<ISaveGameInterface>(Actor);
			if (SaveGame)
			{
				// The level itself might also need to serialize values
				if (Actor->IsA(ALevelScriptActor::StaticClass()) && SaveGame->ShouldSaveForSaveGame())
				{
					SaveGame->PreSaveGameSerialization();

					FMemoryWriter MemoryWriter(ActiveSaveGame->LevelScriptActorData, true);
					FEdensGardenArchive Ar(MemoryWriter);
					Actor->Serialize(Ar);

					continue;
				}

				FName ActorName = STRING_TO_NAME(Actor->GetName());

				// This actor might be getting destroyed, there is no point in saving it
				if (SaveGame->ShouldSaveForSaveGame() && !DestroyData.Names.Contains(ActorName))
				{
					// Notify save game actor (it might need to perform certain actions)
					SaveGame->PreSaveGameSerialization();

					FActorSaveData ActorData;
					ActorData.Name = ActorName;
					ActorData.Class = Actor->GetClass();

					// Save out transform even though it might not be required, as this
					// actor may have the spawn policy, which would require this transform 
					ActorData.Transform = Actor->GetTransform();

					// This is required for handling dynamic actors that can be saved
					ActorData.Policy = SaveGame->GetSaveGameSpawnPolicy();
					
					FMemoryWriter MemoryWriter(ActorData.Data, true);
					FEdensGardenArchive Ar(MemoryWriter);
					Actor->Serialize(Ar);

					SavedActors.Add(MoveTemp(ActorData));
				}
			}
		}

		return true;
	}

	return false;
}

bool UEdensGardenGameInstance::LoadGameWorldSaveData(UWorld* World)
{
	if (!World)
	{
		return false;
	}

	if (IsActiveSaveGameValid())
	{
		SCOPE_CYCLE_COUNTER(STAT_LoadGameWorld)

		TArray<FActorSaveData>& SavedActors = ActiveSaveGame->GameWorldData;

		// This will allow faster searching times the more save game actors have been loaded
		TMap<FName, int32> SavedActorsIndexMap;
		for (int32 Index = 0; Index < SavedActors.Num(); ++Index)
		{
			const FName& ActorName = SavedActors[Index].Name;
			SavedActorsIndexMap.Add(ActorName, Index);
		}

		FDestroyedActorsSaveData& DestroyData = ActiveSaveGame->DestroyedActorsData;
		
		// Record the actors to destroy
		TArray<AActor*> ActorsToDestroy;
		for (TActorIterator<AActor> It(World); It; ++It)
		{
			AActor* Actor = *It;

			// We only need the serialization data from the level
			if (Actor->IsA(ALevelScriptActor::StaticClass()))
			{
				ISaveGameInterface* SaveGame = Cast<ISaveGameInterface>(Actor);
				if (SaveGame)
				{
					FMemoryReader MemoryReader(ActiveSaveGame->LevelScriptActorData, true);
					FEdensGardenArchive Ar(MemoryReader);
					Actor->Serialize(Ar);

					SaveGame->PostLoadGameSerialization();
				}

				continue;
			}

			FName ActorName = STRING_TO_NAME(Actor->GetName());

			// This actor might need to be destroyed
			if (DestroyData.Names.Contains(ActorName))
			{
				// We could check if this actor is also in the index map here,
				// but saving the game should have already prevent said actor from
				// being saved in the game world data already

				ActorsToDestroy.Add(Actor);
				continue;
			}

			ISaveGameInterface* SaveGame = Cast<ISaveGameInterface>(Actor);
			if (SaveGame) 
			{
				// TODO: remove ShouldSave? is only here to prevent weapons in players inventory from logging in the else statement
				if (SaveGame->ShouldSaveForSaveGame())
				{
					const int32* Index = SavedActorsIndexMap.Find(ActorName);
					if (Index != nullptr)
					{
						const FActorSaveData& ActorData = SavedActors[*Index];

						// Verification
						if (!ensure(ActorData.Class == Actor->GetClass()))
						{
							UE_LOG(LogEdensGarden, Warning, TEXT("Save game actor %s is of different type that what was previously saved."
								"Saved class = %s, Actors class = %s"), *Actor->GetName(), *ActorData.Class->GetName(), *Actor->GetClass()->GetName());
							UE_LOG(LogEdensGarden, Warning, TEXT("Aborting loading of actor %s due to class mis-match"), *Actor->GetName());
							continue;
						}

						if (SaveGame->WantsSaveGameTransformSet())
						{
							// Only update transform of moveable actors (we don't want to potentially move the environment)
							USceneComponent* ActorRoot = Actor->GetRootComponent();
							if (ActorRoot && ActorRoot->Mobility == EComponentMobility::Movable)
							{
								// Need to teleport physics as some actors may simulate physics
								ActorRoot->SetWorldTransform(ActorData.Transform, false, nullptr, ETeleportType::TeleportPhysics);
							}
						}

						FMemoryReader MemoryReader(ActorData.Data, true);
						FEdensGardenArchive Ar(MemoryReader);
						Actor->Serialize(Ar);

						// Notify save game actor (it might need to perform certain actions)
						SaveGame->PostLoadGameSerialization();
					}
					else
					{
						UE_LOG(LogEdensGarden, Warning, TEXT("Unaccounted save game actor %s found in world %s. "
							"This actor does not exist in current save game. Slot = %s"), *Actor->GetName(), *World->GetName(), *ActiveSaveSlot);
					}
				}

				SavedActorsIndexMap.Remove(ActorName);
			}
		}

		// Destroy remaining actors
		for (AActor* Actor : ActorsToDestroy)
		{
			Actor->Destroy();
		}

		// There could potentially be un-accounted for actors that don't exist
		if (SavedActorsIndexMap.Num() > 0)
		{
			TArray<int32> Indices;
			SavedActorsIndexMap.GenerateValueArray(Indices);

			for (int32 Index = Indices.Num(); Index--;)
			{
				int32 ActorIndex = Indices[Index];
				const FActorSaveData& ActorData = SavedActors[ActorIndex];

				// This actor might need to be spawned in 
				if (ActorData.Policy == ESaveGameActorSpawnPolicy::Spawn)
				{
					FActorSpawnParameters Params;
					Params.Name = ActorData.Name;
					Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

					AActor* Actor = World->SpawnActor<AActor>(ActorData.Class, ActorData.Transform, Params);
					if (Actor)
					{
						FMemoryReader MemoryReader(ActorData.Data, true);
						FEdensGardenArchive Ar(MemoryReader);
						Actor->Serialize(Ar);

						// All saved actor should be implementing this interface (but we could
						// potentially be dealing with an actor that was save game but no longer is)
						ISaveGameInterface* SaveGame = Cast<ISaveGameInterface>(Actor);
						if (ensure(SaveGame))
						{
							SaveGame->PostLoadGameSerialization();
						}
						else
						{
							UE_LOG(LogEdensGarden, Warning, TEXT("Saved actor %s is not a save game type but was still "
								"spawned into world. Is this intended?"), *ActorData.Name.ToString());
						}
					}
					else
					{
						UE_LOG(LogEdensGarden, Warning, TEXT("Failed spawning save game actor %s"), *ActorData.Name.ToString());
					}
				}

				//UE_LOG(LogEdensGarden, Warning, TEXT("Unaccounted saved actor %s was not found in world %s. "
				//	"This actor exists in save game but not in world. Slot = %s"), *ActorData.Name.ToString(), *World->GetName(), *ActiveSaveSlot);
			}
		}

		return true;
	}

	return false;
}

void UEdensGardenGameInstance::WriteSpecOpAttributesToSaveData(ASpecOpCharacter* SpecOp) const
{
	check(ActiveSaveGame);
	check(SpecOp);

	FSpecOpSaveData& PlayerData = ActiveSaveGame->PlayerData;
	
	// Write attributes
	{
		PlayerData.Health = SpecOp->GetHealth();
		PlayerData.Energy = SpecOp->GetEnergy();
		PlayerData.Breath = SpecOp->GetBreath();
	}

	// Write any additional data
	{
		FMemoryWriter MemoryWriter(PlayerData.Data, true);
		FEdensGardenArchive Ar(MemoryWriter);
		SpecOp->Serialize(Ar);
	}
}

void UEdensGardenGameInstance::CloneSpecOpAttributes(ASpecOpCharacter* SpecOp) const
{
	check(ActiveSaveGame);
	check(SpecOp);

	FSpecOpSaveData& PlayerData = ActiveSaveGame->PlayerData;

	// Copy attributes
	{
		// Design change, allow spec op to respawn with max value attributes
		/*UAbilitySystemComponent* ASC = SpecOp->GetAbilitySystemComponent();
		if (ASC)
		{
			ASC->ApplyModToAttribute(USpecOpAttributeSet::GetHealthAttribute(), EGameplayModOp::Override, PlayerData.Health);
			ASC->ApplyModToAttribute(USpecOpAttributeSet::GetEnergyAttribute(), EGameplayModOp::Override, PlayerData.Energy);
			ASC->ApplyModToAttribute(USpecOpAttributeSet::GetBreathAttribute(), EGameplayModOp::Override, PlayerData.Breath);
		}*/
	}

	// Copy additional data
	{
		FMemoryReader MemoryReader(PlayerData.Data, true);
		FEdensGardenArchive Ar(MemoryReader);
		SpecOp->Serialize(Ar);
	}
}

void UEdensGardenGameInstance::CloneSpecOpInventory(ASpecOpCharacter* SpecOp) const
{
	check(ActiveSaveGame);
	check(SpecOp);

	UWorld* World = SpecOp->GetWorld();
	if (!World)
	{
		UE_LOG(LogEdensGarden, Error, TEXT("Failed to load spec ops inventory. World was null"));
		return;
	}

	FSpecOpSaveData& PlayerData = ActiveSaveGame->PlayerData;

	// Find all weapons that might exist in the world that player
	// already has in inventory. We need to destroy these in order
	// to replace them with new weapons that have the names instead
	{
		TSet<FName> WeaponNames;
		for (const FWeaponSaveData& WeaponData : PlayerData.Guns)
		{
			WeaponNames.Add(WeaponData.Name);
		}
		
		// Melee could potentially be invalid
		if (!PlayerData.MeleeWeapon.Name.IsNone())
		{
			WeaponNames.Add(PlayerData.MeleeWeapon.Name);
		}

		// Find all weapons with matching names to destroy
		TArray<AWeaponBase*> WeaponsToDestroy;
		for (TActorIterator<AWeaponBase> It(World); It; ++It)
		{
			FName ActorName = STRING_TO_NAME(It->GetName());
			if (WeaponNames.Contains(ActorName))
			{
				WeaponsToDestroy.Add(*It);
				WeaponNames.Remove(ActorName);

				if (WeaponNames.Num() <= 0)
				{
					break;
				}
			}
		}

		for (AWeaponBase* Weapon : WeaponsToDestroy)
		{
			Weapon->Destroy();
		}
	}

	SpecOp->SetMaxGunInventorySize(PlayerData.GunInventorySize);

	// Spawn guns
	for (int32 Index = 0; Index < PlayerData.Guns.Num(); ++Index)
	{
		const FWeaponSaveData& WeaponData = PlayerData.Guns[Index];
		if (ensure(WeaponData.Class))
		{
			FActorSpawnParameters Params;
			Params.Name = WeaponData.Name;
			Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			AGunBase* Gun = World->SpawnActor<AGunBase>(WeaponData.Class, Params);
			if (Gun)
			{
				// Initialize
				FMemoryReader MemoryReader(WeaponData.Data, true);
				FEdensGardenArchive Ar(MemoryReader);
				Gun->Serialize(Ar);

				SpecOp->GiveGunDontEquip(Gun);
			}
		}
	}

	// Spawn melee
	const FWeaponSaveData& WeaponData = PlayerData.MeleeWeapon;
	if (WeaponData.Class)
	{
		FActorSpawnParameters Params;
		Params.Name = WeaponData.Name;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		AMeleeBase* Melee = World->SpawnActor<AMeleeBase>(WeaponData.Class, Params);
		if (Melee)
		{
			// Initialize
			FMemoryReader MemoryReader(WeaponData.Data, true);
			FEdensGardenArchive Ar(MemoryReader);
			Melee->Serialize(Ar);

			SpecOp->SetMeleeWeapon(Melee);
		}
	}

	// TODO: Check if play has begun?
	SpecOp->EquipGunImmediately(PlayerData.EquippedGunIndex);
}

bool UEdensGardenGameInstance::GenerateAndWriteInventoryToSaveData(UWorld* World) const
{
	check(ActiveSaveGame);
	check(World);

	// Get default inventory specified 
	UEdensGardenAssetManager& AssetManager = UEdensGardenAssetManager::Get();
	const USpecOpInventory* DefaultInventory = AssetManager.LoadSpecOpInventory(DefaultSpecOpInventory);

	if (!DefaultInventory)
	{
		return false;
	}

	FSpecOpInventory Inventory;

	// Spawn each weapon then write its defaults to memory. We need to spawn
	// instanced actors as the defaults need to be initialized outside of the CDO
	Inventory.Guns.Reserve(DefaultInventory->Guns.Num());

	// Spawn guns
	for (TSubclassOf<AGunBase> GunClass : DefaultInventory->Guns)
	{
		FActorSpawnParameters Params;
		Params.ObjectFlags = RF_Transient;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		AGunBase* InstancedGun = World->SpawnActor<AGunBase>(GunClass, Params);
		if (InstancedGun)
		{
			Inventory.Guns.Add(InstancedGun);
		}
	}

	Inventory.GunInventorySize = DefaultInventory->GunInventorySize;
	Inventory.EquippedGunIndex = 0;

	// Spawn melee
	if (DefaultInventory->MeleeWeapon)
	{
		FActorSpawnParameters Params;
		Params.ObjectFlags = RF_Transient;
		Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		Inventory.MeleeWeapon = World->SpawnActor<AMeleeBase>(DefaultInventory->MeleeWeapon, Params);
	}

	WriteInventoryToSaveData(Inventory);

	// No longer need these actors
	{
		Inventory.MeleeWeapon->Destroy();
		for (AGunBase* Actor : Inventory.Guns)
		{
			Actor->Destroy();
		}
	}

	return true;
}

void UEdensGardenGameInstance::WriteInventoryToSaveData(const FSpecOpInventory& Inventory) const
{
	check(ActiveSaveGame);

	FSpecOpSaveData& PlayerData = ActiveSaveGame->PlayerData;
	PlayerData.Guns.Empty();

	// Write guns
	for (int32 Index = 0; Index < Inventory.Guns.Num(); ++Index)
	{
		AGunBase* Gun = Inventory.Guns[Index];
		if (ensure(Gun))
		{
			FWeaponSaveData WeaponData;
			WeaponData.Name = STRING_TO_NAME(Gun->GetName());
			WeaponData.Class = Gun->GetClass();

			FMemoryWriter MemoryWriter(WeaponData.Data, true);
			FEdensGardenArchive Ar(MemoryWriter);
			Gun->Serialize(Ar);

			PlayerData.Guns.Add(MoveTemp(WeaponData));
		}
	}

	// Write gun inventory
	PlayerData.GunInventorySize = Inventory.GunInventorySize;
	PlayerData.EquippedGunIndex = Inventory.EquippedGunIndex;

	// Write melee
	AMeleeBase* MeleeWeapon = Inventory.MeleeWeapon;
	if (MeleeWeapon)
	{
		FWeaponSaveData WeaponData;
		WeaponData.Name = STRING_TO_NAME(MeleeWeapon->GetName());
		WeaponData.Class = MeleeWeapon->GetClass();

		FMemoryWriter MemoryWriter(WeaponData.Data, true);
		FEdensGardenArchive Ar(MemoryWriter);
		MeleeWeapon->Serialize(Ar);

		PlayerData.MeleeWeapon = MoveTemp(WeaponData);
	}
	else
	{
		// Reset
		PlayerData.MeleeWeapon = FWeaponSaveData();
	}
}

ETeamAttitude::Type UEdensGardenGameInstance::DetermineTeamAttitude(FGenericTeamId Left, FGenericTeamId Right)
{
	EEdensGardenTeamID LeftId = static_cast<EEdensGardenTeamID>(Left.GetId());
	EEdensGardenTeamID RightId = static_cast<EEdensGardenTeamID>(Right.GetId());

	// Both humans and holograms are on the same team, anything else is considered hostile
	if (LeftId == EEdensGardenTeamID::Human || LeftId == EEdensGardenTeamID::Hologram)
	{
		if (RightId == EEdensGardenTeamID::Human || RightId == EEdensGardenTeamID::Hologram)
		{
			return ETeamAttitude::Friendly;
		}
		else
		{
			return ETeamAttitude::Hostile;
		}
	}

	// Monsters are on the same team, anything that isn't a monster is hostile
	if (LeftId == EEdensGardenTeamID::Monster)
	{
		if (RightId == EEdensGardenTeamID::Monster)
		{
			return ETeamAttitude::Friendly;
		}
		else
		{
			return ETeamAttitude::Hostile;
		}
	}

	// Any interaction with No team is considered hostile
	return ETeamAttitude::Hostile;
}

#undef STRING_TO_NAME
