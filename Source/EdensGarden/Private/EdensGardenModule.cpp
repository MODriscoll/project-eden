// Copyright of Sock Goblins

#include "EdensGardenModule.h"
#include "ModuleManager.h"
#include "Package.h"

#include "EdensGardenGlobals.h"

#if EG_GAMEPLAY_DEBUGGER
#include "GameplayDebugger.h"
#include "Debug/GameplayDebuggerCategory_EG.h"
#endif

void FEdensGardenModule::StartupModule()
{
	// We really want to prepare on when the game starts
	GlobalData = nullptr;

	RegisterGameplayDebugger();
}

void FEdensGardenModule::ShutdownModule()
{
	UnregisterGameplayDebugger();

	// Appears to be destroyed on shutdown
	GlobalData = nullptr;
}

bool FEdensGardenModule::IsGameModule() const
{
	return true;
}

void FEdensGardenModule::RegisterGameplayDebugger()
{
	#if EG_GAMEPLAY_DEBUGGER
	auto Delegate = IGameplayDebugger::FOnGetCategory::CreateStatic(&FGameplayDebuggerCategory_EG::MakeInstance);

	IGameplayDebugger& GameplayDebugger = IGameplayDebugger::Get();
	GameplayDebugger.RegisterCategory("EdensGarden", Delegate, EGameplayDebuggerCategoryState::EnabledInGame);
	GameplayDebugger.NotifyCategoriesChanged();
	#endif
}

void FEdensGardenModule::UnregisterGameplayDebugger()
{
	#if EG_GAMEPLAY_DEBUGGER
	if (IGameplayDebugger::IsAvailable())
	{
		IGameplayDebugger& GameplayDebugger = IGameplayDebugger::Get();
		GameplayDebugger.UnregisterCategory("EdensGarden");
		GameplayDebugger.NotifyCategoriesChanged();
	}
	#endif
}

UEdensGardenGlobals* FEdensGardenModule::GetGlobalData()
{
	if (!GlobalData)
	{
		// TODO: Allow BP version to be made that only allows defaults to be set?
		GlobalData = NewObject<UEdensGardenGlobals>(GetTransientPackage(), NAME_None);
		GlobalData->AddToRoot();

		// TODO: Move to game instance?
		GlobalData->Initialize();
	}

	check(GlobalData);
	return GlobalData;
}

IMPLEMENT_PRIMARY_GAME_MODULE(FEdensGardenModule, EdensGarden, "EdensGarden");