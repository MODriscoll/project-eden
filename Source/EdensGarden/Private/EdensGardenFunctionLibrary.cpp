// Copyright of Sock Goblins

#include "EdensGardenFunctionLibrary.h"
#include "EdensGardenGlobals.h"
#include "EdensGardenGameMode.h"
#include "EdensGardenGameState.h"
#include "EdensGardenGameInstance.h"
#include "EdensGardenLoadingScreenModule.h"
#include "EdensGardenSaveGame.h"
#include "MainMenuGameMode.h"

#include "SpecOpCharacter.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "GenericApplication.h"
#include "SlateApplication.h"
#include "UserWidget.h"
#include "Engine/Engine.h"
#include "GameFramework/PlayerController.h"
#include "Navigation/PathFollowingComponent.h"

bool UEdensGardenFunctionLibrary::IsWithEditor()
{
	#if WITH_EDITOR
	return true;
	#else
	return false;
	#endif
}

UEdensGardenGameInstance* UEdensGardenFunctionLibrary::GetEdensGardenGameInstance(const UObject* WorldContextObject)
{
	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	return World ? World->GetGameInstance<UEdensGardenGameInstance>() : nullptr;
}

AEdensGardenGameMode* UEdensGardenFunctionLibrary::GetEdensGardenGameMode(const UObject* WorldContextObject)
{
	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	return World ? World->GetAuthGameMode<AEdensGardenGameMode>() : nullptr;
}

AEdensGardenGameState* UEdensGardenFunctionLibrary::GetEdensGardenGameState(const UObject* WorldContextObject)
{
	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	return World ? World->GetGameState<AEdensGardenGameState>() : nullptr;
}

AMainMenuGameMode* UEdensGardenFunctionLibrary::GetMainMenuGameMode(const UObject* WorldContextObject)
{
	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	return World ? World->GetAuthGameMode<AMainMenuGameMode>() : nullptr;
}

void UEdensGardenFunctionLibrary::DisplayLoadingScreen(bool bAutoComplete, float DisplayTime)
{
	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.DisplayLoadingScreen(bAutoComplete, DisplayTime);
}

void UEdensGardenFunctionLibrary::HideLoadingScreen()
{
	IEdensGardenLoadingScreenModule& LoadingScreenModule = IEdensGardenLoadingScreenModule::Get();
	LoadingScreenModule.HideLoadingScreen();
}

void UEdensGardenFunctionLibrary::NotifyGameCheckpointReached(const UObject* WorldContextObject, int32 CheckpointIndex, FName CheckpointName)
{
	AEdensGardenGameState* GameState = GetEdensGardenGameState(WorldContextObject);
	if (GameState)
	{
		GameState->NotifyCheckpointReached(CheckpointIndex, CheckpointName);
	}
}

void UEdensGardenFunctionLibrary::NotifyGameLevelEndReached(const UObject* WorldContextObject, FName NextLevelName)
{
	AEdensGardenGameState* GameState = GetEdensGardenGameState(WorldContextObject);
	if (GameState)
	{
		GameState->NotifyLevelEndReached(NextLevelName);
	}
}

void UEdensGardenFunctionLibrary::NotifyGameCampaignEndReached(const UObject* WorldContextObject, FName MenuLevelName)
{
	AEdensGardenGameState* GameState = GetEdensGardenGameState(WorldContextObject);
	if (GameState)
	{
		GameState->NotifyGameEndReached(MenuLevelName);
	}
}

bool UEdensGardenFunctionLibrary::GetCurrentCheckpointDetails(const UObject* WorldContextObject, int32& Index, FName& Name)
{
	UEdensGardenGameInstance* GameInstance = GetEdensGardenGameInstance(WorldContextObject);
	if (GameInstance)
	{
		UEdensGardenSaveGame* SaveData = GameInstance->GetActiveSameGame();
		if (SaveData)
		{
			Index = SaveData->CheckpointIndex;
			Name = SaveData->CheckpointName;

			return true;
		}
	}

	return false;
}

void UEdensGardenFunctionLibrary::PlayNarrativePiece(const UObject* WorldContextObject, const FCharacterSpeechRequest& SpeechRequest)
{
	const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (World)
	{
		APlayerController* Controller = World->GetFirstPlayerController();
		ASpecOpCharacter* SpecOp = Controller ? Cast<ASpecOpCharacter>(Controller->GetPawn()) : nullptr;
		if (SpecOp)
		{
			SpecOp->Talk(SpeechRequest);
		}
	}
}

void UEdensGardenFunctionLibrary::SetMovingEnergyRegenThreshold(float Threshold)
{
	auto& Globals = UEdensGardenGlobals::Get();
	Globals.MovingEnergyRegenThreshold = Threshold;
}

void UEdensGardenFunctionLibrary::SetGlobalWeaponDamageMultiplier(float Multiplier)
{
	auto& Globals = UEdensGardenGlobals::Get();
	Globals.WeaponDamageMultiplier = Multiplier;
}

float UEdensGardenFunctionLibrary::GetMovingEnergyRegenThreshold()
{
	const auto& Globals = UEdensGardenGlobals::Get();
	return Globals.MovingEnergyRegenThreshold;
}

float UEdensGardenFunctionLibrary::GetGlobalWeaponDamageMultiplier()
{
	const auto& Globals = UEdensGardenGlobals::Get();
	return Globals.WeaponDamageMultiplier;
}

FTransform UEdensGardenFunctionLibrary::GetTransformFromLocationInfo(const FGameplayAbilityTargetingLocationInfo& LocationInfo)
{
	return LocationInfo.GetTargetingTransform();
}

TArray<FActiveGameplayEffectHandle> UEdensGardenFunctionLibrary::ApplyGameplayEffectSpecToTargets(const FGameplayAbilityTargetDataHandle& TargetData, const FGameplayEffectSpecHandle& SpecHandle, UAbilitySystemComponent* SourceASC)
{
	TArray<FActiveGameplayEffectHandle> EffectHandles;

	if (SpecHandle.IsValid())
	{
		for (TSharedPtr<FGameplayAbilityTargetData> Data : TargetData.Data)
		{
			EffectHandles.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data, SourceASC ? SourceASC->GetPredictionKeyForNewAction() : FPredictionKey()));
		}
	}

	return EffectHandles;
}

FGameplayTargetDataFilterHandle UEdensGardenFunctionLibrary::MakeTeamBasedFilterHandle(FTeamBasedTargetDataFilter Filter, AActor* FilterActor)
{
	FGameplayTargetDataFilterHandle FilterHandle;
	FTeamBasedTargetDataFilter* NewFilter = new FTeamBasedTargetDataFilter(Filter);
	NewFilter->InitializeFilterContext(FilterActor);
	FilterHandle.Filter = TSharedPtr<FGameplayTargetDataFilter>(NewFilter);

	return FilterHandle;
}

FGameplayAbilityTargetDataHandle UEdensGardenFunctionLibrary::AbilityTargetDataFromHitResults(const TArray<FHitResult>& HitResults)
{
	FGameplayAbilityTargetDataHandle TargetHandle;

	for (const FHitResult& Hit : HitResults)
	{
		// The target data handle will manage destroying this for us (via shared pointer)
		TargetHandle.Add(new FGameplayAbilityTargetData_SingleTargetHit(Hit));
	}

	return TargetHandle;
}

FGameplayAbilityTargetDataHandle UEdensGardenFunctionLibrary::AbilityTargetDataFromOriginAndHitResults(const FVector& Origin, const TArray<FHitResult>& HitResults)
{
	FGameplayAbilityTargetingLocationInfo LocationInfo;
	LocationInfo.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
	LocationInfo.LiteralTransform = FTransform(Origin);

	// The target data handle will manage destroying this for us (via shared pointer)
	FGameplayAbilityTargetData_ActorArray* ActorArray = new FGameplayAbilityTargetData_ActorArray();
	ActorArray->SourceLocation = LocationInfo;
	
	for (const FHitResult& Hit : HitResults)
	{
		if (Hit.Actor.IsValid())
		{
			ActorArray->TargetActorArray.Add(Hit.Actor);
		}
	}

	FGameplayAbilityTargetDataHandle TargetHandle;
	TargetHandle.Add(ActorArray);

	return TargetHandle;
}

bool UEdensGardenFunctionLibrary::SphereLineOfSightTrace(const UObject* WorldContextObject, const FVector& Origin, float Radius, ECollisionChannel LOSChannel, const TArray<AActor*>& IgnoreActors, TArray<FHitResult>& HitResults)
{
	UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull);
	if (!World)
	{
		return false;
	}

	// Query parameters
	FCollisionQueryParams QParams;
	QParams.AddIgnoredActors(IgnoreActors);
	
	// Object parameters
	FCollisionObjectQueryParams OParams(FCollisionObjectQueryParams::InitType::AllDynamicObjects);

	TArray<FOverlapResult> TraceOverlaps;
	World->OverlapMultiByObjectType(TraceOverlaps, Origin, FQuat::Identity, OParams, FCollisionShape::MakeSphere(Radius), QParams);

	HitResults.Reset();

	// Tracks all the actors we have hit
	TSet<AActor*> HitActors;
	for (const FOverlapResult& Overlap : TraceOverlaps)
	{
		AActor* Actor = Overlap.GetActor();

		// Ignore if this actor has already been hit
		if (Actor && !HitActors.Contains(Actor))
		{
			// Only allow if we have clear line of sight
			FHitResult Hit;
			if (HasClearLineOfSight(World, Origin, Overlap.GetComponent(), LOSChannel, QParams, Hit))
			{
				HitActors.Add(Actor);
				HitResults.Add(Hit);
			}
		}
	}

	return HitResults.Num() > 0;
}

AActor* UEdensGardenFunctionLibrary::GetPathFollowingComponentsDestinationActor(UPathFollowingComponent* PathFollowingComponent)
{
	if (PathFollowingComponent)
	{
		return PathFollowingComponent->GetMoveGoal();
	}

	return nullptr;
}

FVector UEdensGardenFunctionLibrary::ApplySpreadToDirection(FVector Direction, const FVector& Velocity, float MinSpread, float MaxSpread, float MinVelocity, float MaxVelocity)
{
	// Don't want to divide by zero
	if (MinVelocity > 0.f && MaxVelocity > 0.f)
	{
		// Use square values to avoid square root operations
		MinVelocity = FMath::Square(MinVelocity);
		MaxVelocity = FMath::Square(MaxVelocity);;
		float VelocitySquared = Velocity.SizeSquared();

		// Calculate velocity with min execluded (MinVelocity = 0)
		float ClampedVelocity = FMath::Max(0.f, VelocitySquared - MinVelocity);
		float Alpha = ClampedVelocity / (MaxVelocity - MinVelocity);

		// Calculate spread, but clamp to atleast min spread
		float Spread = ((MaxSpread - MinSpread) * Alpha) + MinSpread;
		float SpreadAngle = FMath::DegreesToRadians(Spread * 0.5f);

		// Apply spread to direction
		Direction = FMath::VRandCone(Direction, SpreadAngle);
	}
	else
	{
		// Get random spread to apply
		float SpreadY = FMath::DegreesToRadians(FMath::RandRange(MinSpread, MaxSpread));
		float SpreadZ = FMath::DegreesToRadians(FMath::RandRange(MinSpread, MaxSpread));

		// Apply spread to direction
		Direction = FMath::VRandCone(Direction, SpreadY, SpreadZ);
	}

	return Direction;
}

bool UEdensGardenFunctionLibrary::AddWidgetToViewport(UUserWidget* Widget, int32 ZOrder)
{
	if (Widget && !Widget->IsInViewport())
	{
		Widget->AddToViewport(ZOrder);
		return true;
	}

	return false;
}

void UEdensGardenFunctionLibrary::RemoveWidgetFromParent(UUserWidget* Widget)
{
	if (Widget && Widget->IsInViewport())
	{
		Widget->RemoveFromParent();
	}
}

void UEdensGardenFunctionLibrary::HighlightActor(const UObject* WorldContextObject, AActor* Actor, FLinearColor Color)
{
	AEdensGardenGameMode* GameMode = GetEdensGardenGameMode(WorldContextObject);
	if (GameMode)
	{
		GameMode->HighlightActor(Actor, Color);
	}
}

void UEdensGardenFunctionLibrary::PingActor(const UObject* WorldContextObject, AActor* Actor, EHighlightColor Color, float Duration)
{
	AEdensGardenGameMode* GameMode = GetEdensGardenGameMode(WorldContextObject);
	if (GameMode)
	{
		GameMode->PingActor(Actor, Color, Duration);
	}
}

void UEdensGardenFunctionLibrary::SetMainMenuWidget(const UObject* WorldContextObject, TSubclassOf<UUserWidget> Widget, int32 ZOrder)
{
	AMainMenuGameMode* GameMode = GetMainMenuGameMode(WorldContextObject);
	if (GameMode)
	{
		GameMode->DisplayWidget(Widget, ZOrder);
	}
}

bool UEdensGardenFunctionLibrary::GetGameplayCueTagsFromTable(FGameplayTag Tag, const UDataTable* DataTable, FGameplayTagContainer & OutCueContainer)
{
	if (!Tag.IsValid())
	{
		return false;
	}

	if (!DataTable)
	{
		return false;
	}

	const FString Context(TEXT("UEdensGardenFunctionLibrary::GetGameplayCueTagsFromTable"));
	const FGameplayCueTags* TaggedCues = DataTable->FindRow<FGameplayCueTags>(Tag.GetTagName(), Context, false);
	if (TaggedCues)
	{
		OutCueContainer.AppendTags(TaggedCues->CueTags);
		return true;
	}

	return false;
}

void UEdensGardenFunctionLibrary::ExecuteGameplayCuesFromTable(UAbilitySystemComponent* AbilitySystem, FGameplayTag Tag, const UDataTable* DataTable, const FGameplayCueParameters& Parameters)
{
	if (AbilitySystem)
	{
		FGameplayTagContainer CueTags;
		if (GetGameplayCueTagsFromTable(Tag, DataTable, CueTags))
		{
			for (const FGameplayTag& Cue : CueTags)
			{
				AbilitySystem->ExecuteGameplayCue(Cue, Parameters);
			}
		}
	}
}

bool UEdensGardenFunctionLibrary::IsComponentMoveable(USceneComponent* SceneComponent)
{
	if (SceneComponent && SceneComponent->Mobility == EComponentMobility::Movable)
	{
		return true;
	}

	return false;
}

bool UEdensGardenFunctionLibrary::IsGamepadAttached()
{
	if (FSlateApplication::IsInitialized())
	{
		const TSharedPtr<GenericApplication> Application = FSlateApplication::Get().GetPlatformApplication();
		if (Application.IsValid())
		{
			return Application->IsGamepadAttached();
		}
	}

	return false;
}

bool UEdensGardenFunctionLibrary::HasClearLineOfSight(UWorld* World, const FVector& Origin, UPrimitiveComponent* Primitive, ECollisionChannel LOSChannel, FCollisionQueryParams& QParams, FHitResult& Hit)
{
	if (!World || !Primitive)
	{
		return false;
	}

	Hit.Reset();
	if (World->LineTraceSingleByChannel(Hit, Origin, Primitive->Bounds.Origin, LOSChannel, QParams))
	{
		return Hit.GetComponent() == Primitive;
	}
	else
	{
		// We haven't hit anything else, meaning line of sight is technically clear
		FVector Location = Primitive->GetComponentLocation();
		FVector Normal = (Origin - Location).GetSafeNormal();

		Hit = FHitResult(Primitive->GetOwner(), Primitive, Location, Normal);
	}

	return true;
}
