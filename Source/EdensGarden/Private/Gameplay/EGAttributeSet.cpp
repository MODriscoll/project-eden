// Copyright of Sock Goblins

#include "EGAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

#include "AbilitySystemComponent.h"

UEGAttributeSet::UEGAttributeSet()
{
	Health = FGameplayAttributeData(100.f);
	MaxHealth = FGameplayAttributeData(100.f);
	Damage = FGameplayAttributeData(0.f);
	DefenseMultiplier = FGameplayAttributeData(1.f);
}

bool UEGAttributeSet::PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data)
{
	if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		FGameplayTagContainer EffectTags;
		Data.EffectSpec.GetAllAssetTags(EffectTags);
		
		// This damage might be forced
		const FGameplayTag ForceTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Force"));
		if (EffectTags.HasTag(ForceTag))
		{
			return true;
		}
		
		// Our owner might be invincible, meaning we should take no damage
		if (IsInvincible())
		{
			return false;
		}

		float Multiplier = GetDefenseMultiplier();
		if (ensure(Multiplier > 0.f))
		{
			Data.EvaluatedData.Magnitude *= GetDefenseMultiplier();
		}
		else
		{
			return false;
		}
	}
	else if (Data.EvaluatedData.Attribute == GetDefenseMultiplierAttribute())
	{
		// Should not ever have an invalid defense multiplier
		if (Data.EvaluatedData.Magnitude <= 0.f)
		{
			return false;
		}
	}

	return true;
}

void UEGAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	// Context and tags of effect
	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	const FGameplayTagContainer& Tags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Source of this effect
	UAbilitySystemComponent* SourceASC = Context.GetOriginalInstigatorAbilitySystemComponent();

	float Delta = Data.EvaluatedData.ModifierOp == EGameplayModOp::Additive ? Data.EvaluatedData.Magnitude : 0.f;

	if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		float DamageApplied = GetDamage();

		// Damage should only negate health
		if (DamageApplied > 0.f)
		{
			// Apply the damage
			float CurHealth = GetHealth();
			CurHealth = FMath::Clamp(CurHealth - DamageApplied, 0.f, GetMaxHealth());
			SetHealth(CurHealth);

			// Get the source of the effect
			AActor* SourceActor = nullptr;
			APawn* SourcePawn = nullptr;
			if (SourceASC && SourceASC->AbilityActorInfo.IsValid() && SourceASC->AbilityActorInfo->AvatarActor.IsValid())
			{
				SourceActor = SourceASC->AbilityActorInfo->AvatarActor.Get();
				AController* SourceController = SourceASC->AbilityActorInfo->PlayerController.Get();

				if (SourceController)
				{
					SourcePawn = SourceController->GetPawn();
				}
				else
				{
					SourcePawn = Cast<APawn>(SourceActor);
					if (!SourcePawn)
					{
						SourcePawn = Cast<APawn>(SourceActor->GetOwner());
					}
				}
			}

			// If we have a causer for the effect, use it instead
			// (Could be a gun or a projectile)
			if (Context.GetEffectCauser())
			{
				SourceActor = Context.GetEffectCauser();
			}

			// Get the hit result of the damage being applied
			bool bValidHit = false;
			FHitResult HitResult;
			if (Context.GetHitResult())
			{
				bValidHit = true;
				HitResult = *Context.GetHitResult();
			}

			OnDamaged.Broadcast(DamageApplied, CurHealth <= 0.f, SourcePawn, SourceActor, Tags, bValidHit, HitResult);
		}

		// Consume the damage
		SetDamage(0.f);
	}
	else if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		float CurHealth = GetHealth();
		CurHealth = FMath::Clamp(CurHealth, 0.f, GetMaxHealth());
		SetHealth(CurHealth);

		OnHealthChanged.Broadcast(CurHealth, Tags);
	}
}

bool UEGAttributeSet::IsInvincible() const
{
	const UAbilitySystemComponent* ASC = GetOwningAbilitySystemComponent();
	if (ASC)
	{
		const FGameplayTag InvincibleTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Invincible"));
		return ASC->HasMatchingGameplayTag(InvincibleTag);
	}

	return false;
}
