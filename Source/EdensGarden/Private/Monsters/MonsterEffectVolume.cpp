// Copyright of Sock Goblins

#include "MonsterEffectVolume.h"
#include "MonsterCharacter.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "Components/BoxComponent.h"

#include "Engine/World.h"

AMonsterEffectVolume::AMonsterEffectVolume()
{
	PrimaryActorTick.bCanEverTick = false;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	bVolumeActive = false;

	// Dummy as the root, so components don't need to be attached to volume
	USceneComponent* Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	EffectVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("EffectVolume"));
	EffectVolume->SetupAttachment(Root);
	EffectVolume->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	EffectVolume->OnComponentBeginOverlap.AddDynamic(this, &AMonsterEffectVolume::VolumeStartOverlap);
	EffectVolume->OnComponentEndOverlap.AddDynamic(this, &AMonsterEffectVolume::VolumeEndOverlap);
}

AMonsterEffectVolume* AMonsterEffectVolume::SpawnMonsterEffectVolume(AMonsterCharacter* Monster, TSubclassOf<AMonsterEffectVolume> VolumeTemplate, 
	const FVector& Location, const FRotator& Rotation, TSubclassOf<UGameplayEffect> EffectTemplate, float Duration)
{
	if (Monster && VolumeTemplate)
	{
		UWorld* World = Monster->GetWorld();

		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = Monster;
		SpawnParams.Instigator = Monster;
		SpawnParams.ObjectFlags = RF_Transient;

		AMonsterEffectVolume* Volume = World->SpawnActor<AMonsterEffectVolume>(VolumeTemplate, Location, Rotation, SpawnParams);
		if (Volume)
		{
			// Need to first create the spec (we might need to snap shot monsters attributes)
			FGameplayEffectSpecHandle EffectSpec;
			{
				if (EffectTemplate && Monster->GetAbilitySystemComponent() != nullptr)
				{
					UAbilitySystemComponent* ASC = Monster->GetAbilitySystemComponent();

					// Generate context for effect with volume as the source
					FGameplayEffectContextHandle Context = ASC->MakeEffectContext();
					Context.AddInstigator(Monster, Volume);
					Context.AddSourceObject(Volume);

					EffectSpec = ASC->MakeOutgoingSpec(EffectTemplate, UGameplayEffect::INVALID_LEVEL, Context);
				}
				else
				{
					UE_LOG(LogMonster, Error, TEXT("Unable to generate effect for monster effect volume %s. Owned by %s"), *Volume->GetName(), *Monster->GetName());
				}
			}

			// Finish activating volume
			Volume->ActivateVolume(EffectSpec, Duration);
		}
		else
		{
			UE_LOG(LogMonster, Error, TEXT("Failed to spawn monster effect volume for monster %s"), *Monster->GetName());
		}

		return Volume;
	}

	return nullptr;
}

void AMonsterEffectVolume::BeginPlay()
{
	Super::BeginPlay();
	bVolumeActive = false;
}

void AMonsterEffectVolume::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (bVolumeActive)
	{
		ExpireVolume();
	}
}

void AMonsterEffectVolume::SetActorHiddenInGame(bool bNewHidden)
{
	if (bNewHidden != bHidden)
	{
		check(RootComponent);

		// We don't want to draw our volume
		TArray<USceneComponent*> Children;
		RootComponent->GetChildrenComponents(false, Children);

		for (USceneComponent* Component : Children)
		{
			if (!bNewHidden || Component != EffectVolume)
			{
				Component->SetHiddenInGame(bNewHidden);
			}
		}

		bHidden = bNewHidden;
	}
}

void AMonsterEffectVolume::LifeSpanExpired()
{
	ExpireVolume();
}

void AMonsterEffectVolume::ActivateVolume(const FGameplayEffectSpecHandle& EffectHandle, float Duration)
{
	if (Duration <= 0.f)
	{
		UE_LOG(LogMonster, Warning, TEXT("Monster effect volume given invalid duration. Defaulting 10 seconds"));
		Duration = 10.f;
	}

	// Save the effect first, as enabling collision can cause immediate overlaps
	VolumeEffectHandle = EffectHandle;
	bVolumeActive = true;

	EffectVolume->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SetLifeSpan(Duration);

	OnVolumeActivated();
}

void AMonsterEffectVolume::ExpireVolume()
{
	RemoveAllEffects();
	bVolumeActive = false;

	EffectVolume->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	// Only derived types to handle destruction
	OnVolumeExpired();
}

void AMonsterEffectVolume::RemoveEffectFromActor(AActor* Actor)
{
	UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Actor);
	if (ASC)
	{
		FGameplayEffectQuery Query;
		Query.EffectSource = this;

		ASC->RemoveActiveEffects(Query);
	}
}

void AMonsterEffectVolume::RemoveAllEffects()
{
	TArray<AActor*> OverlappedActors;
	EffectVolume->GetOverlappingActors(OverlappedActors);

	for (AActor* Actor : OverlappedActors)
	{
		RemoveEffectFromActor(Actor);
	}
}

void AMonsterEffectVolume::VolumeStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == this || OtherActor == Instigator)
	{
		return;
	}

	// Need a valid handle to apply effect with
	if (bVolumeActive && VolumeEffectHandle.IsValid())
	{
		UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(OtherActor);
		if (ASC)
		{
			ASC->ApplyGameplayEffectSpecToSelf(*VolumeEffectHandle.Data);
		}
	}
}

void AMonsterEffectVolume::VolumeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == this || OtherActor == Instigator)
	{
		return;
	}

	RemoveEffectFromActor(OtherActor);
}

