// Copyright of Sock Goblins

#include "MonsterAnimInstance.h"
#include "MonsterAIController.h"
#include "MonsterCharacter.h"

#include "Kismet/KismetMathLibrary.h"

UMonsterAnimInstance::UMonsterAnimInstance()
	: SpineRotation(ForceInit)
{
	SpineDistanceRange = 400.f;
	SpineFOVRange = 90.f;
	SpineInterpSpeed = 5.f;
}

void UMonsterAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	const AMonsterCharacter* Monster = Cast<AMonsterCharacter>(TryGetPawnOwner());
	if (Monster)
	{
		UpdateEGCharacter(Monster, DeltaTime);
		SpineRotation = CalculateSpineRotation(Monster, DeltaTime);
	}
}

FRotator UMonsterAnimInstance::CalculateSpineRotation(const AMonsterCharacter* Monster, float DeltaTime)
{
	AMonsterAIController* Controller = Monster ? Cast<AMonsterAIController>(Monster->GetController()) : nullptr;
	if (Controller)
	{
		// Monsters should only look up or down to see its sight target
		const AActor* Target = Controller->GetSightTarget();
		if (Target && Monster->GetHorizontalDistanceTo(Target) < SpineDistanceRange)
		{
			// We need to be able to see this actor as well
			float AngleDot = FMath::Cos(FMath::DegreesToRadians(SpineFOVRange));
			if (Monster->GetHorizontalDotProductTo(Target) >= AngleDot)
			{
				FVector MonsterEyesLocation;
				FRotator MonsterEyesRotation;
				Monster->GetActorEyesViewPoint(MonsterEyesLocation, MonsterEyesRotation);

				FVector TargetEyesLocation;
				FRotator TargetEyesRotation;
				Target->GetActorEyesViewPoint(TargetEyesLocation, TargetEyesRotation);

				// Should lookat the targets eyes
				FRotator DesiredRotation = UKismetMathLibrary::FindLookAtRotation(TargetEyesLocation, MonsterEyesLocation);
				DesiredRotation = UKismetMathLibrary::NormalizedDeltaRotator(DesiredRotation, MonsterEyesRotation);
				DesiredRotation = DesiredRotation.GetInverse();

				DesiredRotation.Roll = 0.f;

				return FMath::RInterpTo(SpineRotation, DesiredRotation, DeltaTime, SpineInterpSpeed);
			}
		}
	}

	// By default, un-rotate any rotation applied to the spline
	return FMath::RInterpTo(SpineRotation, FRotator::ZeroRotator, DeltaTime, SpineInterpSpeed);
}
