// Copyright of Sock Goblins

#include "ShamblerCharacter.h"
#include "ShamblerAIController.h"
#include "Characters/SteeringForceMovementComponent.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "TimerManager.h"

const FName AShamblerCharacter::MeleeCollisionName(TEXT("MeleeCollision"));

AShamblerCharacter::AShamblerCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer.SetDefaultSubobjectClass<USteeringForceMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag("Character.ID.Monster.Shambler"));
	AIControllerClass = AShamblerAIController::StaticClass();

	SporesVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("SporesVolume"));
	SporesVolume->SetupAttachment(RootComponent);
	SporesVolume->InitBoxExtent(FVector(50.f));
	SporesVolume->bGenerateOverlapEvents = true;
	// TODO: collision profile

	OriginalSporeVolumeScale = FVector::OneVector;

	SporesVolume->OnComponentBeginOverlap.AddDynamic(this, &AShamblerCharacter::SporeVolumeStartOverlap);
	SporesVolume->OnComponentEndOverlap.AddDynamic(this, &AShamblerCharacter::SporeVolumeEndOverlap);

	MeleeCollision = CreateDefaultSubobject<USphereComponent>(MeleeCollisionName);
	MeleeCollision->SetupAttachment(GetMesh(), TEXT("hand_r"));
	MeleeCollision->InitSphereRadius(20.f);
	MeleeCollision->bGenerateOverlapEvents = true;
	MeleeCollision->SetCollisionProfileName(MELEEHITBOX_PROFILE);

	MeleeCollision->OnComponentBeginOverlap.AddDynamic(this, &AShamblerCharacter::OnHitboxOverlap);

	// The default spore stages (specified)
	{
		SporeStages.Stages.Emplace(1.f, 0.f);
		SporeStages.Stages.Emplace(0.65f, 0.75f);
		SporeStages.Stages.Emplace(0.35f, 1.5f);
		SporeStages.Stages.Emplace(0.f, 3.f);
	}

	ExplosionCloudLifespan = 10.f;
	bCanRevertSporeStages = false;
	LastSporeStageIndex = 0;

	SteeringMovement = CastChecked<USteeringForceMovementComponent>(GetCharacterMovement());
}

void AShamblerCharacter::BeginPlay()
{
	Super::BeginPlay();

	SporeStages.Sort();
	OriginalSporeVolumeScale = SporesVolume ? SporesVolume->RelativeScale3D : FVector::OneVector;
	
	// TODO: think of alternative method for initializing
	CheckSporeStage(GetHealth(), 0, FGameplayTagContainer());
}

void AShamblerCharacter::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	// Need to clear any active effects applied from our spore cloud
	TArray<AActor*> OverlappedActors;
	SporesVolume->GetOverlappingActors(OverlappedActors);

	for (AActor* Actor : OverlappedActors)
	{
		UAbilitySystemComponent* TargetASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(Actor);
		if (TargetASC)
		{
			// We are searching for effects applied by our volume
			FGameplayEffectQuery Query;
			Query.EffectSource = SporesVolume;

			TargetASC->RemoveActiveEffects(Query);
		}
	}

	Super::EndPlay(EndPlayReason);
}

#if EG_GAMEPLAY_DEBUGGER
void AShamblerCharacter::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}State: {green}%s\n"
		"{white}Spore Stage: {green}%i\n"
		"{white}Spore Cloud Time Remaining: {green}%s\n");

	// Get current state as string
	FString StateString(TEXT("Unknown"));
	if (AShamblerAIController* Controller = Cast<AShamblerAIController>(GetController()))
	{
		switch (Controller->GetShamblerState())
		{
			case EShamblerState::Roam:				{ StateString = TEXT("Roam"); break; }
			case EShamblerState::Agitated:			{ StateString = TEXT("Agitated"); break; }
			case EShamblerState::Investigating:		{ StateString = TEXT("Investigating"); break; }
			case EShamblerState::Hunting:			{ StateString = TEXT("Hunting"); break; }
		}
	}

	// Get time remaining on spore cloud
	FString CloudString(TEXT("InActive"));
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		if (TimerManager.IsTimerActive(TimerHandle_SporeCloud))
		{
			CloudString = FString::SanitizeFloat(TimerManager.GetTimerRemaining(TimerHandle_SporeCloud));
		}
	}

	String.Append(FString::Printf(*Formatting,
		*StateString, LastSporeStageIndex, *CloudString));
}
#endif

void AShamblerCharacter::OnHealthChanged_Implementation(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags)
{
	Super::OnHealthChanged_Implementation(NewHealth, Delta, GameplayTags);

	// Right now, health changes (and damage) is still reported even when dead
	if (!bIsDead)
	{
		CheckSporeStage(NewHealth, Delta, GameplayTags);
	}
}

void AShamblerCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

	// TODO: sense gameplay tags?
	IHitboxInterface::Execute_DisableHitboxes(this, FGameplayTag::RequestGameplayTag("Character.State.Dead"), FGameplayTagContainer());

	// First update our spore stage, so we use
	// the full extent of the spore cloud
	CheckSporeStage(0.f, -Damage, GameplayTags);

	// Only explode the shambler if life span allows it
	if (ExplosionCloudLifespan > 0.f && ShouldSporesExplode(GameplayTags))
	{
		// Maintain spores current world position
		SporesVolume->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);

		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_SporeCloud, this, &AShamblerCharacter::OnSporeCloudExpired, ExplosionCloudLifespan, false);

		OnSporesExplosion();
	}
	else
	{
		OnSporesExpired(false);
	}
}

void AShamblerCharacter::CheckSporeStage(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags)
{
	float OldHealth = NewHealth - Delta;
	float MaxHealth = GetMaxHealth();

	float MinPercentage = NewHealth / MaxHealth;
	float MaxPercentage = OldHealth / MaxHealth;

	// Our health might have been restored
	bool bIsRevert = false;
	if (NewHealth > OldHealth)
	{
		bIsRevert = true;
		Swap(MinPercentage, MaxPercentage);
	}

	// Find the stage we encapsulate
	for (int32 Index = 0; Index < SporeStages.Num(); ++Index)
	{
		const FShamblerSporeStage& Stage = SporeStages.GetStage(Index);
		if (Stage.RangeEncapsulatesStage(MinPercentage, MaxPercentage) && ShouldEnterNextSporeStage(Stage, bIsRevert, GameplayTags))
		{
			UpdateSporeStage(Stage, Index, bIsRevert);
			break;
		}
	}
}

void AShamblerCharacter::UpdateSporeStage(const FShamblerSporeStage& NewStage, int32 Index, bool bIsRevert)
{
	if (bIsRevert && !bCanRevertSporeStages)
	{
		return;
	}

	// Disable the spore volumes collision based on the multiplier
	// This should ultimately end overlaps and remove effects
	if (NewStage.SporesMultiplier <= 0.f)
	{
		SporesVolume->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
	else
	{
		SporesVolume->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}

	SporesVolume->SetRelativeScale3D(OriginalSporeVolumeScale * FMath::Max(NewStage.SporesMultiplier, 0.f));
	LastSporeStageIndex = Index;

	OnNewSporeStagedEntered(NewStage, Index, bIsRevert);
}

FShamblerSporeStage AShamblerCharacter::GetCurrentSporeStage() const
{
	if (SporeStages.IsValidIndex(LastSporeStageIndex))
	{
		return SporeStages.GetStage(LastSporeStageIndex);
	}

	return FShamblerSporeStage();
}

void AShamblerCharacter::OnSporesExpired_Implementation(bool bAfterExplosion)
{
	if (bAfterExplosion)
	{
		Destroy();
	}
	else
	{
		EnterRagdoll();
		SetLifeSpan(10.f);
	}
}

void AShamblerCharacter::OnSporeCloudExpired()
{
	OnSporesExpired(true);
}

void AShamblerCharacter::SporeVolumeStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == this)
	{
		return;
	}

	if (!SporesEffect)
	{
		UE_LOG(LogMonster, Warning, TEXT("Shambler: Shambler %s failed to apply spores effect to %s as spores effect is null!"), *GetName(), *OtherActor->GetName());
		return;
	}

	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		// Don't bother if target isn't using abilities
		UAbilitySystemComponent* TargetASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OtherActor);
		if (!TargetASC)
		{
			return;
		}

		// Our spores volume is the source of the effect
		FGameplayEffectContextHandle Context = ASC->MakeEffectContext();
		Context.AddSourceObject(SporesVolume);

		FGameplayEffectSpecHandle EffectSpec = ASC->MakeOutgoingSpec(SporesEffect, UGameplayEffect::INVALID_LEVEL, Context);
		ASC->ApplyGameplayEffectSpecToTarget(*EffectSpec.Data, TargetASC);

		//UE_LOG(LogMonster, Log, TEXT("Shambler: Applying spores effect to %s"), *OtherActor->GetName());
	}
}

void AShamblerCharacter::SporeVolumeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == this)
	{
		return;
	}

	UAbilitySystemComponent* TargetASC = UAbilitySystemGlobals::GetAbilitySystemComponentFromActor(OtherActor);
	if (TargetASC)
	{
		// We are searching for effects applied by our volume
		FGameplayEffectQuery Query;
		Query.EffectSource = SporesVolume;

		TargetASC->RemoveActiveEffects(Query);

		//UE_LOG(LogMonster, Log, TEXT("Shambler: Removed spores effect from %s"), *OtherActor->GetName());
	}
}

void AShamblerCharacter::EnableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags)
{
	AttackTag = AssociatedTag;

	// Reset ignored actors
	HitActors.Empty();

	// Activate last as it will update overlaps
	MeleeCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	// Allow derived blueprints to attach the hitbox to appropriate slot
	OnHitboxEnabled(HitboxTags);
}

void AShamblerCharacter::DisableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags)
{
	AttackTag = FGameplayTag::EmptyTag;

	MeleeCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AShamblerCharacter::OnHitboxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == this)
	{
		return;
	}

	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC && !HitActors.Contains(OtherActor))
	{
		// We don't want to hit the same actor twice!
		HitActors.Add(OtherActor);

		const FGameplayTag MeleeHit = FGameplayTag::RequestGameplayTag(TEXT("Event.Melee.Hit"));

		FGameplayEventData Payload;
		Payload.EventTag = AttackTag;
		Payload.Instigator = this;
		Payload.Target = OtherActor;
		Payload.OptionalObject = OtherComp;
		Payload.OptionalObject2 = OtherComp->BodyInstance.GetSimplePhysicalMaterial();

		// Get tags to send
		GetOwnedGameplayTags(Payload.InstigatorTags);
		if (IGameplayTagAssetInterface* TagAsset = Cast<IGameplayTagAssetInterface>(OtherActor))
		{
			TagAsset->GetOwnedGameplayTags(Payload.TargetTags);
		}

		// Send potential hit result
		if (bFromSweep)
		{
			// Handle will manage clearing memory (via shared pointer)
			Payload.TargetData.Add(new FGameplayAbilityTargetData_SingleTargetHit(SweepResult));
		}

		// Send event to ourself
		{
			FScopedPredictionWindow(ASC, true);
			ASC->HandleGameplayEvent(MeleeHit, &Payload);
		}
	}
}

void AShamblerCharacter::OnShamblerEnterState(EShamblerState OldState, EShamblerState NewState)
{
	if (OldState != NewState)
	{
		UpdateStateEffects(NewState);
		OnEnterNewState(OldState, NewState);
	}
}

void AShamblerCharacter::UpdateStateEffects(EShamblerState NewState)
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		// Remove all of the previous states effects
		for (const FActiveGameplayEffectHandle& Handle : ActiveStateEffects)
		{
			ASC->RemoveActiveGameplayEffect(Handle);
		}

		// Apply all of the new effects
		const FWrappedEffectsContainer* EffectsContainer = StateEffects.Find(NewState);
		if (EffectsContainer)
		{
			// Context shared amongst all applied effects
			FGameplayEffectContextHandle Context = ASC->MakeEffectContext();
			Context.AddSourceObject(this);

			for (auto Effect : EffectsContainer->GameplayEffects)
			{
				if (Effect)
				{
					ActiveStateEffects.Add(ASC->ApplyGameplayEffectToSelf(Effect.GetDefaultObject(), UGameplayEffect::INVALID_LEVEL, Context));
				}
			}
		}

		UE_LOG(LogMonster, Log, TEXT("Updated State effects for shambler %s to state %i"), *GetName(), static_cast<int32>(NewState));
	}
}
