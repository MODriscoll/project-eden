// Copyright of Sock Goblins

#include "BrutusCharacter.h"
#include "MonsterEffectVolume.h"

#include "Components/BoxComponent.h"
#include "Components/SkeletalMeshComponent.h"

#include "TimerManager.h"

ABrutusCharacter::ABrutusCharacter(const FObjectInitializer& ObjectInitializer)
{
	PassiveSporeCloudDuration = 10.f;
	bScalePassiveSporeCloudsByStage = false;
	PassiveSporeCloudProduceInterval = 5.f;
	bShoveFriendliesOnContact = false;
}

void ABrutusCharacter::OnEnterNewState_Implementation(EShamblerState OldState, EShamblerState NewState)
{
	bShoveFriendliesOnContact = NewState != EShamblerState::Roam;
}

void ABrutusCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (PassiveSporeCloudProduceInterval > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_ProduceSpores, this, &ABrutusCharacter::OnProduceSpores, PassiveSporeCloudProduceInterval, true);
	}
}

void ABrutusCharacter::OnCharacterInitialHit(AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!bShoveFriendliesOnContact)
	{
		return;
	}

	// We might not be moving, meaning we should push back
	FVector Velocity = GetVelocity();
	if (Velocity.SizeSquared2D() <= 1.f)
	{
		return;
	}

	// Move friendly team-mates out of our way
	if (ensure(OtherActor) && GetTeamAttitudeTowards(*OtherActor) == ETeamAttitude::Friendly)
	{
		AEGCharacter* OtherCharacter = Cast<AEGCharacter>(OtherActor);
		if (OtherCharacter)
		{
			// Might be safe if skewed, but characters should remain upwards at all times
			FVector Right = GetActorRightVector();
			FVector Direction = (OtherActor->GetActorLocation() - GetActorLocation()).GetSafeNormal();

			// We want to push the monster to our sides to clear a path
			FVector Impulse = Right * 3000.f;
			if (FVector::DotProduct(Right, Direction) < 0.f)
			{
				Impulse *= -1.f;
			}

			const FGameplayTag ShoveStun = FGameplayTag::RequestGameplayTag(TEXT("Event.Stun.Shove"));
			OtherCharacter->StunWithImpulse(GetActorLocation(), Impulse, ShoveStun);
		}
	}
}

void ABrutusCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.ClearTimer(TimerHandle_ProduceSpores);
}

void ABrutusCharacter::ProduceSpores(bool bScaleBySporeStage)
{
	if (!(IsDead() || IsRagdolling()))
	{
		FVector SporesLocation = GetActorLocation();
		FRotator SporesRotation = GetActorRotation();

		float ScaleBy = 1.f;
		if (bScaleBySporeStage)
		{
			// Spores multiplier might be to low to actually be usefull
			FShamblerSporeStage Stage = GetCurrentSporeStage();
			if (FMath::IsNearlyZero(Stage.SporesMultiplier))
			{
				return;
			}

			ScaleBy = Stage.SporesMultiplier;
		}

		// Spawn spores
		AMonsterEffectVolume* SporeCloud = AMonsterEffectVolume::SpawnMonsterEffectVolume(this, PassiveSporeCloudTemplate,
			SporesLocation, SporesRotation, SporesEffect, PassiveSporeCloudDuration);
		if (SporeCloud)
		{
			FVector CloudScale = SporeCloud->GetActorScale() * ScaleBy;
			SporeCloud->SetActorScale3D(CloudScale);
		}
	}
}

void ABrutusCharacter::OnProduceSpores()
{
	ProduceSpores(bScalePassiveSporeCloudsByStage);
}
