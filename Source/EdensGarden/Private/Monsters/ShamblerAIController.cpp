// Copyright of Sock Goblins

#include "ShamblerAIController.h"
#include "ShamblerCharacter.h"

#include "BehaviorTree/BlackboardComponent.h"

#include "TimerManager.h"

AShamblerAIController::AShamblerAIController()
{
	ShamblerState = EShamblerState::Roam;
	ShamblerStateKey = TEXT("ShamblerState");
}

void AShamblerAIController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	SetShamblerState(EShamblerState::Roam);
}

void AShamblerAIController::SetShamblerState(EShamblerState NewState)
{
	SetShamblerState(NewState, true);
}

void AShamblerAIController::AgitateShambler(float Duration)
{
	if (Duration <= 0.f)
	{
		UE_LOG(LogMonster, Warning, TEXT("AgitateShambler: Duration is invalid, directly entering agitated state instead"));
		SetShamblerState(EShamblerState::Agitated);

		return;
	}

	// Always set the timer (if already agitated, we can just reset it)
	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.SetTimer(TimerHandle_Agitated, this, &AShamblerAIController::OnAgitatedFinished, Duration, false);

	SetShamblerState(EShamblerState::Agitated, false);
}

void AShamblerAIController::SetShamblerState(EShamblerState NewState, bool bClearAgitateTimer)
{
	// We only want to notify if state is new
	if (ShamblerState != NewState)
	{
		EShamblerState OldState = ShamblerState;
		ShamblerState = NewState;

		// This function might be getting called from agitate shambler
		if (bClearAgitateTimer)
		{
			FTimerManager& TimerManager = GetWorldTimerManager();
			TimerManager.ClearTimer(TimerHandle_Agitated);
		}

		// Potentially set the blackboard value as well
		if (Blackboard && Blackboard->GetKeyID(ShamblerStateKey) != FBlackboard::InvalidKey)
		{
			// Avoid triggering value changed events if states are the same
			EShamblerState BlackboardState = static_cast<EShamblerState>(Blackboard->GetValueAsEnum(ShamblerStateKey));
			if (BlackboardState != NewState)
			{
				Blackboard->SetValueAsEnum(ShamblerStateKey, (uint8)NewState);
			}
		}

		// Notify our shambler of our new state
		AShamblerCharacter* Shambler = GetShambler();
		if (Shambler)
		{
			Shambler->OnShamblerEnterState(OldState, NewState);
		}

		//UE_LOG(LogEGCharacterAI, Log, TEXT("Shambler Controller %s has entered state %i"), *GetName(), (int32)NewState);
	}
}

void AShamblerAIController::OnAgitatedFinished()
{
	SetShamblerState(EShamblerState::Roam, false);
}

AShamblerCharacter* AShamblerAIController::GetShambler() const
{
	if (PossessedMonster.IsValid() && PossessedMonster->IsA(AShamblerCharacter::StaticClass()))
	{
		return static_cast<AShamblerCharacter*>(PossessedMonster.Get());
	}

	return nullptr;
}