// Copyright of Sock Goblins

#include "BTService_MonsterSoundTarget.h"
#include "MonsterAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Hearing.h"

UBTService_MonsterSoundTarget::UBTService_MonsterSoundTarget()
{
	NodeName = "Update Monster Sound Target";
	Interval = 0.2f;
	RandomDeviation = 0.f;

	/** Sound location can only be a vector */
	SoundLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_MonsterSoundTarget, SoundLocationKey));
}

void UBTService_MonsterSoundTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	// Stop now if we can't properly set location
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	if (!Blackboard || SoundLocationKey.IsNone())
	{
		return;
	}

	/*AAIController* Controller = OwnerComp.GetAIOwner();
	UAIPerceptionComponent* PerceptionComp = Controller->GetPerceptionComponent();

	if (PerceptionComp)
	{
		FAISenseID HearingSenseID = UAISense::GetSenseID<UAISense_Hearing>();

		bool bShouldInvestigate = false;
		float YoungestAge = MAX_FLT;
		FVector YoungestAgeLocation = FVector::ZeroVector;

		for (auto Iterator = PerceptionComp->GetPerceptualDataConstIterator(); Iterator; ++Iterator)
		{
			const FActorPerceptionInfo& Info = Iterator->Value;

			if (Info.bIsHostile && Info.LastSensedStimuli.IsValidIndex(HearingSenseID))
			{
				const FAIStimulus& HearingStimulus = Info.LastSensedStimuli[HearingSenseID];
				if (!HearingStimulus.IsExpired() && HearingStimulus.GetAge() < 2.f && HearingStimulus.GetAge() < YoungestAge)
				{
					bShouldInvestigate = true;
					YoungestAge = HearingStimulus.GetAge();
					YoungestAgeLocation = HearingStimulus.StimulusLocation;
				}
			}
		}

		if (bShouldInvestigate)
		{
			Blackboard->SetValueAsVector(SoundLocationKey.SelectedKeyName, YoungestAgeLocation);
		}
	}*/
}
