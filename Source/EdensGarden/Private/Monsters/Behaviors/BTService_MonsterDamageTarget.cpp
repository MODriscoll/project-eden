// Copyright of Sock Goblins

#include "BTService_MonsterDamageTarget.h"
#include "MonsterAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Damage.h"

UBTService_MonsterDamageTarget::UBTService_MonsterDamageTarget()
{
	NodeName = "Update Monster Damage Target";
	Interval = 0.2f;
	RandomDeviation = 0.f;

	/** Damage location can only be a vector */
	DamageLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_MonsterDamageTarget, DamageLocationKey));
}
void UBTService_MonsterDamageTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	//// Stop now if we can't properly set location
	//UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	//if (!Blackboard || DamageLocationKey.IsNone())
	//{
	//	return;
	//}

	//AAIController* Controller = OwnerComp.GetAIOwner();
	//UAIPerceptionComponent* PerceptionComp = Controller->GetPerceptionComponent();

	//if (PerceptionComp)
	//{
	//	FAISenseID DamageSenseID = UAISense::GetSenseID<UAISense_Damage>();

	//	bool bShouldInvestigate = false;
	//	float YoungestAge = MAX_FLT;
	//	FVector YoungestAgeLocation = FVector::ZeroVector;

	//	for (auto Iterator = PerceptionComp->GetPerceptualDataConstIterator(); Iterator; ++Iterator)
	//	{
	//		const FActorPerceptionInfo& Info = Iterator->Value;

	//		if (Info.bIsHostile && Info.LastSensedStimuli.IsValidIndex(DamageSenseID))
	//		{
	//			const FAIStimulus& DamageStimulus = Info.LastSensedStimuli[DamageSenseID];
	//			if (!DamageStimulus.IsExpired() && DamageStimulus.GetAge() < 0.2f && DamageStimulus.GetAge() < YoungestAge)
	//			{
	//				bShouldInvestigate = true;
	//				YoungestAge = DamageStimulus.GetAge();
	//				YoungestAgeLocation = DamageStimulus.StimulusLocation;
	//			}
	//		}
	//	}

	//	if (bShouldInvestigate)
	//	{
	//		Blackboard->SetValueAsVector(DamageLocationKey.SelectedKeyName, YoungestAgeLocation);
	//	}
	//}
}


