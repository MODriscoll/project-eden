// Copyright of Sock Goblins

#include "BTTask_AgitateShambler.h"
#include "ShamblerAIController.h"

UBTTask_AgitateShambler::UBTTask_AgitateShambler()
{	
	NodeName = "Agitate Shambler";

	AgitateDuration = 5.f;
}

EBTNodeResult::Type UBTTask_AgitateShambler::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AShamblerAIController* Controller = Cast<AShamblerAIController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		Controller->AgitateShambler(AgitateDuration);
		return EBTNodeResult::Succeeded;
	}
	
	return EBTNodeResult::Failed;
}

FString UBTTask_AgitateShambler::GetStaticDescription() const
{
	return FString::Printf(TEXT("Agitating shambler for %.2f seconds"), AgitateDuration);
}
