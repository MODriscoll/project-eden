// Copyright of Sock Goblins

#include "BTTask_SetSlugState.h"
#include "SlugAIController.h"

#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SetSlugState::UBTTask_SetSlugState()
{
	NodeName = "Set Slug State";
	State = ESlugState::Roam;
}

EBTNodeResult::Type UBTTask_SetSlugState::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ASlugAIController* Controller = Cast<ASlugAIController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		Controller->SetSlugState(State);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

FString UBTTask_SetSlugState::GetStaticDescription() const
{
	FString StateString;
	switch (State)
	{
		case ESlugState::Roam:				{ StateString = TEXT("Roam"); break; }
		case ESlugState::Investigating:		{ StateString = TEXT("Investigating"); break; }
		case ESlugState::Retreating:		{ StateString = TEXT("Retreating"); break; }
		case ESlugState::Hunting:			{ StateString = TEXT("Hunting"); break; }
	}

	return FString::Printf(TEXT("Setting state to %s"), *StateString);
}