// Copyright of Sock Goblins

#include "BTService_MonsterContactTarget.h"
#include "MonsterAIController.h"
#include "MonsterCharacter.h"

#include "Components/CapsuleComponent.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Touch.h"

UBTService_MonsterContactTarget::UBTService_MonsterContactTarget()
{
	NodeName = "Update Monster Contact Target";
	Interval = 0.2f;
	RandomDeviation = 0.f;

	/** Damage location can only be a vector */
	ContactLocationKey.AddVectorFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_MonsterContactTarget, ContactLocationKey));
}

void UBTService_MonsterContactTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	// Stop now if we can't properly set location
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	if (!Blackboard || ContactLocationKey.IsNone())
	{
		return;
	}

	//AAIController* Controller = OwnerComp.GetAIOwner();
	//APawn* Pawn = Controller->GetPawn();
	//UAIPerceptionComponent* PerceptionComp = Controller->GetPerceptionComponent();

	//if (PerceptionComp)
	//{
	//	if (Blackboard->IsVectorValueSet(ContactLocationKey.SelectedKeyName))
	//	{
	//		return;
	//	}

	//	FAISenseID TouchSenseID = UAISense::GetSenseID<UAISense_Touch>();

	//	bool bShouldInvestigate = false;
	//	float YoungestAge = MAX_FLT;
	//	FVector YoungestAgeLocation = FVector::ZeroVector;

	//	for (auto Iterator = PerceptionComp->GetPerceptualDataConstIterator(); Iterator; ++Iterator)
	//	{
	//		const FActorPerceptionInfo& Info = Iterator->Value;

	//		if (Info.bIsHostile && Info.LastSensedStimuli.IsValidIndex(TouchSenseID))
	//		{
	//			const FAIStimulus& TouchStimulus = Info.LastSensedStimuli[TouchSenseID];
	//			if (!TouchStimulus.IsExpired() && TouchStimulus.GetAge() < 0.2f && TouchStimulus.GetAge() < YoungestAge)
	//			{
	//				bShouldInvestigate = true;
	//				YoungestAge = TouchStimulus.GetAge();
	//				YoungestAgeLocation = TouchStimulus.StimulusLocation;
	//			}
	//		}
	//	}

	//	if (bShouldInvestigate)
	//	{
	//		AMonsterCharacter* Monster = Cast<AMonsterCharacter>(Pawn);
	//		UCapsuleComponent* MonsterCapsule = Monster ? Monster->GetCapsuleComponent() : nullptr;

	//		// We want to offset the touch away so its not directly on top of us
	//		float OffsetToApply = MonsterCapsule ? MonsterCapsule->GetScaledCapsuleRadius() * 2.f : 100.f;
	//		FVector Offset = (YoungestAgeLocation - Pawn->GetActorLocation()).GetSafeNormal() * OffsetToApply;

	//		Blackboard->SetValueAsVector(ContactLocationKey.SelectedKeyName, YoungestAgeLocation + Offset);
	//	}
	//}
}
