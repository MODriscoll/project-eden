// Copyright of Sock Goblins

#include "BTTask_SetShamblerState.h"
#include "ShamblerAIController.h"

#include "BehaviorTree/BlackboardComponent.h"

UBTTask_SetShamblerState::UBTTask_SetShamblerState()
{
	NodeName = "Set Shambler State";
	State = EShamblerState::Roam;
}

EBTNodeResult::Type UBTTask_SetShamblerState::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AShamblerAIController* Controller = Cast<AShamblerAIController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		Controller->SetShamblerState(State);
		return EBTNodeResult::Succeeded;
	}

	return EBTNodeResult::Failed;
}

FString UBTTask_SetShamblerState::GetStaticDescription() const
{
	FString StateString;
	switch (State)
	{
		case EShamblerState::Roam:				{ StateString = TEXT("Roam"); break; }
		case EShamblerState::Agitated:			{ StateString = TEXT("Agitated"); break; }
		case EShamblerState::Investigating:		{ StateString = TEXT("Investigating"); break; }
		case EShamblerState::Hunting:			{ StateString = TEXT("Hunting"); break; }
	}

	return FString::Printf(TEXT("Setting state to %s"), *StateString);
}
