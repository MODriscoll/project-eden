// Copyright of Sock Goblins

#include "BTService_MonsterSightTarget.h"
#include "MonsterAIController.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISense_Sight.h"

UBTService_MonsterSightTarget::UBTService_MonsterSightTarget()
{
	NodeName = "Update Monster Sight Target";
	Interval = 0.2f;
	RandomDeviation = 0.f;

	/** Sight target must be an actor */
	SightTargetKey.AddObjectFilter(this, GET_MEMBER_NAME_CHECKED(UBTService_MonsterSightTarget, SightTargetKey), AActor::StaticClass());
}

void UBTService_MonsterSightTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	// Stop now if we can't properly set target
	UBlackboardComponent* Blackboard = OwnerComp.GetBlackboardComponent();
	if (!Blackboard || SightTargetKey.IsNone())
	{
		return;
	}

	AMonsterAIController* Controller = Cast<AMonsterAIController>(OwnerComp.GetAIOwner());
	if (Controller)
	{
		AActor* CurrentTarget = Controller->GetSightTarget();
		AActor* NewTarget = Controller->UpdateSightTarget();

		// We only want to update blackboard if target has changed, this is to prevent aborts of decorators
		if (CurrentTarget != NewTarget)
		{
			// We have a valid target in our sights
			if (NewTarget)
			{
				Blackboard->SetValueAsObject(SightTargetKey.SelectedKeyName, NewTarget);
			}
			else
			{
				Blackboard->ClearValue(SightTargetKey.SelectedKeyName);
			}
		}
	}
}
