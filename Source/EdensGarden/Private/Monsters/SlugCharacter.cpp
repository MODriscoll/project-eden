// Copyright of Sock Goblins

#include "SlugCharacter.h"
#include "SlugAIController.h"
#include "PatrolRouteActor.h"

#include "AbilitySystemComponent.h"
#include "MonsterEffectVolume.h"

#include "Components/BoxComponent.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "TimerManager.h"

DECLARE_CYCLE_STAT(TEXT("SlugCharacter - SpawnNewSlime"), STAT_SpawnNewSlime, STATGROUP_Monster);

const float ASlugCharacter::NoSlimeDropInterval = -1.f;

ASlugCharacter::ASlugCharacter()
{
	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag("Character.ID.Monster.Slug"));
	AIControllerClass = ASlugAIController::StaticClass();

	PassiveSlimeDuration = 30.f;
	PassiveSlimeDropInterval = 3.f;
}

void ASlugCharacter::BeginPlay()
{
	Super::BeginPlay();

	SetSlimeDropInterval(PassiveSlimeDropInterval);
	UpdateDropSlimeTimer();
}

#if WITH_EDITOR
void ASlugCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName PropertyName = PropertyChangedEvent.Property ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(ASlugCharacter, PassiveSlimeDropInterval))
	{
		if (HasActorBegunPlay())
		{
			SetSlimeDropInterval(PassiveSlimeDropInterval);
		}
	}
}
#endif

void ASlugCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.ClearTimer(TimerHandle_DropSlime);
}

UPatrolRouteComponent* ASlugCharacter::GetPatrolRoute() const
{
	return PatrolRoute ? PatrolRoute->GetPatrolRouteComponent() : nullptr;
}

#if EG_GAMEPLAY_DEBUGGER
void ASlugCharacter::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}State: {green}%s\n");

	// Get current state as string
	FString StateString(TEXT("Unknown"));
	if (ASlugAIController* Controller = Cast<ASlugAIController>(GetController()))
	{
		switch (Controller->GetSlugState())
		{
			case ESlugState::Roam:					{ StateString = TEXT("Roam"); break; }
			case ESlugState::Investigating:			{ StateString = TEXT("Investigating"); break; }
			case ESlugState::Retreating:			{ StateString = TEXT("Retreating"); break; }
			case ESlugState::Hunting:				{ StateString = TEXT("Hunting"); break; }
		}
	}

	String.Append(FString::Printf(*Formatting, *StateString));
}
#endif

void ASlugCharacter::ProduceSlime()
{
	SpawnNewSlime(PassiveSlimeTemplate, PassiveSlimeEffect, PassiveSlimeDuration, GetActorLocation(), GetActorRotation());
}

void ASlugCharacter::ProduceSlimeAtLocation(const FVector& Location, const FRotator& Rotation, bool bFindFloor)
{
	SpawnNewSlime(PassiveSlimeTemplate, PassiveSlimeEffect, PassiveSlimeDuration, Location, Rotation, bFindFloor);
}

void ASlugCharacter::ProduceSlimeOffHit(const FHitResult& HitResult)
{
	SpawnNewSlimeFromHit(PassiveSlimeTemplate, PassiveSlimeEffect, PassiveSlimeDuration, HitResult);
}

void ASlugCharacter::ProduceSlimeOfType(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration)
{
	SpawnNewSlime(SlimeTemplate, SlimeEffect, SlimeDuration, GetActorLocation(), GetActorRotation());
}

void ASlugCharacter::ProduceSlimeOfTypeAtLocation(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration, const FVector& Location, const FRotator& Rotation, bool bFindFloor)
{
	SpawnNewSlime(SlimeTemplate, SlimeEffect, SlimeDuration, Location, Rotation, bFindFloor);
}

void ASlugCharacter::ProduceSlimeOfTypeOffHit(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration, const FHitResult& HitResult)
{
	SpawnNewSlimeFromHit(SlimeTemplate, SlimeEffect, SlimeDuration, HitResult);
}

bool ASlugCharacter::CanDropSlime() const
{
	// Only drop slime in game
	if (HasActorBegunPlay())
	{
		if (!(bIsRagdoll || bIsDead || IsPendingKill()))
		{
			UCharacterMovementComponent* MovementComp = GetCharacterMovement();
			return MovementComp ? !MovementComp->IsFalling() : true;
		}

		return true;
	}

	return false;
}

void ASlugCharacter::SpawnNewSlimeFromHit(TSubclassOf<AMonsterEffectVolume> Template, TSubclassOf<UGameplayEffect> Effect, float Duration, const FHitResult& HitResult)
{
	if (HitResult.IsValidBlockingHit())
	{
		FVector Location = HitResult.Location;
		FRotator Rotation = FRotator::ZeroRotator;

		bool bFindFloor = true;
		if (HitResult.Component.IsValid() && HitResult.Component->Mobility != EComponentMobility::Movable)
		{
			Rotation = FRotationMatrix::MakeFromX(HitResult.ImpactNormal).Rotator();
			bFindFloor = false;
		}
		
		SpawnNewSlime(Template, Effect, Duration, Location, Rotation, bFindFloor);
	}
}

void ASlugCharacter::SpawnNewSlime(TSubclassOf<AMonsterEffectVolume> Template, TSubclassOf<UGameplayEffect> Effect, float Duration, const FVector& Location, const FRotator& Rotation, bool bFindFloor)
{
	if (CanDropSlime())
	{
		if (!Template)
		{
			//UE_LOG(LogMonster, Warning, TEXT("SlugCharacter: Unable to spawn new slime as template was null"));
			return;
		}

		SCOPE_CYCLE_COUNTER(STAT_SpawnNewSlime);

		UWorld* World = GetWorld();

		FVector SlimeLocation = Location;
		FRotator SlimeRotation = Rotation;

		if (bFindFloor)
		{
			SlimeLocation -= (FVector::UpVector * 10000.f);
			SlimeRotation.Pitch = 0.f;
			SlimeRotation.Roll = 0.f;

			// Preferably, we would want to use the slime actors collision channcel
			ECollisionChannel CollisionChannel = RootComponent->GetCollisionObjectType();
			FCollisionQueryParams QParams(TEXT("SpawnNewSlime"), GET_STATID(STAT_SpawnNewSlime), true, this);

			// TODO: need to add for some sort of filtering so we don't hit characters
			QParams.MobilityType = EQueryMobilityType::Static;

			// Find the floor to project the slime onto, we are not using the floor of
			// our character movement as the given location could be hanging off a ledge
			FHitResult Result;
			if (World->LineTraceSingleByChannel(Result, Location, SlimeLocation, CollisionChannel, QParams))
			{
				SlimeLocation = Result.Location;
				SlimeRotation = FRotationMatrix::MakeFromX(Result.ImpactNormal).Rotator();
			}
			else
			{
				return;
			}
		}

		// Spawn the new slime instigated by us
		AMonsterEffectVolume* SlimeVolume = AMonsterEffectVolume::SpawnMonsterEffectVolume(this, Template, SlimeLocation, SlimeRotation, Effect, Duration);
		if (SlimeVolume)
		{
			OnPassiveSlimeProduced(SlimeVolume);
		}
	}
}

void ASlugCharacter::SetSlimeDropInterval(float NewInterval)
{
	PendingSlimeDropInterval = FMath::Max(NewInterval, 0.1f);
}

void ASlugCharacter::OnDropSlime()
{
	ProduceSlime();
	UpdateDropSlimeTimer();
}

void ASlugCharacter::UpdateDropSlimeTimer()
{
	if (PendingSlimeDropInterval != NoSlimeDropInterval)
	{
		if (!ensure(PendingSlimeDropInterval > 0.f))
		{
			PendingSlimeDropInterval = 1.f;
		}

		// Stop current timer
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.ClearTimer(TimerHandle_DropSlime);

		// Consume pending interval
		PassiveSlimeDropInterval = PendingSlimeDropInterval;
		PendingSlimeDropInterval = NoSlimeDropInterval;

		TimerManager.SetTimer(TimerHandle_DropSlime, this, &ASlugCharacter::OnDropSlime, PassiveSlimeDropInterval, true);
	}
}

void ASlugCharacter::OnSlugEnterState(ESlugState OldState, ESlugState NewState)
{
	if (OldState != NewState)
	{
		OnEnterNewState(OldState, NewState);
	}
}
