// Copyright of Sock Goblins

#include "SlugAIController.h"
#include "SlugCharacter.h"
#include "PatrolRouteComponent.h"

#include "BehaviorTree/BlackboardComponent.h"

ASlugAIController::ASlugAIController()
{
	SlugState = ESlugState::Roam;
	SlugStateKey = TEXT("SlugState");
	PatrolRouteKey = TEXT("PatrolRoute");
	DamagedFromKey = TEXT("DamagedFrom");
}

void ASlugAIController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	if (Blackboard)
	{
		ASlugCharacter* Slug = Cast<ASlugCharacter>(Pawn);
		if (Slug)
		{
			Blackboard->SetValueAsObject(PatrolRouteKey, Slug->GetPatrolRoute());
		}
	}
}

void ASlugAIController::SetSlugState(ESlugState NewState)
{
	// We only want to notify if state is new
	if (SlugState != NewState)
	{
		ESlugState OldState = SlugState;
		SlugState = NewState;

		// Potentially set the blackboard value as well
		if (Blackboard && Blackboard->GetKeyID(SlugStateKey) != FBlackboard::InvalidKey)
		{
			// Avoid triggering value changed events if states are the same
			ESlugState BlackboardState = static_cast<ESlugState>(Blackboard->GetValueAsEnum(SlugStateKey));
			if (BlackboardState != NewState)
			{
				Blackboard->SetValueAsEnum(SlugStateKey, (uint8)NewState);
			}
		}

		// Notify our slug of our new state
		ASlugCharacter* Slug = GetSlug();
		if (Slug)
		{
			Slug->OnSlugEnterState(OldState, NewState);
		}

		//UE_LOG(LogEGCharacterAI, Log, TEXT("Slug Controller %s has entered state %i"), *GetName(), (int32)NewState);
	}
}

ASlugCharacter* ASlugAIController::GetSlug() const
{
	if (PossessedMonster.IsValid() && PossessedMonster->IsA(ASlugCharacter::StaticClass()))
	{
		return static_cast<ASlugCharacter*>(PossessedMonster.Get());
	}

	return nullptr;
}

void ASlugAIController::NotifyDamagedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	if (Blackboard)
	{
		Blackboard->SetValueAsVector(DamagedFromKey, Stimulus.StimulusLocation);
	}
}
