// Copyright of Sock Goblins

#include "MonsterCharacter.h"
#include "MonsterAIController.h"

#include "AbilitySystemComponent.h"
#include "EGCharacterAttributeSet.h"
#include "Components/SkeletalMeshComponent.h"
#include "Characters/EGCharacterMovementComponent.h"
#include "Navigation/PathFollowingComponent.h"

#include "EdensGardenAssetManager.h"
#include "EdensGardenGameState.h"

DEFINE_LOG_CATEGORY(LogMonster);

AMonsterCharacter::AMonsterCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	EyesSocket = TEXT("Eyes");

	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag(TEXT("Character.ID.Monster")));

	TeamId = EEdensGardenTeamID::Monster;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = AMonsterAIController::StaticClass();

	bCachedOrientRotationToMovement = false;
}

bool AMonsterCharacter::ShouldSaveForSaveGame() const
{
	// Don't allow saving while dead (the monsters death will
	// still be written by the recorded save data we sent on death)
	if (!bIsDead)
	{
		return !HasAllFlags(RF_Transient);
	}
	
	return false;
}

void AMonsterCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

	// Save us as being destroyed
	AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
	if (GameState)
	{
		GameState->SaveActorDestroyed(this);
	}
}

void AMonsterCharacter::OnCharacterStunned_Implementation(const FVector& Source, FGameplayTag InstigatorTag)
{
	if (AAIController* Controller = Cast<AAIController>(GetController()))
	{
		Controller->SetIgnoreMoveInput(true);
		Controller->SetIgnoreLookInput(true);

		UPathFollowingComponent* PathFollowComp = Controller->GetPathFollowingComponent();
		if (PathFollowComp)
		{
			PathFollowComp->Deactivate();
		}

		UCharacterMovementComponent* MovementComp = GetCharacterMovement();
		bCachedOrientRotationToMovement = MovementComp->bOrientRotationToMovement;
		MovementComp->bOrientRotationToMovement = false;
	}
}

void AMonsterCharacter::OnCharacterRecoveredFromStun_Implementation()
{
	if (AAIController* Controller = Cast<AAIController>(GetController()))
	{
		Controller->SetIgnoreMoveInput(false);
		Controller->SetIgnoreLookInput(false);

		UPathFollowingComponent* PathFollowComp = Controller->GetPathFollowingComponent();
		if (PathFollowComp)
		{
			PathFollowComp->Activate();
		}

		UCharacterMovementComponent* MovementComp = GetCharacterMovement();
		MovementComp->bOrientRotationToMovement = bCachedOrientRotationToMovement;
	}
}

FVector AMonsterCharacter::GetPawnViewLocation() const
{
	USkeletalMeshComponent* Mesh = GetMesh();
	if (false)//Mesh && Mesh->DoesSocketExist(EyesSocket))
	{
		return Mesh->GetSocketLocation(EyesSocket);
	}
	else
	{
		return Super::GetPawnViewLocation();
	}
}

FPrimaryAssetId AMonsterCharacter::GetPrimaryAssetId() const
{
	if (HasAnyFlags(RF_ClassDefaultObject))
	{
		return Super::GetPrimaryAssetId();
	}

	return FPrimaryAssetId(UEdensGardenAssetManager::MonsterType, GetFName());
}

void AMonsterCharacter::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame())
	{
		if (Ar.IsSaving())
		{
			FMonsterCharacterSaveData SaveData;
			SaveData.Health = GetHealth();
			
			Ar << SaveData;
		}

		if (Ar.IsLoading())
		{
			FMonsterCharacterSaveData SaveData;
			Ar << SaveData;

			UEGCharacterAttributeSet* AttributeSet = GetAttributeSet();
			if (AttributeSet)
			{
				AttributeSet->SetHealth(SaveData.Health);

				if (SaveData.Health <= 0.f)
				{
					Kill();
				}
			}
		}
	}
}
