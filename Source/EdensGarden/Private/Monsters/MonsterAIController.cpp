// Copyright of Sock Goblins

#include "MonsterAIController.h"
#include "MonsterCharacter.h"

#include "Components/CapsuleComponent.h"

#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Touch.h"

#include "Perception/AISense_Touch.h"

DECLARE_CYCLE_STAT(TEXT("MonsterAI - UpdateSightTarget"), STAT_UpdateSightTarget, STATGROUP_EGCharacterAI);

AMonsterAIController::AMonsterAIController()
{
	// Defualt perceptions
	{
		UAISenseConfig_Hearing* HearingConfig = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("HearingConfig"));
		HearingConfig->HearingRange = 2000.f;
		HearingConfig->bUseLoSHearing = false;
		HearingConfig->SetMaxAge(15.f);
		HearingConfig->DetectionByAffiliation.bDetectFriendlies = false;
		HearingConfig->DetectionByAffiliation.bDetectNeutrals = true;
		HearingConfig->DetectionByAffiliation.bDetectEnemies = true;

		UAISenseConfig_Touch* TouchConfig = CreateDefaultSubobject<UAISenseConfig_Touch>(TEXT("TouchConfig"));
		TouchConfig->SetMaxAge(5.f);

		UAISenseConfig_Damage* DamageConfig = CreateDefaultSubobject<UAISenseConfig_Damage>(TEXT("DamageConfig"));
		DamageConfig->SetMaxAge(10.f);

		PerceptionComponent->ConfigureSense(*HearingConfig);
		PerceptionComponent->ConfigureSense(*TouchConfig);
		PerceptionComponent->ConfigureSense(*DamageConfig);
	}

	InvestigateLocationKey = TEXT("InvestigateLocation");
	TouchedAtKey = TEXT("TouchedAt");

	bStopAILogicOnUnposses = true;
	bWantsPlayerState = false;  // Might be true in future
	bSetControlRotationFromPawnOrientation = true;	
}

void AMonsterAIController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	PossessedMonster = Cast<AMonsterCharacter>(Pawn);
}

void AMonsterAIController::UnPossess()
{
	Super::UnPossess();

	PossessedMonster.Reset();
}

void AMonsterAIController::OnActorsPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus)
{
	if (Actor)
	{
		bool bHandled = false;

		// Damage perception
		if (Stimulus.Type == UAISense::GetSenseID<UAISense_Damage>())
		{
			if (Stimulus.IsExpired())
			{
				NotifyDamagedByActorExpired(Actor, Stimulus);
			}
			else
			{
				NotifyDamagedByActor(Actor, Stimulus);
			}

			bHandled = true;
		}

		// Touch perception
		if (Stimulus.Type == UAISense::GetSenseID<UAISense_Touch>())
		{
			if (Stimulus.IsExpired())
			{
				NotifyTouchedByActorExpired(Actor, Stimulus);
			}
			else
			{
				NotifyTouchedByActor(Actor, Stimulus);
			}

			bHandled = true;
		}

		if (!bHandled)
		{
			Super::OnActorsPerceptionUpdated(Actor, Stimulus);
		}
	}
}

void AMonsterAIController::NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	Super::NotifyActorLostSight_Implementation(Actor, Stimulus);

	TArray<AActor*> PercievedActors;
	PerceptionComponent->GetKnownPerceivedActors(UAISense_Sight::StaticClass(), PercievedActors);

	// We only want to record the last known location if no other actors are in sight
	bool bNoneInSight = true;
	for (AActor* SeenActor : PercievedActors)
	{
		const FActorPerceptionInfo* Info = PerceptionComponent->GetActorInfo(*SeenActor);
		if (ensure(Info) && Info->IsSenseActive(UAISense::GetSenseID<UAISense_Sight>()))
		{
			bNoneInSight = false;
			continue;
		}
	}

	// Record the last known location of the enemy
	if (bNoneInSight)
	{
		if (Blackboard)
		{
			Blackboard->SetValueAsVector(InvestigateLocationKey, Stimulus.StimulusLocation);
		}
	}
}

void AMonsterAIController::NotifyActorHeard_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	Super::NotifyActorHeard_Implementation(Actor, Stimulus);

	if (Blackboard)
	{
		Blackboard->SetValueAsVector(InvestigateLocationKey, Stimulus.StimulusLocation);
	}
}

void AMonsterAIController::NotifyDamagedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s was just damaged by %s"), *GetName(), *Actor->GetName());

	if (Blackboard)
	{
		Blackboard->SetValueAsVector(InvestigateLocationKey, Stimulus.StimulusLocation);
	}
}

void AMonsterAIController::NotifyDamagedByActorExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just forgotten about being damaged by %s"), *GetName(), *Actor->GetName());
}

void AMonsterAIController::NotifyTouchedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s was just touched by %s"), *GetName(), *Actor->GetName());
	
	// Shamblers only respond to hostile actors
	const FActorPerceptionInfo* ActorInfo = PerceptionComponent->GetActorInfo(*Actor);
	if (!ActorInfo || ActorInfo->bIsHostile)
	{
		// By default, we only want to react to actors who are dynamic,
		// as static/stationary actions were mostly touched by us instead
		USceneComponent* ActorRoot = Actor->GetRootComponent();
		if (!ActorRoot || ActorRoot->Mobility == EComponentMobility::Movable)
		{
			if (Blackboard)
			{
				AMonsterCharacter* Monster = GetMonster();
				UCapsuleComponent* MonsterCapsule = ensure(Monster) ? Monster->GetCapsuleComponent() : nullptr;

				// We want to offset the touch away so its not directly on top of us
				float OffsetToApply = MonsterCapsule ? MonsterCapsule->GetScaledCapsuleRadius() * 2.f : 100.f;
				FVector Offset = (Stimulus.StimulusLocation - Stimulus.ReceiverLocation).GetSafeNormal() * OffsetToApply;

				Blackboard->SetValueAsVector(TouchedAtKey, Stimulus.StimulusLocation + Offset);
			}
		}
	}
}

void AMonsterAIController::NotifyTouchedByActorExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogEGCharacterAI, Log, TEXT("%s has just forgot about touching %s"), *GetName(), *Actor->GetName());
}

AMonsterCharacter* AMonsterAIController::GetMonster() const
{
	if (PossessedMonster.IsValid())
	{
		return PossessedMonster.Get();
	}

	return nullptr;
}

EMonsterTargetPriority AMonsterAIController::GetTargetsPriorityOverOther_Implementation(const AEGCharacter* Target, const AEGCharacter* Other) const
{
	if (!Target)
	{
		return EMonsterTargetPriority::LessPriority;
	}

	if (!Other)
	{
		return EMonsterTargetPriority::MorePriority;
	}

	// Characters use the EEdensGardenTeamID enum to set their teams.
	// The teams with higher priority have higher ID values
	int32 TargetPriority = Target->GetGenericTeamId().GetId();
	int32 OtherPriority = Other->GetGenericTeamId().GetId();

	if (TargetPriority > OtherPriority)
	{
		return EMonsterTargetPriority::MorePriority;
	}
	else if (TargetPriority < OtherPriority)
	{
		return EMonsterTargetPriority::LessPriority;
	}
	else
	{
		return EMonsterTargetPriority::SamePriority;
	}
}

AActor* AMonsterAIController::UpdateSightTarget()
{
	AMonsterCharacter* Monster = GetMonster();
	if (Monster)
	{
		SCOPE_CYCLE_COUNTER(STAT_UpdateSightTarget);

		FAISenseID SightSenseID = UAISense::GetSenseID<UAISense_Sight>();

		AActor* ClosestTarget = nullptr;
		float ClosestTargetDist = MAX_FLT;

		// The closest actor but as a character, so priority can be compared
		AEGCharacter* ClosestCharacter = nullptr;

		for (auto Iterator = PerceptionComponent->GetPerceptualDataConstIterator(); Iterator; ++Iterator)
		{
			AActor* Actor = Iterator->Key;
			const FActorPerceptionInfo& Info = Iterator->Value;

			// Monsters only react to hostile opponents
			if (Info.bIsHostile && Info.LastSensedStimuli.IsValidIndex(SightSenseID))
			{
				// We are only considering actors still in sight of the monster
				const FAIStimulus& SightStimulus = Info.LastSensedStimuli[SightSenseID];
				if (SightStimulus.IsActive())
				{
					bool bSet = false;

					// Need to check if this actor has higher priority then current character
					AEGCharacter* Character = Cast<AEGCharacter>(Actor);
					if (Character)
					{
						// Ignore this character entirely if they are dead
						if (Character->IsDead())
						{
							continue;
						}

						switch (GetTargetsPriorityOverOther(Character, ClosestCharacter))
						{
							// Skip processing this character as we shouldn't target it
							case EMonsterTargetPriority::LessPriority:
							{
								continue;
							}
							case EMonsterTargetPriority::MorePriority:
							{
								// Override current target with this character
								bSet = true;
								break;
							}
						}
					}

					// Determine based on distance, even if we are already setting due
					// to target having higher priority. We need to record the distance
					// in-case another actor with same priority is in sight
					float Distance = Monster->GetSquaredDistanceTo(Actor);
					if (!bSet)
					{
						bSet = Distance < ClosestTargetDist;
					}

					// Record this actor for next iteration
					if (bSet)
					{
						ClosestTarget = Actor;
						ClosestTargetDist = Distance;
						ClosestCharacter = Character;
					}
				}
			}
		}

		SightTarget = ClosestTarget;
	}

	return SightTarget.Get();
}

AActor* AMonsterAIController::GetSightTarget() const
{
	if (SightTarget.IsValid())
	{
		return SightTarget.Get();
	}

	return nullptr;
}

AEGCharacter* AMonsterAIController::GetSightTargetAsCharacter() const
{
	if (SightTarget.IsValid())
	{
		return Cast<AEGCharacter>(SightTarget.Get());
	}

	return nullptr;
}
