// Copyright of Sock Goblins

#include "CheckpointStart.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"

#include "EdensGardenGameState.h"
#include "Engine/World.h"
#include "GameFramework/Pawn.h"

const FName ACheckpointStart::StartingCheckpointName(TEXT("StartingCheckpoint"));

ACheckpointStart::ACheckpointStart(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	PlayerStartTag = StartingCheckpointName;
	CheckpointIndex = 0;
	bSaveOnTrigger = true;

	CheckpointTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("CheckpointTrigger"));
	CheckpointTrigger->SetupAttachment(GetCapsuleComponent());
	CheckpointTrigger->SetMobility(EComponentMobility::Static);
	CheckpointTrigger->InitBoxExtent(FVector(100.f, 300.f, 100.f));
	CheckpointTrigger->SetSimulatePhysics(false);
	CheckpointTrigger->SetCollisionProfileName(TEXT("Trigger"));
	CheckpointTrigger->bGenerateOverlapEvents = true;

	CheckpointTrigger->OnComponentBeginOverlap.AddDynamic(this, &ACheckpointStart::OnCheckpointTriggerOverlapped);
}

void ACheckpointStart::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame())
	{
		if (Ar.IsSaving())
		{
			FCheckpointStartSaveData SaveData;
			SaveData.bSaveOnTrigger = bSaveOnTrigger;

			Ar << SaveData;
		}

		if (Ar.IsLoading())
		{
			FCheckpointStartSaveData SaveData;
			Ar << SaveData;

			bSaveOnTrigger = SaveData.bSaveOnTrigger;
		}
	}
}

void ACheckpointStart::SetSaveOnTrigger(bool bEnable)
{
	if (bSaveOnTrigger != bEnable)
	{
		bSaveOnTrigger = bEnable;

		// Player might already be in trigger volume
		TArray<AActor*> OverlappedActors;
		CheckpointTrigger->GetOverlappingActors(OverlappedActors, APawn::StaticClass());
		for (AActor* Actor : OverlappedActors)
		{
			APawn* Pawn = CastChecked<APawn>(Actor);
			if (Pawn && Pawn->IsPlayerControlled())
			{
				NotifyGameStateCheckpointReached();
			}
		}
	}
}

void ACheckpointStart::OnCheckpointTriggerOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	if (bSaveOnTrigger)
	{
		APawn* Pawn = Cast<APawn>(OtherActor);
		if (Pawn && Pawn->IsPlayerControlled())
		{
			NotifyGameStateCheckpointReached();
		}
	}
}

void ACheckpointStart::NotifyGameStateCheckpointReached() const
{
	// TODO: Preferably have either some sort of checkpoint manager or
	// the game mode should handle this, but for now we will handle it here
	// Might also need to check if its safe to save here, but check to make sure
	// its safe to save right now (if no monsters are nearby sort of thing

	AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
	if (GameState)
	{
		GameState->NotifyCheckpointReached(CheckpointIndex, PlayerStartTag);
	}
}
