// Copyright of Sock Goblins

#include "SpecOpInventory.h"
#include "EdensGardenAssetManager.h"

USpecOpInventory::USpecOpInventory()
{
	GunInventorySize = 4;
}

FPrimaryAssetId USpecOpInventory::GetPrimaryAssetId() const
{
	return FPrimaryAssetId(UEdensGardenAssetManager::SpecOpInventoryType, GetFName());
}

#if WITH_EDITOR
void USpecOpInventory::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (Guns.Num() >= GunInventorySize)
	{
		Guns.SetNum(GunInventorySize);
	}
}
#endif