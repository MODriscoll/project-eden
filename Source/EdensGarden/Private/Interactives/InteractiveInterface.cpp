// Copyright of Sock Goblins

#include "InteractiveInterface.h"

DEFINE_LOG_CATEGORY(LogInteractive);

#define LOCTEXT_NAMESPACE "Interactive"

void IInteractiveInterface::OnInteractiveGainFocus_Implementation(ASpecOpCharacter* Character)
{
	UE_LOG(LogInteractive, Log, TEXT("Interactive Gain Focus"));
}

void IInteractiveInterface::OnInteractiveLoseFocus_Implementation(ASpecOpCharacter* Character)
{
	UE_LOG(LogInteractive, Log, TEXT("Interactive Lose Focus"));
}

void IInteractiveInterface::GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& HighlightColor) const
{
	DisplayTitle = LOCTEXT("InteractiveDisplayTitle", "Interactive");
	HighlightColor = FLinearColor::Green;
}

#undef LOCTEXT_NAMESPACE