// Copyright of Sock Goblins

#include "InteractiveActor.h"
#include "Components/StaticMeshComponent.h"

AInteractiveActor::AInteractiveActor()
{
	PrimaryActorTick.bCanEverTick = false;

	HighlightColor = FLinearColor::Green;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionProfileName(INTERACTIVE_PROFILE);
	Mesh->SetMobility(EComponentMobility::Static);
}

void AInteractiveActor::GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const
{
	DisplayTitle = InteractiveTitle;
	DisplayColor = HighlightColor;
}

float AInteractiveActor::GetInteractDuration_Implementation() const
{
	return InteractiveDuration;
}
