// Copyright of Sock Goblins

#include "Tasks/AbilityTask_Interact.h"
#include "AbilitySystemComponent.h"
#include "Interactives/InteractiveComponent.h"
#include "SpecOp/SpecOpCharacter.h"

void UAbilityTask_Interact::OnReleaseCallback()
{
	UInteractiveComponent* IntComp = MySpecOp->GetInteractiveArea();
	IntComp->StopInteracting();
}

void UAbilityTask_Interact::OnInteractionFinishCallback(bool bCancelled)
{
	UnbindCallbacks();

	if (ShouldBroadcastAbilityTaskDelegates())
	{
		if (bIsInteracting)
		{
			InteractionFinished.Broadcast(bCancelled);
		}
		else
		{
			InstantInteraction.Broadcast(bCancelled);
		}
	}

	EndTask();
}

void UAbilityTask_Interact::OnAbilityCancelled()
{
	UInteractiveComponent* IntComp = MySpecOp->GetInteractiveArea();
	IntComp->StopInteracting();
}

UAbilityTask_Interact* UAbilityTask_Interact::Interact(UGameplayAbility* OwningAbility)
{
	UAbilityTask_Interact* Task = NewAbilityTask<UAbilityTask_Interact>(OwningAbility);
	return Task;
}

void UAbilityTask_Interact::Activate()
{
	if (!Ability)
	{
		return;
	}

	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(GetAvatarActor());
	if (SpecOp)
	{
		CancelledHandle = Ability->OnGameplayAbilityCancelled.AddUObject(this, &UAbilityTask_Interact::OnAbilityCancelled);

 		UInteractiveComponent* IntComp = SpecOp->GetInteractiveArea();
		InteractHandle = IntComp->OnInteractionFinished.AddUObject(this, &UAbilityTask_Interact::OnInteractionFinishCallback);

		ReleaseHandle = AbilitySystemComponent->AbilityReplicatedEventDelegate(EAbilityGenericReplicatedEvent::InputReleased, GetAbilitySpecHandle(), GetActivationPredictionKey()).AddUObject(this, &UAbilityTask_Interact::OnReleaseCallback);

		MySpecOp = SpecOp;

		// Mark interacting as false. If the interaction is instant,
		// the callback will be called before we set this flag to true
		bIsInteracting = false;

		// Needs to be called last, as its possible the closest interactive is instant
		if (IntComp->StartInteracting())
		{
			bIsInteracting = true;
		}
		else
		{
			if (ShouldBroadcastAbilityTaskDelegates())
			{
				NoInteraction.Broadcast(false);
			}

			UnbindCallbacks();
		}
	}

	SetWaitingOnRemotePlayerData();
}

void UAbilityTask_Interact::ExternalCancel()
{
	Super::ExternalCancel();
	OnAbilityCancelled();
}

void UAbilityTask_Interact::OnDestroy(bool bAbilityEnded)
{
	if (Ability)
	{
		Ability->OnGameplayAbilityCancelled.Remove(CancelledHandle);
	}

	Super::OnDestroy(bAbilityEnded);
}

void UAbilityTask_Interact::UnbindCallbacks()
{
	AbilitySystemComponent->AbilityReplicatedEventDelegate(EAbilityGenericReplicatedEvent::InputReleased, GetAbilitySpecHandle(), GetActivationPredictionKey()).Remove(ReleaseHandle);

	UInteractiveComponent* IntComp = MySpecOp->GetInteractiveArea();
	IntComp->OnInteractionFinished.Remove(InteractHandle);
}
