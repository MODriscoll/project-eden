// Copyright of Sock Goblins

#include "InteractiveComponent.h"
#include "Camera/CameraComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "SpecOp/SpecOpCharacter.h"

#include "EdensGardenFunctionLibrary.h"
#include "InteractiveInterface.h"
#include "TimerManager.h"

DECLARE_CYCLE_STAT(TEXT("Interactive - FindClosestInteractive"), STAT_FindClosestInteractive, STATGROUP_SpecOp);

UInteractiveComponent::UInteractiveComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
	bGenerateOverlapEvents = true;
	SetCollisionProfileName(INTERACTIVEQUERY_PROFILE);

	bOnlyAcknowledgeReadyInteractives = false;

	bAutoRefresh = true;
	RefreshInterval = 0.1f;

	OnComponentBeginOverlap.AddDynamic(this, &UInteractiveComponent::InteractAreaBeginOverlap);
	OnComponentEndOverlap.AddDynamic(this, &UInteractiveComponent::InteractAreaEndOverlap);

	bInteractiveLock = false;
}

void UInteractiveComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (!SpecOpOwner)
	{
		SpecOpOwner = Cast<ASpecOpCharacter>(GetOwner());
	}
}

bool UInteractiveComponent::StartInteracting()
{
	if (!ClosestInteractive || bIsInteracting || !SpecOpOwner)
	{
		return false;
	}

	check(ClosestInteractive->Implements<UInteractiveInterface>());
	
	// Make sure we can still interact with this object
	if (IInteractiveInterface::Execute_CanInteractWith(ClosestInteractive, SpecOpOwner))
	{
		float Duration = IInteractiveInterface::Execute_GetInteractDuration(ClosestInteractive);
		bool bShouldHold = Duration > 0.f;

		if (bShouldHold)
		{
			FTimerManager& TimerManager = GetWorld()->GetTimerManager();
			TimerManager.SetTimer(TimerHandle_Interaction, this, &UInteractiveComponent::OnDurationComplete, Duration, false);

			UE_LOG(LogInteractive, Log, TEXT("Starting iteraction with %s"), *ClosestInteractive->GetName());

			bInteractiveLock = true;

			IInteractiveInterface::Execute_OnInteractStart(ClosestInteractive, SpecOpOwner);
			bIsInteracting = true;
			CachedInteractDuration = Duration;

			bInteractiveLock = false;

			OnInteractionStart.Broadcast();
		}
		else
		{
			// We are waiting to next tick before interacting, as this function could possibly be called
			// during an ability system update, which would block other abilities from being activated (during this frame)
			FTimerManager& TimerManager = GetWorld()->GetTimerManager();
			TimerManager.SetTimerForNextTick(this, &UInteractiveComponent::OnInstantComplete);

			bIsInteracting = true;
		}

		return true;
	}
	else
	{
		IInteractiveInterface::Execute_OnInteractFailed(ClosestInteractive);

		UpdateClosestInteractive();
		bIsInteracting = false;
	}

	return false;
}

void UInteractiveComponent::StopInteracting()
{
	StopInteraction(false);
}

void UInteractiveComponent::StopInteraction(bool bHoldTimeComplete)
{
	if (!ClosestInteractive || !bIsInteracting || !SpecOpOwner)
	{
		return;
	}

	check(ClosestInteractive->Implements<UInteractiveInterface>());

	bool bCancelled = !bHoldTimeComplete;

	if (bCancelled)
	{
		FTimerManager& TimerManager = GetWorld()->GetTimerManager();
		TimerManager.ClearTimer(TimerHandle_Interaction);
	}

	bInteractiveLock = true;

	// Possible chance this function call could ultimately result in the closest interactive being updated (new object or null)
	// This could be due it calling UpdateClosestInteractive or it disabling its collision, resulting in the end of overlap
	IInteractiveInterface::Execute_OnInteractEnd(ClosestInteractive, SpecOpOwner, bCancelled);
	bIsInteracting = false;
	CachedInteractDuration = 0.f;

	UE_LOG(LogInteractive, Log, TEXT("Stopping interaction with %s. Was Completed = %s"), *ClosestInteractive->GetName(), bHoldTimeComplete ? TEXT("True") : TEXT("False"));

	bInteractiveLock = false;

	// Update closest interactive now, as interaction could have just changed it
	UpdateClosestInteractive();
	OnInteractionFinished.Broadcast(bCancelled);
}

void UInteractiveComponent::OnInstantComplete()
{
	if (ClosestInteractive && SpecOpOwner)
	{
		check(ClosestInteractive->Implements<UInteractiveInterface>());

		bInteractiveLock = true;

		// Possible chance this function call could ultimately result in the closest interactive being updated (new object or null)
		// This could be a result of this calling it directly or it disabling its collision, resulting in the end of overlap
		IInteractiveInterface::Execute_OnInteract(ClosestInteractive, SpecOpOwner);
		bIsInteracting = false;

		UE_LOG(LogInteractive, Log, TEXT("Instantly interacted with %s"), *ClosestInteractive->GetName());

		bInteractiveLock = false;

		// Update closest interactive now, as interaction could have just changed it
		UpdateClosestInteractive();
		OnInteractionFinished.Broadcast(false);
	}
}

void UInteractiveComponent::OnDurationComplete()
{
	StopInteraction(true);
}

AActor* UInteractiveComponent::FindClosestInteractive() const
{
	SCOPE_CYCLE_COUNTER(STAT_FindClosestInteractive);

	AActor* Closest = nullptr;

	// Some interactives will need to read the players
	// state to determine if they can be interacted with
	if (SpecOpOwner && SpecOpOwner->CanInteract())
	{
		float ClosestDistance = MAX_FLT;

		for (AActor* Actor: Interactives)
		{
			check(Actor != nullptr);

			// This interactive may not be interactive right now
			if (!bOnlyAcknowledgeReadyInteractives || IInteractiveInterface::Execute_CanInteractWith(Actor, SpecOpOwner))
			{
				float Distance = SpecOpOwner->GetSquaredDistanceTo(Actor);
				if (Distance < ClosestDistance)
				{
					Closest = Actor;
					ClosestDistance = Distance;
				}
			}
		}
	}
	else
	{
		if (!SpecOpOwner)
		{
			UE_LOG(LogInteractive, Warning, TEXT("Unable to find closest interactive as owner was null"));
		}
	}

	return Closest;
}

AActor* UInteractiveComponent::UpdateClosestInteractive(bool bStopInteraction)
{
	AActor* NewClosestInteractive = FindClosestInteractive();
	if (NewClosestInteractive != ClosestInteractive)
	{
		if (bStopInteraction)
		{
			// This might be called from an interact end function,
			// no need to call it two times in a row
			if (bIsInteracting && !bInteractiveLock)
			{
				StopInteraction(false);
			}
		}

		if (!bIsInteracting)
		{
			if (ClosestInteractive && !ClosestInteractive->IsActorBeingDestroyed())
			{
				IInteractiveInterface::Execute_OnInteractiveLoseFocus(ClosestInteractive, SpecOpOwner);
			}

			// We don't want to reference an actor being destroyed
			if (NewClosestInteractive && NewClosestInteractive->IsActorBeingDestroyed())
			{
				ClosestInteractive = nullptr;
			}
			else
			{
				ClosestInteractive = NewClosestInteractive;
			}

			if (ClosestInteractive)
			{
				IInteractiveInterface::Execute_OnInteractiveGainFocus(ClosestInteractive, SpecOpOwner);
			}

			OnClosestInteractiveChanged.Broadcast(ClosestInteractive);
		}
	}

	// Refresh display info (this might have been called from an
	// interactive that only wants to update the display data)
	{
		FText DisplayTitle = FText::GetEmpty();
		FLinearColor HighlightColor = FLinearColor::Red;

		if (ClosestInteractive)
		{
			IInteractiveInterface::Execute_GetInteractiveDisplayInfo(ClosestInteractive, SpecOpOwner, DisplayTitle, HighlightColor);
		}

		UEdensGardenFunctionLibrary::HighlightActor(this, ClosestInteractive, HighlightColor);
		OnClosestInteractiveUpdated.Broadcast(ClosestInteractive, DisplayTitle);
	}

	return ClosestInteractive;
}

float UInteractiveComponent::GetTimeTillInteractComplete() const
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	if (TimerManager.IsTimerActive(TimerHandle_Interaction))
	{
		return TimerManager.GetTimerRemaining(TimerHandle_Interaction);
	}

	return 0.f;
}

float UInteractiveComponent::GetInteractionProgress() const
{
	if (bIsInteracting)
	{
		float TimeRemaining = GetTimeTillInteractComplete();
		if (!FMath::IsNearlyZero(TimeRemaining))
		{
			return (CachedInteractDuration - TimeRemaining) / CachedInteractDuration;
		}
	}

	return 0.f;
}

void UInteractiveComponent::InteractAreaBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == GetOwner())
	{
		return;
	}

	if (OtherActor->Implements<UInteractiveInterface>())
	{
		Interactives.AddUnique(OtherActor);
		UpdateClosestInteractive(false);

		// Only activate timer on initial interactive found
		if (Interactives.Num() == 1)
		{
			ActivateRefreshTimer();
		}
	}
}

void UInteractiveComponent::InteractAreaEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == GetOwner())
	{
		return;
	}

	int32 Index = Interactives.Find(OtherActor);
	if (Index != INDEX_NONE)
	{
		Interactives.RemoveAt(Index);
		UpdateClosestInteractive(true);

		// No longer need to auto refresh if no interactive is in range
		if (Interactives.Num() <= 0)
		{
			DeActivateRefreshTimer();
		}
	}
}

void UInteractiveComponent::SetAutoRefreshClosestInteractive(bool bAuto, float Interval)
{
	if (bAuto != bAutoRefresh)
	{
		bAutoRefresh = bAuto;

		if (bAutoRefresh)
		{
			RefreshInterval = FMath::Max(0.05f, Interval);
			ActivateRefreshTimer();
		}
		else
		{
			DeActivateRefreshTimer();
		}
	}
	// Update timer if interval has changed
	else if (bAutoRefresh)
	{
		Interval = FMath::Max(0.05f, Interval);
		if (Interval != RefreshInterval)
		{
			RefreshInterval = Interval;
			ActivateRefreshTimer();
		}
	}
}

void UInteractiveComponent::ActivateRefreshTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(TimerHandle_RefreshClosest, this, &UInteractiveComponent::OnAutoRefresh, RefreshInterval, true);
}

void UInteractiveComponent::DeActivateRefreshTimer()
{
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(TimerHandle_RefreshClosest);
}

void UInteractiveComponent::OnAutoRefresh()
{
	// We don't want to override the interactive
	// if its currently being interacted with
	UpdateClosestInteractive(false);
}