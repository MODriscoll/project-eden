// Copyright of Sock Goblins

#include "EdensGardenTypes.h"

#include "GameplayAbility.h"
#include "GenericTeamAgentInterface.h"

bool FSpecOpAbilityInputBinding::IsValid() const
{
	return (*Ability != nullptr);
}

FTeamBasedTargetDataFilter::FTeamBasedTargetDataFilter()
{
	bFilterOutFriendlies = true;
	bFilterOutNeutrals = false;
	bFilterOutHostiles = false;
}

bool FTeamBasedTargetDataFilter::FilterPassesForActor(const AActor* ActorToBeFiltered) const
{
	// Require original filter to also pass
	if (FGameplayTargetDataFilter::FilterPassesForActor(ActorToBeFiltered))
	{
		switch (GetAttitudeTowards(ActorToBeFiltered))
		{
			case ETeamAttitude::Friendly:
			{
				if (bFilterOutFriendlies)
				{
					return (bReverseFilter ^ false);
				}
				break;
			}
			case ETeamAttitude::Neutral:
			{
				if (bFilterOutNeutrals)
				{
					return (bReverseFilter ^ false);
				}
				break;
			}
			case ETeamAttitude::Hostile:
			{
				if (bFilterOutHostiles)
				{
					return (bReverseFilter ^ false);
				}
				break;
			}
		}

		return (bReverseFilter ^ true);
	}

	return false;
}

ETeamAttitude::Type FTeamBasedTargetDataFilter::GetAttitudeTowards(const AActor* Actor) const
{
	return FGenericTeamId::GetAttitude(SelfActor, Actor);
}
