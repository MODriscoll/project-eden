// Copyright of Sock Goblins

#include "AnimNotify_SpecOpMakeNoise.h"
#include "Components/SkeletalMeshComponent.h"

#include "SpecOpCharacter.h"

void UAnimNotify_SpecOpMakeNoise::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		// Modifier has the chance of being zero
		float ActualLoudness = Loudness * SpecOp->GetSoundModifier();
		if (!FMath::IsNearlyZero(ActualLoudness))
		{
			SpecOp->MakeNoise(ActualLoudness, SpecOp, FVector::ZeroVector, MaxRange, NoiseTag.GetTagName());
		}
	}
	else
	{
		Super::Notify(MeshComp, Animation);
	}
}
