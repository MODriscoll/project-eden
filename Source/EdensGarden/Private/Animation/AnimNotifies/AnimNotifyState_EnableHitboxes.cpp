// Copyright of Sock Goblins

#include "AnimNotifyState_EnableHitboxes.h"
#include "Components/SkeletalMeshComponent.h"

#include "Gameplay/HitboxInterface.h"

UAnimNotifyState_EnableHitboxes::UAnimNotifyState_EnableHitboxes()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Red;
	#endif
}

void UAnimNotifyState_EnableHitboxes::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	Super::NotifyBegin(MeshComp, Animation, TotalDuration);

	AActor* Owner = MeshComp->GetOwner();
	if (Owner && Owner->Implements<UHitboxInterface>())
	{
		IHitboxInterface::Execute_EnableHitboxes(Owner, ActivationTag, HitboxTags);
	}
}

void UAnimNotifyState_EnableHitboxes::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::NotifyEnd(MeshComp, Animation);

	AActor* Owner = MeshComp->GetOwner();
	if (Owner && Owner->Implements<UHitboxInterface>())
	{
		IHitboxInterface::Execute_DisableHitboxes(Owner, ActivationTag, HitboxTags);
	}
}

