// Copyright of Sock Goblins

#include "Animation/AnimNotifies/AnimNotify_GameplayEvent.h"
#include "AbilitySystemBlueprintLibrary.h"

UAnimNotify_GameplayEvent::UAnimNotify_GameplayEvent()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Orange;
	#endif
}

FString UAnimNotify_GameplayEvent::GetNotifyName_Implementation() const
{
	return FString::Printf(TEXT("%s Tag Event"), *EventTag.ToString());
}

void UAnimNotify_GameplayEvent::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	// TODO: have some optional payload data to maybe send?
	UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(MeshComp->GetOwner(), EventTag, FGameplayEventData());
}


