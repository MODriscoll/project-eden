// Copyright of Sock Goblins

#include "AnimNotify_MakeNoise.h"
#include "Components/SkeletalMeshComponent.h"

#include "GameFramework/Pawn.h"

UAnimNotify_MakeNoise::UAnimNotify_MakeNoise()
{
	Loudness = 1.f;
	MaxRange = 0.f;

	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Emerald;
	#endif
}

void UAnimNotify_MakeNoise::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	APawn* Pawn = Cast<APawn>(MeshComp->GetOwner());
	if (Pawn)
	{
		Pawn->MakeNoise(Loudness, Pawn, FVector::ZeroVector, MaxRange, NoiseTag.GetTagName());
	}
}
