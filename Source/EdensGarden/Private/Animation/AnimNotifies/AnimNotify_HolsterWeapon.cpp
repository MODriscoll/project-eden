// Copyright of Sock Goblins

#include "AnimNotify_HolsterWeapon.h"
#include "Components/SkeletalMeshComponent.h"

#include "SpecOp/SpecOpCharacter.h"

UAnimNotify_HolsterWeapon::UAnimNotify_HolsterWeapon()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Magenta;
	#endif
}

void UAnimNotify_HolsterWeapon::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		SpecOp->PhysicallyHolsterWeapon();
	}
}
