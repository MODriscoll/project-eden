// Copyright of Sock Goblins

#include "AnimNotify_Ragdoll.h"
#include "Components/SkeletalMeshComponent.h"

#include "Characters/EGCharacter.h"

UAnimNotify_Ragdoll::UAnimNotify_Ragdoll()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor(255, 105, 180); // Hot Pink
	#endif
}

void UAnimNotify_Ragdoll::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	AEGCharacter* Character = Cast<AEGCharacter>(MeshComp->GetOwner());
	if (Character)
	{
		Character->EnterRagdoll();
	}
}
