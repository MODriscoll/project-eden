// Copyright of Sock Goblins

#include "AnimNotify_PlayWeaponSound.h"
#include "Components/SkeletalMeshComponent.h"

#include "Guns/Gun.h"
#include "SpecOp/SpecOpCharacter.h"
#include "Weapons/MeleeWeapon.h"

UAnimNotify_PlayWeaponSound::UAnimNotify_PlayWeaponSound()
{
	bPlayFromMelee = true;

	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Emerald;
	#endif
}

void UAnimNotify_PlayWeaponSound::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (!Sound)
	{
		return;
	}

	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		AWeaponBase* Weapon = nullptr;

		if (bPlayFromMelee)
		{
			Weapon = SpecOp->GetMeleeWeapon();
		}
		else
		{
			Weapon = SpecOp->GetEquippedGun();
		}

		if (Weapon)
		{
			Weapon->PlayWeaponSound(Sound);
		}
	}
}
