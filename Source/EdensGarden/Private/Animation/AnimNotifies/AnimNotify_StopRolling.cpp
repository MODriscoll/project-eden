// Copyright of Sock Goblins

#include "AnimNotify_StopRolling.h"
#include "Components/SkeletalMeshComponent.h"

#include "SpecOp/SpecOpCharacter.h"

UAnimNotify_StopRolling::UAnimNotify_StopRolling()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Silver;
	#endif
}

void UAnimNotify_StopRolling::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ASpecOpCharacter* Character = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (Character && Character->IsRolling())
	{
		Character->StopRolling();
	}
}
