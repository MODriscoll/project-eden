// Copyright of Sock Goblins

#include "AnimNotify_StopStun.h"
#include "Components/SkeletalMeshComponent.h"

#include "Characters/EGCharacter.h"

UAnimNotify_StopStun::UAnimNotify_StopStun()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Blue;
	#endif
}

void UAnimNotify_StopStun::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	AEGCharacter* Character = Cast<AEGCharacter>(MeshComp->GetOwner());
	if (Character)
	{
		Character->StopStun();
	}
}
