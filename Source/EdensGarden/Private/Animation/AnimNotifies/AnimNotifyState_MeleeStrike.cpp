// Copyright of Sock Goblins

#include "AnimNotifyState_MeleeStrike.h"
#include "Components/SkeletalMeshComponent.h"

#include "Gameplay/HitboxInterface.h"
#include "SpecOp/SpecOpCharacter.h"
#include "Weapons/MeleeWeapon.h"

UAnimNotifyState_MeleeStrike::UAnimNotifyState_MeleeStrike()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Magenta;
	#endif
}

void UAnimNotifyState_MeleeStrike::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration)
{
	Super::NotifyBegin(MeshComp, Animation, TotalDuration);

	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		AMeleeBase* Melee = SpecOp->GetMeleeWeapon();
		if (Melee)
		{
			IHitboxInterface::Execute_EnableHitboxes(Melee, ActivationTag, HitboxTags);
		}
	}
}

void UAnimNotifyState_MeleeStrike::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::NotifyEnd(MeshComp, Animation);
		
	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		AMeleeBase* Melee = SpecOp->GetMeleeWeapon();
		if (Melee)
		{
			IHitboxInterface::Execute_DisableHitboxes(Melee, ActivationTag, HitboxTags);
		}
	}
}
