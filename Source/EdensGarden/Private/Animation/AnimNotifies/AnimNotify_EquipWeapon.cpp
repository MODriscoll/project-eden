// Copyright of Sock Goblins

#include "AnimNotify_EquipWeapon.h"
#include "Components/SkeletalMeshComponent.h"

#include "SpecOp/SpecOpCharacter.h"

UAnimNotify_EquipWeapon::UAnimNotify_EquipWeapon()
{
	#if WITH_EDITORONLY_DATA
	NotifyColor = FColor::Cyan;
	#endif
}

void UAnimNotify_EquipWeapon::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(MeshComp->GetOwner());
	if (SpecOp)
	{
		SpecOp->PhysicallyEquipWeapon();
	}
}
