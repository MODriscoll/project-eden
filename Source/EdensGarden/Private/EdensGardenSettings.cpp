// Copyright of Sock Goblins

#include "EdensGardenSettings.h"

UEdensGardenSettings::UEdensGardenSettings()
{
	CategoryName = FName(TEXT("Game"));

	GameSaveSlot = FString("PersistantSaveSlot");
	bCanSaveInEditor = false;
	EditorSaveSlot = FString("EditorSaveSlot");
}

UEdensGardenSettings* UEdensGardenSettings::Get()
{
	return CastChecked<UEdensGardenSettings>(UEdensGardenSettings::StaticClass()->GetDefaultObject());
}