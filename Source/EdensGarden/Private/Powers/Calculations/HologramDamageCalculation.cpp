// Copyright of Sock Goblins

#include "Calculations/HologramDamageCalculation.h"
#include "EGAttributeSet.h"

#include "GenericTeamAgentInterface.h"

struct HologramDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Health);

	HologramDamageStatics()
	{
		// This calculation should be used on target

		// Use the damage we were created with
		DEFINE_ATTRIBUTE_CAPTUREDEF(UEGAttributeSet, Damage, Target, true);

		// Use the targets current health
		DEFINE_ATTRIBUTE_CAPTUREDEF(UEGAttributeSet, Health, Target, false);
	}
};

static const HologramDamageStatics& GetHologramDamageStatics()
{
	static HologramDamageStatics DmgStatics;
	return DmgStatics;
}

UHologramDamageCalculation::UHologramDamageCalculation()
{
	const HologramDamageStatics& DmgStatics = GetHologramDamageStatics();

	RelevantAttributesToCapture.Add(DmgStatics.DamageDef);
	RelevantAttributesToCapture.Add(DmgStatics.HealthDef);

#	if WITH_EDITOR
	InvalidScopedModifierAttributes.Add(DmgStatics.HealthDef);
	#endif
}

void UHologramDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const HologramDamageStatics& DmgStatics = GetHologramDamageStatics();

	UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
	UAbilitySystemComponent* TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();

	AActor* SourceActor = SourceASC ? SourceASC->AvatarActor : nullptr;
	AActor* TargetActor = TargetASC ? TargetASC->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float Damage = 0.f;
	if (ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DmgStatics.DamageDef, EvaluationParameters, Damage))
	{
		// We will only ignore applying this damage if both avatars are on the same team (friends)
		// We will still apply damage if both avatars happen to be the same
		IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(SourceActor);
		if (!TeamAgent || !TargetActor || SourceActor == TargetActor ||
			TeamAgent->GetTeamAttitudeTowards(*TargetActor) != ETeamAttitude::Friendly)
		{
			float Health = 0.f;
			ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DmgStatics.HealthDef, EvaluationParameters, Health);

			// The health should not drop below one, so we don't kill the target
			float NewHealth = FMath::Max(1.f, Health - Damage);

			// The damage to apply based on clamped new health
			float Alpha = FMath::Max(0.f, Health - NewHealth);

			// Damage should only ever be possible
			if (Alpha > 0.f)
			{
				OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DmgStatics.DamageProperty, EGameplayModOp::Additive, Alpha));
			}
		}
	}
}
