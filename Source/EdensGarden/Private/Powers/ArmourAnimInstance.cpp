// Copyright of Sock Goblins

#include "ArmourAnimInstance.h"

void UArmourAnimInstance::SetMeshToCopy(USkeletalMeshComponent* Mesh)
{
	ArmourMesh = Mesh;
}

USkeletalMeshComponent* UArmourAnimInstance::GetSkeletalMeshToCopy() const
{
	return ArmourMesh;
}
