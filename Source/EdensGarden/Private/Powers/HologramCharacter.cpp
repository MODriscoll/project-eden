// Copyright of Sock Goblins

#include "HologramCharacter.h"
#include "HologramAIController.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

#include "AbilitySystemBlueprintLibrary.h"

AHologramCharacter::AHologramCharacter()
{
	PrimaryActorTick.bCanEverTick = false;
	AIControllerClass = AHologramAIController::StaticClass();
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	bUseControllerRotationYaw = false;
	InitialLifeSpan = 10.f;

	InheritYawRotationSpeed = 5.f;
	InheritYawRotationSpeedWhileFiring = 15.f;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	WeaponMesh->SetupAttachment(GetMesh(), TEXT("LancerSocket"));
	WeaponMesh->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	WeaponMesh->SetSimulatePhysics(false);

	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->MaxWalkSpeed = 400.f;
	MovementComponent->bOrientRotationToMovement = true;

	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag(TEXT("Character.ID.Hologram")));
	TeamId = EEdensGardenTeamID::Hologram;

	bIsInFiringState = false;
}

FVector AHologramCharacter::GetPawnViewLocation() const
{
	const FName EyesSocket = TEXT("Eyes");

	USkeletalMeshComponent* Mesh = GetMesh();
	if (Mesh && Mesh->DoesSocketExist(EyesSocket))
	{
		return Mesh->GetSocketLocation(EyesSocket);
	}
	else
	{
		return Super::GetPawnViewLocation();
	}
}

void AHologramCharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	if (IsStunned())
	{
		return;
	}

	if (bUseControllerRotationPitch || bUseControllerRotationYaw || bUseControllerRotationRoll)
	{
		const FRotator CurrentRotation = GetActorRotation();

		if (!bUseControllerRotationPitch)
		{
			NewControlRotation.Pitch = CurrentRotation.Pitch;
		}

		if (!bUseControllerRotationYaw)
		{
			NewControlRotation.Yaw = CurrentRotation.Yaw;
		}
		else
		{
			// We might be firing, so we should use the firing rotation speed
			float YawRotationSpeed = IsHologramFiring() ? InheritYawRotationSpeedWhileFiring : InheritYawRotationSpeed;
			if (DeltaTime > 0.f && YawRotationSpeed > 0.f)
			{
				FRotator CharRot(0.f, CurrentRotation.Yaw, 0.f);
				FRotator ContRot(0.f, NewControlRotation.Yaw, 0.f);

				NewControlRotation.Yaw = FMath::RInterpTo(CurrentRotation, NewControlRotation, DeltaTime, YawRotationSpeed).Yaw;
			}
		}

		if (!bUseControllerRotationRoll)
		{
			NewControlRotation.Roll = CurrentRotation.Roll;
		}

		SetActorRotation(NewControlRotation);
	}
}

void AHologramCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (Instigator)
	{
		// We want to ignore our instigator (as we might spawn inside them)
		MoveIgnoreActorAdd(Instigator);
		Instigator->MoveIgnoreActorAdd(this);
	}
}

void AHologramCharacter::BeginPlay()
{
	Super::BeginPlay();	

	// Holograms should persist, as they will probaly block other abilities
	float LifeSpan = GetLifeSpan();
	if (LifeSpan <= 0.f)
	{
		UE_LOG(LogEGPowers, Warning, TEXT("Hologram character %s was given no life span! This character might never be destroyed!"), *GetName());
	}
}

void AHologramCharacter::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	// Notify our insitigator we have expired
	{
		const FGameplayTag Expired = FGameplayTag::RequestGameplayTag("Event.HologramPower.Expired");

		FGameplayEventData Payload;
		Payload.EventTag = Expired;

		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, Expired, Payload);
	}

	Super::EndPlay(EndPlayReason);
}

void AHologramCharacter::EnterFiringState()
{
	if (!bIsInFiringState)
	{
		bIsInFiringState = true;
	}
}

void AHologramCharacter::ExitFiringState()
{
	if (bIsInFiringState)
	{
		bIsInFiringState = false;
	}
}
