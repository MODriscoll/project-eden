// Copyright of Sock Goblins

#include "InspectorTraceActor.h"
#include "SpecOp/SpecOpCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/SphereComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

#include "AbilitySystemBlueprintLibrary.h"

AInspectorTraceActor::AInspectorTraceActor()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	TraceMaterialOriginParameter = TEXT("Origin");
	TraceMaterialRadiusParameter = TEXT("Radius");

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(CollisionSphere);

	CollisionSphere->bGenerateOverlapEvents = true;
	CollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AInspectorTraceActor::TraceOverlappedActor);

	TraceRadius = 3000.f;
	TraceTime = 4.f;
}

void AInspectorTraceActor::BeginPlay()
{
	Super::BeginPlay();

	// TODO: Log a warning instead
	TraceTime = TraceTime <= 0.f ? 1.f : TraceTime;

	// We are saving our owner here, as could potentially overlap lots of actors during the trace
	SpecOpOwner = CastChecked<ASpecOpCharacter>(Instigator);
	if (SpecOpOwner)
	{
		CollisionSphere->IgnoreActorWhenMoving(SpecOpOwner, true);
	}

	// Initialize the trace material
	InitializeTraceMaterial();
	if (TraceMaterialInstance)
	{
		TraceMaterialInstance->SetVectorParameterValue(TraceMaterialOriginParameter, GetActorLocation());
	}

	TraceStartTime = GetWorld()->GetTimeSeconds();
}

void AInspectorTraceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float TimeNow = GetWorld()->GetTimeSeconds();
	float Delta = TimeNow - TraceStartTime;

	if (Delta > TraceTime)
	{
		Destroy();
	}
	else
	{
		float Alpha = Delta / TraceTime;
		float Radius = TraceRadius * Alpha;

		CollisionSphere->SetSphereRadius(Radius);

		if (TraceMaterialInstance)
		{
			TraceMaterialInstance->SetScalarParameterValue(TraceMaterialRadiusParameter, Radius);
		}
	}
}

void AInspectorTraceActor::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	ClearTraceMaterial();

	// Inform our instigiator we have concluded
	{
		const FGameplayTag Finished = FGameplayTag::RequestGameplayTag(TEXT("Event.InspectorPower.Finished"));

		FGameplayEventData Payload;
		Payload.EventTag = Finished;

		if (SpecOpOwner)
		{
			UAbilitySystemComponent* ASC = SpecOpOwner->GetAbilitySystemComponent();

			FScopedPredictionWindow NewScopedWindow(ASC, true);
			ASC->HandleGameplayEvent(Finished, &Payload);
		}
		else
		{
			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, Finished, Payload);
		}
	}
}

void AInspectorTraceActor::TraceOverlappedActor(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == this || OtherActor == Instigator)
	{
		return;
	}

	// Notify spec ops ability system
	{
		const FGameplayTag TracedActor = FGameplayTag::RequestGameplayTag(TEXT("Event.InspectorPower.ActorTraced"));

		FGameplayEventData Payload;
		Payload.EventTag = TracedActor;
		Payload.Target = OtherActor;
		Payload.OptionalObject = this;

		// Capture tags from hit object if any
		IGameplayTagAssetInterface* TagAsset = Cast<IGameplayTagAssetInterface>(OtherActor);
		if (TagAsset)
		{
			TagAsset->GetOwnedGameplayTags(Payload.TargetTags);
		}

		if (SpecOpOwner)
		{
			UAbilitySystemComponent* ASC = SpecOpOwner->GetAbilitySystemComponent();

			FScopedPredictionWindow NewScopedWindow(ASC, true);
			ASC->HandleGameplayEvent(TracedActor, &Payload);
		}
		else
		{
			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, TracedActor, Payload);
		}
	}
}

void AInspectorTraceActor::InitializeTraceMaterial()
{
	if (TraceMaterialTemplate)
	{
		TraceMaterialInstance = UMaterialInstanceDynamic::Create(TraceMaterialTemplate, this);

		if (SpecOpOwner)
		{
			UCameraComponent* Camera = SpecOpOwner->GetCamera();
			Camera->AddOrUpdateBlendable(TraceMaterialInstance);
		}
	}
}

void AInspectorTraceActor::ClearTraceMaterial()
{
	if (TraceMaterialInstance)
	{
		if (SpecOpOwner)
		{
			UCameraComponent* Camera = SpecOpOwner->GetCamera();
			Camera->RemoveBlendable(TraceMaterialInstance);
		}

		TraceMaterialInstance = nullptr;
	}
}

