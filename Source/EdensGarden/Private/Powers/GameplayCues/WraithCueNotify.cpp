// Copyright of Sock Goblins

#include "GameplayCues/WraithCueNotify.h"
#include "SpecOp/SpecOpCharacter.h"
#include "Weapons/Weapon.h"

#include "Materials/MaterialInstanceDynamic.h"

bool AWraithCueNotify::OnActive_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters)
{
	SpecOp = Cast<ASpecOpCharacter>(MyTarget);
	if (!SpecOp.IsValid())
	{
		return false;
	}

	CreateMaterialInstance();
	if (!WraithMaterialInstance)
	{
		return false;
	}
	
	// Set up
	{
		SetUpCallbacks();
		CreateMaterialInstance();
	}

	ApplyWraithToActor(SpecOp.Get());
	TArray<AWeaponBase*> Weapons = SpecOp->GetAllWeapons();
	for (auto Weapon : Weapons)
	{
		ApplyWraithToActor(Weapon);
	}

	return true;
}

bool AWraithCueNotify::OnRemove_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters)
{
	if (SpecOp.IsValid())
	{
		RestoreActorsMaterials(SpecOp.Get());
		TArray<AWeaponBase*> Weapons = SpecOp->GetAllWeapons();
		for (auto Weapon : Weapons)
		{
			RestoreActorsMaterials(Weapon);
		}
	}

	// Clean up
	{
		DestroyMaterialInstance();
		CleanUpCallbacks();
	}

	if (!bAutoDestroyOnRemove)
	{
		GameplayCueFinishedCallback();
	}

	return true;
}

ASpecOpCharacter* AWraithCueNotify::GetSpecOp() const
{
	if (SpecOp.IsValid())
	{
		return SpecOp.Get();
	}

	return nullptr;
}

void AWraithCueNotify::ApplyWraithToActor(AActor* Actor)
{
	if (!WraithMaterialInstance)
	{
		return;
	}

	check(Actor);

	TArray<UMeshComponent*> ActorsMeshes;

	TArray<UActorComponent*> MeshComps = Actor->GetComponentsByClass(UMeshComponent::StaticClass());
	for (auto Comp : MeshComps)
	{
		UMeshComponent* Mesh = CastChecked<UMeshComponent>(Comp);

		// Save the original materials of the mesh
		TArray<UMaterialInterface*> OGMaterials = Mesh->GetMaterials();

		// Override materials for this mesh
		for (int32 Index = 0; Index < OGMaterials.Num(); ++Index)
		{
			Mesh->SetMaterial(Index, WraithMaterialInstance);
		}

		ActorsMeshes.Add(Mesh);
		ChangedMeshes.Add(Mesh, MoveTemp(OGMaterials));
	}

	ChangedActors.Add(Actor, MoveTemp(ActorsMeshes));
}

void AWraithCueNotify::RestoreActorsMaterials(AActor* Actor)
{
	check(Actor);

	TArray<UMeshComponent*>* ActorsMeshes = ChangedActors.Find(Actor);
	if (ActorsMeshes)
	{
		for (auto Mesh : *ActorsMeshes)
		{
			TArray<UMaterialInterface*>* OGMaterials = ChangedMeshes.Find(Mesh);
			if (OGMaterials)
			{
				for (int32 Index = 0; Index < OGMaterials->Num(); ++Index)
				{
					Mesh->SetMaterial(Index, (*OGMaterials)[Index]);
				}

				ChangedMeshes.Remove(Mesh);
			}	
		}

		ChangedActors.Remove(Actor);
	}
}

void AWraithCueNotify::SetUpCallbacks()
{
	if (SpecOp.IsValid())
	{
		WeaponInventoryHandle = SpecOp->OnWeaponInventoryUpdated.AddUObject(this, &AWraithCueNotify::OnSpecOpInventoryChanged);
	}
}

void AWraithCueNotify::CleanUpCallbacks()
{
	if (SpecOp.IsValid())
	{
		SpecOp->OnWeaponInventoryUpdated.Remove(WeaponInventoryHandle);
	}
}

void AWraithCueNotify::CreateMaterialInstance()
{
	if (WraithMaterialTemplate)
	{
		if (!WraithMaterialInstance || WraithMaterialInstance->GetClass() != WraithMaterialTemplate->GetClass())
		{
			WraithMaterialInstance = UMaterialInstanceDynamic::Create(WraithMaterialTemplate, this);
		}
	}
}

void AWraithCueNotify::DestroyMaterialInstance()
{
	WraithMaterialInstance = nullptr;
}

void AWraithCueNotify::OnSpecOpInventoryChanged(ASpecOpCharacter* SpecOp, AWeaponBase* Weapon, bool bAdded)
{
	if (bAdded)
	{
		ApplyWraithToActor(Weapon);
	}
	else
	{
		RestoreActorsMaterials(Weapon);
	}
}
