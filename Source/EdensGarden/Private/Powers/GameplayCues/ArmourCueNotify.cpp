// Copyright of Sock Goblins

#include "ArmourCueNotify.h"
#include "ArmourAnimInstance.h"
#include "SpecOpCharacter.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

AArmourCueNotify::AArmourCueNotify()
{
	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);

	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);
	Mesh->CastShadow = false;
	Mesh->bHiddenInGame = true;
}

bool AArmourCueNotify::OnActive_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters)
{
	if (!Mesh)
	{
		return false;
	}

	SpecOp = Cast<ASpecOpCharacter>(MyTarget);
	if (!SpecOp.IsValid())
	{
		return false;
	}

	CreateMaterialInstance();
	if (!ArmourMaterialInstance)
	{
		return false;
	}

	// Set up mesh
	USkeletalMeshComponent* CharacterMesh = SpecOp->GetMesh();
	if (CharacterMesh)
	{
		Mesh->AttachToComponent(CharacterMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		Mesh->SetSkeletalMesh(CharacterMesh->SkeletalMesh);

		UArmourAnimInstance* AnimInstance = GetArmourAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->SetMeshToCopy(CharacterMesh);
		}

		// Update meshes materials
		for (int32 Index = 0; Index < Mesh->GetNumMaterials(); ++Index)
		{
			Mesh->SetMaterial(Index, ArmourMaterialInstance);
		}

		Mesh->SetHiddenInGame(CharacterMesh->bHiddenInGame);
	}
	else
	{
		DestroyMaterialInstance();
		return false;
	}

	return true;
}

bool AArmourCueNotify::OnRemove_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters)
{
	if (Mesh)
	{
		Mesh->SetHiddenInGame(true);
		Mesh->SetSkeletalMesh(nullptr, true);

		Mesh->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
	}

	DestroyMaterialInstance();

	if (!bAutoDestroyOnRemove)
	{
		GameplayCueFinishedCallback();
	}

	return true;
}

UArmourAnimInstance* AArmourCueNotify::GetArmourAnimInstance() const
{
	if (Mesh && Mesh->AnimScriptInstance)
	{
		return Cast<UArmourAnimInstance>(Mesh->AnimScriptInstance);
	}

	return nullptr;
}

void AArmourCueNotify::CreateMaterialInstance()
{
	if (ArmourMaterialTemplate)
	{
		if (!ArmourMaterialInstance || ArmourMaterialInstance->GetClass() != ArmourMaterialTemplate->GetClass())
		{
			ArmourMaterialInstance = UMaterialInstanceDynamic::Create(ArmourMaterialTemplate, this);
		}
	}
}

void AArmourCueNotify::DestroyMaterialInstance()
{
	ArmourMaterialInstance = nullptr;
}
