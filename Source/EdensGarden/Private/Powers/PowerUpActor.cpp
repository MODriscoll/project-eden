// Copyright of Sock Goblins

#include "PowerUpActor.h"
#include "EdensGardenAssetManager.h"

#include "SpecOpAbilitySystemComponent.h"
#include "SpecOpCharacter.h"

APowerUpActor::APowerUpActor()
{
	PrimaryActorTick.bCanEverTick = false;

	bHaveBeenPickedUp = false;
}

void APowerUpActor::OnInteractiveGainFocus_Implementation(ASpecOpCharacter* Character)
{
	Super::OnInteractiveGainFocus_Implementation(Character);

	if (PowerAbility.IsValid() && !PowerAbility.IsPending())
	{
		FSoftObjectPath ClassPath = PowerAbility.ToSoftObjectPath();
		
		FStreamableManager& Streamable = UEdensGardenAssetManager::GetStreamableManager();
		Streamable.RequestAsyncLoad(ClassPath);
	}
}

void APowerUpActor::OnInteractiveLoseFocus_Implementation(ASpecOpCharacter* Character)
{
	Super::OnInteractiveLoseFocus_Implementation(Character);

	if (!PowerAbility.IsPending())
	{
		PowerAbility.Reset();
	}
}

bool APowerUpActor::CanInteractWith_Implementation(const ASpecOpCharacter* Character) const
{
	return !bHaveBeenPickedUp;
}

void APowerUpActor::OnInteract_Implementation(ASpecOpCharacter* Interactor)
{
	if (!bHaveBeenPickedUp)
	{
		if (PowerAbility.IsPending())
		{
			// We will wait for it to finish loading
			PowerAbility.LoadSynchronous();
		}

		// TODO: give to player
		// for now
		USpecOpAbilitySystemComponent* ASC = Interactor->GetSpecOpAbilitySystem();
		ASC->GivePower(PowerAbility.Get());

		bHaveBeenPickedUp = true;
	}
}

void APowerUpActor::OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled)
{
	if (!bWasCancelled && !bHaveBeenPickedUp)
	{
		if (PowerAbility.IsPending())
		{
			// We will wait for it to finish loading
			PowerAbility.LoadSynchronous();
		}

		// TODO: give to player
		// for now
		USpecOpAbilitySystemComponent* ASC = Interactor->GetSpecOpAbilitySystem();
		ASC->GivePower(PowerAbility.Get());

		bHaveBeenPickedUp = true;
	}
}

