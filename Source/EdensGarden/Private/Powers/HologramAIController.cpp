// Copyright of Sock Goblins

#include "HologramAIController.h"
#include "HologramCharacter.h"

AHologramAIController::AHologramAIController()
{
	bStopAILogicOnUnposses = true;
	bWantsPlayerState = false;
	bSetControlRotationFromPawnOrientation = false;

	bIgnoreAlmostDeadCharacters = true;
}

void AHologramAIController::NotifyActorFirstSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	AEGCharacter* Character = Cast<AEGCharacter>(Actor);
	if (Character && CanHologramRespondTo(Character))
	{
		CharactersToRespondTo.AddUnique(Character);
	}
}

void AHologramAIController::NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus)
{
	AEGCharacter* Character = Cast<AEGCharacter>(Actor);
	CharactersToRespondTo.Remove(Character);
}

FVector AHologramAIController::GetFocalPointOnActor(const AActor* Actor) const
{
	if (Actor)
	{
		FVector Location;
		FRotator Rotation;

		Actor->GetActorEyesViewPoint(Location, Rotation);
		return Location;
	}

	return FAISystem::InvalidLocation;
}

bool AHologramAIController::CanHologramRespondTo_Implementation(const AEGCharacter* EGCharacter) const
{
	check(EGCharacter);

	APawn* ControlledPawn = GetPawn();
	if (ControlledPawn)
	{
		if (EGCharacter != ControlledPawn)
		{
			return true;
		}
	}

	return false;
}
