// Copyright of Sock Goblins

#include "DoorActor.h"
#include "TimerManager.h"

#include "Components/BoxComponent.h"
#include "AI/Navigation/NavigationSystem.h"
#include "AI/Navigation/NavAreas/NavArea_Null.h"
#include "AI/Navigation/NavAreas/NavArea_Default.h"

ADoorActor::ADoorActor()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = false;

	NavModifierVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("Nav Modifier Volume"));
	SetRootComponent(NavModifierVolume);
	NavModifierVolume->SetMobility(EComponentMobility::Static);
	NavModifierVolume->bDynamicObstacle = true;
	NavModifierVolume->AreaClass = UNavArea_Null::StaticClass();
	NavModifierVolume->bGenerateOverlapEvents = false;
	NavModifierVolume->SetCollisionProfileName(UCollisionProfile::NoCollision_ProfileName);

	FOnTimelineEvent InterpFinCallback;
	InterpFinCallback.BindUFunction(this, TEXT("OnInterpFinish"));
	DoorInterpTrack.SetTimelineFinishedFunc(InterpFinCallback);

	TimeBeforeAutoClose = 5.f;
	bIsOpen = false;
	bIsLocked = false;

	ClosedNavArea = UNavArea_Null::StaticClass();
	OpenNavArea = UNavArea_Default::StaticClass();
}

void ADoorActor::PostLoadGameSerialization()
{
	if (false)//HasActorBegunPlay())
	{
		LerpDoors(bIsOpen ? 1.f : 0.f);
		SetActorTickEnabled(false);

		// Need to disable interp track so IsOpeningOrClosing returns false after this
		DoorInterpTrack.Stop();

		// Update navmesh
		UpdateDoorNavModifier(bIsOpen, true);

		// Checks if open
		SetAutoCloseTimer();

		// Only notify derived types of change (as we are basically resetting)
		DoorStateChanged(bIsOpen);
	}
}

void ADoorActor::BeginPlay()
{
	Super::BeginPlay();

	if (InterpolationTrack)
	{
		FOnTimelineFloat InterpCallback;
		InterpCallback.BindUFunction(this, TEXT("OnInterpUpdated"));
		DoorInterpTrack.AddInterpFloat(InterpolationTrack, InterpCallback);

		LerpDoors(bIsOpen ? 1.f : 0.f);

		// Checks if open
		SetAutoCloseTimer();
	}
	else
	{
		UE_LOG(LogEGProps, Warning, TEXT("No interpolation track has been provided for door actor %s"), *GetName());
	}
}

void ADoorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Tick should only be called when we are in the progress of opening or closing.
	// It is set up this way as we only need to tick to call tick for the timeline
	ensure(IsOpeningOrClosing());
	DoorInterpTrack.TickTimeline(DeltaTime);
}

void ADoorActor::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame())
	{
		if (Ar.IsSaving())
		{
			FDoorActorSaveData SaveData;
			SaveData.bIsOpen = bIsOpen;
			SaveData.bIsLocked = bIsLocked;

			Ar << SaveData;
		}

		if (Ar.IsLoading())
		{
			FDoorActorSaveData SaveData;
			Ar << SaveData;

			//bIsOpen = SaveData.bIsOpen;
			bIsLocked = SaveData.bIsLocked;
		}
	}
}

#if WITH_EDITOR
bool ADoorActor::CanEditChange(const UProperty* InProperty) const
{
	bool bCan = Super::CanEditChange(InProperty);

	// We don't want the is open flag to be changed while playing!
	FName PropertyName = InProperty ? InProperty->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(ADoorActor, bIsOpen))
	{
		bCan &= !HasActorBegunPlay();
	}

	return bCan;
}

void ADoorActor::PostEditUndo()
{
	Super::PostEditUndo();

	UNavigationSystem::UpdateActorInNavOctree(*this);
}

void ADoorActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	if (NavModifierVolume)
	{
		const FName PropertyName = PropertyChangedEvent.Property ? PropertyChangedEvent.Property->GetFName() : NAME_None;
		if (PropertyName == GET_MEMBER_NAME_CHECKED(ADoorActor, NavModifierVolume))
		{
			UNavigationSystem::UpdateComponentInNavOctree(*NavModifierVolume);
		}
		else
		{
			bool bShouldUpdateOctree = false;

			// Can edit change should prevent this being called during PIE
			bShouldUpdateOctree |= (PropertyName == GET_MEMBER_NAME_CHECKED(ADoorActor, bIsOpen));

			bShouldUpdateOctree |= (PropertyName == GET_MEMBER_NAME_CHECKED(ADoorActor, OpenNavArea));
			bShouldUpdateOctree |= (PropertyName == GET_MEMBER_NAME_CHECKED(ADoorActor, ClosedNavArea));

			if (bShouldUpdateOctree)
			{
				NavModifierVolume->AreaClass = bIsOpen ? OpenNavArea : ClosedNavArea;
				UNavigationSystem::UpdateComponentInNavOctree(*NavModifierVolume);
			}
		}
	}
}
#endif

void ADoorActor::Open()
{
	if (CanOpen())
	{
		DoorInterpTrack.PlayFromStart(); // If allowing for open while closing, could just use .Play()?
		SetActorTickEnabled(true);

		// Update navigiation around door
		UpdateDoorNavModifier(true);

		DoorStartStateChange(true);
	}
}

void ADoorActor::Close()
{
	if (CanClose())
	{
		DoorInterpTrack.ReverseFromEnd(); // If allowing for close while opening, could just use .Reverse()?
		SetActorTickEnabled(true);

		// Update navigation around door
		UpdateDoorNavModifier(false);

		DoorStartStateChange(false);

		// Need to clear auto close timer
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.ClearTimer(TimerHandle_AutoClose);
	}
}

void ADoorActor::ToggleDoor()
{
	if (bIsOpen)
	{
		Close();
	}
	else
	{
		Open();
	}
}

bool ADoorActor::Unlock()
{
	if (CanUnlock())
	{
		bIsLocked = false;

		DoorUnlocked();
		OnDoorUnlocked.Broadcast();

		return true;
	}

	return false;
}

// Using sweep method, only problem is if the doors are a little in the ground
// that will not close since they hit the floor. (Could try implementing a trigger
// volume that will help us record whos within the doors and prevent closing if
// something is there, or making custom movement component)
// Another method is on state changed finished, check for any dynamic mobility overlaps
// and forcefully move them out
//void ADoorActor::LerpDoors_Implementation(float Alpha)
//{
//	// Calculate offset from origin (relative origin)
//	float HalfGap = DoorGapWidth * 0.5f;
//	FVector Offset(FMath::Lerp(0.f, HalfGap, Alpha), 0.f, 0.f);
//
//	// Track if we hit something while moving
//	FHitResult Result;
//	bool bReverted = false;
//
//	// Used to 'revert' current lerp if we hit something. We need to do
//	// this as not doing so would eventually cause the interp track to finish
//	// with the doors not closing properly 
//	float DeltaTime = GetWorld()->GetDeltaSeconds();
//	DeltaTime = bIsOpen ? DeltaTime : -DeltaTime;
//
//	// Move this door, revert back if we hit something
//	LeftDoorMesh->SetRelativeLocation(LeftOrigin - Offset, bSweepInterpolation, &Result);
//	if (Result.IsValidBlockingHit())
//	{
//		DoorInterpTrack.SetNewTime(DoorInterpTrack.GetPlaybackPosition() + DeltaTime);
//		bReverted = true;
//	}
//
//	// Move this door, only revert if we hadn't hit something and we have now
//	RightDoorMesh->SetRelativeLocation(RightOrigin + Offset, bSweepInterpolation, &Result);
//	if (Result.IsValidBlockingHit() && !bReverted) 
//	{
//		DoorInterpTrack.SetNewTime(DoorInterpTrack.GetPlaybackPosition() + DeltaTime);
//	}
//}

void ADoorActor::UpdateDoorNavModifier(bool bIsOpening, bool bFromLoad)
{
	// We only need to update the nav modifier if we are switching states
	if (bFromLoad || bIsOpening != bIsOpen)
	{
		if (NavModifierVolume)
		{
			// Save time by only updating if area class is different
			TSubclassOf<UNavArea> RelevantNavArea = bIsOpening ? OpenNavArea : ClosedNavArea;
			if (RelevantNavArea != NavModifierVolume->AreaClass)
			{
				NavModifierVolume->AreaClass = RelevantNavArea;
				UNavigationSystem::UpdateComponentInNavOctree(*NavModifierVolume);
			}
		}
	}
}

bool ADoorActor::IsOpeningOrClosing() const
{
	return DoorInterpTrack.IsPlaying();
}

bool ADoorActor::CanOpen() const
{
	return !bIsOpen && !IsOpeningOrClosing();
}

bool ADoorActor::CanClose() const
{
	return bIsOpen && !IsOpeningOrClosing();
}

bool ADoorActor::CanUnlock() const
{
	return bIsLocked;
}

bool ADoorActor::WillAutoClose() const
{
	FTimerManager& TimerManager = GetWorldTimerManager();
	return TimerManager.IsTimerActive(TimerHandle_AutoClose);
}

void ADoorActor::SetAutoCloseTimer()
{
	if (bIsOpen && TimeBeforeAutoClose > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_AutoClose, this, &ADoorActor::Close, TimeBeforeAutoClose, false);
	}
}

void ADoorActor::OnInterpUpdated(float Value)
{
	LerpDoors(FMath::Clamp(Value, 0.f, 1.f));
}

void ADoorActor::OnInterpFinish()
{
	bIsOpen = !bIsOpen;
	SetActorTickEnabled(false);

	// Checks if open
	SetAutoCloseTimer();

	DoorStateChanged(bIsOpen);
	OnDoorOpenOrClose.Broadcast(bIsOpen);
}

