// Copyright of Sock Goblins

#include "AmmoBoxActor.h"
#include "Guns/AmmoGun.h"
#include "SpecOp/SpecOpCharacter.h"

#include "Components/StaticMeshComponent.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "EdensGardenGameState.h"

#include "Effects/GameplayEffect_AmmoBox.h"

#define LOCTEXT_NAMESPACE "AmmoBox"

AAmmoBoxActor::AAmmoBoxActor()
{
	PrimaryActorTick.bCanEverTick = false;

	GunAmmoType = AAmmoGun::StaticClass();
	Ammo = 100.f;
	InteractTime = 1.f;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionProfileName(INTERACTIVE_PROFILE);
	Mesh->SetMobility(EComponentMobility::Static);
}

bool AAmmoBoxActor::ShouldSaveForSaveGame() const
{
	if (Ammo > 0.f)
	{
		return !HasAllFlags(RF_Transient);
	}

	return false;
}

bool AAmmoBoxActor::CanInteractWith_Implementation(const ASpecOpCharacter* Character) const
{
	return GetInteractResponse(Character) == EAmmoBoxInteractResponse::CanGetAmmo;
}

float AAmmoBoxActor::GetInteractDuration_Implementation() const
{
	return InteractTime;
}

void AAmmoBoxActor::GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const
{
	switch (GetInteractResponse(Character))
	{
		case EAmmoBoxInteractResponse::CanGetAmmo:
		{
			const AGunBase* DefaultGun = nullptr;
			if (GunAmmoType == AAmmoGun::StaticClass())
			{
				DefaultGun = Character->GetEquippedGun();
			}
			else
			{
				DefaultGun = GunAmmoType.GetDefaultObject();
			}

			if (DefaultGun)
			{
				FFormatNamedArguments Arguments;
				Arguments.Add(TEXT("WeaponName"), DefaultGun->WeaponName);

				DisplayTitle = FText::Format(LOCTEXT("AmmoBoxDisplayTitle", "{WeaponName} Ammo"), Arguments);
				DisplayColor = FColor::Green;
			}

			break;
		}
		case EAmmoBoxInteractResponse::AmmoAlreadyFull:
		{
			DisplayTitle = LOCTEXT("AmmoBoxDisplayTitle", "Ammo already full");
			DisplayColor = FColor::Red;
			break;
		}
		case EAmmoBoxInteractResponse::MissingRequiredGun:
		{
			DisplayTitle = LOCTEXT("AmmoBoxDisplayTitle", "Ammo doesn't match weapon specifications");
			DisplayColor = FColor::Red;
			break;
		}
		case EAmmoBoxInteractResponse::OutOfAmmo:
		{
			DisplayTitle = LOCTEXT("AmmoBoxDisplayTitle", "Out of Ammo");
			DisplayColor = FColor::Red;
			break;
		}
	}
}

void AAmmoBoxActor::OnInteract_Implementation(ASpecOpCharacter* Interactor)
{
	GiveAmmoTo(Interactor);
}

void AAmmoBoxActor::OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled)
{
	if (!bWasCancelled)
	{
		GiveAmmoTo(Interactor);
	}
}

void AAmmoBoxActor::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	if (Ar.IsSaveGame())
	{
		if (Ar.IsSaving())
		{
			FAmmoBoxActorSaveData SaveData;
			SaveData.Ammo = Ammo;

			Ar << SaveData;
		}

		if (Ar.IsLoading())
		{
			FAmmoBoxActorSaveData SaveData;
			Ar << SaveData;

			Ammo = SaveData.Ammo;
		}
	}
}

void AAmmoBoxActor::GiveAmmoTo(ASpecOpCharacter* SpecOp)
{
	if (SpecOp)
	{
		AAmmoGun* AmmoGun = GetGunToRefill(SpecOp);
		if (AmmoGun)
		{
			float AmountToGive = AmmoGun->GetAmmoNeededToRefill(Ammo);

			UAbilitySystemComponent* ASC = AmmoGun->GetAbilitySystemComponent();
			if (ASC)
			{
				const UAbilitySystemGlobals& AbilityGlobals = UAbilitySystemGlobals::Get();

				// We are the source of this effect
				FGameplayEffectContextHandle Context(AbilityGlobals.AllocGameplayEffectContext());
				Context.AddInstigator(this, this);
				Context.AddSourceObject(this);

				// Should always be valid (check to be safe)
				FGameplayEffectSpecHandle SpecHandle = ASC->MakeOutgoingSpec(UGameplayEffect_AmmoBox::StaticClass(), UGameplayEffect::INVALID_LEVEL, Context);
				if (SpecHandle.IsValid())
				{
					// Need to dynamically set ammo to give
					SpecHandle.Data->SetSetByCallerMagnitude(UGameplayEffect_AmmoBox::AmmoDataName, AmountToGive);
					ASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);

					// Consume ammo from box
					Ammo -= AmountToGive;
				}
			}		
		}
	}

	if (Ammo <= 0.f)
	{
		OnOutOfAmmo();
	}
}

void AAmmoBoxActor::OnOutOfAmmo_Implementation()
{
	AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
	if (GameState)
	{
		GameState->SaveActorDestroyed(this);
	}

	Destroy();
}

EAmmoBoxInteractResponse AAmmoBoxActor::GetInteractResponse(const ASpecOpCharacter* Interactor) const
{
	if (Ammo <= 0.f)
	{
		return EAmmoBoxInteractResponse::OutOfAmmo;
	}

	EAmmoBoxInteractResponse Response = EAmmoBoxInteractResponse::MissingRequiredGun;
	
	if (Interactor)
	{
		AAmmoGun* AmmoGun = GetGunToRefill(Interactor);
		if (AmmoGun)
		{
			float AmountToGive = AmmoGun->GetAmmoNeededToRefill(Ammo);
			if (AmountToGive > 0.f)
			{
				Response = EAmmoBoxInteractResponse::CanGetAmmo;
			}
			else
			{
				Response = EAmmoBoxInteractResponse::AmmoAlreadyFull;
			}
		}
	}

	return Response;
}

AAmmoGun* AAmmoBoxActor::GetGunToRefill(const ASpecOpCharacter* SpecOp) const
{
	// By default, use the interactors current equipped weapon as the source
	if (GunAmmoType == AAmmoGun::StaticClass())
	{
		return Cast<AAmmoGun>(SpecOp->GetEquippedGun());
	}
	else
	{
		return Cast<AAmmoGun>(SpecOp->GetGunOfClass(GunAmmoType));
	}
}

#undef LOCTEXT_NAMESPACE