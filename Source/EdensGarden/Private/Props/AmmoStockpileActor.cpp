// Copyright of Sock Goblins

#include "AmmoStockpileActor.h"
#include "Guns/AmmoGun.h"
#include "SpecOp/SpecOpCharacter.h"

#include "Components/StaticMeshComponent.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "EdensGardenGameState.h"

#include "Effects/GameplayEffect_AmmoBox.h"

#define LOCTEXT_NAMESPACE "AmmoStockpile"

AAmmoStockpileActor::AAmmoStockpileActor()
{
	PrimaryActorTick.bCanEverTick = false;

	RefillPercentage = 0.2f;
	InteractTime = 1.f;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
	Mesh->SetSimulatePhysics(false);
	Mesh->SetCollisionProfileName(INTERACTIVE_PROFILE);
	Mesh->SetMobility(EComponentMobility::Static);
}

bool AAmmoStockpileActor::CanInteractWith_Implementation(const ASpecOpCharacter* Character) const
{
	if (RefillPercentage > 0.f)
	{
		return GetInteractResponse(Character) == EAmmoStockpileInteractResponse::CanGetAmmo;
	}
	else
	{
		return false;
	}
}

float AAmmoStockpileActor::GetInteractDuration_Implementation() const
{
	return InteractTime;
}

void AAmmoStockpileActor::GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const
{
	if (RefillPercentage > 0.f)
	{
		switch (GetInteractResponse(Character))
		{
			case EAmmoStockpileInteractResponse::CanGetAmmo:
			{
				DisplayTitle = LOCTEXT("AmmoStockpileDisplayTitle", "Ammo Stockpile");
				DisplayColor = FColor::Green;
				break;
			}
			case EAmmoStockpileInteractResponse::AmmoAlreadyFull:
			{
				DisplayTitle = LOCTEXT("AmmoStockpileDisplayTitle", "Ammo already full");
				DisplayColor = FColor::Red;
				break;
			}
			case EAmmoStockpileInteractResponse::MissingRequiredGun:
			{
				DisplayTitle = LOCTEXT("AmmoStockpileDisplayTitle", "Ammo doesn't meet any weapon specifications");
				DisplayColor = FColor::Red;
				break;
			}
		}
	}
}

void AAmmoStockpileActor::OnInteract_Implementation(ASpecOpCharacter* Interactor)
{
	GiveAmmoTo(Interactor);
}

void AAmmoStockpileActor::OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled)
{
	if (!bWasCancelled)
	{
		GiveAmmoTo(Interactor);
	}
}

void AAmmoStockpileActor::GiveAmmoTo(ASpecOpCharacter* SpecOp)
{
	if (SpecOp)
	{
		TArray<AGunBase*> Guns;
		if (GetGunsToRefill(SpecOp, Guns))
		{
			const UAbilitySystemGlobals& AbilityGlobals = UAbilitySystemGlobals::Get();

			// Allocate context before applying 
			FGameplayEffectContextHandle Context(AbilityGlobals.AllocGameplayEffectContext());
			Context.AddInstigator(this, this);
			Context.AddSourceObject(this);

			for (AGunBase* Gun : Guns)
			{
				GiveAmmoToGun(CastChecked<AAmmoGun>(Gun), Context);
			}
		}
	}

	OnAmmoGiven();
}

void AAmmoStockpileActor::GiveAmmoToGun(AAmmoGun* Gun, const FGameplayEffectContextHandle& ContextHandle)
{
	if (Gun && Gun->GetAbilitySystemComponent() != nullptr)
	{
		float MaxAmountToGive = Gun->GetMaxReserveAmmo() * RefillPercentage;
		float AmountToGive = Gun->GetAmmoNeededToRefill(MaxAmountToGive);

		if (AmountToGive > 0.f)
		{
			UAbilitySystemComponent* ASC = Gun->GetAbilitySystemComponent();

			// Should always be valid (check to be safe)
			FGameplayEffectSpecHandle SpecHandle = ASC->MakeOutgoingSpec(UGameplayEffect_AmmoBox::StaticClass(), UGameplayEffect::INVALID_LEVEL, ContextHandle);
			if (SpecHandle.IsValid())
			{
				// Dynamically set ammo to give
				SpecHandle.Data->SetSetByCallerMagnitude(UGameplayEffect_AmmoBox::AmmoDataName, AmountToGive);
				ASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
			}
		}
	}
}

void AAmmoStockpileActor::OnAmmoGiven_Implementation()
{
	AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
	if (GameState)
	{
		GameState->SaveActorDestroyed(this);
	}

	Destroy();
}

EAmmoStockpileInteractResponse AAmmoStockpileActor::GetInteractResponse(const ASpecOpCharacter* Interactor) const
{
	EAmmoStockpileInteractResponse Response = EAmmoStockpileInteractResponse::MissingRequiredGun;

	if (Interactor)
	{
		TArray<AGunBase*> Guns;
		if (GetGunsToRefill(Interactor, Guns))
		{
			// We now have a gun meeting requirements
			Response = EAmmoStockpileInteractResponse::AmmoAlreadyFull;

			for (AGunBase* Gun : Guns)
			{
				AAmmoGun* AmmoGun = CastChecked<AAmmoGun>(Gun);
				
				// Gun might have a full reserve but be missing some ammo from its current clip
				if (AmmoGun && (AmmoGun->GetTotalAmmo() < AmmoGun->GetMaxTotalAmmo()))
				{
					Response = EAmmoStockpileInteractResponse::CanGetAmmo;
				}
			}
		}
	}

	return Response;
}

bool AAmmoStockpileActor::GetGunsToRefill(const ASpecOpCharacter* SpecOp, TArray<AGunBase*>& Guns) const
{
	if (SpecOp)
	{
		SpecOp->GetAllGunsOfClass(AAmmoGun::StaticClass(), Guns);
		return Guns.Num() > 0;
	}

	return false;
}

#undef LOCTEXT_NAMESPACE