// Copyright of Sock Goblins

#include "EffectActor.h"
#include "TimerManager.h"

#include "EdensGardenGameState.h"
#include "SpecOpCharacter.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

AEffectActor::AEffectActor()
{
	bDestroyOnInteract = true;
	InteractionDelay = 0.f;
}

bool AEffectActor::CanInteractWith_Implementation(const ASpecOpCharacter* Character) const
{
	if (!GameplayEffect)
	{
		UE_LOG(LogEGProps, Warning, TEXT("Effect actor %s has null gameplay effect!"), *GetName());
		return false;
	}

	return !IsInteractionDelayed();
}

void AEffectActor::OnInteract_Implementation(ASpecOpCharacter* Interactor)
{
	ApplyGameplayEffect(Interactor);
}

void AEffectActor::OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled)
{
	if (!bWasCancelled)
	{
		ApplyGameplayEffect(Interactor);
	}
}

bool AEffectActor::IsInteractionDelayed() const
{
	FTimerManager& TimerManager = GetWorldTimerManager();
	return TimerManager.IsTimerActive(TimerHandle_InteractDelay);
}

void AEffectActor::ApplyGameplayEffect(ASpecOpCharacter* SpecOp)
{
	UAbilitySystemComponent* ASC = SpecOp->GetAbilitySystemComponent();
	if (ASC && GameplayEffect)
	{
		FGameplayEffectContextHandle Context(UAbilitySystemGlobals::Get().AllocGameplayEffectContext());
		Context.AddInstigator(SpecOp, this);
		Context.AddSourceObject(this);

		ASC->ApplyGameplayEffectToSelf(GameplayEffect.GetDefaultObject(), UGameplayEffect::INVALID_LEVEL, Context);
		OnEffectApplied(SpecOp);
	}

	if (bDestroyOnInteract)
	{
		AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
		if (GameState)
		{
			GameState->SaveActorDestroyed(this);
		}

		Destroy();
	}
	else if (InteractionDelay > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_InteractDelay, InteractionDelay, false);
	}

	// We should inform spec op to update closest interactive,
	// as we might not be interactive anymore
	SpecOp->UpdateClosestInteractive();
}
