// Copyright of Sock Goblins

#include "EdensGardenLevelScriptActor.h"
#include "EdensGardenGameState.h"
#include "SpecOpCharacter.h"

#include "Engine/World.h"

void AEdensGardenLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();

	UWorld* World = GetWorld();
	if (World)
	{
		AEdensGardenGameState* GameState = World->GetGameState<AEdensGardenGameState>();
		if (GameState)
		{
			GameState->OnNewCheckpointReached.AddDynamic(this, &AEdensGardenLevelScriptActor::OnCheckpointReached);
		}
	}
}