// Copyright of Sock Goblins

#include "Projectiles/ProjectileActor.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

#include "GameplayTagContainer.h"

AProjectileActor::AProjectileActor(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	MovementComponent->InitialSpeed = 500.f;
	MovementComponent->MaxSpeed = 500.f;

	ProjectileResponse |= (1 << static_cast<int32>(EProjectileResponse::Hit));
	bIgnoreInstigator = true;
}

void AProjectileActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (bIgnoreInstigator && Instigator)
	{
		UPrimitiveComponent* RootPrimitiveComponent = Cast<UPrimitiveComponent>(GetRootComponent());
		if (RootPrimitiveComponent)
		{
			RootPrimitiveComponent->IgnoreActorWhenMoving(Instigator, true);
		}

		Instigator->MoveIgnoreActorAdd(this);
	}
}

void AProjectileActor::BeginPlay()
{
	Super::BeginPlay();

	// Set up overlap response
	if ((ProjectileResponse) & (1 << static_cast<int32>(EProjectileResponse::Overlap)))
	{
		OnActorBeginOverlap.AddDynamic(this, &AProjectileActor::OnProjectileOverlap);
	}

	// Set up hit response
	if ((ProjectileResponse) & (1 << static_cast<int32>(EProjectileResponse::Hit)))
	{
		OnActorHit.AddDynamic(this, &AProjectileActor::OnProjectileHit);
	}

	if (ProjectileLifeSpan > 0)
	{
		SetLifeSpan(ProjectileLifeSpan);
	}
}

void AProjectileActor::LifeSpanExpired()
{
	// We can let our derived types handle this
	OnExpired();
}

void AProjectileActor::OnExpired_Implementation()
{
	DestroyProjectile();
}

void AProjectileActor::OnProjectileOverlap(AActor* OverlappedActor, AActor* OtherActor)
{
	if (OtherActor == this)
	{
		return;
	}

	if (!bIgnoreInstigator || OtherActor != Instigator)
	{
		OnProjectileOverlappedActor(OtherActor);
	}
}

void AProjectileActor::OnProjectileHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor == this)
	{
		return;
	}

	if (!bIgnoreInstigator || OtherActor != Instigator)
	{
		OnProjectileHitActor(OtherActor, NormalImpulse, Hit);
	}
}

void AProjectileActor::DestroyProjectile()
{
	if (Instigator)
	{
		const FGameplayTag ProjectileDestruction = FGameplayTag::RequestGameplayTag(TEXT("Event.Projectile.Destruction"));

		FGameplayEventData Payload;
		Payload.EventTag = ProjectileDestruction;
		Payload.Instigator = this;
		Payload.OptionalObject = this;
		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, ProjectileDestruction, Payload);
	}

	Destroy();
}

void AProjectileActor::ApplyEffectToActor(AActor* Actor, const FGameplayEffectSpecHandle& Handle) const
{
	if (Handle.IsValid())
	{
		UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Actor);
		if (ASC)
		{
			ASC->ApplyGameplayEffectSpecToSelf(*Handle.Data);
		}
	}
}
