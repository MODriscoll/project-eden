// Copyright of Sock Goblins

#include "FireBombProjectile.h"
#include "EdensGardenFunctionLibrary.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "AbilitySystemInterface.h"

#include "AI/Navigation/NavigationSystem.h"
#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

#include "TimerManager.h"

AFireBombProjectile::AFireBombProjectile()
{
	bIgnoreInstigator = false;
	ProjectileLifeSpan = 15.f;
	ExplosionRadius = 400.f;
	AreaOfEffectTime = 6.f;

	// Base of projectile, is used by the projectile movement component for collision
	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	SetRootComponent(CollisionSphere);
	CollisionSphere->SetCollisionProfileName(UCollisionProfile::BlockAllDynamic_ProfileName);

	AreaOfEffect = CreateDefaultSubobject<USphereComponent>(TEXT("AreaOfEffect"));
	AreaOfEffect->SetupAttachment(GetRootComponent());
	AreaOfEffect->InitSphereRadius(300.f);
	AreaOfEffect->bGenerateOverlapEvents = true;
	AreaOfEffect->bDynamicObstacle = true;

	AreaOfEffect->OnComponentBeginOverlap.AddDynamic(this, &AFireBombProjectile::AreaOfEffectStartOverlap);
	AreaOfEffect->OnComponentEndOverlap.AddDynamic(this, &AFireBombProjectile::AreaOfEffectEndOverlap);

	UProjectileMovementComponent* MovementComp = GetProjectileMovement();
	if (MovementComp)
	{
		MovementComp->UpdatedComponent = CollisionSphere;
	}

	bIsAreaOfEffectActive = false;
}

void AFireBombProjectile::BeginPlay()
{
	Super::BeginPlay();

	bIsAreaOfEffectActive = false;
	AreaOfEffect->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AFireBombProjectile::OnProjectileHitActor_Implementation(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit)
{
	Explode();
}

void AFireBombProjectile::OnExpired_Implementation()
{
	// The diminishing will handle expiration if active
	if (!bIsAreaOfEffectActive)
	{
		Explode();
	}
}

void AFireBombProjectile::Explode()
{
	TArray<FHitResult> HitResults;

	// Origin of explosion is projectile location
	FVector Origin = GetActorLocation();

	// Apply explosion effect to any actor caught within explosion radius
	{
		TArray<AActor*> IgnoreActors;
		IgnoreActors.Add(this);

		// Trace world, apply effects
		if (UEdensGardenFunctionLibrary::SphereLineOfSightTrace(this, Origin, ExplosionRadius, ECollisionChannel::ECC_WorldStatic, IgnoreActors, HitResults))
		{
			if (ExplosionEffect.IsValid())
			{
				for (const FHitResult& Result : HitResults)
				{
					ApplyEffectToActor(Result.GetActor(), ExplosionEffect);
				}
			}
		}
	}

	OnExploded(Origin, HitResults);
	
	// Disbale any physical interaction with world
	{
		CollisionSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		UProjectileMovementComponent* MovementComp = GetProjectileMovement();
		if (MovementComp)
		{
			MovementComp->Deactivate();
		}
	}

	if (AreaOfEffectTime > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_Diminish, this, &AFireBombProjectile::OnAreaOfEffectDiminished, AreaOfEffectTime, false);

		// Move to a suitable floor prior to enabling collision, 
		// as some actors may enter then immediately exit
		FindAndMoveToFloor();

		// Activate area of effect (we might want to set effect active
		// after enabling collision, so all those actors that were caught
		// in explosion don't immediatly reci
		bIsAreaOfEffectActive = true;
		AreaOfEffect->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

		// Update navigation modifier to allow AI to try to avoid this area
		UNavigationSystem::UpdateComponentInNavOctree(*AreaOfEffect);
	}
	else
	{
		DestroyProjectile();
	}
}

void AFireBombProjectile::FindAndMoveToFloor()
{
	FVector Start = GetActorLocation();
	FVector End = Start + (FVector::UpVector * (AreaOfEffect->GetScaledSphereRadius() * -2.f));

	UWorld* World = GetWorld();
	if (World)
	{
		FHitResult Hit;
		if (World->LineTraceSingleByChannel(Hit, Start, End, ECollisionChannel::ECC_Camera))
		{
			SetActorLocation(Hit.Location);
		}
	}

	// Restore rotation (helps NavMesh update better with modifier)
	SetActorRotation(FQuat::Identity);
}

void AFireBombProjectile::OnAreaOfEffectDiminished()
{
	// Remove all the effects we applied to actors in area
	{
		TArray<AActor*> ActorsInArea;
		AreaOfEffect->GetOverlappingActors(ActorsInArea);

		for (AActor* Actor : ActorsInArea)
		{
			RemoveAreaEffectFrom(Actor);
		}
	}

	bIsAreaOfEffectActive = false;
	OnDiminished();
}

void AFireBombProjectile::RemoveAreaEffectFrom(AActor* Actor)
{
	UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Actor);
	if (ASC)
	{
		// Remove any effects applied by us
		FGameplayEffectQuery Query;
		Query.EffectSource = this;
		ASC->RemoveActiveEffects(Query);
	}
}

void AFireBombProjectile::AreaOfEffectStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == this || (OtherActor == Instigator && bIgnoreInstigator))
	{
		return;
	}

	if (!AreaEffect)
	{
		return;
	}

	UAbilitySystemComponent* InstigatorASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(Instigator);
	if (InstigatorASC)
	{
		UAbilitySystemComponent* TargetASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(OtherActor);
		if (TargetASC)
		{
			// We are the source of this effect
			FGameplayEffectContextHandle Context = InstigatorASC->MakeEffectContext();
			Context.AddSourceObject(this);

			InstigatorASC->ApplyGameplayEffectToTarget(AreaEffect.GetDefaultObject(), TargetASC, UGameplayEffect::INVALID_LEVEL, Context);
		}
	}
}

void AFireBombProjectile::AreaOfEffectEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor != this)
	{
		RemoveAreaEffectFrom(OtherActor);
	}
}
