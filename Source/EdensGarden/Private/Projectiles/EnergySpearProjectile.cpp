// Copyright of Sock Goblins

#include "EnergySpearProjectile.h"

AEnergySpearProjectile::AEnergySpearProjectile()
{
	bIgnoreInstigator = true;

	ProjectileResponse |= static_cast<int32>(EProjectileResponse::Overlap);
	ProjectileResponse |= static_cast<int32>(EProjectileResponse::Hit);
}

void AEnergySpearProjectile::OnProjectileOverlappedActor_Implementation(AActor* Actor)
{
	ApplyEffectToActor(Actor, PhaseEffect);
	OnPhase(Actor);
}
