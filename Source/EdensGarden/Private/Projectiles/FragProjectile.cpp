// Copyright of Sock Goblins

#include "FragProjectile.h"
#include "EdensGardenFunctionLibrary.h"

AFragProjectile::AFragProjectile()
{
	bIgnoreInstigator = false;
	bExplosionIgnoresInstigator = false;
	ProjectileLifeSpan = 10.f;
	ExplosionRadius = 400.f;
	
	ProjectileResponse = 1 << static_cast<int32>(EProjectileResponse::Hit);
}

void AFragProjectile::OnProjectileHitActor_Implementation(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit)
{
	Detonate();
}

void AFragProjectile::OnExpired_Implementation()
{
	Detonate();
}

void AFragProjectile::Detonate()
{
	TArray<FHitResult> HitResults;

	// Origin of explosion is projectile location
	FVector Origin = GetActorLocation();

	// Apply explosion effect to any actor caught within explosion radius
	{
		TArray<AActor*> IgnoreActors;
		IgnoreActors.Add(this);

		// We should also ignore our instigator
		if (bExplosionIgnoresInstigator)
		{
			IgnoreActors.Add(Instigator);
		}

		// Trace world, apply effects
		if (UEdensGardenFunctionLibrary::SphereLineOfSightTrace(this, Origin, ExplosionRadius, ECollisionChannel::ECC_WorldStatic, IgnoreActors, HitResults))
		{
			if (ExplosionEffect.IsValid())
			{
				for (const FHitResult& Result : HitResults)
				{
					ApplyEffectToActor(Result.GetActor(), ExplosionEffect);
				}
			}
		}
	}

	OnDetonate(Origin, HitResults);

	// We shouldn't exist after detonating
	DestroyProjectile();
}
