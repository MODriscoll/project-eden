// Copyright of Sock Goblins

#pragma once

#if EG_GAMEPLAY_DEBUGGER

#include "CoreMinimal.h"
#include "GameplayDebuggerCategory.h"

/** Gameplay debugger for edens gardens characters */
class FGameplayDebuggerCategory_EG : public FGameplayDebuggerCategory
{
public:

	FGameplayDebuggerCategory_EG();

public:

	// Begin FGameplayDebuggerCategory Interface
	virtual void CollectData(APlayerController* OwnerPC, AActor* DebugActor) override;
	virtual void DrawData(APlayerController* OwnerPC, FGameplayDebuggerCanvasContext& CanvasContext) override;
	// End FGameplayDebuggerCategory Interface

public:

	static TSharedRef<FGameplayDebuggerCategory> MakeInstance();

protected:

	struct FDebugData
	{
		FString DebugString;

		void Serialize(FArchive& Ar);
	};

	FDebugData DebugData;
};

#endif
