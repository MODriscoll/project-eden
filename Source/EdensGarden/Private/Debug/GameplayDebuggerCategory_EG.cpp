// Copyright of Sock Goblins

#include "GameplayDebuggerCategory_EG.h"

#if EG_GAMEPLAY_DEBUGGER

#include "Characters/EGCharacter.h"
#include "GameFramework/PlayerController.h"

FGameplayDebuggerCategory_EG::FGameplayDebuggerCategory_EG()
{
	bShowOnlyWithDebugActor = false;

	SetDataPackReplication<FDebugData>(&DebugData);
}

TSharedRef<FGameplayDebuggerCategory> FGameplayDebuggerCategory_EG::MakeInstance()
{
	return MakeShareable(new FGameplayDebuggerCategory_EG());
}

void FGameplayDebuggerCategory_EG::FDebugData::Serialize(FArchive& Ar)
{
	Ar << DebugString;
}

void FGameplayDebuggerCategory_EG::CollectData(APlayerController* OwnerPC, AActor* DebugActor)
{
	DebugData.DebugString.Empty();

	AEGCharacter* Character = Cast<AEGCharacter>(DebugActor);
	if (!Character && OwnerPC)
	{
		// Try using the players pawn instead
		Character = Cast<AEGCharacter>(OwnerPC->GetPawn());
	}

	if (Character)
	{
		Character->GetDebugString(DebugData.DebugString);
	}
}

void FGameplayDebuggerCategory_EG::DrawData(APlayerController* OwnerPC, FGameplayDebuggerCanvasContext& CanvasContext)
{
	CanvasContext.Printf(TEXT("%s"), *DebugData.DebugString);
}

#endif