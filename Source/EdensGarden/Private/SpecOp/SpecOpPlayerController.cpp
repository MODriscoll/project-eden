// Copyright of Sock Goblins

#include "SpecOpPlayerController.h"
#include "SpecOpCameraManager.h"
#include "SpecOpCharacter.h"
#include "SpecOpCheatManager.h"
#include "SpecOpHUD.h"

#include "TimerManager.h"
#include "Components/InputComponent.h"
#include "Engine/LocalPlayer.h"

DEFINE_LOG_CATEGORY(LogSpecOpController);

ASpecOpPlayerController::ASpecOpPlayerController()
{
	CheatClass = USpecOpCheatManager::StaticClass();
	PlayerCameraManagerClass = ASpecOpCameraManager::StaticClass();
	bShowMouseCursor = false;

	GameOverScreenStallTime = 3.f;
}

void ASpecOpPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Pause input binding, we want to be able to execute it while paused
	{
		FInputActionBinding PauseBinding("Pause", EInputEvent::IE_Pressed);
		PauseBinding.bExecuteWhenPaused = true;
		PauseBinding.ActionDelegate.BindDelegate(this, &ASpecOpPlayerController::Pause);

		InputComponent->AddActionBinding(PauseBinding);
	}

	#if WITH_EDITOR
	// Escape is already used to end play in editor, so use a different key instead
	UWorld* World = GetWorld();
	if (World && World->WorldType == EWorldType::PIE)
	{
		FInputKeyBinding PIEPauseBinding(FInputChord(EKeys::P), EInputEvent::IE_Pressed);
		PIEPauseBinding.bExecuteWhenPaused = true;
		PIEPauseBinding.KeyDelegate.BindDelegate(this, &ASpecOpPlayerController::Pause);

		InputComponent->KeyBindings.Add(PIEPauseBinding);
	}
	#endif
}

bool ASpecOpPlayerController::SetPause(bool bPause, FCanUnpause CanUnpauseDelegate)
{
	// We shouldn't be able to pause while dead
	if (bPause && (SpecOp && SpecOp->IsDead()))
	{
		return false;
	}

	bool bResult = Super::SetPause(bPause, CanUnpauseDelegate);

	ASpecOpHUD* HUD = GetSpecOpHUD();
	if (bResult && HUD)
	{
		if (bPause)
		{
			PauseGame();
		}
		else
		{
			UnpauseGame();
		}
	}

	return bResult;
}

void ASpecOpPlayerController::Possess(APawn* Pawn)
{
	Super::Possess(Pawn);

	SpecOp = Cast<ASpecOpCharacter>(Pawn);
	if (SpecOp)
	{
		SpecOp->OnCharacterDeath.AddDynamic(this, &ASpecOpPlayerController::OnSpecOpDeath);
	}
	else
	{
		UE_LOG(LogSpecOpController, Warning, TEXT("Possess a pawn but pawn is not a spec op character!"));
	}
}

void ASpecOpPlayerController::UnPossess()
{
	if (SpecOp)
	{
		SpecOp->OnCharacterDeath.RemoveDynamic(this, &ASpecOpPlayerController::OnSpecOpDeath);

		CleanUpHUD();
	}

	Super::UnPossess();
	SpecOp = nullptr;
}

void ASpecOpPlayerController::BeginPlay()
{
	Super::BeginPlay();

	SetInputMode(FInputModeGameOnly());

	if (SpecOp)
	{
		InitializeHUD();
	}

	ASpecOpHUD* HUD = GetSpecOpHUD();
	if (HUD)
	{
		HUD->DisplayMainHUD();
	}
}

void ASpecOpPlayerController::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	if (SpecOp)
	{
		CleanUpHUD();
	}

	Super::EndPlay(EndPlayReason);
}

void ASpecOpPlayerController::PauseGame()
{
	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;

	// Center the mouse, as rotating the camera has
	// probaly offset it to the corners of the screen
	CenterMouse();

	ASpecOpHUD* HUD = GetSpecOpHUD();
	if (HUD)
	{
		HUD->DisplayPauseMenu();
	}
}

void ASpecOpPlayerController::UnpauseGame()
{
	SetInputMode(FInputModeGameOnly());
	bShowMouseCursor = false;

	ASpecOpHUD* HUD = GetSpecOpHUD();
	if (HUD)
	{
		HUD->DisplayMainHUD();
	}
}

void ASpecOpPlayerController::CenterMouse()
{
	ULocalPlayer* LocalPlayer = Cast<ULocalPlayer>(Player);
	if (LocalPlayer)
	{
		UGameViewportClient* ViewportClient = LocalPlayer->ViewportClient;
		if (ViewportClient)
		{
			FVector2D ViewportSize;
			ViewportClient->GetViewportSize(ViewportSize);

			FViewport* Viewport = ViewportClient->Viewport;
			if (Viewport)
			{
				Viewport->SetMouse(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);
			}
		}
	}
}

void ASpecOpPlayerController::InitializeHUD()
{
	check(SpecOp);

	CleanUpHUD();

	SpecOpHUD = Cast<ASpecOpHUD>(MyHUD);
	if (SpecOpHUD.IsValid())
	{
		SpecOpHUD->Initialize(SpecOp);
	}
	else
	{
		UE_LOG(LogSpecOpController, Error, TEXT("Unable to initialize HUD. HUD is invalid"));
	}
}

void ASpecOpPlayerController::CleanUpHUD()
{
	if (SpecOpHUD.IsValid())
	{
		SpecOpHUD->CleanUp();
	}

	SpecOpHUD.Reset();
}

ASpecOpHUD* ASpecOpPlayerController::GetSpecOpHUD() const
{
	if (SpecOpHUD.IsValid())
	{
		return SpecOpHUD.Get();
	}

	return nullptr;
}
void ASpecOpPlayerController::DisplayEndScreen()
{
	SetInputMode(FInputModeUIOnly());
	bShowMouseCursor = true;
	CenterMouse();

	ASpecOpHUD* HUD = GetSpecOpHUD();
	if (HUD)
	{
		HUD->DisplayGameOverScreen();
	}
}

void ASpecOpPlayerController::OnSpecOpDeath(AEGCharacter* DeadCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags)
{
	if (ensure(SpecOp != nullptr) && SpecOp == DeadCharacter)
	{
		// Game should not be paused while dead!
		if (IsPaused())
		{
			SetPause(false);
		}

		if (GameOverScreenStallTime > 0.f)
		{
			// Have camera focus on the player for dramatic effect
			ASpecOpCameraManager* SpecCamManager = Cast<ASpecOpCameraManager>(PlayerCameraManager);
			if (SpecCamManager)
			{
				SpecCamManager->bFocusOnSpecOp = true;
			}

			DeadCharacter->DisableInput(this);

			// Allow for clear view
			ASpecOpHUD* HUD = GetSpecOpHUD();
			if (HUD)
			{
				HUD->HideMainHUD();
			}

			FTimerManager& TimerManager = GetWorldTimerManager();
			TimerManager.SetTimer(TimerHandle_EndScreenStall, this, &ASpecOpPlayerController::OnEndScreenStallTimeComplete, GameOverScreenStallTime, false);
		}
		else
		{
			DisplayEndScreen();
		}
	}
}

void ASpecOpPlayerController::OnEndScreenStallTimeComplete()
{
	DisplayEndScreen();
}
