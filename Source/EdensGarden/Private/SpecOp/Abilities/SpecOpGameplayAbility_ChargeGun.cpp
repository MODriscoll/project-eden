// Copyright of Sock Goblins

#include "SpecOpGameplayAbility_ChargeGun.h"
#include "SpecOp/SpecOpCharacter.h"
#include "Guns/Gun.h"
#include "Guns/GunAttributeSet.h"

#include "AbilitySystemComponent.h"

USpecOpGameplayAbility_ChargeGun::USpecOpGameplayAbility_ChargeGun()
{
	AmmoAttritbuteOverride = UAmmoGunAttributeSet::GetClipCountAttribute();
}

bool USpecOpGameplayAbility_ChargeGun::HasEnoughAmmo(float RequiredAmmo) const
{
	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		FGameplayAttribute AmmoAttribute = GetAmmoAttribute();

		UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();
		if (GunASC && GunASC->HasAttributeSetForAttribute(AmmoAttribute))
		{
			float CurValue = GunASC->GetNumericAttribute(AmmoAttribute);
			return (CurValue - RequiredAmmo) >= 0.f;
		}
		else
		{
			return false;
		}
	}

	return true;
}

FGameplayAttribute USpecOpGameplayAbility_ChargeGun::GetAmmoAttribute() const
{
	if (AmmoAttritbuteOverride.IsValid())
	{
		return AmmoAttritbuteOverride;
	}
	else
	{
		return UAmmoGunAttributeSet::GetClipCountAttribute();
	}
}
