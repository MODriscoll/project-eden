// Copyright of Sock Goblins

#include "SpecOpGameplayAbility_FireGun.h"
#include "SpecOp/SpecOpCharacter.h"
#include "Guns/Gun.h"
#include "Guns/GunAttributeSet.h"

#include "AbilitySystemBlueprintLibrary.h"

#include "TimerManager.h"

USpecOpGameplayAbility_FireGun::USpecOpGameplayAbility_FireGun()
{
	bCommitAbilityEveryShot = false;
	ShotFailedTryActivateTags.AddLeafTag(FGameplayTag::RequestGameplayTag("Weapon.Action.Reload"));

	bShouldEnd = false;
	BurstIteration = 0;

	ListenForTags.AddTag(FGameplayTag::RequestGameplayTag("Character.State.Falling"));
}

void USpecOpGameplayAbility_FireGun::OnTagEvent_Implementation(const FGameplayTag& Tag, bool bWasAdded)
{
	if (Tag.MatchesTag(FGameplayTag::RequestGameplayTag("Character.State.Falling")))
	{
		bShouldEnd = true;
	}
}

FGameplayEffectContextHandle USpecOpGameplayAbility_FireGun::MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const
{
	FGameplayEffectContextHandle Context = Super::MakeEffectContext(Handle, ActorInfo);

	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		Context.AddInstigator(ActorInfo->OwnerActor.Get(), Gun);
	}

	return Context;
}

bool USpecOpGameplayAbility_FireGun::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	// Weapon might not be a gun, but acts similar to one
	ASpecOpCharacter* Character = Cast<ASpecOpCharacter>(ActorInfo->AvatarActor.Get());
	AGunBase* EquippedGun = Character ? Character->GetEquippedGun() : nullptr;

	if (EquippedGun)
	{
		UAbilitySystemComponent* GunASC = EquippedGun->GetAbilitySystemComponent();
		if (ensure(GunASC) && GunASC->HasAttributeSetForAttribute(UAmmoGunAttributeSet::GetClipCountAttribute()))
		{
			float CurClipCount = GunASC->GetNumericAttribute(UAmmoGunAttributeSet::GetClipCountAttribute());

			// We might have ammo to take a shot
			if (CurClipCount > 0.f)
			{
				return true;
			}
			else
			{
				UAbilitySystemComponent* ASC = ActorInfo->AbilitySystemComponent.Get();
				check(ASC);

				// We might be reloading, which we should cancel
				FGameplayTag Reloading = FGameplayTag::RequestGameplayTag("Character.State.Reload");
				return !ASC->HasMatchingGameplayTag(Reloading);
			}
		}
		else
		{
			return true;
		}
	}

	return false;
}

void USpecOpGameplayAbility_FireGun::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData * TriggerEventData)
{
	AGunBase* Gun = GetEquippedGun();
	if (!Gun)
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
		return;
	}

	bShouldEnd = false; // Should be in pre-activate

	OnStartFiring();
	TryTakeShot(true);
}

void USpecOpGameplayAbility_FireGun::InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	bShouldEnd = true;
}

void USpecOpGameplayAbility_FireGun::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.ClearTimer(TimerHandle_TakeShot);
}

void USpecOpGameplayAbility_FireGun::FinishShot(float WaitInterval, bool bEndAbility)
{
	if (bEndAbility)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		return;
	}

	// Make sure weapon is still valid
	AGunBase* EquippedGun = GetEquippedGun();
	if (!(EquippedGun && EquippedGun->GetAbilitySystemComponent()))
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		return;
	}

	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	UAbilitySystemComponent* GunASC = EquippedGun->GetAbilitySystemComponent();

	// Re-check if we could possibly take another shot,
	// we might not be able to, so their is not point in waiting
	{
		bool bCanContinue = false;

		float AmmoRequired = 0.f;
		bCanContinue = GetAmmoToConsume(ASC, GunASC, AmmoRequired);
		if (bCanContinue)
		{
			bCanContinue = CanTakeShot(GunASC, AmmoRequired);
		}
		
		if (!bCanContinue)
		{
			// Try activating the other abilities (we might
			// have run out of ammo and now need to reload)
			OnShotFailed(ASC);
			return;
		}
	}

	// Validate interval
	WaitInterval = WaitInterval <= 0.f ? 0.2f : WaitInterval;
	
	FTimerManager& TimerManager = GetWorld()->GetTimerManager();
	TimerManager.SetTimer(TimerHandle_TakeShot, this, &USpecOpGameplayAbility_FireGun::OnShotIntervalPassed, WaitInterval, false);
}

float USpecOpGameplayAbility_FireGun::GetFireRateInterval(float FireRate) const
{
	if (FMath::IsNearlyZero(FireRate))
	{
		FireRate = 5.f;
	}

	return 1.f / FireRate;
}

void USpecOpGameplayAbility_FireGun::TryTakeShot(bool bFirstShot)
{
	// Need a valid weapon to shoot with
	AGunBase* EquippedGun = GetEquippedGun();
	if (!(EquippedGun && EquippedGun->GetAbilitySystemComponent()))
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, true);
		return;
	}

	// Are we able to continue firing?
	bool bShouldCommit = bCommitAbilityEveryShot ? true : bFirstShot;
	if (bShouldCommit && !CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo))
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
		return;
	}

	UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
	UAbilitySystemComponent* GunASC = EquippedGun->GetAbilitySystemComponent();

	bool bTakeShot = false;

	// Do we have the ammo required to take this shot?
	float AmmoRequired = 0.f;
	if (GetAmmoToConsume(ASC, GunASC, AmmoRequired))
	{
		bTakeShot = CanTakeShot(GunASC, AmmoRequired);
	}

	if (bTakeShot && CommitShot(GunASC, AmmoRequired))
	{
		AmmoConsumedThisShot = AmmoRequired;
		TakeShot(0, AmmoRequired);
	}
	else
	{
		OnShotFailed(ASC);
	}
}

bool USpecOpGameplayAbility_FireGun::CanTakeShot(UAbilitySystemComponent* WeaponASC, float AmmoRequired) const
{
	check(WeaponASC);

	FGameplayAttribute AmmoAttribute = GetAmmoAttribute();
	if (WeaponASC->HasAttributeSetForAttribute(AmmoAttribute))
	{
		float CurValue = WeaponASC->GetNumericAttribute(AmmoAttribute);
		return (CurValue - AmmoRequired) >= 0.f;
	}
	else
	{
		ABILITY_LOG(Error, TEXT("Unable to take shot! Weapon %s does not have attribute set for %s"),
			*WeaponASC->GetOwner()->GetName(), *AmmoAttribute.GetName());
	}

	return false;
}

bool USpecOpGameplayAbility_FireGun::CommitShot(UAbilitySystemComponent* WeaponASC, float AmmoConsumed)
{
	check(WeaponASC);

	if (FiringCostGameplayEffectClass)
	{
		FGameplayEffectSpecHandle SpecHandle = WeaponASC->MakeOutgoingSpec(FiringCostGameplayEffectClass, UGameplayEffect::INVALID_LEVEL, MakeEffectContext(CurrentSpecHandle, CurrentActorInfo));
		
		FGameplayTag AmmoTag = FGameplayTag::RequestGameplayTag(TEXT("SetByCaller.Ability.FireGunAmmo"));
		SpecHandle.Data->SetSetByCallerMagnitude(AmmoTag, -AmmoConsumed);

		WeaponASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);

		return true;
	}
	else
	{
		// Apply manually, this won't trigger post execute though
		// (TODO): maybe force weapons with this ability to be guns, and notify
		// gun directly instead of using gameplay effects?

		FGameplayAttribute AmmoAttribute = GetAmmoAttribute();
		if (WeaponASC->HasAttributeSetForAttribute(AmmoAttribute))
		{
			WeaponASC->ApplyModToAttribute(AmmoAttribute, EGameplayModOp::Additive, -AmmoConsumed);
			return true;
		}
	}

	return false;
}

void USpecOpGameplayAbility_FireGun::OnShotFailed(UAbilitySystemComponent* ASC)
{
	// Try to activate another ability (for example, a reload ability)
	if (ShotFailedTryActivateTags.IsValid())
	{
		check(ASC);
		ASC->TryActivateAbilitiesByTag(ShotFailedTryActivateTags);
	}

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

void USpecOpGameplayAbility_FireGun::CommitAndTakeShot(bool bFirstShot)
{
	// First commit this ability
	bool bShouldCommit = bCommitAbilityEveryShot ? true : bFirstShot;
	if (bShouldCommit && !CommitAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo))
	{
		OnShotFailed();
		return;
	}

	// Check if we can take another shot
	if (!CanTakeShot())
	{
		OnShotFailed();
		return;
	}

	if (bFirstShot)
	{
		OnStartFiring();
	}
}

bool USpecOpGameplayAbility_FireGun::CanTakeShot()
{
	return CheckFiringCost();
}

bool USpecOpGameplayAbility_FireGun::CheckFiringCost()
{
	AGunBase* Gun = GetEquippedGun();
	if (!Gun)
	{
		return true;
	}

	const FGameplayAbilityActorInfo* ActorInfo = GetCurrentActorInfo();
	UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();

	check(ActorInfo);

	UGameplayEffect* Cost = FiringCostGameplayEffectClass ? FiringCostGameplayEffectClass->GetDefaultObject<UGameplayEffect>() : nullptr;
	if (Cost && GunASC)
	{
		return GunASC->CanApplyAttributeModifiers(Cost, GetAbilityLevel(CurrentSpecHandle, ActorInfo), MakeEffectContext(CurrentSpecHandle, ActorInfo));
	}

	return true;
}

void USpecOpGameplayAbility_FireGun::OnShotFailed()
{
	// Try to activate another ability (for example, a reload ability)
	if (ShotFailedTryActivateTags.IsValid())
	{
		UAbilitySystemComponent* ASC = CurrentActorInfo->AbilitySystemComponent.Get();
		check(ASC);

		ASC->TryActivateAbilitiesByTag(ShotFailedTryActivateTags);
	}

	EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
}

bool USpecOpGameplayAbility_FireGun::GetAmmoToConsume(UAbilitySystemComponent* ASC, UAbilitySystemComponent* WeaponASC, float& OutAmmo) const
{
	check(ASC);
	check(WeaponASC);

	// Check if we (or the weapon) has infinite ammo
	{
		bool bHasInfAmmo = false;
		FGameplayTag InfiniteAmmo = FGameplayTag::RequestGameplayTag(TEXT("Weapon.Ammo.Infinite"));

		// Does owner have infinite ammo?
		bHasInfAmmo = ASC->HasMatchingGameplayTag(InfiniteAmmo);
		
		// Does our equipped weapon have infinite ammo?
		if (!bHasInfAmmo)
		{
			bHasInfAmmo = WeaponASC->HasMatchingGameplayTag(InfiniteAmmo);
		}

		if (bHasInfAmmo)
		{
			OutAmmo = 0.f;
			return true;
		}
	}

	OutAmmo = CalculateAmmoToConsume();
	return OutAmmo > 0.f;
}

FGameplayAttribute USpecOpGameplayAbility_FireGun::GetAmmoAttribute() const
{
	if (AmmoAttritbuteOverride.IsValid())
	{
		return AmmoAttritbuteOverride;
	}
	else
	{
		return UAmmoGunAttributeSet::GetClipCountAttribute();
	}
}

UGameplayEffect* USpecOpGameplayAbility_FireGun::GetFiringCostGameplayEffect() const
{
	if (FiringCostGameplayEffectClass)
	{
		return FiringCostGameplayEffectClass->GetDefaultObject<UGameplayEffect>();
	}
	else
	{
		return nullptr;
	}
}

void USpecOpGameplayAbility_FireGun::OnShotIntervalPassed()
{
	// We might have stopped shooting while waiting (we check here to prevent
	// the interval from being cheated by activating the ability over and over)
	if (bShouldEnd)
	{
		EndAbility(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, true, false);
	}
	else
	{
		TryTakeShot(false);
	}
}
