// Copyright of Sock Goblins

#include "SpecOpGameplayAbility_Focus.h"
#include "SpecOpCharacter.h"

#include "AbilitySystemComponent.h"
#include "GameplayAbilitySpec.h"

USpecOpGameplayAbility_Focus::USpecOpGameplayAbility_Focus()
{
	bCanFocusWhileFalling = false;
	FocusPoint.TargetArmLength = 85.f;
	FocusPoint.SocketOffset = FVector(0.f, 60.f, 15.f);
	BlendTime = 0.125f;

	ListenForTags.AddTag(FGameplayTag::RequestGameplayTag("Character.State.Falling"));
	ListenForTags.AddTag(FGameplayTag::RequestGameplayTag("Character.State.Rolling"));
}

void USpecOpGameplayAbility_Focus::OnTagEvent_Implementation(const FGameplayTag& Tag, bool bWasAdded)
{
	if (Tag.MatchesTag(FGameplayTag::RequestGameplayTag("Character.State.Falling")))
	{
		// Remove focus while falling, apply it when landed
		if (bWasAdded && !bCanFocusWhileFalling)
		{
			bIsFalling = true;
			RemoveFocus();
		}
		else
		{
			if (!bIsRolling)
			{
				ApplyFocus();
			}

			bIsFalling = false;
		}
	}
	else if (Tag.MatchesTag(FGameplayTag::RequestGameplayTag("Character.State.Rolling")))
	{
		// Remove focus while rolling, apply it when stopped
		if (bWasAdded)
		{
			bIsRolling = true;
			RemoveFocus();
		}
		else
		{
			if (!bIsFalling)
			{
				ApplyFocus();
			}

			bIsRolling = false;
		}
	}
}

bool USpecOpGameplayAbility_Focus::CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const
{
	if (!Super::CanActivateAbility(Handle, ActorInfo, SourceTags, TargetTags, OptionalRelevantTags))
	{
		return false;
	}

	const ASpecOpCharacter* Character = Cast<ASpecOpCharacter>(ActorInfo->AvatarActor.Get());
	return (Character && Character->CanFocus());
}

void USpecOpGameplayAbility_Focus::ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
{
	ASpecOpCharacter* SpecOp = GetSpecOp();
	if (!SpecOp || !CommitAbility(Handle, ActorInfo, ActivationInfo))
	{
		EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
		return;
	}

	bIsFalling = false;
	bIsFocusing = false;

	ApplyFocus();
}

void USpecOpGameplayAbility_Focus::InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
{
	EndAbility(Handle, ActorInfo, ActivationInfo, true, false);
}

void USpecOpGameplayAbility_Focus::EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled)
{
	Super::EndAbility(Handle, ActorInfo, ActivationInfo, bReplicateEndAbility, bWasCancelled);

	RemoveFocus();
}

void USpecOpGameplayAbility_Focus::ApplyFocus()
{
	if (!bIsFocusing)
	{
		// Apply optional effect
		if (FocusGameplayEffect)
		{
			FocusEffectHandle = ApplyGameplayEffectToOwner(CurrentSpecHandle, CurrentActorInfo, CurrentActivationInfo, FocusGameplayEffect.GetDefaultObject(), 1);
		}

		ASpecOpCharacter* SpecOp = GetSpecOp();
		UFocusingSpringArmComponent* CameraBoom = SpecOp->GetCameraBoom();
		CameraBoom->SetNewFocusSpot(FocusPoint, BlendTime, BlendFuncs);

		bIsFocusing = true;
	}
}

void USpecOpGameplayAbility_Focus::RemoveFocus()
{
	if (bIsFocusing)
	{
		ASpecOpCharacter* SpecOp = GetSpecOp();
		UFocusingSpringArmComponent* CameraBoom = SpecOp->GetCameraBoom();
		CameraBoom->RestoreDefaultFocusSpot(true, BlendTime, BlendFuncs);

		// Clear focusing effect
		if (FocusEffectHandle.IsValid())
		{
			BP_RemoveGameplayEffectFromOwnerWithHandle(FocusEffectHandle);
		}

		bIsFocusing = false;
	}
}