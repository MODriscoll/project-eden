// Copyright of Sock Goblins

#include "SpecOpCheatManager.h"
#include "SpecOpPlayerController.h"
#include "SpecOpCharacter.h"

#include "AmmoGun.h"
#include "MonsterCharacter.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"
#include "EdensGardenAssetManager.h"
#include "EdensGardenFunctionLibrary.h"
#include "EdensGardenUserSettings.h"
#include "Engine/World.h"
#include "Kismet/KismetMathLibrary.h"

#include "GameplayEffect_AmmoBox.h"

void USpecOpCheatManager::God()
{
	APlayerController* Controller = GetOuterAPlayerController();
	if (Controller)
	{
		APawn* Pawn = Controller->GetPawn();
		IAbilitySystemInterface* SystemInterface = Cast<IAbilitySystemInterface>(Pawn);
		if (SystemInterface && SystemInterface->GetAbilitySystemComponent() != nullptr)
		{
			UAbilitySystemComponent* ASC = SystemInterface->GetAbilitySystemComponent();
			const FGameplayTag InvicibilityTag = FGameplayTag::RequestGameplayTag(TEXT("Damage.Invincible"));
			
			if (ASC->HasMatchingGameplayTag(InvicibilityTag))
			{
				ASC->SetLooseGameplayTagCount(InvicibilityTag, 0);
				Controller->ClientMessage(TEXT("God Off"));
			}
			else
			{
				ASC->SetLooseGameplayTagCount(InvicibilityTag, 1);
				Controller->ClientMessage(TEXT("God On"));
			}
		}
	}
}

void USpecOpCheatManager::SpawnWeapon(const FName& WeaponName)
{
	FPrimaryAssetId WeaponAssetId(UEdensGardenAssetManager::WeaponType, WeaponName);
	UEdensGardenAssetManager& AssetManager = UEdensGardenAssetManager::Get();

	TSubclassOf<AWeaponBase> WeaponClass = AssetManager.LoadWeapon(WeaponAssetId, false);
	if (WeaponClass)
	{
		APlayerController* Controller = GetOuterAPlayerController();
		if (Controller)
		{
			APawn* Pawn = Controller->GetPawn();
			if (Pawn)
			{
				// Move infront of pawn a bit
				FVector SpawnLocation = Pawn->GetActorLocation();
				FVector Offset = Pawn->GetActorForwardVector() * 150.f;

				UWorld* World = GetWorld();
				if (World)
				{
					FActorSpawnParameters Params;
					Params.ObjectFlags = RF_Transient;
					Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

					AWeaponBase* WeaponInstance = World->SpawnActor<AWeaponBase>(WeaponClass, SpawnLocation + Offset, FRotator::ZeroRotator, Params);
					if (WeaponInstance)
					{
						Controller->ClientMessage(TEXT("Weapon Spawned"));
					}
				}
			}
		}
	}
}

void USpecOpCheatManager::SpawnMonster(const FName& MonsterName)
{
	FPrimaryAssetId MonsterAssetId(UEdensGardenAssetManager::MonsterType, MonsterName);
	UEdensGardenAssetManager& AssetManager = UEdensGardenAssetManager::Get();

	TSubclassOf<AMonsterCharacter> MonsterClass = AssetManager.LoadMonster(MonsterAssetId, false);
	if (MonsterClass)
	{
		APlayerController* Controller = GetOuterAPlayerController();
		if (Controller)
		{
			FHitResult Hit;
			if (GetTarget(Controller, Hit) != nullptr)
			{
				// Move away from surface a bit
				FVector SpawnLocation = Hit.Location;
				FVector Offset = Hit.Normal * 150.f;

				// Have it face direction of normal (only on yaw axis)
				FRotator SpawnRotation = UKismetMathLibrary::MakeRotFromX(Hit.Normal);
				SpawnRotation.Pitch = 0.f;
				SpawnRotation.Roll = 0.f;

				UWorld* World = GetWorld();
				if (World)
				{
					FActorSpawnParameters Params;
					Params.ObjectFlags = RF_Transient;
					Params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

					AMonsterCharacter* MonsterInstance = World->SpawnActor<AMonsterCharacter>(MonsterClass, SpawnLocation, SpawnRotation, Params);
					if (MonsterInstance)
					{
						Controller->ClientMessage(TEXT("Monster Spawned"));
					}
				}
			}
		}
	}
}

void USpecOpCheatManager::SetTeam(EEdensGardenTeamID TeamID)
{
	APlayerController* Controller = GetOuterAPlayerController();
	if (Controller)
	{
		ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(Controller->GetPawn());
		if (SpecOp)
		{
			SpecOp->SetGenericTeamId(FGenericTeamId(static_cast<uint8>(TeamID)));
			Controller->ClientMessage(TEXT("Team Changed"));
		}
	}
}

void USpecOpCheatManager::SetTeamTarget(EEdensGardenTeamID TeamID)
{
	APlayerController* Controller = GetOuterAPlayerController();
	FHitResult Hit;
	AActor* TargetActor = GetTarget(Controller, Hit);
	if (TargetActor)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(TargetActor);
		if (Character)
		{
			Character->SetGenericTeamId(FGenericTeamId(static_cast<uint8>(TeamID)));
			Controller->ClientMessage(TEXT("Targets Team Changed"));
		}
	}
}

void USpecOpCheatManager::Kill()
{
	APlayerController* Controller = GetOuterAPlayerController();
	if (Controller)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(Controller->GetPawn());
		if (Character)
		{
			Character->Kill();
			Controller->ClientMessage(TEXT("Self Killed"));
		}
	}
}

void USpecOpCheatManager::KillTarget()
{
	APlayerController* Controller = GetOuterAPlayerController();
	FHitResult Hit;
	AActor* TargetActor = GetTarget(Controller, Hit);
	if (TargetActor)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(TargetActor);
		if (Character)
		{
			Character->Kill();
			Controller->ClientMessage(FString::Printf(TEXT("Character %s Killed"), *Character->GetName()));
		}
	}
}

void USpecOpCheatManager::Ragdoll()
{
	APlayerController* Controller = GetOuterAPlayerController();
	if (Controller)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(Controller->GetPawn());
		if (Character)
		{
			if (Character->IsRagdolling())
			{
				Character->ExitRagdoll();
				Controller->ClientMessage(TEXT("Ragdoll Stopped"));
			}
			else
			{
				Character->EnterRagdoll();
				Controller->ClientMessage(TEXT("Ragdoll Entered"));
			}
		}
	}
}

void USpecOpCheatManager::RagdollTarget()
{
	APlayerController* Controller = GetOuterAPlayerController();
	FHitResult Hit;
	AActor* TargetActor = GetTarget(Controller, Hit);
	if (TargetActor)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(TargetActor);
		if (Character)
		{
			if (Character->IsRagdolling())
			{
				Character->ExitRagdoll();
				Controller->ClientMessage(FString::Printf(TEXT("Character %s Ragdolled Stopped"), *Character->GetName()));
			}
			else
			{
				Character->EnterRagdoll();
				Controller->ClientMessage(FString::Printf(TEXT("Character %s Ragdolled Entered"), *Character->GetName()));
			}
		}
	}
}

void USpecOpCheatManager::RagdollTargetWithImpulse(float Power)
{
	APlayerController* Controller = GetOuterAPlayerController();
	FHitResult Hit;
	AActor* TargetActor = GetTarget(Controller, Hit);
	if (TargetActor)
	{
		AEGCharacter* Character = Cast<AEGCharacter>(TargetActor);
		if (Character && !Character->IsRagdolling())
		{
			FVector Direction = (Hit.TraceEnd - Hit.TraceStart).GetSafeNormal();

			Character->EnterRagdollAndApplyImpulse(Direction * Power, Hit.BoneName);
			Controller->ClientMessage(FString::Printf(TEXT("Character %s Ragdolled Entered"), *Character->GetName()));
		}
	}
}

void USpecOpCheatManager::RefillAmmo()
{
	APlayerController* Controller = GetOuterAPlayerController();
	ASpecOpCharacter* SpecOp = Controller ? Cast<ASpecOpCharacter>(Controller->GetPawn()) : nullptr;
	if (SpecOp)
	{
		const TArray<AGunBase*>& AllGuns = SpecOp->GetAllGuns();
		for (AGunBase* Gun : AllGuns)
		{
			AAmmoGun* AmmoGun = Cast<AAmmoGun>(Gun);
			if (AmmoGun)
			{
				// Should be enough
				float AmountToGive = AmmoGun->GetAmmoNeededToRefill(99999);

				UAbilitySystemComponent* ASC = AmmoGun->GetAbilitySystemComponent();
				if (ASC)
				{
					const UAbilitySystemGlobals& AbilityGlobals = UAbilitySystemGlobals::Get();

					// Set player as source of effect
					FGameplayEffectContextHandle Context(AbilityGlobals.AllocGameplayEffectContext());
					Context.AddInstigator(SpecOp, SpecOp);
					Context.AddSourceObject(SpecOp);

					// Should always be valid (check to be safe)
					FGameplayEffectSpecHandle SpecHandle = ASC->MakeOutgoingSpec(UGameplayEffect_AmmoBox::StaticClass(), UGameplayEffect::INVALID_LEVEL, Context);
					if (SpecHandle.IsValid())
					{
						// Need to dynamically set ammo to give
						SpecHandle.Data->SetSetByCallerMagnitude(UGameplayEffect_AmmoBox::AmmoDataName, AmountToGive);
						ASC->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);
					}
				}
			}
		}
	}
	
}

void USpecOpCheatManager::TravelToLevel(const FName& LevelName)
{
	APlayerController* Controller = GetOuterAPlayerController();
	if (Controller)
	{
		// Character should be alive
		AEGCharacter* Character = Cast<AEGCharacter>(Controller->GetPawn());
		if (Character && !Character->IsDead())
		{
			UEdensGardenFunctionLibrary::NotifyGameLevelEndReached(Controller, LevelName);
		}
	}
}

void USpecOpCheatManager::SetSavingEnabled(int32 Value)
{
	UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	if (UserSettings)
	{
		UserSettings->SetSavingEnabled(static_cast<bool>(Value));
	}
}

void USpecOpCheatManager::SetSubtitlesEnabled(int32 Value)
{
	UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	if (UserSettings)
	{
		UserSettings->SetSubtitlesEnabled(static_cast<bool>(Value));
	}
}
