// Copyright of Sock Goblins

#include "SpecOpAbilitySystemComponent.h"
#include "SpecOpCharacter.h"
#include "Weapons/WeaponAttributeSet.h"

static TArray<int32> CharacterPowersInputIDs = { (int32)EPowerInputID::Slot1, (int32)EPowerInputID::Slot2, (int32)EPowerInputID::Slot3, (int32)EPowerInputID::Slot4 };

USpecOpAbilitySystemComponent::USpecOpAbilitySystemComponent()
{
	bWantsInitializeComponent = true;
}

void USpecOpAbilitySystemComponent::OnRemoveAbility(FGameplayAbilitySpec& AbilitySpec)
{
	// Need to remove our reference to this ability if its something we bound
	FGameplayAbilitySpecHandle* SpecHandle = BoundPowers.Find(AbilitySpec.InputID);
	if (SpecHandle && *SpecHandle == AbilitySpec.Handle)
	{
		ensure(AbilitySpec.SourceObject == this);
		BoundPowers.Remove(AbilitySpec.InputID);

		UE_LOG(LogSpecOp, Log, TEXT("Removing Power. InputID: %i"), AbilitySpec.InputID);
	}

	Super::OnRemoveAbility(AbilitySpec);
}

void USpecOpAbilitySystemComponent::InitializeComponent()
{
	Super::InitializeComponent();

	GameplayTagCountContainer.RegisterGenericGameplayEvent().AddUObject(this, &USpecOpAbilitySystemComponent::OnGameplayTagCountChanged);
}

void USpecOpAbilitySystemComponent::OnComponentDestroyed(bool bDestroyingHierarchy)
{
	// We need to remove the external attribute sets as the actual owners might not be getting destroyed
	for (UAttributeSet* Set : ExternalAttributeSets)
	{
		SpawnedAttributes.Remove(Set);
	}

	ExternalAttributeSets.Empty();

	Super::OnComponentDestroyed(bDestroyingHierarchy);
}

void USpecOpAbilitySystemComponent::BindAbilityActivationToEGInputs(UInputComponent* InputComponent)
{
	BindAbilityActivationToInputComponent(InputComponent, FGameplayAbiliyInputBinds("ConfirmInput", "CancelInput", "EEdensGardenInputID"));
}

FGameplayAbilitySpecHandle USpecOpAbilitySystemComponent::BindAbilityToInput(TSubclassOf<UGameplayAbility> Ability, EEdensGardenInputID InputID)
{
	if (!Ability)
	{
		return FGameplayAbilitySpecHandle();
	}

	return GiveAbility(FGameplayAbilitySpec(Ability.GetDefaultObject(), 1, (int32)InputID, nullptr));
}

EPowerInputID USpecOpAbilitySystemComponent::GivePower(TSubclassOf<UGameplayAbility> PowerUp, bool bForce)
{
	if (!PowerUp)
	{
		return EPowerInputID::None;
	}

	// Cycle through all possible inputs
	for (int32 ID : CharacterPowersInputIDs)
	{
		if (!BoundPowers.Contains(ID))
		{
			BoundPowers.Add(ID, GiveAbility(FGameplayAbilitySpec(PowerUp.GetDefaultObject(), 1, ID, this)));
			return (EPowerInputID)ID;
		}
	}

	// Force binding this power
	if (bForce)
	{
		UnbindPowerAtInput(EPowerInputID::Slot1);
		BoundPowers.Add((int32)EPowerInputID::Slot1, GiveAbility(FGameplayAbilitySpec(PowerUp.GetDefaultObject(), 1, (int32)EPowerInputID::Slot1, this)));

		return EPowerInputID::Slot1;
	}

	return EPowerInputID::None;
}

bool USpecOpAbilitySystemComponent::BindPowerToInput(TSubclassOf<UGameplayAbility> PowerUp, EPowerInputID InputID, bool bForce)
{
	if (InputID == EPowerInputID::None)
	{
		return false;
	}

	bool bBind = bForce;

	if (BoundPowers.Contains((int32)InputID))
	{
		if (bForce)
		{
			UnbindPowerAtInput(InputID);
		}
	}
	else
	{
		bBind = true;
	}

	if (bBind)
	{
		BoundPowers.Add((int32)InputID, GiveAbility(FGameplayAbilitySpec(PowerUp.GetDefaultObject(), 1, (int32)InputID, this)));
	}

	return bBind;
}

void USpecOpAbilitySystemComponent::UnbindPowerAtInput(EPowerInputID InputID)
{
	FGameplayAbilitySpecHandle* SpecHandlePtr = BoundPowers.Find((int32)InputID);
	if (SpecHandlePtr)
	{
		// Need a copy as removing the key will destroy the handle
		FGameplayAbilitySpecHandle SpecHandle = *SpecHandlePtr;

		BoundPowers.Remove((int32)InputID);
		
		if (SpecHandle.IsValid())
		{
			ClearAbility(SpecHandle);
		}
	}
}

void USpecOpAbilitySystemComponent::ClearPowers()
{
	TArray<FGameplayAbilitySpecHandle> SpecHandles;
	BoundPowers.GenerateValueArray(SpecHandles);

	// Clear the map to prevent OnRemoveAbility from doing what we do here
	BoundPowers.Empty();

	for (const FGameplayAbilitySpecHandle& Handle : SpecHandles)
	{
		if (Handle.IsValid())
		{
			ClearAbility(Handle);
		}
	}
}

bool USpecOpAbilitySystemComponent::IsAnyPowerBoundToInput(EPowerInputID InputID, bool bMustBeValid) const
{
	const FGameplayAbilitySpecHandle* SpecHandle = BoundPowers.Find((int32)InputID);
	if (SpecHandle)
	{
		if (bMustBeValid)
		{
			return SpecHandle->IsValid();
		}
		else
		{
			return true;
		}
	}

	return false;
}

void USpecOpAbilitySystemComponent::AddSharedAttributeSet(UAttributeSet* AttributeSet)
{
	if (AttributeSet)
	{
		if (!ExternalAttributeSets.Contains(AttributeSet))
		{
			SpawnedAttributes.AddUnique(AttributeSet);
			ExternalAttributeSets.Add(AttributeSet);

			bIsNetDirty = true;
		}
	}
}

void USpecOpAbilitySystemComponent::RemoveSharedAttribute(UAttributeSet* AttributeSet)
{
	if (ExternalAttributeSets.Contains(AttributeSet))
	{
		SpawnedAttributes.Remove(AttributeSet);
		ExternalAttributeSets.Remove(AttributeSet);

		bIsNetDirty = true;
	}
}

void USpecOpAbilitySystemComponent::OnGameplayTagCountChanged(const FGameplayTag GameplayTag, int32 Count)
{
	const FGameplayTag HUDRoot = FGameplayTag::RequestGameplayTag(TEXT("HUD"));
	if (HUDRoot.IsValid() && GameplayTag.MatchesTag(HUDRoot))
	{
		OnHUDTagCountChanged.Broadcast(GameplayTag, Count);
	}
}
