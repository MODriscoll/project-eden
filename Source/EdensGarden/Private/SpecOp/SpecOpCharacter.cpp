// Copyright of Sock Goblins

#include "SpecOpCharacter.h"
#include "SpecOpAbilitySystemComponent.h"
#include "SpecOpAnimInstance.h"
#include "SpecOpAttributeSet.h"
#include "SpecOpMovementComponent.h"
#include "EdensGardenUserSettings.h"

#include "FocusingSpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/MeshComponent.h"
#include "Components/SpotlightComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"

#include "Weapons/Weapon.h"

#include "TimerManager.h"

// temp
#include "SpecOpPlayerController.h"
#include "SpecOpHUD.h"
#include "SpecOpHUDWidget.h"

#include "Guns/Gun.h"
#include "Weapons/MeleeWeapon.h"

DEFINE_LOG_CATEGORY(LogSpecOp);

static TAutoConsoleVariable<float> CVarHidePlayerAtBoomDistance(
	TEXT("eg.HidePlayerDistance"),
	50.f,
	TEXT("Will hide the players mesh (and all attached components) if cameras distance\n")
	TEXT("from the mesh is less than the given distance.\n")
	TEXT("Value of zero or lower means no hiding."),
	ECVF_Cheat);

ASpecOpCharacter::ASpecOpCharacter(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer
		.SetDefaultSubobjectClass<USpecOpAbilitySystemComponent>(AEGCharacter::AbilitySystemComponentName)
		.SetDefaultSubobjectClass<USpecOpAttributeSet>(AEGCharacter::AttributeSetName)
		.SetDefaultSubobjectClass<USpecOpMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	TurnRate = 45.f;
	FocusedTurnRate = 30.f;
	bInvertYTurnInput = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;
	InheritYawRotationSpeed = 5.f;

	TeamId = EEdensGardenTeamID::Human;

	IdentityTags.AddLeafTag(FGameplayTag::RequestGameplayTag(TEXT("Character.ID.SpecOp")));

	UCharacterMovementComponent* MovementComponent = GetCharacterMovement();
	MovementComponent->bOrientRotationToMovement = true;
	MovementComponent->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	MovementComponent->JumpZVelocity = 400.f;
	MovementComponent->AirControl = 0.2f;
	MovementComponent->NavAgentProps.bCanCrouch = true;

	UCapsuleComponent* Capsule = GetCapsuleComponent();
	Capsule->InitCapsuleSize(42.f, 96.0f);

	CameraBoom = CreateDefaultSubobject<UFocusingSpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f;	
	CameraBoom->bUsePawnControlRotation = true; 

	// We want to tick after our camera boom, so we can
	// use up to data information for hiding the mesh
	{
		AddTickPrerequisiteComponent(CameraBoom);
	}

	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	Camera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	Camera->bUsePawnControlRotation = false; 

	InteractiveArea = CreateDefaultSubobject<UInteractiveComponent>(TEXT("InteractiveArea"));
	InteractiveArea->SetupAttachment(RootComponent);
	InteractiveArea->InitBoxExtent(FVector(90.f, 90.f, 120.f));

	HelmetTorch = CreateDefaultSubobject<USpotLightComponent>(TEXT("HelmetTorch"));
	HelmetTorch->SetupAttachment(GetMesh(), TEXT("HelmetTorchSocket"));
	HelmetTorch->SetMobility(EComponentMobility::Movable);
	HelmetTorch->SetVisibility(false);

	// TODO: Helmet torch should not light the player (same with weapons in inventory)

	SpecOpAbilitySystem = CastChecked<USpecOpAbilitySystemComponent>(GetAbilitySystemComponent());
	SpecOpAttributeSet = CastChecked<USpecOpAttributeSet>(GetAttributeSet());
	SpecOpMovementComponent = CastChecked<USpecOpMovementComponent>(GetCharacterMovement());

	MaxGunInventorySize = 4;
	bGunScrollSkipsHolstering = false;
	bShouldSwitchGun = false;
	bIsMeleeInHand = false;
}

void ASpecOpCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	check(PlayerInputComponent);

	// Movement controls
	PlayerInputComponent->BindAxis("MoveForward", this, &ASpecOpCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ASpecOpCharacter::MoveRight);

	// Rotation controls
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &ASpecOpCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ASpecOpCharacter::LookUpAtRate);

	// Weapon controls
	PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &ASpecOpCharacter::ScrollNextGun);
	PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &ASpecOpCharacter::ScrollPreviousGun);

	// General controls
	SpecOpAbilitySystem->BindAbilityActivationToEGInputs(PlayerInputComponent);
}

void ASpecOpCharacter::UpdateSensitivityValues()
{
	UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	if (UserSettings)
	{
		TurnRate = UserSettings->GetSensitivity(false);
		FocusedTurnRate = UserSettings->GetSensitivity(true);
		bInvertYTurnInput = UserSettings->ShouldInvertYAxis();
	}
}

void ASpecOpCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * (IsFocusing() ? FocusedTurnRate : TurnRate) * GetWorld()->GetDeltaSeconds());
}

void ASpecOpCharacter::LookUpAtRate(float Rate)
{
	Rate = bInvertYTurnInput ? -Rate : Rate;
	AddControllerPitchInput(Rate * (IsFocusing() ? FocusedTurnRate : TurnRate) * GetWorld()->GetDeltaSeconds());
}

void ASpecOpCharacter::InitializeAttributeDefaults()
{
	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	ASC->InitStats(USpecOpAttributeSet::StaticClass(), AttributeDefaults);
}

void ASpecOpCharacter::OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	// Might be hidden due to actor being to close
	SetActorHiddenInGame(false);

	// Drop whatever weapon may be in our hand
	{
		// Dropping weapons first, as cancelling abilities could potentially
		// remove melee from hand, but we want it to drop it while in hand 

		// Might be performing melee strike
		if (bIsMeleeInHand)
		{
			check(MeleeWeapon);
			MeleeWeapon->DetachWeaponFromOwnerOnly();

			if (EquippedGun)
			{
				EquippedGun->SetActorHiddenInGame(false);
			}
		}

		// Drop equipped gun (only if physically equipped, it
		// wouldn't make sense to drop the gun thats in our hand
		if (EquippedGun && EquippedGun->IsPhysicallyEquipped())
		{
			EquippedGun->DetachWeaponFromOwnerOnly();
		}
	}

	Super::OnDeath_Implementation(Damage, bValidHit, Hit, DamageInstigator, Source, GameplayTags);

	// Exit any other state (we shouldn't be in since we are dead)
	TryEnterState(ESpecOpState::Idle);

	// Stop any interaction
	if (InteractiveArea)
	{
		InteractiveArea->SetAutoRefreshClosestInteractive(false);
		InteractiveArea->UpdateClosestInteractive(true);
	}
}

void ASpecOpCharacter::OnRagdoll_Implementation()
{
	// Stops any action we are performing
	TryEnterState(ESpecOpState::Idle);
}

void ASpecOpCharacter::OnRagdollStop_Implementation()
{
	if (EquippedGun)
	{
		USpecOpAnimInstance* AnimInstance = GetSpecOpAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->SetGunAnimationSet(EquippedGun->GetAnimationSet());
		}
	}
}

bool ASpecOpCharacter::CanCrouch()
{
	if (Super::CanCrouch() && !IsRolling())
	{
		if (!EquippedGun || EquippedGun->CanCrouchWhileEquipped())
		{
			return true;
		}
	}

	return false;
}

void ASpecOpCharacter::Landed(const FHitResult& Hit)
{
	Super::Landed(Hit);
	TryEnterWantingStates(true);
}

void ASpecOpCharacter::BeginPlay()
{
	Super::BeginPlay();

	State = ESpecOpState::Idle;

	UpdateSensitivityValues();
	BindDefaultAbilities();
}

void ASpecOpCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	const FName FocusName = TEXT("Character.State.Focusing");
	FGameplayTag FocusTag = FGameplayTag::RequestGameplayTag(FocusName);

	const FName OrientationName = TEXT("Character.State.Orientation");
	FGameplayTag OrientationTag = FGameplayTag::RequestGameplayTag(OrientationName);

	SpecOpAbilitySystem->RegisterGameplayTagEvent(FocusTag).AddUObject(this, &ASpecOpCharacter::OnFocusTagUpdated);
	SpecOpAbilitySystem->RegisterGameplayTagEvent(OrientationTag).AddUObject(this, &ASpecOpCharacter::OnOrientationTagUpdated);
}

void ASpecOpCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Update the visibility of our mesh (only if controlled by local player)
	if (IsPlayerControlled() && !(bIsRagdoll || bIsDead))
	{
		float Distance = CameraBoom->GetSocketTransform(USpringArmComponent::SocketName, RTS_Component).GetTranslation().Size();
		float CVarMaxDistance = CVarHidePlayerAtBoomDistance.GetValueOnGameThread();

		// We should hide our mesh if camera is too close to it
		bool bShouldHide = Distance < CVarMaxDistance;
		SetActorHiddenInGame(bShouldHide);
	}
}

void ASpecOpCharacter::SetActorHiddenInGame(bool bNewHidden)
{
	if (bHidden != bNewHidden)
	{		
		USkeletalMeshComponent* Mesh = GetMesh();
		Mesh->SetHiddenInGame(bNewHidden, true);

		// Hide all attached guns
		for (AGunBase* Gun : GunInventory)
		{
			if (Gun)
			{
				Gun->SetActorHiddenInGame(bNewHidden);
			}
		}

		// Hide melee weapon
		if (MeleeWeapon)
		{
			MeleeWeapon->SetActorHiddenInGame(bNewHidden);
		}

		bHidden = bNewHidden;
	}
}

void ASpecOpCharacter::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (EndPlayReason == EEndPlayReason::Destroyed)
	{
		DropAllGuns(true);
		SetMeleeWeapon(nullptr);
	}
}

void ASpecOpCharacter::FellOutOfWorld(const UDamageType& DamageType)
{
	// Destroy all of the guns we had equipped, as they have also fallen out of world
	DestroyAllGuns();
	DestroyMeleeWeapon();

	Super::FellOutOfWorld(DamageType);
}

#if WITH_EDITOR
void ASpecOpCharacter::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	UWorld* World = GetWorld();

	FName PropertyName = (PropertyChangedEvent.Property != nullptr) ? PropertyChangedEvent.Property->GetFName() : NAME_None;
	if (PropertyName == GET_MEMBER_NAME_CHECKED(ASpecOpCharacter, MaxGunInventorySize))
	{
		if (World)
		{
			SetGunInventorySize(MaxGunInventorySize, World->WorldType);
		}
	}

	// Whenever changing a property in PIE, all abilities are cleared
	if (World && World->WorldType == EWorldType::PIE)
	{
		SpecOpAbilitySystem->ClearPowers();
		BindDefaultAbilities();
	}
}
#endif

void ASpecOpCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void ASpecOpCharacter::MoveRight(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

#if EG_GAMEPLAY_DEBUGGER
void ASpecOpCharacter::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}Energy: %s%.2f {white}/ Max Energy: {green}%.2f\n"
		"{white}Breath: %s%.2f {white}/ Max Breath: {green}%.2f\n"
		"{white}Jump Mod: {green}%.2f{white}, Sprint Mod: {green}%.2f{white}, Sneak Mod: {green}%.2f{white}, Sound Mod: {green}%.2f\n"
		"{white}Is Crouching = {green}%s{white}, Is Sprinting = {green}%s{white}, Is Sneaking = {green}%s\n"
		"{white}Is Focusing = {green}%s{white}, Is Rolling = {green}%s\n\n"
		"{white}Guns: {green}%i{white}/ Max Guns: {green}%i\n"
		"%s\n%s"); // Guns and Melee weapons

	float Energy = GetEnergy();
	float MaxEnergy = GetMaxEnergy();
	float Breath = GetBreath();
	float MaxBreath = GetMaxBreath();

	float ERatio = Energy / MaxEnergy;
	float BRatio = Breath / MaxBreath;

	FString GunsString;
	for (int32 Index = 0; Index < GunInventory.Num(); ++Index)
	{
		// TODO: Make a get debug string in weapon
		AGunBase* Gun = GunInventory[Index];
		if (Gun)
		{
			GunsString.Append(FString::Printf(TEXT("{Blue}Slot %i:\n"), Index + 1));
			Gun->GetDebugString(GunsString);
		}
		else
		{
			GunsString.Append(FString::Printf(TEXT("{Blue}Slot %i: {green}NULL\n"), Index + 1));
		}
	}

	FString MeleeString;
	if (MeleeWeapon)
	{
		MeleeString = FString(TEXT("{white}Melee Weapon:\n")); 
		MeleeWeapon->GetDebugString(MeleeString);
	}
	else
	{
		MeleeString.Append(TEXT("{white}Melee Weapon: NULL\n"));
	}

	String.Append(FString::Printf(*Formatting,
		ERatio <= 0.1f ? TEXT("{red}") : (ERatio <= 0.4f ? *FString("{yellow}") : *FString("{green}")), Energy, MaxEnergy,
		BRatio <= 0.1f ? TEXT("{red}") : (BRatio <= 0.4f ? *FString("{yellow}") : *FString("{green}")), Breath, MaxBreath,
		GetJumpModifier(), GetSprintModifier(), GetSneakModifier(), GetSoundModifier(),
		bIsCrouched ? TEXT("True") : TEXT("False"), IsSprinting() ? TEXT("True") : TEXT("False"), IsSneaking() ? TEXT("True") : TEXT("False"),
		IsFocusing() ? TEXT("True") : TEXT("False"), IsRolling() ? TEXT("True") : TEXT("False"),
		GunInventory.Num(), MaxGunInventorySize, *GunsString, *MeleeString));
}
#endif

float ASpecOpCharacter::GetBreath() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetBreath();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetMaxBreath() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetMaxBreath();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetEnergy() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetEnergy();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetMaxEnergy() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetMaxEnergy();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetSprintModifier() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetSprintModifier();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetSneakModifier() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetSneakModifier();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetJumpModifier() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetJumpModifier();
	}
	else
	{
		return 0.f;
	}
}

float ASpecOpCharacter::GetSoundModifier() const
{
	if (SpecOpAttributeSet)
	{
		return SpecOpAttributeSet->GetSoundModifier();
	}
	else
	{
		return 0.f;
	}
}

void ASpecOpCharacter::OnOutOfEnergy_Implementation(float Delta, const FGameplayTagContainer& GameplayTags)
{
	/*if (HelmetTorch->IsVisible())
	{
		HelmetTorch->SetVisibility(false);
	}*/
}

void ASpecOpCharacter::UpdateClosestInteractive()
{
	if (InteractiveArea)
	{
		InteractiveArea->UpdateClosestInteractive();
	}
}

bool ASpecOpCharacter::CanInteract() const
{
	return CanPerformAnyAction();
}

bool ASpecOpCharacter::IsSprinting(bool bConsiderVelocity) const
{
	// We might be stuck under something that is forcing 
	// us to crouch even though we are sprinting
	UCharacterMovementComponent* MovementComp = GetCharacterMovement();
	if (MovementComp && !bIsCrouched )
	{
		if (State == ESpecOpState::Sprinting && bIsSprinting && !(MovementComp->IsFalling()))
		{
			// We might also be standing still
			return bConsiderVelocity ? GetVelocity().SizeSquared2D() > 1.f : true;
		}
	}

	return false;
}

bool ASpecOpCharacter::IsSneaking() const
{
	return State == ESpecOpState::Sneaking && bIsSneaking && !(SpecOpMovementComponent->IsFalling());
}

bool ASpecOpCharacter::IsFocusing() const
{
	return State == ESpecOpState::Focusing && bIsFocusing;
}

bool ASpecOpCharacter::IsRolling() const
{
	return State == ESpecOpState::Rolling && bIsRolling;
}

void ASpecOpCharacter::StartSprinting()
{
	bWantsToSprint = true;
	TryEnterState(ESpecOpState::Sprinting);
}

void ASpecOpCharacter::StopSprinting()
{
	bWantsToSprint = false;
	TryEnterState(ESpecOpState::Idle);
}

void ASpecOpCharacter::StartSneaking()
{
	bWantsToSneak = true;
	TryEnterState(ESpecOpState::Sneaking);
}

void ASpecOpCharacter::StopSneaking()
{
	bWantsToSneak = false;
	TryEnterState(ESpecOpState::Idle);
}

void ASpecOpCharacter::StartFocusing()
{
	bWantsToFocus = true;
	TryEnterState(ESpecOpState::Focusing);
}

void ASpecOpCharacter::StopFocusing()
{
	bWantsToFocus = false;
	TryEnterState(ESpecOpState::Idle);
}

void ASpecOpCharacter::Roll()
{
	TryEnterState(ESpecOpState::Rolling);
}

void ASpecOpCharacter::StopRolling()
{
	// Manually stopping rolling here, as
	// no other state should override roll
	InternalStopRolling();
	TryEnterState(ESpecOpState::Idle);
}

void ASpecOpCharacter::InternalStartSprinting()
{
	if (!bIsSprinting)
	{
		bIsSprinting = true;

		UnCrouch();
		InternalStopFocusing();

		CancelPendingHolster();
	}
}

void ASpecOpCharacter::InternalStopSprinting()
{
	bIsSprinting = false;
}

void ASpecOpCharacter::InternalStartSneaking()
{
	if (!bIsSneaking)
	{
		bIsSneaking = true;

		InternalStopSprinting();
	}
}

void ASpecOpCharacter::InternalStopSneaking()
{
	bIsSneaking = false;
}

void ASpecOpCharacter::InternalStartFocusing()
{
	if (!bIsFocusing)
	{
		bIsFocusing = true;

		InternalStopSprinting();
	}
}

void ASpecOpCharacter::InternalStopFocusing()
{
	bIsFocusing = false;
}

// Rolling works via root motion in the anim instance of our mesh

void ASpecOpCharacter::InternalStartRolling(ESpecOpState FromState)
{
	if (!bIsRolling)
	{
		bIsRolling = true;

		Crouch();
		InternalStopSprinting();
		InternalStopSneaking();

		CancelPendingHolster();

		// If not focusing, we want to roll in the direction our controller is looking at (keep?)
		// If using this, should use the non-focus roll method 2 in spec op anim instace
		if (GetVelocity().SizeSquared2D() <= 1.f &&
			FromState != ESpecOpState::Focusing && Controller)
		{
			FRotator ActorRotation = GetActorRotation();
			FRotator ControlRotation = Controller->GetControlRotation();
			ControlRotation.Pitch = ActorRotation.Pitch;
			ControlRotation.Roll = ActorRotation.Roll;

			SetActorRotation(ControlRotation);
		}

		// We want to be able to roll off of ledges
		bCachedCanWalkOffLedgesWhenCrouching = SpecOpMovementComponent->bCanWalkOffLedgesWhenCrouching;
		SpecOpMovementComponent->bCanWalkOffLedgesWhenCrouching = true;

		SpecOpAbilitySystem->AddLooseGameplayTag(FGameplayTag::RequestGameplayTag(TEXT("Character.State.Rolling")));
	}
}

void ASpecOpCharacter::InternalStopRolling()
{
	if (bIsRolling)
	{
		bIsRolling = false;

		UnCrouch();

		SpecOpMovementComponent->bCanWalkOffLedgesWhenCrouching = bCachedCanWalkOffLedgesWhenCrouching;

		SpecOpAbilitySystem->RemoveLooseGameplayTag(FGameplayTag::RequestGameplayTag(TEXT("Character.State.Rolling")));
	}
}

bool ASpecOpCharacter::CanPerformAnyAction(bool bFromLanded) const
{
	// When landing (through Landed event), Movement mode is still set to falling
	return (!SpecOpMovementComponent->IsFalling() || bFromLanded) && !(bIsDead || bIsRagdoll);
}

bool ASpecOpCharacter::CanSprint(bool bFromLanded) const
{
	if (CanPerformAnyAction(bFromLanded) && !IsRolling())
	{
		return CanSprintWithEquippedGun();
	}

	return false;
}

bool ASpecOpCharacter::CanSneak(bool bFromLanded) const
{
	return CanPerformAnyAction(bFromLanded) && !IsRolling();
}

bool ASpecOpCharacter::CanFocus(bool bFromLanded) const
{
	return CanPerformAnyAction(bFromLanded) && !IsRolling();
}

bool ASpecOpCharacter::CanRoll(bool bFromLanded) const
{
	if (CanPerformAnyAction(bFromLanded))
	{
		// We shouldn't start rolling again if we are still animating the
		// current roll. Doing so could possibly lock us in a rolling state
		USpecOpAnimInstance* AnimInstance = GetSpecOpAnimInstance();
		if (AnimInstance && AnimInstance->IsAnimatingRoll())
		{
			return false;
		}

		return CanRollWithEquippedGun();
	}

	return false;
}

bool ASpecOpCharacter::CanSprintWithEquippedGun() const
{
	if (EquippedGun)
	{
		if (EquippedGun->CanSprintWhileEquipped())
		{
			// Gun must be physically equipped for either
			// holster or equip ability to be cancelled
			return EquippedGun->IsPhysicallyEquipped();
		}
		else
		{
			return false;
		}
	}
	else
	{
		// We can sprint with no gun
		return true;
	}
}

bool ASpecOpCharacter::CanRollWithEquippedGun() const
{
	if (EquippedGun)
	{
		if (EquippedGun->CanRollWhileEquipped())
		{
			// Gun must be physically equipped for either
			// holster or equip ability to be cancelled
			return EquippedGun->IsPhysicallyEquipped();
		}
		else
		{
			return false;
		}
	}
	else
	{
		// We can roll with no gun
		return true;
	}
}

bool ASpecOpCharacter::ShouldFaceCamera() const
{
	// Should be facing camera if focusing
	if (bOrientationFocused || IsFocusing() || IsHelmetTorchActive())
	{
		// Orientation should updated based on movement if sprinting or rolling
		if (!(IsSprinting(false) || IsRolling()))
		{
			return true;
		}
	}

	return false;
}

void ASpecOpCharacter::TryEnterState(ESpecOpState NewState)
{
	if (NewState != State)
	{
		// Exit current state
		switch (State)
		{
			case ESpecOpState::Sprinting: { InternalStopSprinting(); break; }
			case ESpecOpState::Sneaking: { InternalStopSneaking(); break; }
			case ESpecOpState::Focusing: { InternalStopFocusing(); break; }
			default: { }
		}

		switch (NewState)
		{
			case ESpecOpState::Idle:
			{
				// We might be wanting to perform other actions
				TryEnterWantingStates();
				return;
			}

			case ESpecOpState::Sprinting:
			{
				if (CanSprint())
				{
					InternalStartSprinting();
					State = ESpecOpState::Sprinting;
				}

				break;
			}

			case ESpecOpState::Sneaking:
			{
				if (CanSneak())
				{
					InternalStartSneaking();
					State = ESpecOpState::Sneaking;
				}

				break;
			}

			case ESpecOpState::Focusing:
			{
				if (CanFocus())
				{
					InternalStartFocusing();
					State = ESpecOpState::Focusing;
				}

				break;
			}

			case ESpecOpState::Rolling:
			{
				if (CanRoll())
				{
					InternalStartRolling(State);
					State = ESpecOpState::Rolling;
				}

				break;
			}

			default:
			{

			}
		}

		DetermineOrientation();
	}
}

void ASpecOpCharacter::TryEnterWantingStates(bool bFromLandedEvent)
{
	// No real priority here
	if (bWantsToSprint && CanSprint(bFromLandedEvent))
	{
		InternalStartSprinting();
		State = ESpecOpState::Sprinting;
	}
	else if (bWantsToSneak && CanSneak(bFromLandedEvent))
	{
		InternalStartSneaking();
		State = ESpecOpState::Sneaking;
	}
	else if (bWantsToFocus && CanFocus(bFromLandedEvent))
	{
		InternalStartFocusing();
		State = ESpecOpState::Focusing;
	}
	else
	{
		// We can't override rolling
		if (!bIsRolling)
		{
			State = ESpecOpState::Idle;
		}
	}
	
	// Temp solution to helmet torch problem. Problem is when after rolling and going
	// straight into sprint. The character enters sprint but due to being crouched from
	// roll and when checking should face camera in determine orientation, the CanSprint
	// will return false as character is still technically crouched, even though we are wanting to sprint
	FTimerHandle DummyHandle;
	FTimerManager& TimerManager = GetWorldTimerManager();
	TimerManager.SetTimer(DummyHandle, this, &ASpecOpCharacter::DetermineOrientation, 0.1f);
}

void ASpecOpCharacter::DetermineOrientation()
{
	if (ShouldFaceCamera())
	{
		bUseControllerRotationYaw = true;
		SpecOpMovementComponent->bOrientRotationToMovement = false;
	}
	else
	{
		bUseControllerRotationYaw = false;
		SpecOpMovementComponent->bOrientRotationToMovement = true;
	}
}

void ASpecOpCharacter::OnCharacterInitialHit(AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	if (IsRolling())
	{
		AEGCharacter* OtherCharacter = Cast<AEGCharacter>(OtherActor);
		if (OtherCharacter)
		{
			const FGameplayTag RollStun = FGameplayTag::RequestGameplayTag(TEXT("Event.Stun.Roll"));
			OtherCharacter->Stun(GetActorLocation(), 2000.f, RollStun);
		}
	}
}

void ASpecOpCharacter::BindDefaultAbilities()
{
	RollBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(RollBinding.Ability, EEdensGardenInputID::Roll);
	CrouchBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(CrouchBinding.Ability, EEdensGardenInputID::Crouch);
	SprintBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(SprintBinding.Ability, EEdensGardenInputID::Sprint);
	SneakBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(SneakBinding.Ability, EEdensGardenInputID::Sneak);
	InteractBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(InteractBinding.Ability, EEdensGardenInputID::Interact);
	TorchBinding.Handle = SpecOpAbilitySystem->BindAbilityToInput(TorchBinding.Ability, EEdensGardenInputID::Torch);

	SpecOpAbilitySystem->GivePower(PowerUp1);
	SpecOpAbilitySystem->GivePower(PowerUp2);
	SpecOpAbilitySystem->GivePower(PowerUp3);
	SpecOpAbilitySystem->GivePower(PowerUp4);
}

void ASpecOpCharacter::OnFocusTagUpdated(const FGameplayTag Tag, int32 Stack)
{
	if (Stack >= 1)
	{
		StartFocusing();
	}
	else
	{
		StopFocusing();
	}
}

void ASpecOpCharacter::OnOrientationTagUpdated(const FGameplayTag Tag, int32 Stack)
{
	bOrientationFocused = Stack >= 1;
	DetermineOrientation();
}

void ASpecOpCharacter::NotifyBreathChanged(float NewBreath, float Delta, bool bOutOfBreath, const FGameplayTagContainer& GameplayTags)
{
	// Initially running out of breath
	if (bOutOfBreath && !bIsOutOfBreath)
	{
		ensureMsgf(NewBreath <= 0.f, TEXT("Notified as running out of breath but breath is greater than zero!"));

		bIsOutOfBreath = true;

		OnOutOfBreath(Delta, GameplayTags);
		OnOutOfBreath_Implementation(Delta, GameplayTags);
	}
	// Bit of breath being restored (from 0)
	else if (!bOutOfBreath && bIsOutOfBreath)
	{
		bIsOutOfBreath = false;

		OnBreathRestored(false);
	}
	
	// Breath being fully restored
	if (NewBreath >= GetMaxBreath())
	{
		OnBreathRestored(true);
	}

	bIsOutOfBreath = bOutOfBreath;

	OnBreathChanged(NewBreath, Delta, GameplayTags);
	OnBreathChanged_Implementation(NewBreath, Delta, GameplayTags);

	OnCharacterBreathChanged.Broadcast(this, NewBreath, Delta, GameplayTags);
}

void ASpecOpCharacter::NotifyBreathRegenRateChanged(float NewRate, float Delta, const FGameplayTagContainer& GameplayTags)
{
	OnBreathRegenRateChanged(NewRate, Delta, GameplayTags);
	OnBreathRegenRateChanged_Implementation(NewRate, Delta, GameplayTags);
}

void ASpecOpCharacter::NotifyBreathRegenerated(float NewBreath, float Delta, bool bFullyRestored)
{
	OnBreathRegenerated(NewBreath, Delta, bFullyRestored);
	OnBreathRegenerated_Implementation(NewBreath, Delta, bFullyRestored);
}

void ASpecOpCharacter::NotifyEnergyChanged(float NewEnergy, float Delta, bool bOutOfEnergy, const FGameplayTagContainer& GameplayTags)
{
	// Initially running out of energy
	if (bOutOfEnergy && !bIsOutOfEnergy)
	{
		ensureMsgf(NewEnergy <= 0.f, TEXT("Notified as running out of energy but energy is greater than zero!"));

		bIsOutOfEnergy = true;

		OnOutOfEnergy(Delta, GameplayTags);
		OnOutOfEnergy_Implementation(Delta, GameplayTags);
	}
	// Bit of energy being restored (from 0)
	else if (!bOutOfEnergy && bIsOutOfEnergy)
	{
		bIsOutOfEnergy = false;

		OnEnergyRestored(false);
	}

	// Energy being fully restored
	if (NewEnergy >= GetMaxEnergy())
	{
		OnEnergyRestored(true);
	}
	
	bIsOutOfEnergy = bOutOfEnergy;

	OnEnergyChanged(NewEnergy, Delta, GameplayTags);
	OnEnergyChanged_Implementation(NewEnergy, Delta, GameplayTags);

	OnCharacterEnergyChanged.Broadcast(this, NewEnergy, Delta, GameplayTags);
}

void ASpecOpCharacter::NotifyEnergyRegenRateChanged(float NewRate, float Delta, bool bIsIdleRate, const FGameplayTagContainer& GameplayTags)
{
	OnEnergyRegenRateChanged(NewRate, Delta, bIsIdleRate, GameplayTags);
	OnEnergyRegenRateChanged_Implementation(NewRate, Delta, bIsIdleRate, GameplayTags);
}

void ASpecOpCharacter::NotifyEnergyRegenerated(float NewEnergy, float Delta, bool bFullyRestored)
{
	OnEnergyRegenerated(NewEnergy, Delta, bFullyRestored);
	OnEnergyRegenerated_Implementation(NewEnergy, Delta, bFullyRestored);
}

void ASpecOpCharacter::NotifySprintModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags)
{
	OnSprintModifierChanged(NewModifier, Delta, GameplayTags);
	OnSprintModifierChanged_Implementation(NewModifier, Delta, GameplayTags);
}

void ASpecOpCharacter::NotifySneakModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags)
{
	OnSneakModifierChanged(NewModifier, Delta, GameplayTags);
	OnSneakModifierChanged_Implementation(NewModifier, Delta, GameplayTags);
}

void ASpecOpCharacter::NotifyJumpModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags)
{
	OnJumpModifierChanged(NewModifier, Delta, GameplayTags);
	OnJumpModifierChanged_Implementation(NewModifier, Delta, GameplayTags);
}

void ASpecOpCharacter::ApplyRecoil(float YawRecoil, float PitchRecoil)
{
	AddControllerYawInput(YawRecoil);
	AddControllerPitchInput(PitchRecoil);
}

void ASpecOpCharacter::ApplyRandomRecoil(float YawMin, float YawMax, float PitchMin, float PitchMax)
{
	if (YawMin > YawMax)
	{
		Swap(YawMin, YawMax);
	}

	if (PitchMin > PitchMax)
	{
		Swap(PitchMin, PitchMax);
	}

	ApplyRecoil(FMath::RandRange(YawMin, YawMax), FMath::RandRange(PitchMin, PitchMax));
}

void ASpecOpCharacter::OnInstigatedDamage(AEGCharacter* Character, float Damage, const FGameplayTagContainer& GameplayTags)
{
	if (Character != this)
	{
		ASpecOpPlayerController* Controller = Cast<ASpecOpPlayerController>(GetController());
		if (Controller && Controller->GetSpecOpHUD() && Controller->GetSpecOpHUD()->GetMainHUDWidget())
		{
			Controller->GetSpecOpHUD()->GetMainHUDWidget()->DisplayHitMarker();
		}
	}
}

void ASpecOpCharacter::SetHelmetTorchEnabled(bool bEnable)
{
	HelmetTorch->SetVisibility(bEnable);
	DetermineOrientation();

	UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
	if (ASC)
	{
		const FGameplayTag TorchIcon = FGameplayTag::RequestGameplayTag(TEXT("HUD.Icon.Torch"));
		ASC->SetLooseGameplayTagCount(TorchIcon, static_cast<int32>(bEnable));
	}
}

bool ASpecOpCharacter::ToggleHelmetTorch()
{
	bool bEnable = !HelmetTorch->IsVisible();
	SetHelmetTorchEnabled(bEnable);
	return bEnable;
}

bool ASpecOpCharacter::IsHelmetTorchActive() const
{
	return HelmetTorch->IsVisible();
}

USpecOpAnimInstance* ASpecOpCharacter::GetSpecOpAnimInstance() const
{
	USkeletalMeshComponent* Mesh = GetMesh();
	UAnimInstance* AnimInstance = Mesh ? Mesh->GetAnimInstance() : nullptr;

	if (AnimInstance)
	{
		return Cast<USpecOpAnimInstance>(AnimInstance);
	}

	return nullptr;
}

bool ASpecOpCharacter::GiveGun(AGunBase* Gun, bool bForce)
{
	int32 Index = AddGunToInventory(Gun, bForce);
	if (Index != INDEX_NONE)
	{
		// We need to auto equip this gun if its our only one
		if (Index < 1)
		{
			EquipGunInInventory(Index, false);
		}

		return true;
	}
	
	return false;
}

bool ASpecOpCharacter::GiveGunDontEquip(AGunBase* Gun)
{
	return AddGunToInventory(Gun, true) != INDEX_NONE;
}

bool ASpecOpCharacter::GiveAndEquipGun(AGunBase* Gun, bool bForce, bool bSkipEquip)
{
	int32 Index = AddGunToInventory(Gun, bForce);
	if (Index != INDEX_NONE)
	{
		EquipGunAtIndex(Index, bSkipEquip);
		return true;
	}

	return false;
}

bool ASpecOpCharacter::DropGun(AGunBase* Gun)
{
	int32 Index = GunInventory.Find(Gun);
	return DropAndReplaceGun(Index);
}

bool ASpecOpCharacter::DropGunAtIndex(int32 Index)
{
	return DropAndReplaceGun(Index);
}

void ASpecOpCharacter::DropAllGuns(bool bForce)
{
	if (bForce || CanDropGuns(true))
	{
		int32 NumGuns = GunInventory.Num();
		for (int32 Index = 0; Index < NumGuns; ++Index)
		{
			RemoveGunFromInventory(0, true);
		}

		// Current animation set might not support all animations (the default set should)
		if (USpecOpAnimInstance* AnimInstance = GetSpecOpAnimInstance())
		{
			AnimInstance->ResetGunAnimationSet();
		}
	}
}

void ASpecOpCharacter::DestroyAllGuns()
{
	for (int32 Index = 0; Index < GunInventory.Num();)
	{
		AGunBase* Gun = GunInventory[Index];
		if (Gun)
		{
			Gun->Destroy();
		}
	}

	EquippedGun = nullptr;
	GunInventory.Empty();

	// Current animation set might not support all animations (the default set should)
	if (USpecOpAnimInstance* AnimInstance = GetSpecOpAnimInstance())
	{
		AnimInstance->ResetGunAnimationSet();
	}
}

bool ASpecOpCharacter::CanDropGuns(bool bDropAll) const
{
	// If not dropping all, one gun should always be in inventory
	if (bDropAll || GunInventory.Num() > 1)
	{
		// We are able to drop guns while airborne
		if (CanPerformAnyAction(true) && !IsRolling())
		{
			// Shouldn't be able to perform actions during melee attack
			return MeleeWeapon ? !MeleeWeapon->IsMeleeAbilityActive() : true;
		}
	}

	return false;
}

bool ASpecOpCharacter::IsSwitchingGuns() const
{
	if (EquippedGun)
	{
		switch (EquippedGun->GetSwitchState())
		{
			case ESwitchGunState::Equipped:
			case ESwitchGunState::Holstered:
			{
				return false;
			}
			case ESwitchGunState::PendingEquip:
			case ESwitchGunState::PendingHolster:
			{
				return true;
			}
		}
	}

	return false;
}

bool ASpecOpCharacter::DropAndReplaceGun(int32 Index)
{
	// We might not be able to drop this gun just yet
	Index = RemoveGunFromInventory(Index);
	if (Index != INDEX_NONE)
	{
		// We might have dropped the last gun in inventory, which would make index out of range
		Index = FMath::Min(Index, GunInventory.Num() - 1);
		return EquipGunInInventory(Index, false);
	}

	return false;
}

int32 ASpecOpCharacter::AddGunToInventory(AGunBase* Gun, bool bForce)
{
	if (!Gun)
	{
		return INDEX_NONE;
	}

	// This gun might already be in our inventory
	int32 Index = GunInventory.Find(Gun);
	if (Index != INDEX_NONE)
	{
		return Index;
	}

	// If a gun is being replaced, we want to insert
	// this new gun where the old one is to keep order
	int32 OldIndex = INDEX_NONE;

	// We may not be able to add another gun at this point
	if (IsGunInventoryFull())
	{
		// Forcefully drop our equipped gun (this will allow player
		// to choose what gun they want to drop by equipping it)
		if (bForce)
		{
			int32 EquippedIndex = GunInventory.Find(EquippedGun);
			OldIndex = RemoveGunFromInventory(EquippedIndex, true);
		}
		else
		{
			return INDEX_NONE;
		}
	}

	// Keep original order of inventory
	if (OldIndex != INDEX_NONE)
	{
		Index = GunInventory.Insert(Gun, OldIndex);
	}
	else
	{
		Index = GunInventory.Add(Gun);
	}

	Gun->OnAddToInventory(this);
	OnWeaponInventoryUpdated.Broadcast(this, Gun, true);

	return Index;
}

int32 ASpecOpCharacter::RemoveGunFromInventory(int32 Index, bool bForce)
{
	if (!GunInventory.IsValidIndex(Index))
	{
		return INDEX_NONE;
	}

	if (!bForce)
	{
		// We might not be able to drop guns at this time
		if (!CanDropGuns())
		{
			return INDEX_NONE;
		}
	}

	// We might be switching this gun out (or have it equipped)
	AGunBase* Gun = GunInventory[Index];
	if (ensure(Gun))
	{
		// We might be dropping the gun we have equipped
		if (Gun == EquippedGun)
		{
			switch (EquippedGun->GetSwitchState())
			{		
				case ESwitchGunState::PendingEquip:
				{
					CancelPendingEquip();
					break;
				}
				case ESwitchGunState::PendingHolster:
				{
					CancelPendingHolster();
				}
				// Deliberate fall through
				case ESwitchGunState::Equipped:
				{
					EquippedGun->OnHolsterSkipped();
					break;
				}
			}

			EquippedGun = nullptr;

			// Calling this here, even though the high probability of this also
			// getting called again right after. Doing this as there is the
			// chance there is no other gun to equip right after
			OnEquippedGunUpdated.Broadcast(this, nullptr);
		}

		// If forced, we might be performing the melee attack.
		// If this is the case, the weapon will be hidden, which we should revert
		if (bIsMeleeInHand)
		{
			EquippedGun->SetActorHiddenInGame(false);
		}

		Gun->OnRemoveFromInventory();
	}

	GunInventory.RemoveAt(Index);
	OnWeaponInventoryUpdated.Broadcast(this, Gun, false);

	return Index;
}

void ASpecOpCharacter::EquipGun(AGunBase* Gun, bool bSkipHolster)
{
	int32 Index = GunInventory.Find(Gun);
	EquipGunInInventory(Index, bSkipHolster);
}

void ASpecOpCharacter::EquipGunAtIndex(int32 Index, bool bSkipHolster)
{
	EquipGunInInventory(Index, bSkipHolster);
}

void ASpecOpCharacter::EquipGunImmediately(int32 Index)
{
	// TODO: Be able to handle this based on equipped guns state
	// right now, this only gets called on game start after loading in save game
	if (!EquippedGun)
	{
		if (GunInventory.IsValidIndex(Index))
		{
			EquipGunWithAbility(GunInventory[Index], true);
		}
	}
}

void ASpecOpCharacter::EquipNextGun(bool bSkipHolster)
{
	// We want to base the next gun on the queued gun, rather
	// than the equipped gun, this will allow for guns to be skipped
	// over even while waiting for holster to finish
	AGunBase* SwitchFromGun = QueuedGun ? QueuedGun : EquippedGun;

	int32 Index = GunInventory.Find(SwitchFromGun);
	if (GunInventory.IsValidIndex(Index))
	{
		// Loop back to first gun in inventory
		if (++Index >= GunInventory.Num())
		{
			Index = 0;
		}
	}
	else
	{
		Index = 0;
	}

	EquipGunInInventory(Index, bSkipHolster);
}

void ASpecOpCharacter::EquipPreviousGun(bool bSkipHolster)
{
	// We want to base the next gun on the queued gun, rather
	// than the equipped gun, this will allow for guns to be skipped
	// over even while waiting for holster to finish
	AGunBase* SwitchFromGun = QueuedGun ? QueuedGun : EquippedGun;

	int32 Index = GunInventory.Find(SwitchFromGun);
	if (GunInventory.IsValidIndex(Index))
	{
		// Loop back to last gun in inventory
		if (--Index < 0)
		{
			Index = GunInventory.Num() - 1;
		}
	}
	else
	{
		Index = GunInventory.Num() - 1;
	}

	EquipGunInInventory(Index, bSkipHolster);
}

void ASpecOpCharacter::ScrollNextGun()
{
	EquipNextGun(bGunScrollSkipsHolstering);
}

void ASpecOpCharacter::ScrollPreviousGun()
{
	EquipPreviousGun(bGunScrollSkipsHolstering);
}

void ASpecOpCharacter::PhysicallyEquipWeapon()
{
	if (EquippedGun)
	{
		EquippedGun->OnPhysicallyEquipped();

		// Notify ability system
		{
			const FGameplayTag EquippedTag = FGameplayTag::RequestGameplayTag(TEXT("Event.SwitchWeapon.Equipped"));

			FGameplayEventData Payload;
			Payload.EventTag = EquippedTag;
			Payload.Instigator = this;
			Payload.OptionalObject = EquippedGun;

			FScopedPredictionWindow NewScopedWindow(SpecOpAbilitySystem);
			SpecOpAbilitySystem->HandleGameplayEvent(EquippedTag, &Payload);
		}
	}
}

void ASpecOpCharacter::PhysicallyHolsterWeapon()
{
	if (EquippedGun)
	{
		EquippedGun->OnPhysicallyHolstered();
		bShouldSwitchGun = true;

		// Notify ability system
		{
			const FGameplayTag HolsteredTag = FGameplayTag::RequestGameplayTag(TEXT("Event.SwitchWeapon.Holstered"));

			FGameplayEventData Payload;
			Payload.EventTag = HolsteredTag;
			Payload.Instigator = this;
			Payload.OptionalObject = EquippedGun;

			FScopedPredictionWindow NewScopedWindow(SpecOpAbilitySystem);
			SpecOpAbilitySystem->HandleGameplayEvent(HolsteredTag, &Payload);
		}
	}
}

bool ASpecOpCharacter::CanSwitchGuns() const
{
	// We are able to switch guns while airborne
	if (CanPerformAnyAction(true) && !IsRolling())
	{
		// Can't switch guns when performing the melee strike
		return MeleeWeapon ? !MeleeWeapon->IsMeleeAbilityActive() : true;
	}

	return false;
}

bool ASpecOpCharacter::EquipGunInInventory(int32 Index, bool bSkipHolster)
{
	if (!GunInventory.IsValidIndex(Index))
	{
		return false;
	}

	// We might be busy and unable to equip weapons
	if (!CanSwitchGuns())
	{
		return false;
	}

	// We should only be referencing valid weapons
	AGunBase* Gun = GunInventory[Index];
	if (!ensure(Gun))
	{
		GunInventory.RemoveAt(Index);
		return false;
	}

	StopSprinting();

	// Try equiping this gun (either instantly or latent)
	TryEquipGun(Gun, bSkipHolster);
	return true;
}

void ASpecOpCharacter::TryEquipGun(AGunBase* Gun, bool bSkipAction)
{
	if (!Gun)
	{
		return;
	}

	// We should stop holstering if equipping the same gun
	if (Gun == EquippedGun)
	{
		if (EquippedGun->IsPendingHolster())
		{
			CancelPendingHolster();			
		}

		return;
	}

	if (EquippedGun && !EquippedGun->IsHolstered())
	{
		switch (EquippedGun->GetSwitchState())
		{
			case ESwitchGunState::Equipped:
			case ESwitchGunState::PendingEquip:
			{
				HolsterAndQueueGun(Gun, bSkipAction);
				break;
			}
			case ESwitchGunState::PendingHolster:
			{
				// We can just queue this gun to be equipped
				QueuedGun = Gun;
			}
		}

		return;
	}

	// Equip gun directly if nothing else is equipped
	EquipGunWithAbility(Gun);
}

void ASpecOpCharacter::EquipGunWithAbility(AGunBase* Gun, bool bSkipAction)
{
	if (Gun)
	{
		EquippedGun = Gun;

		// Tracks if we are now waiting for gun to be equipped
		bool bIsPending = false;

		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			// We might be skipping this action
			const FGameplayAbilitySpecHandle& EquipHandle = Gun->GetEquipAbilityHandle();
			if (!bSkipAction && EquipHandle.IsValid() && ASC->TryActivateAbility(EquipHandle))
			{
				bIsPending = true;
			}
		}

		// Notify gun of its new status
		if (bIsPending)
		{
			BindSwitchGunCallback();
			EquippedGun->OnEquipStart();
		}
		else
		{
			EquippedGun->OnEquipSkipped();
		}

		// Post equip functionality
		{
			if (!EquippedGun->CanCrouchWhileEquipped())
			{
				UnCrouch();
			}

			OnEquippedGunUpdated.Broadcast(this, EquippedGun);
		}
	}
}

void ASpecOpCharacter::EquipQueuedGun()
{
	AGunBase* Gun = PopQueuedGun();
	
	if (Gun)
	{
		EquipGunWithAbility(Gun);
	}
	else
	{
		// This shouldn't be called, but in-case it is
		
		// Just re-equip this gun if we can't get queued weapon
		if (EquippedGun && EquippedGun->IsHolstered())
		{
			EquipGunWithAbility(EquippedGun);
		}
	}
}

void ASpecOpCharacter::HolsterAndQueueGun(AGunBase* GunToQueue, bool bSkipAction)
{
	check(EquippedGun);

	// Cancel potential equip in progress
	bool bEquipCancelled = false;
	if (EquippedGun->IsPendingEquip())
	{
		CancelPendingEquip();
		bEquipCancelled = true;
	}

	if (GunToQueue)
	{
		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			QueuedGun = GunToQueue;

			// We might be skipping this task all together
			const FGameplayAbilitySpecHandle& HolsterHandle = EquippedGun->GetHolsterAbilityHandle();
			if (!(bSkipAction || bEquipCancelled) && 
				HolsterHandle.IsValid() && ASC->TryActivateAbility(HolsterHandle))
			{
				BindSwitchGunCallback();
				EquippedGun->OnHolsterStart();
			}
			else
			{
				// We might have cancelled pending equip (meaning gun is holstered)
				if (!bEquipCancelled)
				{
					EquippedGun->OnHolsterSkipped();
				}

				EquipQueuedGun();
			}
		}
	}
}

bool ASpecOpCharacter::CancelPendingEquip()
{
	if (EquippedGun && EquippedGun->IsPendingEquip())
	{
		ClearSwitchGunCallback();
		EquippedGun->OnEquipCancelled();

		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			const FGameplayAbilitySpecHandle& EquipHandle = EquippedGun->GetEquipAbilityHandle();
			ASC->CancelAbilityHandle(EquipHandle);
		}

		return true;
	}

	return false;
}

bool ASpecOpCharacter::CancelPendingHolster()
{
	if (EquippedGun && EquippedGun->IsPendingHolster())
	{
		ClearSwitchGunCallback();
		EquippedGun->OnHolsterCancelled();

		// Resetting queued gun to reset gun scroll
		PopQueuedGun();
		
		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			const FGameplayAbilitySpecHandle& HolsterHandle = EquippedGun->GetHolsterAbilityHandle();
			ASC->CancelAbilityHandle(HolsterHandle);
		}

		return true;
	}

	return false;
}

void ASpecOpCharacter::BindSwitchGunCallback()
{
	if (!SwitchGunHandle.IsValid())
	{
		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			SwitchGunHandle = ASC->OnAbilityEnded.AddUObject(this, &ASpecOpCharacter::OnSwitchGunFinished);
		}
	}
}

void ASpecOpCharacter::ClearSwitchGunCallback()
{
	if (SwitchGunHandle.IsValid())
	{
		UAbilitySystemComponent* ASC = GetAbilitySystemComponent();
		if (ASC)
		{
			ASC->OnAbilityEnded.Remove(SwitchGunHandle);
			SwitchGunHandle.Reset();
		}
	}
}

void ASpecOpCharacter::OnSwitchGunFinished(const FAbilityEndedData& EndedData)
{
	if (!EquippedGun)
	{
		return;
	}

	switch (EquippedGun->GetSwitchState())
	{
		case ESwitchGunState::PendingEquip:
		{
			// Equip action should have finished
			const FGameplayAbilitySpecHandle& EquipHandle = EquippedGun->GetEquipAbilityHandle();
			if (EquipHandle == EndedData.AbilitySpecHandle)
			{
				ClearSwitchGunCallback();
				EquippedGun->OnEquipFinished();
			}

			break;
		}
		case ESwitchGunState::PendingHolster:
		{
			// Holster action should have finished
			const FGameplayAbilitySpecHandle& HolsterHandle = EquippedGun->GetHolsterAbilityHandle();
			if (HolsterHandle == EndedData.AbilitySpecHandle)
			{
				if (bShouldSwitchGun)
				{
					ClearSwitchGunCallback();
					EquippedGun->OnHolsterFinished();
					bShouldSwitchGun = false;

					// We should have a gun waiting to be equipped
					EquipQueuedGun();
				}
				else
				{
					CancelPendingHolster();
				}
			}

			break;
		}
	}
}

void ASpecOpCharacter::RemoveWeapon(AWeaponBase* Weapon)
{
	if (Weapon == MeleeWeapon)
	{
		SetMeleeWeapon(nullptr);
	}
	else
	{
		AGunBase* Gun = Cast<AGunBase>(Weapon);
		if (Gun)
		{
			if (Gun == EquippedGun)
			{
				EquippedGun = nullptr;
			}

			int32 Index = GunInventory.Find(Gun);
			if (Index != INDEX_NONE)
			{
				if (Weapon == EquippedGun)
				{
					EquipNextGun(true);
				}

				GunInventory.RemoveAt(Index);
			}
		}
	}
}

void ASpecOpCharacter::AttachWeaponToMesh(AWeaponBase* Weapon, const FName& Socket) const
{
	if (Weapon)
	{
		USkeletalMeshComponent* WeaponMesh = Weapon->GetMesh();
		WeaponMesh->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, Socket);
	}
}

TArray<AWeaponBase*> ASpecOpCharacter::GetAllWeapons() const
{
	TArray<AWeaponBase*> AllWeapons;

	for (AGunBase* Gun : GunInventory)
	{
		if (Gun)
		{
			AllWeapons.Add(Gun);
		}
	}

	if (MeleeWeapon)
	{
		AllWeapons.Add(MeleeWeapon);
	}

	return AllWeapons;
}

void ASpecOpCharacter::SetMaxGunInventorySize(int32 MaxSize)
{
	UWorld* World = GetWorld();
	SetGunInventorySize(MaxSize, World ? World->WorldType : EWorldType::None);
}

bool ASpecOpCharacter::IsGunPhysicallyEquipped() const
{
	return EquippedGun ? EquippedGun->IsPhysicallyEquipped() : false;
}

AGunBase* ASpecOpCharacter::GetGunOfClass(TSubclassOf<AGunBase> GunClass, bool bPrioritizeEquipped) const
{
	if (!GunClass)
	{
		return nullptr;
	}

	// Prioritize our equipped gun over holstered ones
	if (bPrioritizeEquipped)
	{
		if (EquippedGun && EquippedGun->IsA(GunClass))
		{
			return EquippedGun;
		}
	}

	// Find first gun matching type
	for (AGunBase* Gun : GunInventory)
	{
		if (Gun && Gun->IsA(GunClass))
		{
			return Gun;
		}
	}

	return nullptr;
}

void ASpecOpCharacter::GetAllGunsOfClass(TSubclassOf<AGunBase> GunClass, TArray<AGunBase*>& OutGuns) const
{
	OutGuns.Empty();

	if (GunClass)
	{
		for (AGunBase* Gun : GunInventory)
		{
			if (Gun && Gun->IsA(GunClass))
			{
				OutGuns.Add(Gun);
			}
		}
	}
}

void ASpecOpCharacter::SetGunInventorySize(int32 NewSize, EWorldType::Type WorldType)
{
	// TODO: Should make sure size isn't too big

	// We should always leave room for one gun
	int32 Size = FMath::Max(1, NewSize);

	// Only cases we need to handle if is size has been reduced
	if (Size < GunInventory.Num())
	{
		// Equip a valid weapon before removing invalid ones
		int32 EquippedIndex = GunInventory.Find(EquippedGun);
		if (EquippedIndex >= Size)
		{
			EquipGunAtIndex(0, true);
		}

		switch (WorldType)
		{
			case EWorldType::Game:
			case EWorldType::PIE:
			{
				// Remove guns out of range
				int32 NumGuns = GunInventory.Num();
				for (int32 Index = Size; Index < NumGuns; ++Index)
				{
					RemoveGunFromInventory(Size, true);
				}

				break;
			}
			case EWorldType::Editor:
			case EWorldType::EditorPreview:
			case EWorldType::GamePreview:
			{
				// Get all guns being removed
				TArray<AGunBase*> GunsToRemove;
				for (int32 Index = Size; Index < GunInventory.Num(); ++Index)
				{
					GunsToRemove.Add(GunInventory[Index]);
				}

				// Destroy invalid guns
				for (AGunBase* Gun : GunsToRemove)
				{
					if (Gun)
					{
						Gun->Destroy();
					}
				}
				
				break;
			}
		}
	}

	// Only shrink inventory (don't increase it)
	if (GunInventory.Num() > Size)
	{
		GunInventory.SetNum(Size);
	}

	MaxGunInventorySize = Size;
}

AGunBase* ASpecOpCharacter::PopQueuedGun()
{
	AGunBase* Temp = QueuedGun;
	QueuedGun = nullptr;
	return Temp;
}

void ASpecOpCharacter::SetMeleeWeapon(AMeleeBase* Melee)
{
	if (MeleeWeapon != Melee)
	{
		if (MeleeWeapon)
		{
			// We shouldn't be able to pick up anything during melee strike
			if (MeleeWeapon->IsMeleeAbilityActive())
			{
				return;
			}

			// Melee shouldn't be in hand if ability is not active
			ensure(!bIsMeleeInHand);

			MeleeWeapon->OnRemoveFromInventory();
			OnWeaponInventoryUpdated.Broadcast(this, MeleeWeapon, false);
		}

		MeleeWeapon = Melee;
		bIsMeleeInHand = false;
		
		if (MeleeWeapon)
		{
			MeleeWeapon->OnAddToInventory(this);
			OnWeaponInventoryUpdated.Broadcast(this, MeleeWeapon, true);
		}
	}
}

void ASpecOpCharacter::DestroyMeleeWeapon()
{
	if (MeleeWeapon)
	{
		// Have possibility of this being called with guns still in inventory
		SetShowMeleeInHand(false);

		MeleeWeapon->Destroy();
		MeleeWeapon = nullptr;
	}
}

bool ASpecOpCharacter::IsUsingMeleeWeapon() const
{
	if (MeleeWeapon)
	{
		return MeleeWeapon->IsMeleeAbilityActive();
	}

	return false;
}

void ASpecOpCharacter::SetShowMeleeInHand(bool bNewShow)
{
	if (bNewShow != bIsMeleeInHand)
	{
		if (MeleeWeapon)
		{
			if (bNewShow)
			{
				if (EquippedGun)
				{
					EquippedGun->SetActorHiddenInGame(true);
				}

				AttachWeaponToMesh(MeleeWeapon, MeleeWeapon->EquipSocket);
			}
			else
			{
				if (EquippedGun)
				{
					EquippedGun->SetActorHiddenInGame(false);
				}

				AttachWeaponToMesh(MeleeWeapon, MeleeWeapon->HolsterSocket);
			}

			bIsMeleeInHand = bNewShow;
		}
		else
		{
			// Melee can never be in hand if we don't have one
			bIsMeleeInHand = false;
		}
	}
}
