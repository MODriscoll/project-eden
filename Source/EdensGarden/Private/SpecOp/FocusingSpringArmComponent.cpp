// Copyright of Sock Goblins

#include "FocusingSpringArmComponent.h"
#include "SpecOpCharacter.h"
#include "SpecOpMovementComponent.h"

#include "Engine/World.h"

const FFocusSpringArmState UFocusingSpringArmComponent::DefaultState;

UFocusingSpringArmComponent::UFocusingSpringArmComponent()
{
	bWantsInitializeComponent = true;

	FocusStates.Add(TEXT("Default"), DefaultState);
	FocusBlendFunction = EEasingFunc::Linear;
	CurrentStateName = TEXT("Default");

	bAutoUpdateFocusState = true;
	IdleStandStateName = TEXT("StandIdle");
	IdleCrouchStateName = TEXT("CrouchedIdle");
	FocusStandStateName = TEXT("StandFocused");
	FocusCrouchStateName = TEXT("CrouchedFocused");

	bBlendSpring = false;
	StateStartBlendTime = 0.f;

	bIsBlending = false;
	BlendDuration = 0.f;
	BlendStartTime = 0.f;
}

void UFocusingSpringArmComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	// We might need to auto update the state
	if (ShouldAutoUpdateFocusState())
	{
		FName StateToSet(NAME_None);

		// Determine state based on spec ops current state
		USpecOpMovementComponent* MovementComp = SpecOpOwner->GetSpecOpMovementComponent();
		if (MovementComp && MovementComp->IsFalling())
		{
			// Falling takes priority
			StateToSet = IdleStandStateName;
		}
		else
		{
			if (SpecOpOwner->IsFocusing())
			{
				StateToSet = SpecOpOwner->bIsCrouched ? FocusCrouchStateName : FocusStandStateName;
			}
			else
			{
				StateToSet = SpecOpOwner->bIsCrouched ? IdleCrouchStateName : IdleStandStateName;
			}
		}

		if (!SetState(StateToSet))
		{
			UE_LOG(LogSpecOp, Warning, TEXT("FocusingSpringArm: Auto update state failed to set state"));
		}
	}

	if (bBlendSpring)
	{
		UpdateSpringValues();
	}

	//if (bIsBlending)
	//{
	//	UWorld* World = GetWorld();

	//	// Current blend alpha
	//	float Alpha = FMath::Clamp((World->GetTimeSeconds() - BlendStartTime) / BlendDuration, 0.f, 1.f);

	//	// Target arm length
	//	{
	//		float EasedAlpha = UKismetMathLibrary::Ease(0.f, 1.f, Alpha, BlendFuncs.TargetArmLengthFunc);
	//		TargetArmLength = FMath::Lerp(StartData.TargetArmLength, TargetData.TargetArmLength, EasedAlpha);
	//	}

	//	// Socket offset
	//	{
	//		float EasedAlpha = UKismetMathLibrary::Ease(0.f, 1.f, Alpha, BlendFuncs.SocketOffsetFunc);
	//		SocketOffset = FMath::Lerp(StartData.SocketOffset, TargetData.SocketOffset, EasedAlpha);
	//	}

	//	// Socket offset
	//	{
	//		float EasedAlpha = UKismetMathLibrary::Ease(0.f, 1.f, Alpha, BlendFuncs.TargetOffsetFunc);
	//		TargetOffset = FMath::Lerp(StartData.TargetOffset, TargetData.TargetOffset, EasedAlpha);
	//	}

	//	bIsBlending = Alpha < 1.f;
	//}

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UFocusingSpringArmComponent::InitializeComponent()
{
	Super::InitializeComponent();

	SpecOpOwner = Cast<ASpecOpCharacter>(GetOwner());

	const FFocusSpringArmState StartingState = GetState(CurrentStateName);
	TargetArmLength = StartingState.TargetArmLength;
	SocketOffset = StartingState.SocketOffset;
	TargetOffset = StartingState.TargetOffset;
	bEnableCameraLag = StartingState.bEnableCameraLag;

	DefaultData.TargetArmLength = TargetArmLength;
	DefaultData.SocketOffset = SocketOffset;
	DefaultData.TargetOffset = TargetOffset;

	StartData = DefaultData;
	TargetData = DefaultData;
}

FFocusBlendData UFocusingSpringArmComponent::GetDefaultBlendData() const
{
	return DefaultData;
}

bool UFocusingSpringArmComponent::SetState(const FName& NewState)
{
	// Technically, state is still being set if already set
	if (NewState == CurrentStateName)
	{
		return true;
	}

	const FFocusSpringArmState* State = FocusStates.Find(NewState);
	if (State)
	{
		PreviousStateName = CurrentStateName;
		CurrentStateName = NewState;
		bBlendSpring = true;

		// Starting blending to time from here
		UWorld* World = GetWorld();
		if (World)
		{
			StateStartBlendTime = World->GetTimeSeconds();
		}

		return true;
	}
	else
	{
		if (GetOwner() != nullptr)
		{
			UE_LOG(LogEGCharacter, Warning, TEXT("FocusingSpringArm: State %s does not exist for actor %s"), *NewState.ToString(), *GetOwner()->GetName());
		}

		return false;
	}
}

const FFocusSpringArmState& UFocusingSpringArmComponent::GetState(const FName& StateName) const
{
	const FFocusSpringArmState* State = FocusStates.Find(StateName);
	if (State)
	{
		return *State;
	}
	else
	{
		return DefaultState;
	}
}

bool UFocusingSpringArmComponent::ShouldAutoUpdateFocusState() const
{
	return bAutoUpdateFocusState ? SpecOpOwner.IsValid() : false;
}

void UFocusingSpringArmComponent::UpdateSpringValues()
{
	UWorld* World = GetWorld();
	if (World)
	{
		const FFocusSpringArmState& CurrentState = GetState(CurrentStateName);
		const FFocusSpringArmState& PreviousState = GetState(PreviousStateName);

		// Current blend alpha
		float Alpha = FMath::Clamp((World->GetTimeSeconds() - StateStartBlendTime) / CurrentState.GetBlendTimeSafe(), 0.f, 1.f);

		// Eased blend alpha
		float EasedAlpha = UKismetMathLibrary::Ease(0.f, 1.f, Alpha, FocusBlendFunction);

		TargetArmLength = FMath::Lerp(PreviousState.TargetArmLength, CurrentState.TargetArmLength, EasedAlpha);
		SocketOffset = FMath::Lerp(PreviousState.SocketOffset, CurrentState.SocketOffset, EasedAlpha);
		TargetOffset = FMath::Lerp(PreviousState.TargetOffset, CurrentState.TargetOffset, EasedAlpha);
		bEnableCameraLag = CurrentState.bEnableCameraLag;

		// Should no longer blend if at state focus point
		bBlendSpring = Alpha < 1.f;
	}
}

void UFocusingSpringArmComponent::SetNewFocusSpot(const FFocusBlendData& Data, float BlendTime, const FFocusBlendFuncs& Funcs, bool bIgnoreIfAlreadyBlending)
{
	if (bIsBlending && bIgnoreIfAlreadyBlending)
	{
		return;
	}

	UWorld* World = GetWorld();
	
	StartData = TargetData;
	TargetData = Data;
	
	BlendFuncs = Funcs;
	BlendDuration = BlendTime;
	BlendStartTime = World->GetTimeSeconds();

	bIsBlending = true;
}

void UFocusingSpringArmComponent::RestoreDefaultFocusSpot(bool bBlendTo, float BlendTime, const FFocusBlendFuncs& Funcs, bool bIgnoreIfAlreadyBlending)
{
	if (bIsBlending && bIgnoreIfAlreadyBlending)
	{
		return;
	}

	FFocusBlendData DefaultData = GetDefaultBlendData();
	SetNewFocusSpot(DefaultData, BlendTime, Funcs);

	if (!bBlendTo)
	{
		TargetArmLength = DefaultData.TargetArmLength;
		SocketOffset = DefaultData.SocketOffset;
		TargetOffset = DefaultData.TargetOffset;

		bIsBlending = false;
	}
}
