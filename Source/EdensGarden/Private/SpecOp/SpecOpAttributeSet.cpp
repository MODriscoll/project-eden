// Copyright of Sock Goblins

#include "SpecOpAttributeSet.h"
#include "SpecOpCharacter.h"

#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"

#include "EdensGardenGlobals.h"

DECLARE_CYCLE_STAT(TEXT("SpecOpAttributeSet - Tick"), STAT_TickSpecOpAttributeSet, STATGROUP_SpecOp);

USpecOpAttributeSet::USpecOpAttributeSet()
{
	Breath = FGameplayAttributeData(100.f);
	MaxBreath = FGameplayAttributeData(100.f);
	BreathRegen = FGameplayAttributeData(1.f);
	Energy = FGameplayAttributeData(100.f);
	MaxEnergy = FGameplayAttributeData(100.f);
	IdleEnergyRegen = FGameplayAttributeData(2.f);
	MovingEnergyRegen = FGameplayAttributeData(3.f);
	SprintModifier = FGameplayAttributeData(1.75f);
	SneakModifier = FGameplayAttributeData(0.35f);
	JumpModifier = FGameplayAttributeData(1.f);
	TorchConsumptionRate = FGameplayAttributeData(2.f);
	SoundModifier = FGameplayAttributeData(1.f);
}

void USpecOpAttributeSet::Tick(float DeltaTime)
{
	SCOPE_CYCLE_COUNTER(STAT_TickSpecOpAttributeSet);

	// Get spec op to report changes
	const FGameplayAbilityActorInfo* ActorInfo = GetActorInfo();
	ASpecOpCharacter* SpecOp = ActorInfo ? Cast<ASpecOpCharacter>(ActorInfo->AvatarActor.Get()) : nullptr;

	if (SpecOp && !SpecOp->IsDead())
	{
		RegenerateBreath(DeltaTime, SpecOp, ActorInfo);
		RegenerateEnergy(DeltaTime, SpecOp, ActorInfo);
	}
}

bool USpecOpAttributeSet::ShouldTick() const
{
	return true;
}

void USpecOpAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	// Will report damage, health events
	Super::PostGameplayEffectExecute(Data);

	// Context and tags of effect
	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	const FGameplayTagContainer& Tags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Source of this effect
	UAbilitySystemComponent* SourceASC = Context.GetOriginalInstigatorAbilitySystemComponent();

	float Delta = Data.EvaluatedData.ModifierOp == EGameplayModOp::Additive ? Data.EvaluatedData.Magnitude : 0.f;

	// Get target of effect
	ASpecOpCharacter* Target = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		Target = Cast<ASpecOpCharacter>(Data.Target.AbilityActorInfo->AvatarActor.Get());
	}

	if (Data.EvaluatedData.Attribute == GetBreathAttribute())
	{
		float CurBreath = GetBreath();
		CurBreath = FMath::Clamp(CurBreath, 0.f, GetMaxBreath());
		SetBreath(CurBreath);

		if (Target)
		{
			// Notify Breath has been changed
			Target->NotifyBreathChanged(CurBreath, Delta, CurBreath <= 0.f, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetEnergyAttribute())
	{
		float CurEnergy = GetEnergy();
		CurEnergy = FMath::Clamp(CurEnergy, 0.f, GetMaxEnergy());
		SetEnergy(CurEnergy);

		if (Target)
		{
			// Notify energy has been changed
			Target->NotifyEnergyChanged(CurEnergy, Delta, CurEnergy <= 0.f, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetIdleEnergyRegenAttribute())
	{
		if (Target)
		{
			Target->NotifyEnergyRegenRateChanged(GetIdleEnergyRegen(), Delta, true, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetMovingEnergyRegenAttribute())
	{
		if (Target)
		{
			Target->NotifyEnergyRegenRateChanged(GetMovingEnergyRegen(), Delta, false, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetSprintModifierAttribute())
	{
		if (Target)
		{
			Target->NotifySprintModifierChanged(GetSprintModifier(), Delta, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetSneakModifierAttribute())
	{
		if (Target)
		{
			Target->NotifySprintModifierChanged(GetSneakModifier(), Delta, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetJumpModifierAttribute())
	{
		if (Target)
		{
			Target->NotifySprintModifierChanged(GetJumpModifier(), Delta, Tags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetSoundModifierAttribute())
	{
		float CurModifier = GetSoundModifier();
		CurModifier = FMath::Max(0.f, CurModifier);
		SetSoundModifier(CurModifier);
	}
}

void USpecOpAttributeSet::RegenerateBreath(float DeltaTime, ASpecOpCharacter* SpecOp, const FGameplayAbilityActorInfo* ActorInfo)
{
	static const FGameplayTag CantRegenerateBreath = FGameplayTag::RequestGameplayTag(TEXT("Character.Disabled.Breathing"));

	// Don't bother updating if character can't current regenerate (by only regen, we mean increase)
	UAbilitySystemComponent* ASC = ActorInfo->AbilitySystemComponent.Get();
	bool bRegenDisabled = ASC && ASC->HasMatchingGameplayTag(CantRegenerateBreath);

	float CBreath = GetBreath();
	float MBreath = GetMaxBreath();

	float Regen = GetBreathRegen();
	if (!FMath::IsNearlyZero(Regen))
	{
		bool bShouldRegen = false;

		// Sign should never been zero due to previous check
		if (FMath::Sign(Regen) > 0)
		{
			bShouldRegen = CBreath < MBreath && !bRegenDisabled;
		}
		else
		{
			bShouldRegen = CBreath > 0;
		}

		if (bShouldRegen)
		{
			float NewBreath = FMath::Min(CBreath + (Regen * DeltaTime), MBreath);
			float Delta = NewBreath - CBreath;
			SetBreath(NewBreath);

			// Don't want to notify spec op if we haven't regenrated anything
			if (!FMath::IsNearlyZero(Delta) && SpecOp)
			{
				SpecOp->NotifyBreathRegenerated(NewBreath, Delta, NewBreath >= GetMaxBreath());

				// Breath rate could be negative, meaning we are losing instead of regenerating
				// TODO: Have gameplay tag for notifying breath regen
				FGameplayTagContainer Tags(FGameplayTag::RequestGameplayTag(TEXT("Character.Breath")));
				SpecOp->NotifyBreathChanged(NewBreath, Delta, NewBreath <= 0.f, Tags);
			}
		}
	}
}

void USpecOpAttributeSet::RegenerateEnergy(float DeltaTime, ASpecOpCharacter* SpecOp, const FGameplayAbilityActorInfo* ActorInfo)
{
	static const FGameplayTag CantRegenerateTag = FGameplayTag::RequestGameplayTag(TEXT("Character.Disabled.Recharge"));

	// Don't bother updating if character can't current regenerate (by only regen, we mean increase)
	UAbilitySystemComponent* ASC = ActorInfo->AbilitySystemComponent.Get();
	bool bRegenDisabled = ASC && ASC->HasMatchingGameplayTag(CantRegenerateTag);

	float Regen = 0.f;

	const auto& Globals = UEdensGardenGlobals::Get();

	// Get which regen rate we should use
	if (SpecOp)
	{
		bool bOnlyNegate = false;

		// Torch by default consumes light (TODO: make this an effect)
		/*if (SpecOp->IsHelmetTorchActive())
		{
			bOnlyNegate = true;
			Regen = -GetTorchConsumptionRate();
		}*/
		
		// Appply movement based modifiers after
		{
			float Rate = 0.f;

			// Only caring for horizontal based movement
			if (SpecOp->GetVelocity().SizeSquared2D() > Globals.MovingEnergyRegenThreshold)
			{
				Rate = GetMovingEnergyRegen();
			}
			else
			{
				Rate = GetIdleEnergyRegen();
			}

			if (bOnlyNegate)
			{
				Regen += FMath::Sign(Rate) < 0.f ? Rate : 0.f;
			}
			else
			{
				Regen += Rate;
			}
		}
	}
	else
	{
		AActor* Avatar = ActorInfo ? ActorInfo->AvatarActor.Get() : nullptr;
		if (Avatar && Avatar->GetVelocity().SizeSquared2D() > Globals.MovingEnergyRegenThreshold)
		{
			Regen = GetMovingEnergyRegen();
		}
		else
		{
			Regen = GetIdleEnergyRegen();
		}
	}

	float CEnergy = GetEnergy();
	float MEnergy = GetMaxEnergy();

	// Check if nearly zero before scaling 
	// (as scaling might make it nearly zero)
	if (!FMath::IsNearlyZero(Regen))
	{
		bool bShouldRegen = false;
		
		// Sign should never been zero due to previous check
		if (FMath::Sign(Regen) > 0)
		{
			bShouldRegen = CEnergy < MEnergy && !bRegenDisabled;
		}
		else
		{
			bShouldRegen = CEnergy > 0;
		}

		if (bShouldRegen)
		{
			float NewEnergy = FMath::Clamp(CEnergy + (Regen * DeltaTime), 0.f, MEnergy);
			float Delta = NewEnergy - CEnergy;
			SetEnergy(NewEnergy);

			// Don't want to notify spec op if we haven't regenrated anything
			if (!FMath::IsNearlyZero(Delta) && SpecOp)
			{
				SpecOp->NotifyEnergyRegenerated(NewEnergy, Delta, (NewEnergy >= GetMaxEnergy()));

				// Energy rate could be negative, meaning we are losing instead of regenerating
				// TODO: Have gameplay tag for energy regen
				FGameplayTagContainer Tags(FGameplayTag::RequestGameplayTag(TEXT("Character.Energy")));
				SpecOp->NotifyEnergyChanged(NewEnergy, Delta, NewEnergy <= 0.f, Tags);
			}
		}
	}
}
