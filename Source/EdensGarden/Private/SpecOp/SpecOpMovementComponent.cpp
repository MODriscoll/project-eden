// Copyright of Sock Goblins

#include "SpecOpMovementComponent.h"
#include "SpecOp/SpecOpCharacter.h"

bool USpecOpMovementComponent::DoJump(bool bReplayingMoves)
{
	// Track initial jump velocity
	float InitialJumpZVelocity = JumpZVelocity;

	if (SpecOpOwner)
	{
		ApplyModifierToSpeed_LogIfNearlyZero(JumpZVelocity, SpecOpOwner->GetJumpModifier(), TEXT("Jump Velocity Modifier"));
	}

	// Now do our jump, it should be using the
	// potentially modified jump velocity value
	bool bSuccess = Super::DoJump(bReplayingMoves);

	// Restore initial value
	JumpZVelocity = InitialJumpZVelocity;

	return bSuccess;
}

void USpecOpMovementComponent::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	Super::SetUpdatedComponent(NewUpdatedComponent);

	SpecOpOwner = Cast<ASpecOpCharacter>(CharacterOwner);
}

float USpecOpMovementComponent::GetMaxSpeed() const
{
	return GetMaxSpeedWithoutModifiers(false, false);
}

void USpecOpMovementComponent::PostLoad()
{
	Super::PostLoad();

	SpecOpOwner = Cast<ASpecOpCharacter>(CharacterOwner);
}

float USpecOpMovementComponent::GetMaxSpeedWithoutModifiers(bool bWithoutSprint, bool bWithoutSneak, bool bWithoutGeneral) const
{
	if (!SpecOpOwner)
	{
		return Super::GetMaxSpeed();
	}

	float Speed = 0.f;

	// Modes to ignore: Falling, Flying
	switch (MovementMode)
	{
		case MOVE_Walking:
		case MOVE_NavWalking:
		{
			Speed = IsCrouching() ? MaxWalkSpeedCrouched : MaxWalkSpeed;
			break;
		}
		case MOVE_Falling:
		{
			return MaxWalkSpeed;
		}
		case MOVE_Swimming:
		{
			Speed = MaxWalkSpeed;
			break;
		}
		case MOVE_Flying:
		{
			return MaxFlySpeed;
		}
		case MOVE_Custom:
		{
			Speed = MaxCustomMovementSpeed;
			break;
		}
		case MOVE_None:
		default:
		{
			return 0.f;
		}
	}

	// Apply state based speed modifiers
	{
		if (SpecOpOwner->IsSprinting() && !bWithoutSprint)
		{
			ApplyModifierToSpeed_LogIfNearlyZero(Speed, SpecOpOwner->GetSprintModifier(), TEXT("Sprint Speed Modifier"));
		}
		else if (SpecOpOwner->IsSneaking() && !bWithoutSneak)
		{
			ApplyModifierToSpeed_LogIfNearlyZero(Speed, SpecOpOwner->GetSneakModifier(), TEXT("Sneak Speed Modifier"));
		}
	}

	// Apply constant modifiers
	if (!bWithoutGeneral)
	{
		ApplyModifierToSpeed_LogIfNearlyZero(Speed, SpecOpOwner->GetSpeedModifier(), TEXT("General Speed Modifier"));
	}

	return Speed;
}
