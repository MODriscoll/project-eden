// Copyright of Sock Goblins

#include "SpecOpAnimInstance.h"
#include "SpecOpCharacter.h"
#include "SpecOpMovementComponent.h"

#include "Weapons/MeleeWeapon.h"
#include "Kismet/KismetMathLibrary.h"

USpecOpAnimInstance::USpecOpAnimInstance()
{
	MaxOffsetAngle = 90.f;
	ResetOffsetsAngle = 135.f;
	OffsetInterpSpeed = 5.f;
	OffsetResetInterpSpeed = 10.f;
	BreakInterval = 5.f;

	YawOffset = 0.f;
	PitchOffset = 0.f;
	
	bIsSprinting = false;
	bIsSneaking = false;
	bIsFocusing = false;
	bIsUsingMelee = false;
	bIsReloading = false;
	bStartedJumping = false;
	RollDirection = 0.f;
	BreakStateElapsed = 0.f;

	GunSet = nullptr;
	MySpecOp = nullptr;
}

void USpecOpAnimInstance::NativeInitializeAnimation()
{
	MySpecOp = TryGetSpecOp();
}

void USpecOpAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	ASpecOpCharacter* SpecOp = TryGetSpecOp();
	if (SpecOp)
	{
		UpdateEGCharacter(SpecOp, DeltaTime);

		// TODO: Tidy up
		if (IsInValidBreakState())
		{
			BreakStateElapsed += DeltaTime;
			if (BreakStateElapsed >= BreakInterval)
			{
				const TArray<UAnimSequenceBase*> BreakAnims = GunAnimations.IdleBreakAnimations;
				if (!bCanTakeBreak && BreakAnims.Num() > 0)
				{
					int32 RandIndex = FMath::RandRange(0, BreakAnims.Num() - 1);
					BreakAnimation = BreakAnims[RandIndex];

					bCanTakeBreak = BreakAnimation != nullptr;
				}
			}
		}
		else
		{
			BreakStateElapsed = 0.f;
			bCanTakeBreak = false;
		}
	}
}

#if WITH_EDITOR
void USpecOpAnimInstance::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	FName PropertyName = PropertyChangedEvent.GetPropertyName();
	if (PropertyName == GET_MEMBER_NAME_CHECKED(USpecOpAnimInstance, GunSet))
	{
		if (GunSet)
		{
			GunAnimations = GunSet->AnimationData;
		}
		else
		{
			GunAnimations = FGunAnimationData();
		}
	}
}
#endif

void USpecOpAnimInstance::SetGunAnimationSet(UGunAnimationSet* AnimSet)
{
	GunSet = AnimSet;
	if (GunSet)
	{
		GunAnimations = GunSet->AnimationData;
	}
	else
	{
		UE_LOG(LogSpecOp, Warning, TEXT("SpecOpAnimInstance: Invalid gun animation set is being set!"));

		// Try using the default gun set
		ResetGunAnimationSet();
	}
}

void USpecOpAnimInstance::ResetGunAnimationSet()
{
	USpecOpAnimInstance* DefaultAnimInstance = GetClass()->GetDefaultObject<USpecOpAnimInstance>();
	if (DefaultAnimInstance && DefaultAnimInstance->GunSet)
	{
		GunAnimations = DefaultAnimInstance->GunSet->AnimationData;
	}
}

bool USpecOpAnimInstance::IsInValidBreakState() const
{
	if (SpeedRatio < 0.1f && GunAnimations.IdleBreakAnimations.Num() > 0)
	{
		// Character should be facing forwards
		if (FMath::IsNearlyZero(YawOffset, 1.f) && FMath::IsNearlyZero(PitchOffset, 1.f))
		{
			// Even though we may be facing forwards, we could be performing another action
			return !(bIsFocusing || bIsReloading);
		}
	}

	return false;
}

float USpecOpAnimInstance::CalculateSpeedRatio(const AEGCharacter* Character, float DeltaTime)
{
	if (!MySpecOp)
	{
		return Super::CalculateSpeedRatio(Character, DeltaTime);
	}

	USpecOpMovementComponent* MovementComp = MySpecOp->GetSpecOpMovementComponent();

	bool bWithoutGeneral = MySpecOp->bIsCrouched ? MySpecOp->IsFocusing() : false;

	// We want to ignore sneak modifier so blendspaces will blend to a slower animation
	float VelocitySquared = MovementComp->Velocity.SizeSquared();
	float MaxSpeedSquared = FMath::Square(MovementComp->GetMaxSpeedWithoutModifiers(false, true, bWithoutGeneral));

	if (FMath::IsNearlyZero(MaxSpeedSquared))
	{
		return 0.f;
	}

	return VelocitySquared / MaxSpeedSquared;
}

void USpecOpAnimInstance::CalculateYawPitchOffset(const AEGCharacter* Character, float DeltaTime, float ResetAt)
{
	FRotator CharRot = Character->GetActorRotation();
	FRotator ContRot = Character->GetControlRotation();

	FRotator Delta = UKismetMathLibrary::NormalizedDeltaRotator(ContRot, CharRot);

	if (bIsFocusing)
	{
		YawOffset = Delta.Yaw;
		PitchOffset = Delta.Pitch;
	}
	else
	{
		FRotator Current(PitchOffset, YawOffset, 0.f);

		// We want to interpolate back to no offset if reloading
		if (FMath::Abs(Delta.Yaw) >= ResetAt)
		{
			FRotator NewOffset = FMath::RInterpTo(Current, FRotator::ZeroRotator, DeltaTime, OffsetResetInterpSpeed);
			YawOffset = NewOffset.Yaw;
			PitchOffset = NewOffset.Pitch;
		}
		else
		{
			// Interp to target offset
			FRotator NewOffset = FMath::RInterpTo(Current, Delta, DeltaTime, OffsetInterpSpeed);
			PitchOffset = ClampOffsetAngle(NewOffset.Pitch, MaxOffsetAngle);

			// Prevents flinging when activating torch while already having some yaw offset
			// This also keeps the offset we initially had and just 'rotates' the lower part of the body
			YawOffset = (MySpecOp && MySpecOp->IsHelmetTorchActive()) ? Delta.Yaw : ClampOffsetAngle(NewOffset.Yaw, MaxOffsetAngle);
		}
	}
}

void USpecOpAnimInstance::UpdateStateFlags(const AEGCharacter* Character, float DeltaTime)
{
	Super::UpdateStateFlags(Character, DeltaTime);

	if (MySpecOp)
	{
		bStartedJumping = MySpecOp->bPressedJump && !MySpecOp->bWasJumping;

		bIsSprinting = MySpecOp->IsSprinting();
		bIsSneaking = MySpecOp->IsSneaking();

		bool bNowRolling = MySpecOp->IsRolling();
		bIsFocusing = (MySpecOp->IsFocusing() || MySpecOp->bOrientationFocused) && !bNowRolling;

		// The roll animation (as of 28/9/18) is driven by root motion
		if (!bIsRolling && bNowRolling)
		{
			if (SpeedRatio > 0.1f)
			{
				// Roll in direction of movement
				RollDirection = CalculateDirection(MySpecOp->GetVelocity(), MySpecOp->GetActorRotation());
			}
			else
			{
				// Roll in direction of controller
				//RollDirection = -CalculateDirection(MySpecOp->GetActorForwardVector(), MySpecOp->GetControlRotation());

				// Spec op rotation is now set in spec op itself when rolling while standing
				RollDirection = 0.f;
			}
		}

		bIsRolling = bNowRolling;

		// Should blend full body if using melee ability
		AMeleeBase* MeleeWeapon = MySpecOp->GetMeleeWeapon();
		if (MeleeWeapon && MeleeWeapon->IsMeleeAbilityActive())
		{
			bIsUsingMelee = true;
		}
		else
		{
			bIsUsingMelee = false;
		}

		// These actions override any upper body montages to blend
		if (bIsSprinting || bIsRolling || bIsUsingMelee)
		{
			bBlendUpperBodyMontages = false;
		}
		else
		{
			bBlendUpperBodyMontages = true;
		}

		// These actions override any offsets to apply
		if (bIsSprinting || bIsFalling || bIsRolling || bIsUsingMelee || bIsReloading ||
			MySpecOp->IsSwitchingGuns())
		{
			bBlendAimOffsets = false;
		}
		else
		{
			bBlendAimOffsets = true;
		}
	}
}

ASpecOpCharacter* USpecOpAnimInstance::TryGetSpecOp() const
{
	if (MySpecOp)
	{
		return MySpecOp;
	}

	return Cast<ASpecOpCharacter>(TryGetPawnOwner());
}
