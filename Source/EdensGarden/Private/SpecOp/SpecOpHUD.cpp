// Copyright of Sock Goblins

#include "SpecOpHUD.h"
#include "SpecOpAbilitySystemComponent.h"
#include "SpecOpCharacter.h"

#include "UMG/PauseMenuWidget.h"
#include "UMG/SpecOpHUDWidget.h"
#include "UMG/GameOverWidget.h"

#include "EdensGardenFunctionLibrary.h"
#include "EdensGardenGameState.h"
#include "EdensGardenUserSettings.h"

#include "SubtitleManager.h"

void ASpecOpHUD::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	CleanUp();
}

void ASpecOpHUD::Initialize(ASpecOpCharacter* SpecOp)
{
	check(SpecOp);

	// Shouldn't be valid at this point, but just to be safe
	if (MySpecOp.IsValid())
	{
		CleanUp();
	}

	MySpecOp = SpecOp;
	MySpecOp->OnCharacterHealthChanged.AddDynamic(this, &ASpecOpHUD::OnHealthChanged);
	MySpecOp->OnCharacterBreathChanged.AddDynamic(this, &ASpecOpHUD::OnBreathChanged);
	MySpecOp->OnCharacterEnergyChanged.AddDynamic(this, &ASpecOpHUD::OnEnergyChanged);
	NewGunEquippedHandle = MySpecOp->OnEquippedGunUpdated.AddUObject(this, &ASpecOpHUD::OnNewGunEquipped);

	UInteractiveComponent* InteractiveComp = MySpecOp->GetInteractiveArea();
	if (InteractiveComp)
	{
		InteractiveComp->OnClosestInteractiveUpdated.AddDynamic(this, &ASpecOpHUD::OnClosestInteractiveChanged);

		InteractionStartHandle = InteractiveComp->OnInteractionStart.AddUObject(this, &ASpecOpHUD::OnInteractionStart);
		InteractionFinishedHandle = InteractiveComp->OnInteractionFinished.AddUObject(this, &ASpecOpHUD::OnInteractionFinished);
	}

	USpecOpAbilitySystemComponent* AbilitySystem = MySpecOp->GetSpecOpAbilitySystem();
	if (AbilitySystem)
	{
		TagCountChangeHandle = AbilitySystem->OnHUDTagCountChanged.AddUObject(this, &ASpecOpHUD::OnHUDTagCountChanged);
		AbilityFailedHandle = AbilitySystem->AbilityFailedCallbacks.AddUObject(this, &ASpecOpHUD::OnAbilityActivationFailed);
		EffectAppliedHandle = AbilitySystem->OnGameplayEffectAppliedDelegateToSelf.AddUObject(this, &ASpecOpHUD::OnEffectApplied);
		PeriodicEffectAppliedHandle = AbilitySystem->OnPeriodicGameplayEffectExecuteDelegateOnSelf.AddUObject(this, &ASpecOpHUD::OnEffectApplied);
	}

	AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
	if (GameState)
	{
		GameState->OnNewCheckpointReached.AddDynamic(this, &ASpecOpHUD::OnCheckpointReached);
	}

	FSubtitleManager* SubtitleManager = FSubtitleManager::GetSubtitleManager();
	if (SubtitleManager)
	{
		SubtitlesSetHandle = SubtitleManager->OnSetSubtitleText().AddUObject(this, &ASpecOpHUD::OnSubtitlesSet);
	}
}

void ASpecOpHUD::CleanUp()
{
	HideAllWidgets();

	// Should no longer need this, as we are no longer representing a spec op
	MainWidgetInstance = nullptr;

	if (MySpecOp.IsValid())
	{
		MySpecOp->OnCharacterHealthChanged.RemoveDynamic(this, &ASpecOpHUD::OnHealthChanged);
		MySpecOp->OnCharacterBreathChanged.RemoveDynamic(this, &ASpecOpHUD::OnBreathChanged);
		MySpecOp->OnCharacterEnergyChanged.RemoveDynamic(this, &ASpecOpHUD::OnEnergyChanged);
		MySpecOp->OnEquippedGunUpdated.Remove(NewGunEquippedHandle);

		UInteractiveComponent* InteractiveComp = MySpecOp->GetInteractiveArea();
		if (InteractiveComp)
		{
			InteractiveComp->OnClosestInteractiveUpdated.RemoveDynamic(this, &ASpecOpHUD::OnClosestInteractiveChanged);

			InteractiveComp->OnInteractionStart.Remove(InteractionStartHandle);
			InteractiveComp->OnInteractionFinished.Remove(InteractionFinishedHandle);
		}

		USpecOpAbilitySystemComponent* AbilitySystem = MySpecOp->GetSpecOpAbilitySystem();
		if (AbilitySystem)
		{
			AbilitySystem->OnHUDTagCountChanged.Remove(TagCountChangeHandle);
			AbilitySystem->AbilityFailedCallbacks.Remove(AbilityFailedHandle);
			AbilitySystem->OnGameplayEffectAppliedDelegateToSelf.Remove(EffectAppliedHandle);
			AbilitySystem->OnPeriodicGameplayEffectExecuteDelegateOnSelf.Remove(PeriodicEffectAppliedHandle);
		}

		AEdensGardenGameState* GameState = GetWorld()->GetGameState<AEdensGardenGameState>();
		if (GameState)
		{
			GameState->OnNewCheckpointReached.RemoveDynamic(this, &ASpecOpHUD::OnCheckpointReached);
		}

		FSubtitleManager* SubtitleManager = FSubtitleManager::GetSubtitleManager();
		if (SubtitleManager)
		{
			SubtitleManager->OnSetSubtitleText().Remove(SubtitlesSetHandle);
		}
	}

	MySpecOp.Reset();
}

void ASpecOpHUD::DisplayMainHUD(bool bHideRest)
{
	bool bShouldDisplay = true;
	if (bHideRest)
	{
		HidePauseMenu();
		HideGameOverScreen();
	}
	else
	{
		bShouldDisplay = !(IsPauseMenuVisible() || IsGameOverScreenVisible());
	}

	if (bShouldDisplay)
	{
		USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
		if (MainHUD)
		{
			// Need to manually notify widget of current equipped gun
			if (MySpecOp.IsValid())
			{
				MainHUD->NotifyEquippedGunChanged(MySpecOp->GetEquippedGun());
			}

			UEdensGardenFunctionLibrary::AddWidgetToViewport(MainHUD);
		}
	}
}

void ASpecOpHUD::DisplayPauseMenu()
{
	HideMainHUD();

	if (IsGameOverScreenVisible())
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Displaying pause menu while game over screen is up! Is this intentional?"));
		HideGameOverScreen();
	}

	UPauseMenuWidget* PauseMenu = GetPauseMenuWidget();
	if (PauseMenu)
	{
		PauseMenu->AddToViewport();
		//PauseMenu->SetUserFocus(PlayerOwner);
	}
}

void ASpecOpHUD::DisplayGameOverScreen()
{
	HideMainHUD();

	if (IsPauseMenuVisible())
	{
		UE_LOG(LogEGHUD, Warning, TEXT("Displaying game over screen while pause menu is up! Is this intentional?"));
		HidePauseMenu();
	}

	UGameOverWidget* GameOverScreen = GetGameOverWidget();
	if (GameOverScreen)
	{
		GameOverScreen->AddToViewport();
		//GameOverScreen->SetUserFocus(PlayerOwner);
	}
}

void ASpecOpHUD::HideMainHUD()
{
	UEdensGardenFunctionLibrary::RemoveWidgetFromParent(MainWidgetInstance);
}

void ASpecOpHUD::HidePauseMenu()
{
	UEdensGardenFunctionLibrary::RemoveWidgetFromParent(PauseWidgetInstance);
	PauseWidgetInstance = nullptr;
}

void ASpecOpHUD::HideGameOverScreen()
{
	UEdensGardenFunctionLibrary::RemoveWidgetFromParent(GameOverWidgetInstance);
	GameOverWidgetInstance = nullptr;
}

USpecOpHUDWidget* ASpecOpHUD::GetMainHUDWidget()
{
	if (!MainWidgetInstance)
	{
		if (MainHUDWidgetTemplate)
		{
			MainWidgetInstance = CreateWidget<USpecOpHUDWidget>(PlayerOwner, MainHUDWidgetTemplate);
			if (!MainWidgetInstance)
			{
				UE_LOG(LogEGHUD, Warning, TEXT("Failed to make instance of %s widget!"), *MainHUDWidgetTemplate.GetDefaultObject()->GetName());
				return nullptr;
			}

			MainWidgetInstance->SetOwner(this, MySpecOp.Get());
		}
		else
		{
			UE_LOG(LogEGHUD, Error, TEXT("Failed to make Main HUD widget for spec op. Template was null"));
			return nullptr;
		}

	}

	return MainWidgetInstance;
}

UPauseMenuWidget* ASpecOpHUD::GetPauseMenuWidget()
{
	if (!PauseWidgetInstance)
	{
		if (PauseMenuWidgetTemplate)
		{
			PauseWidgetInstance = CreateWidget<UPauseMenuWidget>(PlayerOwner, PauseMenuWidgetTemplate);
			if (!PauseWidgetInstance)
			{
				UE_LOG(LogEGHUD, Warning, TEXT("Failed to make instance of %s widget!"), *PauseMenuWidgetTemplate.GetDefaultObject()->GetName());
				return nullptr;
			}
		}
		else
		{
			UE_LOG(LogEGHUD, Error, TEXT("Failed to make pause menu widget. Template was null"));
			return nullptr;
		}

	}

	return PauseWidgetInstance;
}

UGameOverWidget* ASpecOpHUD::GetGameOverWidget()
{
	if (!GameOverWidgetInstance)
	{
		if (GameOverWidgetTemplate)
		{
			GameOverWidgetInstance = CreateWidget<UGameOverWidget>(PlayerOwner, GameOverWidgetTemplate);
			if (!GameOverWidgetInstance)
			{
				UE_LOG(LogEGHUD, Warning, TEXT("Failed to make instance of %s widget!"), *GameOverWidgetTemplate.GetDefaultObject()->GetName());
				return nullptr;
			}
		}
		else
		{
			UE_LOG(LogEGHUD, Error, TEXT("Failed to make game over screen widget. Template was null"));
			return nullptr;
		}

	}

	return GameOverWidgetInstance;
}

bool ASpecOpHUD::IsMainHUDVisible() const
{
	return IsWidgetInViewport(MainWidgetInstance);
}

bool ASpecOpHUD::IsPauseMenuVisible() const
{
	return IsWidgetInViewport(PauseWidgetInstance);
}

bool ASpecOpHUD::IsGameOverScreenVisible() const
{
	return IsWidgetInViewport(GameOverWidgetInstance);
}

bool ASpecOpHUD::IsWidgetInViewport(UUserWidget* Widget) const
{
	return Widget != nullptr ? Widget->IsInViewport() : false;
}

void ASpecOpHUD::OnSubtitlesSet(const FText& SubtitlesText)
{
	// Need to manually check if subtitles are enabled
	UEdensGardenUserSettings* UserSettings = UEdensGardenUserSettings::GetEdensGardenUserSettings();
	if (UserSettings && UserSettings->IsSubtitlesEnabled())
	{
		USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
		if (MainHUD)
		{
			MainHUD->NotifySubtitlesSet(SubtitlesText);
		}
	}
}

void ASpecOpHUD::OnHealthChanged(AEGCharacter* Character, float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyHealthChanged(NewHealth, Delta, GameplayTags);
	}
}

void ASpecOpHUD::OnBreathChanged(AEGCharacter* Character, float NewBreath, float Delta, const FGameplayTagContainer& GameplayTags)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyBreathChanged(NewBreath, Delta, GameplayTags);
	}
}

void ASpecOpHUD::OnEnergyChanged(AEGCharacter* Character, float NewEnergy, float Delta, const FGameplayTagContainer& GameplayTags)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyEnergyChanged(NewEnergy, Delta, GameplayTags);
	}
}

void ASpecOpHUD::OnNewGunEquipped(ASpecOpCharacter* Character, AGunBase* Gun)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyEquippedGunChanged(Gun);
	}
}

void ASpecOpHUD::OnClosestInteractiveChanged(AActor* Interactive, const FText& DisplayTitle)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyInteractiveChanged(Interactive, DisplayTitle);
	}
}

void ASpecOpHUD::OnInteractionStart()
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyInteractionStart();
	}
}

void ASpecOpHUD::OnInteractionFinished(bool bWasCancelled)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyInteractionFinished(bWasCancelled);
	}
}

void ASpecOpHUD::OnHUDTagCountChanged(const FGameplayTag& HUDTag, int32 Count)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyHUDTagCountUpdated(HUDTag, Count);
	}
}

void ASpecOpHUD::OnEffectApplied(UAbilitySystemComponent* AbilitySystem, const FGameplayEffectSpec& EffectSpec, FActiveGameplayEffectHandle ActiveEffect)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		FGameplayTagContainer EffectTags;
		EffectSpec.GetAllAssetTags(EffectTags);

		// At this point, the root HUD tag should be valid
		const FGameplayTag RootHUDTag = FGameplayTag::RequestGameplayTag(TEXT("HUD"));
		if (RootHUDTag.IsValid())
		{
			FGameplayTagContainer HUDTags = EffectTags.Filter(RootHUDTag.GetSingleTagContainer());
			if (!HUDTags.IsEmpty())
			{
				MainHUD->NotifyHUDTagEventRecieved(HUDTags);
			}
		}
	}
}

void ASpecOpHUD::OnAbilityActivationFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& FailureReasons)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		const FGameplayTag HUDRoot = FGameplayTag::RequestGameplayTag(TEXT("HUD"));
		if (HUDRoot.IsValid())
		{
			FGameplayTagContainer HUDTags = FailureReasons.Filter(HUDRoot.GetSingleTagContainer());
			if (!HUDTags.IsEmpty())
			{
				for (const FGameplayTag& GameplayTag : HUDTags)
				{
					MainHUD->NotifyHUDFailureTagRecieved(GameplayTag);
				}
			}
		}
	}
}

void ASpecOpHUD::OnCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved)
{
	USpecOpHUDWidget* MainHUD = GetMainHUDWidget();
	if (MainHUD)
	{
		MainHUD->NotifyCheckpointReached(CheckpointIndex, CheckpointName, bProgressSaved);
	}
}

void ASpecOpHUD::HideAllWidgets()
{
	HideMainHUD();
	HidePauseMenu();
	HideGameOverScreen();
}
