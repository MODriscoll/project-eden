// Copyright of Sock Goblins

#include "SpecOpGameplayAbility.h"
#include "SpecOpCharacter.h"
#include "Weapons/Weapon.h"

#include "Gun.h"
#include "MeleeWeapon.h"

#include "AbilitySystemGlobals.h"
#include "AbilitySystemBlueprintLibrary.h"

#include "EdensGardenFunctionLibrary.h"

USpecOpGameplayAbility::USpecOpGameplayAbility()
{
	InstancingPolicy = EGameplayAbilityInstancingPolicy::InstancedPerActor;
	SpecOp = nullptr;
}

void USpecOpGameplayAbility::PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate)
{
	Super::PreActivate(Handle, ActorInfo, ActivationInfo, OnGameplayAbilityEndedDelegate);

	if (IsInstantiated() && ActorInfo && ActorInfo->AvatarActor.IsValid())
	{
		SpecOp = Cast<ASpecOpCharacter>(ActorInfo->AvatarActor.Get());
	}
}

FActiveGameplayEffectHandle USpecOpGameplayAbility::ApplyGameplayEffectToEquippedGun(TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel, int32 Stacks, bool bOnlyIfEquipped)
{
	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		FGameplayAbilityTargetDataHandle TargetData = UAbilitySystemBlueprintLibrary::AbilityTargetDataFromActor(Gun);
		TArray<FActiveGameplayEffectHandle> Handles = BP_ApplyGameplayEffectToTarget(TargetData, GameplayEffectClass, GameplayEffectLevel, Stacks);

		if (Handles.Num() > 0)
		{
			return Handles[0];
		}
	}

	return FActiveGameplayEffectHandle();
}

void USpecOpGameplayAbility::RemoveGameplayEffectFromEquippedGunWithHandle(FActiveGameplayEffectHandle Handle, int32 StacksToRemove)
{
	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();
		GunASC->RemoveActiveGameplayEffect(Handle, StacksToRemove);
	}
}

FActiveGameplayEffectHandle USpecOpGameplayAbility::ApplyGameplayEffectFromGunToOwner(TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel, int32 Stacks, bool bOnlyIfEquipped)
{
	check(CurrentActorInfo);
	check(CurrentSpecHandle.IsValid());

	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		UGameplayEffect* GameplayEffect = GameplayEffectClass.GetDefaultObject();
		if (GameplayEffect)
		{
			UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();
			if (GunASC)
			{
				return GunASC->ApplyGameplayEffectToTarget(GameplayEffect, CurrentActorInfo->AbilitySystemComponent.Get());
			}
		}
		else
		{
			ABILITY_LOG(Error, TEXT("ApplyGameplayEffectFromGunToOwner called on ability %s with no GameplayEffectClass"), *GetName());
		}
	}

	return FActiveGameplayEffectHandle();
}

TArray<FActiveGameplayEffectHandle> USpecOpGameplayAbility::ApplyGameplayEffectFromGunToTarget(FGameplayAbilityTargetDataHandle TargetData, TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel, int32 Stacks, bool bIgnoreIfEquipping)
{
	TArray<FActiveGameplayEffectHandle> Handles;

	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();
		if (GunASC && GunASC->AbilityActorInfo.IsValid() && GameplayEffectClass)
		{
			FGameplayEffectSpecHandle SpecHandle = MakeOutgoingGameplayEffectSpecFromGun(Gun, GameplayEffectClass, GameplayEffectLevel);
			SpecHandle.Data->StackCount = Stacks;

			if (SpecHandle.IsValid())
			{
				for (TSharedPtr<FGameplayAbilityTargetData> Data : TargetData.Data)
				{
					Handles.Append(Data->ApplyGameplayEffectSpec(*SpecHandle.Data.Get(), GunASC->AbilityActorInfo->AbilitySystemComponent->GetPredictionKeyForNewAction()));
				}
			}
		}
	}

	return Handles;
}

FGameplayEffectSpecHandle USpecOpGameplayAbility::BP_MakeOutgoingGameplayEffectSpecFromGun(TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level) const
{
	AGunBase* Gun = GetEquippedGun();
	if (Gun)
	{
		return MakeOutgoingGameplayEffectSpecFromGun(Gun, GameplayEffectClass, Level);
	}

	return FGameplayEffectSpecHandle();
}

FGameplayEffectSpecHandle USpecOpGameplayAbility::MakeOutgoingGameplayEffectSpecFromGun(AGunBase* Gun, TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level) const
{
	check(Gun);

	UAbilitySystemComponent* GunASC = Gun->GetAbilitySystemComponent();
	if (GunASC)
	{
		return GunASC->MakeOutgoingSpec(GameplayEffectClass, Level, MakeEffectContextFromGun(Gun, CurrentSpecHandle, CurrentActorInfo));
	}

	return FGameplayEffectSpecHandle();
}

FGameplayEffectContextHandle USpecOpGameplayAbility::MakeEffectContextFromGun(AGunBase* Gun, const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const
{
	check(Gun);
	check(ActorInfo);

	FGameplayEffectContextHandle Context = FGameplayEffectContextHandle(UAbilitySystemGlobals::Get().AllocGameplayEffectContext());
	Context.AddInstigator(ActorInfo->OwnerActor.Get(), Gun);

	Context.SetAbility(this);

	if (FGameplayAbilitySpec* AbilitySpec = ActorInfo->AbilitySystemComponent->FindAbilitySpecFromHandle(Handle))
	{
		Context.AddSourceObject(AbilitySpec->SourceObject);
	}

	return Context;
}

FGameplayAbilityTargetingLocationInfo USpecOpGameplayAbility::MakeTargetLocationInfoFromEquippedGun(FName SocketName)
{
	ASpecOpCharacter* Avatar = GetSpecOp();
	if (Avatar)
	{
		AGunBase* Gun = Avatar->GetEquippedGun();
		if (Gun)
		{
			FGameplayAbilityTargetingLocationInfo ReturnLocation;
			ReturnLocation.LocationType = EGameplayAbilityTargetingLocationType::SocketTransform;
			ReturnLocation.SourceComponent = Gun->GetMesh();
			ReturnLocation.SourceSocketName = SocketName;
			ReturnLocation.SourceAbility = this;
			return ReturnLocation;
		}
	}

	return MakeTargetLocationInfoFromOwnerActor();
}

FGameplayAbilityTargetingLocationInfo USpecOpGameplayAbility::MakeTargetLocationInfoFromEquippedGunWithSpread(FName SocketName, float MinSpread, float MaxSpread, float MinVelocity, float MaxVelocity)
{
	ASpecOpCharacter* Avatar = GetSpecOp();
	if (Avatar)
	{
		AGunBase* Gun = Avatar->GetEquippedGun();
		if (Gun)
		{
			USkeletalMeshComponent* Mesh = Gun->GetMesh();
			
			FVector Velocity = Avatar->GetVelocity();
			FTransform Muzzle = Mesh->GetSocketTransform(SocketName);
			FQuat MuzzleRot = Muzzle.GetRotation();

			// Get direction with spread applied
			FVector MuzzleDir = UEdensGardenFunctionLibrary::ApplySpreadToDirection(
				MuzzleRot.GetForwardVector(),
				Avatar->GetVelocity(),
				MinSpread, MaxSpread,
				MinVelocity, MaxVelocity);
			
			Muzzle.SetRotation(FRotationMatrix::MakeFromX(MuzzleDir).ToQuat());

			FGameplayAbilityTargetingLocationInfo ReturnLocation;
			ReturnLocation.LocationType = EGameplayAbilityTargetingLocationType::LiteralTransform;
			ReturnLocation.LiteralTransform = Muzzle;
			ReturnLocation.SourceAbility = this;
			return ReturnLocation;
		}
	}

	return MakeTargetLocationInfoFromOwnerActor();
}


ASpecOpCharacter* USpecOpGameplayAbility::GetSpecOp() const
{
	if (IsInstantiated())
	{
		if (SpecOp)
		{
			return SpecOp;
		}
		else
		{
			return Cast<ASpecOpCharacter>(GetAvatarActorFromActorInfo());
		}
	}
	else
	{
		return nullptr;
	}
}

AGunBase* USpecOpGameplayAbility::GetEquippedGun() const
{
	ASpecOpCharacter* SpecOp = GetSpecOp();
	if (SpecOp)
	{
		return SpecOp->GetEquippedGun();
	}

	return nullptr;
}

AMeleeBase* USpecOpGameplayAbility::GetMeleeWeapon() const
{
	ASpecOpCharacter* SpecOp = GetSpecOp();
	if (SpecOp)
	{
		return SpecOp->GetMeleeWeapon();
	}

	return nullptr;
}
