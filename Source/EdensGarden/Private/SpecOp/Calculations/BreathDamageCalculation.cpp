// Copyright of Sock Goblins

#include "BreathDamageCalculation.h"
#include "SpecOp/SpecOpAttributeSet.h"

struct BreathStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Breath);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);

	BreathStatics()
	{
		// This calculation should be used on target

		// We want the latest breath value to modify
		DEFINE_ATTRIBUTE_CAPTUREDEF(USpecOpAttributeSet, Breath, Target, false);

		// We keep the damage we were created with
		DEFINE_ATTRIBUTE_CAPTUREDEF(USpecOpAttributeSet, Damage, Target, true);
	}
};

static const BreathStatics& GetBreathStatics()
{
	static BreathStatics BrthStatics;
	return BrthStatics;
}

UBreathDamageCalculation::UBreathDamageCalculation()
{
	const BreathStatics& BrthStatics = GetBreathStatics();

	RelevantAttributesToCapture.Add(BrthStatics.BreathDef);
	RelevantAttributesToCapture.Add(BrthStatics.DamageDef);

	#if WITH_EDITOR
	InvalidScopedModifierAttributes.Add(BrthStatics.BreathDef);
	#endif
}

void UBreathDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const BreathStatics& BrthStatics = GetBreathStatics();

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float Breath = 0.f;
	
	// Ignore rest if we failed to capture breath attribute, this prob means
	// this calculation is being applied to something other than the spec op 
	if (ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(BrthStatics.BreathDef, EvaluationParameters, Breath))
	{
		float Damage = 0.f;
		ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(BrthStatics.DamageDef, EvaluationParameters, Damage);

		// Don't bother if no damage is being applied
		if (Damage > 0.f)
		{
			// Simply apply damage if already out of breath
			if (Breath <= 0.f)
			{
				OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(BrthStatics.DamageProperty, EGameplayModOp::Additive, Damage));
			}
			else
			{
				float NewBreath = FMath::Max(Breath - Damage, 0.f);

				// Remove breath from target
				OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(BrthStatics.BreathProperty, EGameplayModOp::Additive, -(Breath - NewBreath)));
			}
		}
	}
}
