// Copyright of Sock Goblins

#include "SpecOpCameraManager.h"
#include "SpecOpCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"

ASpecOpCameraManager::ASpecOpCameraManager()
{
	bFocusOnSpecOp = false;
	FocusSocketName = TEXT("spine_03");
	FocusInterpSpeed = 5.f;
}

void ASpecOpCameraManager::UpdateViewTargetInternal(FTViewTarget& OutVT, float DeltaTime)
{
	if (bFocusOnSpecOp)
	{
		ASpecOpCharacter* SpecOp = Cast<ASpecOpCharacter>(GetViewTargetPawn());
		if (SpecOp && SpecOp->GetMesh() != nullptr)
		{
			UCameraComponent* Camera = SpecOp->GetCamera();
			USkeletalMeshComponent* Mesh = SpecOp->GetMesh();

			FVector OriginPoint = Camera->GetComponentLocation();
			FVector TargetPoint;
			if (Mesh->DoesSocketExist(FocusSocketName))
			{
				TargetPoint = Mesh->GetSocketLocation(FocusSocketName);
			}
			else
			{
				TargetPoint = Mesh->GetComponentLocation();
			}
			
			FRotator DesiredRotation = FRotationMatrix::MakeFromX(TargetPoint - OriginPoint).Rotator();

			OutVT.POV.Location = OriginPoint;
			OutVT.POV.FOV = Camera->FieldOfView;

			// Interpolate to focus target over time
			if (FocusInterpSpeed > 0.f)
			{
				OutVT.POV.Rotation = FMath::RInterpTo(CameraCache.POV.Rotation, DesiredRotation, DeltaTime, FocusInterpSpeed);
			}
			else
			{
				OutVT.POV.Rotation = DesiredRotation;
			}

			return;
		}
	}
	
	Super::UpdateViewTargetInternal(OutVT, DeltaTime);
}
