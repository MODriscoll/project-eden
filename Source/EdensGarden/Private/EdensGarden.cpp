// Copyright of Sock Goblins

#include "EdensGarden.h"

DEFINE_LOG_CATEGORY(LogEdensGarden);
DEFINE_LOG_CATEGORY(LogEGHUD);
DEFINE_LOG_CATEGORY(LogEGPowers);
DEFINE_LOG_CATEGORY(LogEGProps);