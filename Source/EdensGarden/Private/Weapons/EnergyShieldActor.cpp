
// Copyright of Sock Goblins

#include "EnergyShieldActor.h"
#include "Components/SphereComponent.h"
#include "GameFramework/MovementComponent.h"

#include "Characters/EGCharacter.h"
#include "Gameplay/EGAttributeSet.h"

#include "Monsters/MonsterCharacter.h"
#include "Monsters/MonsterAIController.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemBlueprintLibrary.h"

#include "TimerManager.h"

AEnergyShieldActor::AEnergyShieldActor()
{
	PrimaryActorTick.bCanEverTick = true;

	CollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision"));
	SetRootComponent(CollisionSphere);

	CollisionSphere->InitSphereRadius(300.f);
	CollisionSphere->bAutoActivate = false;
	
	AbilitySystem = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystem"));
	AttributeSet = CreateDefaultSubobject<UEGAttributeSet>(TEXT("AttributeSet"));

	ShieldDuration = 15.f;
}

void AEnergyShieldActor::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AbilitySystem->InitAbilityActorInfo(this, this);

	if (ShieldEffect)
	{
		AbilitySystem->ApplyGameplayEffectToSelf(ShieldEffect.GetDefaultObject(), 1.f, AbilitySystem->MakeEffectContext());
	}
}

void AEnergyShieldActor::BeginPlay()
{
	Super::BeginPlay();	

	if (Instigator)
	{
		// We want the our instigator to ignore colliding with us
		UMovementComponent* MovementComp = Instigator->FindComponentByClass<UMovementComponent>();
		if (MovementComp && MovementComp->UpdatedPrimitive)
		{
			MovementComp->UpdatedPrimitive->IgnoreActorWhenMoving(this, true);
		}

		CollisionSphere->AttachToComponent(Instigator->GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		CollisionSphere->Activate(true);
	}

	AttributeSet->OnDamaged.AddDynamic(this, &AEnergyShieldActor::OnShieldTakenDamage);

	if (ShieldDuration > 0.f)
	{
		FTimerManager& TimerManager = GetWorldTimerManager();
		TimerManager.SetTimer(TimerHandle_Expired, this, &AEnergyShieldActor::OnShieldHasExpired, ShieldDuration, false);
	}
}

void AEnergyShieldActor::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (Instigator)
	{
		// Remove us from ignored list
		UMovementComponent* MovementComp = Instigator->FindComponentByClass<UMovementComponent>();
		if (MovementComp && MovementComp->UpdatedPrimitive)
		{
			MovementComp->UpdatedPrimitive->MoveIgnoreActors.Remove(this);
		}
		
	}
}

void AEnergyShieldActor::OnShieldDestroyed_Implementation(AEGCharacter* Character, const FGameplayTagContainer& GameplayTags, bool bValidHit, const FHitResult& HitResult)
{
	Destroy();
}

void AEnergyShieldActor::OnShieldExpired_Implementation()
{
	Destroy();
}

void AEnergyShieldActor::OnShieldTakenDamage(float Damage, bool bKillingBlow, APawn* DamageInstigator, AActor* SourceActor, const FGameplayTagContainer& SourceTags, bool bValidHit, const FHitResult& HitResult)
{
	AEGCharacter* Character = Cast<AEGCharacter>(DamageInstigator);
	if (!Character)
	{
		Character = Cast<AEGCharacter>(SourceActor);
	}

	if (bKillingBlow)
	{
		OnShieldDestroyed(Character, SourceTags, bValidHit, HitResult);

		// Notify our instigator we have been destroyed
		{
			const FGameplayTag ShieldFinished = FGameplayTag::RequestGameplayTag(TEXT("Event.EnergyShield.Finished"));
			const FGameplayTag ShieldDestroyed = FGameplayTag::RequestGameplayTag(TEXT("Event.EnergyShield.Finished.Destroyed"));

			FGameplayEventData Payload;
			Payload.EventTag = ShieldDestroyed;
			Payload.Instigator = DamageInstigator;
			Payload.InstigatorTags.AppendTags(SourceTags);
			Payload.OptionalObject = SourceActor;

			if (bValidHit)
			{
				// The target data will handle destroying the instance (via shared pointer)
				Payload.TargetData.Add(new FGameplayAbilityTargetData_SingleTargetHit(HitResult));
			}

			UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, ShieldFinished, Payload);
		}

		// We no longer want notifications once dead
		AttributeSet->OnDamaged.RemoveDynamic(this, &AEnergyShieldActor::OnShieldTakenDamage);
	}
	else
	{
		OnShieldDamaged(Character, Damage, SourceTags, bValidHit, HitResult);
	}
}

float AEnergyShieldActor::GetHealth() const
{
	check(AttributeSet);
	return AttributeSet->GetHealth();
}

void AEnergyShieldActor::OnShieldHasExpired()
{
	OnShieldExpired();

	// Notify our instigator we have expired
	{
		const FGameplayTag ShieldFinished = FGameplayTag::RequestGameplayTag(TEXT("Event.EnergyShield.Finished"));
		const FGameplayTag ShieldExpired = FGameplayTag::RequestGameplayTag(TEXT("Event.EnergyShield.Finished.Expired"));

		FGameplayEventData Payload;
		Payload.EventTag = ShieldExpired;

		UAbilitySystemBlueprintLibrary::SendGameplayEventToActor(Instigator, ShieldFinished, Payload);
	}

	// We no longer want notifications once expired
	AttributeSet->OnDamaged.RemoveDynamic(this, &AEnergyShieldActor::OnShieldTakenDamage);
}

void AEnergyShieldActor::OnShieldHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit)
{
	AMonsterCharacter* MonsterCharacter = Cast<AMonsterCharacter>(OtherActor);
	if (MonsterCharacter)
	{
		AMonsterAIController* MonsterController = Cast<AMonsterAIController>(MonsterCharacter->GetController());
		if (MonsterController)
		{
			//MonsterController->SetOverridePriorityTarget(this);
		}
	}
}
