// Copyright of Sock Goblins

#include "Weapon.h"
#include "WeaponAttributeSet.h"
#include "EdensGardenAssetManager.h"
#include "SpecOp/SpecOpAbilitySystemComponent.h"
#include "SpecOp/SpecOpCharacter.h"

#include "AbilitySystemComponent.h"
#include "AbilitySystemGlobals.h"

#include "Components/AudioComponent.h"
#include "Components/SkeletalMeshComponent.h"

DEFINE_LOG_CATEGORY(LogWeapon);

#define LOCTEXT_NAMESPACE "WeaponBase"

AWeaponBase::AWeaponBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponName = LOCTEXT("WeaponBaseWeaponName", "Weapon");
	InteractTime = 1.f;

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	SetRootComponent(Mesh);
	Mesh->bGenerateOverlapEvents = true;
	Mesh->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_Yes;
	Mesh->SetSimulatePhysics(true);
	Mesh->SetCollisionProfileName(INTERACTIVE_PROFILE);

	WeaponAudio = CreateDefaultSubobject<UAudioComponent>(TEXT("WeaponAudio"));
	WeaponAudio->SetupAttachment(Mesh);
	WeaponAudio->bIsUISound = false;
	WeaponAudio->bIsMusic = false;
	WeaponAudio->bAlwaysPlay = true;
}

bool AWeaponBase::ShouldSaveForSaveGame() const
{
	// This weapon will be saved automatically if in players inventory
	if (IsInInventory())
	{
		return false;
	}

	#if WITH_EDITORONLY_DATA
	if (bIsEditorPreviewActor)
	{
		//return false;
	}
	#endif
	
	return !HasAllFlags(RF_Transient);
}

void AWeaponBase::GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& Title, FLinearColor& Color) const
{
	if (CanAddToInventory(Character))
	{
		Title = WeaponName;
		Color = FColor::Green;
	}
}

bool AWeaponBase::CanInteractWith_Implementation(const ASpecOpCharacter* Character) const
{
	return CanAddToInventory(Character);
}

float AWeaponBase::GetInteractDuration_Implementation() const
{
	return InteractTime;
}

void AWeaponBase::OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled)
{
	if (!bWasCancelled)
	{
		GiveSelfTo(Interactor);
	}
}

void AWeaponBase::EndPlay(EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	if (EndPlayReason == EEndPlayReason::Destroyed)
	{
		// Need to remove ourselves from our owner if being destroyed,
		// so our owner doesn't reference a null weapon
		if (SpecOpOwner.IsValid())
		{
			// Derived types might need to remove anything they have given the owner
			CleanupWeapon();
			SpecOpOwner->RemoveWeapon(this);
		}
	}
}

FPrimaryAssetId AWeaponBase::GetPrimaryAssetId() const
{
	if (HasAnyFlags(RF_ClassDefaultObject))
	{
		return Super::GetPrimaryAssetId();
	}

	return FPrimaryAssetId(UEdensGardenAssetManager::WeaponType, GetFName());
}

#if EG_GAMEPLAY_DEBUGGER
void AWeaponBase::GetDebugString(FString& String) const
{
	const FString Formatting = TEXT(
		"{white}Weapon Name: {green}%s\n"
		"{white}Type: {green}%s\n");

	String.Append(FString::Printf(*Formatting,
		*WeaponName.ToString(),
		GetWeaponType() == EWeaponType::Melee ? TEXT("Melee") : TEXT("Gun")));
}
#endif

void AWeaponBase::OnAddToInventory(ASpecOpCharacter* SpecOp)
{
	if (SpecOp != SpecOpOwner)
	{
		// Can't be in two inventories at once
		if (SpecOpOwner.IsValid())
		{
			RemoveSelfFrom(SpecOpOwner.Get());

			// Should be reset via call to remove and drop, but clear here to be certain
			SpecOpOwner.Reset();
		}

		check(SpecOp);
		SetOwner(SpecOp);
		Instigator = SpecOp;
		SpecOpOwner = SpecOp;

		// Convert to inventory state, we no longer require collision
		Mesh->SetSimulatePhysics(false);
		Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		// Holster ourselves onto owner
		AttachWeaponToOwner(HolsterSocket);

		BlueprintOnAddToInventory(SpecOp);
	}
	else
	{
		if (SpecOp)
		{
			UE_LOG(LogWeapon, Log, TEXT("OnAddToInventory: Weapon was added to same characters inventory twice"));
		}
	}
}

void AWeaponBase::OnRemoveFromInventory()
{
	// We want to drop ourselve away from our owner,
	// so we don't start clipping into the mesh
	DropWeaponAroundOwner();

	SetOwner(nullptr);
	Instigator = nullptr;
	SpecOpOwner.Reset();

	BlueprintOnRemoveFromInventory();
}

void AWeaponBase::GiveSelfTo(ASpecOpCharacter* SpecOp)
{
	checkf(false, TEXT("AWeaponBase: GiveSelfTo has not been derived!"));
}

void AWeaponBase::RemoveSelfFrom(ASpecOpCharacter* SpecOp)
{
	checkf(false, TEXT("AWeaponBase: RemoveSelfFrom has not been derived!"));
}

bool AWeaponBase::CanAddToInventory(const ASpecOpCharacter* SpecOp) const
{
	return !IsInInventory();
}

bool AWeaponBase::CanRemoveFromInventory(const ASpecOpCharacter* SpecOp) const
{
	return IsInInventory();
}

FGameplayAbilitySpecHandle AWeaponBase::GiveAbilityIfValid(UAbilitySystemComponent* ASC, TSubclassOf<UGameplayAbility> Ability, int32 InputID)
{
	if (ASC && Ability)
	{
		// Mark us as the source, so it can be traced back to us
		return ASC->GiveAbility(FGameplayAbilitySpec(Ability.GetDefaultObject(), 1, static_cast<int32>(InputID), this));
	}

	return FGameplayAbilitySpecHandle();
}

FGameplayAbilitySpecHandle AWeaponBase::GiveAbilityIfValid(UAbilitySystemComponent* ASC, TSoftClassPtr<UGameplayAbility> Ability, bool bForce, int32 InputID)
{
	if (ASC && !Ability.IsNull())
	{
		if (Ability.IsPending())
		{
			Ability.LoadSynchronous();
		}

		// Double check incase load failed
		if (Ability.IsValid())
		{
			// Mark us as the source, so it can be traced back to us
			TSubclassOf<UGameplayAbility> AbilityClass = Ability.Get();
			return ASC->GiveAbility(FGameplayAbilitySpec(AbilityClass.GetDefaultObject(), 1, static_cast<int32>(InputID), this));
		}
	}

	return FGameplayAbilitySpecHandle();
}

void AWeaponBase::DetachWeaponFromOwnerOnly()
{
	if (IsInInventory())
	{
		USkeletalMeshComponent* Mesh = GetMesh();

		// We want to detach first, so we can keep our relative scale
		FDetachmentTransformRules DetachmentRules(EDetachmentRule::KeepWorld, true);
		DetachmentRules.ScaleRule = EDetachmentRule::KeepRelative;
		Mesh->DetachFromComponent(DetachmentRules);

		// Return to world state
		Mesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		Mesh->SetSimulatePhysics(true);
	}
}

void AWeaponBase::AttachWeaponToOwner(const FName& Socket)
{
	if (IsInInventory())
	{
		SpecOpOwner->AttachWeaponToMesh(this, Socket);
	}
}

void AWeaponBase::DropWeaponAroundOwner(float OffsetScale)
{
	if (SpecOpOwner.IsValid())
	{
		// Might already be in world state
		if (!Mesh->IsSimulatingPhysics())
		{
			UWorld* World = GetWorld();

			FVector Location = GetActorLocation();
			FVector DropLocation = SpecOpOwner->GetActorLocation();

			// Stay vertical with where we currently are
			DropLocation.Z = Location.Z;

			// Move this weapon away from owner
			DropLocation = DropLocation + SpecOpOwner->GetActorForwardVector() * OffsetScale;
			if (!World->FindTeleportSpot(this, DropLocation, GetActorRotation()))
			{
				UE_LOG(LogWeapon, Warning, TEXT("Dropped weapon %s is overlapping collision"), *GetName());
			}

			SetActorLocation(DropLocation);

			// Return to world state
			DetachWeaponFromOwnerOnly();
		}
	}
}

void AWeaponBase::PlayWeaponSound(USoundBase* Sound)
{
	if (WeaponAudio && Sound)
	{
		WeaponAudio->SetSound(Sound);
		WeaponAudio->Play();
	}
}

void AWeaponBase::StopWeaponSound()
{
	if (WeaponAudio && WeaponAudio->IsPlaying())
	{
		WeaponAudio->Stop();
	}
}

void AWeaponBase::StopWeaponSoundIfPlaying(USoundBase* Sound)
{
	if (WeaponAudio->Sound == Sound)
	{
		StopWeaponSound();
	}
}

#undef LOCTEXT_NAMESPACE
