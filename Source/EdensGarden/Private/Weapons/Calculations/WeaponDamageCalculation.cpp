// Copyright of Sock Goblins

#include "Calculations/WeaponDamageCalculation.h"
#include "Weapons/WeaponAttributeSet.h"
#include "Characters/EGCharacterAttributeSet.h"

#include "EdensGardenGlobals.h"

struct DamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(WeaponDamage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(WeaponDamageMultiplier);

	DamageStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(UEGCharacterAttributeSet, Damage, Target, false);

		DEFINE_ATTRIBUTE_CAPTUREDEF(UWeaponAttributeSet, WeaponDamage, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(UWeaponAttributeSet, WeaponDamageMultiplier, Source, true);
	}
};

static const DamageStatics& GetDamageStatics()
{
	static DamageStatics DmgStatics;
	return DmgStatics;
}

UWeaponDamageCalculation::UWeaponDamageCalculation()
{
	const DamageStatics& DmgStatics = GetDamageStatics();

	RelevantAttributesToCapture.Add(DmgStatics.DamageDef);
	RelevantAttributesToCapture.Add(DmgStatics.WeaponDamageDef);
	RelevantAttributesToCapture.Add(DmgStatics.WeaponDamageMultiplierDef);

	#if WITH_EDITOR
	InvalidScopedModifierAttributes.Add(DmgStatics.DamageDef);
	#endif
}

void UWeaponDamageCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	const DamageStatics& DmgStatics = GetDamageStatics();

	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float WeaponDamage = 0.f;
	float WeaponDamageMultiplier = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DmgStatics.WeaponDamageDef, EvaluationParameters, WeaponDamage);
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DmgStatics.WeaponDamageMultiplierDef, EvaluationParameters, WeaponDamageMultiplier);

	float DamageToApply = WeaponDamage * WeaponDamageMultiplier;

	// Damage should only negate health
	if (DamageToApply > 0.f)
	{
		OutExecutionOutput.AddOutputModifier(FGameplayModifierEvaluatedData(DmgStatics.DamageProperty, EGameplayModOp::Additive, DamageToApply));
	}
}
