// Copyright of Sock Goblins

#include "MeleeWeapon.h"
#include "Components/BoxComponent.h"
#include "Physics/EGPhysicalMaterial.h"

#include "AbilitySystemComponent.h"
#include "SpecOp/SpecOpCharacter.h"

AMeleeBase::AMeleeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;

	bIsMeleeActive = false;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(GetMesh());
	CollisionBox->SetSimulatePhysics(false);
	CollisionBox->SetCollisionProfileName(MELEEHITBOX_PROFILE);
	CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	CollisionBox->bVisible = false;

	CollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AMeleeBase::OnCollisionBoxOverlapped);
}

void AMeleeBase::EnableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags)
{
	EnableAttackCollision(AssociatedTag);
}

void AMeleeBase::DisableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags)
{
	DisableAttackCollision();
}

void AMeleeBase::OnAddToInventory(ASpecOpCharacter* SpecOp)
{
	Super::OnAddToInventory(SpecOp);

	UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
	MeleeAbilityHandle = GiveAbilityIfValid(OwnerASC, MeleeAbility, static_cast<int32>(EEdensGardenInputID::Melee));

	SpecOpOwner->OnCharacterDeath.AddDynamic(this, &AMeleeBase::OnOwnerDeath);
}

void AMeleeBase::OnRemoveFromInventory()
{
	if (bIsMeleeActive)
	{
		DisableAttackCollision();
	}

	UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
	check(OwnerASC);
	OwnerASC->ClearAbility(MeleeAbilityHandle);

	SpecOpOwner->OnCharacterDeath.RemoveDynamic(this, &AMeleeBase::OnOwnerDeath);

	Super::OnRemoveFromInventory();
}

#if EG_GAMEPLAY_DEBUGGER
void AMeleeBase::GetDebugString(FString& String) const
{
	Super::GetDebugString(String);

	const FString Formatting = TEXT(
		"{white}Melee Active = {Green}%s\n"
		"{white}Attack Tag = {Green}%s\n");

	String.Append(FString::Printf(*Formatting,
		bIsMeleeActive ? TEXT("True") : TEXT("False"),
		*AttackTag.ToString()));
}
#endif

void AMeleeBase::CleanupWeapon()
{
	check(SpecOpOwner.IsValid());

	// Remove the ability we gave our owner
	UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
	check(OwnerASC);
	OwnerASC->ClearAbility(MeleeAbilityHandle);

	SpecOpOwner->OnCharacterDeath.RemoveDynamic(this, &AMeleeBase::OnOwnerDeath);
}

void AMeleeBase::GiveSelfTo(ASpecOpCharacter* SpecOp)
{
	SpecOp->SetMeleeWeapon(this);
}

void AMeleeBase::RemoveSelfFrom(ASpecOpCharacter* SpecOp)
{
	SpecOp->SetMeleeWeapon(this);
}

void AMeleeBase::EnableAttackCollision(FGameplayTag ActivationTag)
{
	if (!bIsMeleeActive)
	{
		bIsMeleeActive = true;
		AttackTag = ActivationTag;

		// Notify blueprints
		OnMeleeActivated(AttackTag);

		HitActors.Empty();

		// Activate collision last, as anything already overlap will probaly trigger the callback
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}

void AMeleeBase::DisableAttackCollision()
{
	if (bIsMeleeActive)
	{
		bIsMeleeActive = false;
		AttackTag = FGameplayTag::EmptyTag;

		// Notify blueprints
		OnMeleeDeactivated();
		
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		HitActors.Empty();
	}
}

bool AMeleeBase::IsMeleeAbilityActive() const
{
	if (IsInInventory())
	{
		UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
		if (const FGameplayAbilitySpec* AbilitySpec = OwnerASC->FindAbilitySpecFromHandle(MeleeAbilityHandle))
		{
			return AbilitySpec->IsActive();
		}
	}

	return false;
}

void AMeleeBase::OnCollisionBoxOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == SpecOpOwner || !bIsMeleeActive)
	{
		return;
	}

	// We only want to strike an actor once per attack'
	if (!HitActors.Contains(OtherActor))
	{
		HitActors.Add(OtherActor);

		UAbilitySystemComponent* OwnerASC = SpecOpOwner->GetAbilitySystemComponent();
		if (OwnerASC)
		{
			FGameplayTag MeleeTag = FGameplayTag::RequestGameplayTag(TEXT("Event.Melee.Hit"));

			FGameplayEventData Payload;
			Payload.EventTag = AttackTag;
			Payload.Instigator = SpecOpOwner.Get();
			Payload.OptionalObject = this;
			Payload.Target = OtherActor;

			FScopedPredictionWindow Window(OwnerASC, true);
			OwnerASC->HandleGameplayEvent(MeleeTag, &Payload);
		}
	}
}

void AMeleeBase::OnOwnerDeath(AEGCharacter* Character, float Damage, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags)
{
	DisableAttackCollision();
}
