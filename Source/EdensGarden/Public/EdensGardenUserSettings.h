// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "EdensGardenUserSettings.generated.h"

/** Extended settings that relate to Edens Garden */
UCLASS()
class EDENSGARDEN_API UEdensGardenUserSettings : public UGameUserSettings
{
	GENERATED_BODY()
	
public:

	UEdensGardenUserSettings();

public:

	// Begin UGameUserSettings Interface
	virtual void SetToDefaults() override;
	// End UGameUserSettings Interface

public:

	/** Get edens garden user settings */
	UFUNCTION(BlueprintCallable, Category = Settings)
	static UEdensGardenUserSettings* GetEdensGardenUserSettings();

public:
	
	/** Sets the user setting for subtitles */
	UFUNCTION(BlueprintCallable, Category = Settings)
	void SetSubtitlesEnabled(bool bEnable);

	/** Returns the user setting for subtitles */
	UFUNCTION(BlueprintPure, Category = Settings)
	bool IsSubtitlesEnabled() const;

	/** Sets the user setting for checkpoint saving */
	UFUNCTION(BlueprintCallable, Category = Settings)
	void SetSavingEnabled(bool bEnable);

	/** Returns the user setting for saving */
	UFUNCTION(BlueprintPure, Category = Settings)
	bool IsSavingEnabled() const;

	/** Sets the user setting for the sensitivity when turning the camera using a game pad */
	UFUNCTION(BlueprintCallable, Category = Settings)
	void SetSensitivity(float Sensitivity, bool bFocused);

	/** Returns the user setting for sensitivity */
	UFUNCTION(BlueprintPure, Category = Settings)
	float GetSensitivity(bool bFocused) const;

	/** Sets the user setting for inverting Y-Axis turn input */
	UFUNCTION(BlueprintCallable, Category = Settings)
	void SetInvertYAxis(bool bInvert);

	/** Returns the user setting for inverting the Y-Axis turn input */
	UFUNCTION(BlueprintPure, Category = Settings)
	bool ShouldInvertYAxis() const;

protected:

	/** If subtitles are enabled */
	UPROPERTY(config)
	uint32 bSubtitlesEnabled : 1;

	/** If saving is enabled. This will make the save never write to disk, but
	does record several stuff to memory, such as initial checkpoint for level */
	UPROPERTY(config)
	uint32 bSavingEnabled : 1;

	/** The camera turn sensitivity for gamepads */
	UPROPERTY(config)
	float Sensitivity;

	/** The camera turn sensitivity for gamepads while focused */
	UPROPERTY(config)
	float FocusedSensitivity;

	/** If Y-axis for turning camera should be inversed */
	UPROPERTY(config)
	uint32 bInvertYSensitivity : 1;
};
