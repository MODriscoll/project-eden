// Copyright of Sock Goblins

#pragma once

#include "EdensGardenTypes.h"
#include "EdensGardenAssetManager.h"
#include "Engine/CollisionProfile.h"
#include "Engine/EngineTypes.h"

DECLARE_LOG_CATEGORY_EXTERN(LogEdensGarden, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogEGHUD, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogEGPowers, Log, All);
DECLARE_LOG_CATEGORY_EXTERN(LogEGProps, Log, All);

DECLARE_STATS_GROUP(TEXT("EdensGarden"), STATGROUP_EdensGarden, STATCAT_Advanced);

/** Constants */

/** 
* Options to be parsed by edens garden game mode when starting the game
*/

#define EGGM_OPTION_SAVEPLAYER		"SavePlayer"		// The players attributes and inventory should be generated and save
#define EGGM_OPTION_SAVEWORLD		"SaveWorld"			// The state of the world should be saved before starting game
#define EGGM_OPTION_RESTART			"Restart"			// The players level start save data should overwrite the current save data

// Level is being loaded from a new game
#define EGGM_NEWGAME "?" EGGM_OPTION_SAVEPLAYER "?" EGGM_OPTION_SAVEWORLD

// Level is being loaded from a saved game
#define EGGM_LOADGAME ""

// Level was transitioned to from another
#define EGGM_TRANSITION "?" EGGM_OPTION_SAVEWORLD

// Level was requested to restart from beginning of level
#define EGGM_RESTART_START "?" EGGM_OPTION_RESTART "?" EGGM_OPTION_SAVEWORLD

// Level was requested to restart from last checkpoint
#define EGGM_RESTART_CHECKPOINT ""



// The max amount of powers the spec op character can carry at once
// This should match the amount specified in EdensGardenInputID
#define MAX_SPECOP_POWERS 4



/** Object types */
#define COLLISION_INTERACTIVE	ECC_GameTraceChannel1
#define COLLISION_PHASINGPAWN	ECC_GameTraceChannel3

/** Trace types */
#define COLLISION_WEAPON		ECC_GameTraceChannel2

/** Collision profiles */
#define INTERACTIVE_PROFILE				TEXT("Interactive")
#define INTERACTIVEQUERY_PROFILE		TEXT("InteractiveQuery")
#define DISABLEDCHARACTER_PROFILE		TEXT("DisabledCharacter")
#define PHASINGPAWN_PROFILE				TEXT("PhasingProfile")
#define MELEEHITBOX_PROFILE				TEXT("MeleeHitbox")

/** Stencil values for highlight actors (interactives, inspector power) */
#define HIGHLIGHT_COLOR_CUSTOM		252
#define HIGHLIGHT_COLOR_COLOR1		253
#define HIGHLIGHT_COLOR_COLOR2		254
#define HIGHLIGHT_COLOR_COLOR3		255



/** 
* Helper enum to allow access to highlight colors in blueprints.
* The colors are set in the highlight material (M_MultiOutline) 
*/

/** Stencil values used by the highlight material */
UENUM(BlueprintType)
enum class EHighlightColor : uint8
{
	Color1	= HIGHLIGHT_COLOR_COLOR1,
	Color2	= HIGHLIGHT_COLOR_COLOR2,
	Color3	= HIGHLIGHT_COLOR_COLOR3
};
