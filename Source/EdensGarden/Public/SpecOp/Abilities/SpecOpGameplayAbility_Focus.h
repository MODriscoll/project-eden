// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "SpecOp/SpecOpGameplayAbility.h"
#include "SpecOp/FocusingSpringArmComponent.h"
#include "SpecOpGameplayAbility_Focus.generated.h"

/** 
* Simple gameplay ability that will focus the spec ops camera
* on start and revert it once input has been released. Can also
* apply a gameplay effect which will be removed on ability end.
*/
UCLASS()
class EDENSGARDEN_API USpecOpGameplayAbility_Focus : public USpecOpGameplayAbility
{
	GENERATED_BODY()
	
public:

	USpecOpGameplayAbility_Focus();

public:

	// Begin UEGCharacterGameplayAbility Interface
	virtual void OnTagEvent_Implementation(const FGameplayTag& Tag, bool bWasAdded) override;
	// End UEGCharacterGameplayAbility Interface

	// Begin UGameplayAbility Interface
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	// End UGameplayAbility Interface

protected:

	/** Applies focusing and applies effect */
	void ApplyFocus();

	/** Stops focusing and removes effect */
	void RemoveFocus();

public:

	/** If we are to allow focusing while airborne */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Focusing)
	uint8 bCanFocusWhileFalling : 1;

	/** The gameplay effect to apply on activation */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Focusing)
	TSubclassOf<UGameplayEffect> FocusGameplayEffect;

	/** Focus blend data when initially focusing */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Focusing)
	FFocusBlendData FocusPoint;

	/** Time to use for blends */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Focusing)
	float BlendTime;

	/** Blend functions to use */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Focusing)
	FFocusBlendFuncs BlendFuncs;

protected:

	/** If focus is being applied (only valid if instanced) */
	UPROPERTY(BlueprintReadOnly, Category = Focusing)
	uint8 bIsFocusing : 1;

	/** Handle to effect spec for focusing */
	FActiveGameplayEffectHandle FocusEffectHandle;

private:

	/** If we have removed focus due to falling */
	uint8 bIsFalling : 1;

	/** If we have removed focus due to rolling */
	uint8 bIsRolling : 1;
};
