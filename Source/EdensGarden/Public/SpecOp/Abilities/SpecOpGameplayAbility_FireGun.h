// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "SpecOp/SpecOpGameplayAbility.h"
#include "SpecOpGameplayAbility_FireGun.generated.h"

class AWeaponBase;

/** 
* An ability that simulates the firing of a gun. Instead of using Activate,
* Commit and EndAbility, you simply override TakeShot and end it with FinishShot.
* This ability expects to be consumming ammo, so it acknowledges the infinite ammo tags
*/
UCLASS(abstract)
class EDENSGARDEN_API USpecOpGameplayAbility_FireGun : public USpecOpGameplayAbility
{
	GENERATED_BODY()
	
public:

	USpecOpGameplayAbility_FireGun();
	
public:

	// Begin UEGCharacterGameplayAbility Interface
	virtual void OnTagEvent_Implementation(const FGameplayTag& Tag, bool bWasAdded) override;
	// End UEGCharacterGameplayAbility Interface

	// Begin UGameplayAbility Interface
	virtual FGameplayEffectContextHandle MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const override;
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags, const FGameplayTagContainer* TargetTags, OUT FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData) override;
	virtual void InputReleased(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	// End UGameplayAbility Interface

public:

	/** Event called when this ability starts successfully */
	UFUNCTION(BlueprintImplementableEvent, Category = Ability)
	void OnStartFiring();

	/** Event for when we take a shot, this is what should be derived
	for performing actual shooting logic. This function needs to call 
	the FinishShot function when shot has finished (can be latent) */
	UFUNCTION(BlueprintNativeEvent, Category = Fire)
	void TakeShot(int32 Iteration, float AmmoConsumed);
	virtual void TakeShot_Implementation(int32 Iteration, float AmmoConsumed) { FinishShot(0.2f); }

	/** The function to call when your shot has technically finished (this can be called latent)
	This can be used to set the interval to wait before taking the next shot or ending the ability */
	UFUNCTION(BlueprintCallable, Category = Fire)
	void FinishShot(float WaitInterval, bool bEndAbility = false);

public:

	/** Helper function for converting a fire rate to an
	interval to use for firing the gun, basically 1 / X */
	UFUNCTION(BlueprintPure, Category = Fire)
	float GetFireRateInterval(float FireRate = 5.f) const;

protected:

	/** Called when checking and consumming ammo when taking
	a shot to get how much ammo the next shot should consume.
	If a value of zero or smaller is returned, its considered out of ammo */
	UFUNCTION(BlueprintNativeEvent, Category = "Ability|Ammo")
	float CalculateAmmoToConsume() const;
	virtual float CalculateAmmoToConsume_Implementation() const { return 1.f; }

protected:

	/** Attempts to take another shot. Will commit ability
	and check if we have enough ammo required to take it */
	void TryTakeShot(bool bFirstShot);

	/** Checks if we can take a shot with given amount of ammo */
	bool CanTakeShot(UAbilitySystemComponent* WeaponASC, float AmmoRequired) const;

	/** Commits the fire gameplay effect to weapon asc using the ammo consumed */
	bool CommitShot(UAbilitySystemComponent* WeaponASC, float AmmoConsumed);

	/** Handles if shot attempt failed */
	void OnShotFailed(UAbilitySystemComponent* ASC);

	/** Commits costs and takes a shot. Will end ability if fails */
	void CommitAndTakeShot(bool bFirstShot);

	/** Tries to take a shot, will call TakeShot if commit ability succeeds */
	bool CanTakeShot();

	/** Similar to check cost, but using the firing cost gameplay effect */
	bool CheckFiringCost();

	/** Called when commit fails, will try to activate another ability while ending this one */
	void OnShotFailed();

private:

	/** Gets the ammo to consume with infinite ammo taken into consideration */
	bool GetAmmoToConsume(UAbilitySystemComponent* ASC, UAbilitySystemComponent* WeaponASC, float& OutAmmo) const;

	/** Gets the ammo attribute to compare with */
	FGameplayAttribute GetAmmoAttribute() const;

protected:

	/** The gameplay effect to apply to weapon every shot. This effect should allow for the
	magnitude to be set using the SetByCaller.Ability.FireGunAmmo tag but doesn't require it*/
	UPROPERTY(EditDefaultsOnly, Category = Costs)
	TSubclassOf<UGameplayEffect> FiringCostGameplayEffectClass;

	/** An optional override attribute to use for ammo consumption. By default, we use UGunAttributeSet::ClipCount */
	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FGameplayAttribute AmmoAttritbuteOverride;

	/** If we should commit normal costs and cooldowns everytime we take a shot */
	UPROPERTY(EditDefaultsOnly, Category = Fire)
	uint8 bCommitAbilityEveryShot : 1;

	/** The tag to use when taking a shot fails. Will be used to try and activate another ability */
	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FGameplayTagContainer ShotFailedTryActivateTags;

protected:

	/** The amount of ammo we consumed this shot */
	UPROPERTY(BlueprintReadOnly, Transient, Category = Fire)
	float AmmoConsumedThisShot;

private:

	/** Get the gameplay effect for the fire cost */
	UGameplayEffect* GetFiringCostGameplayEffect() const;

private:

	/** If this ability should end */
	uint8 bShouldEnd : 1;

	/** Iteration of current burst */
	int32 BurstIteration;

private:

	/** Callback for when shot interval has finished */
	UFUNCTION()
	void OnShotIntervalPassed();

private:

	/** Timer handle for managing shot intervals */
	FTimerHandle TimerHandle_TakeShot;
};
