// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "SpecOp/SpecOpGameplayAbility.h"
#include "SpecOpGameplayAbility_ChargeGun.generated.h"

/** Gameplay ability for the spec op that requires an initial charge before taking the shot */
UCLASS()
class EDENSGARDEN_API USpecOpGameplayAbility_ChargeGun : public USpecOpGameplayAbility
{
	GENERATED_BODY()
	
public:

	USpecOpGameplayAbility_ChargeGun();

public:

	/** Get if spec ops current gun has the required ammo */
	UFUNCTION(BlueprintPure, Category = Attribute)
	bool HasEnoughAmmo(float RequiredAmmo = 1.f) const;	

private:

	/** Get the attribute to use to compare required ammo */
	FGameplayAttribute GetAmmoAttribute() const;

public:

	/** The attribute to compare with required ammo */
	UPROPERTY(EditDefaultsOnly, Category = Fire)
	FGameplayAttribute AmmoAttritbuteOverride;
};
