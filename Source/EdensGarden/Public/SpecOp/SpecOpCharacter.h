// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Characters/EGCharacter.h"
#include "Interactives/InteractiveComponent.h"
#include "SpecOpAttributeSet.h"
#include "SpecOpCharacter.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSpecOp, Log, All);

DECLARE_STATS_GROUP(TEXT("SpecOp"), STATGROUP_SpecOp, STATCAT_Advanced);

class ASpecOpCharacter;
class AGunBase;
class AMeleeBase;
class AWeaponBase;

class UCameraComponent;
class UFocusingSpringArmComponent;
class USpecOpAbilitySystemComponent;
class USpecOpAnimInstance;
class USpecOpMovementComponent;
class USpotLightComponent;

/** Delegate for when weapon inventory has changed (both guns and melee weapon). Passes the weapon and if the weapon was added */
DECLARE_MULTICAST_DELEGATE_ThreeParams(FWeaponInventoryChangeSignature, ASpecOpCharacter*, AWeaponBase*, bool);

/** Delegate for when a the spec ops equipped gun has changed. This has the possibility of being null */
DECLARE_MULTICAST_DELEGATE_TwoParams(FEquippedGunChangeSignature, ASpecOpCharacter*, AGunBase*);

/** The state of the spec op (in terms of actions). This
enum is used to help ID*/
UENUM(BlueprintType)
enum class ESpecOpState : uint8
{
	/** Spec op is idle (this includes moving around) */
	Idle,

	/** Spec op is sprinting */
	Sprinting,

	/** Spec op is sneaking */
	Sneaking,

	/** Spec op is focusing */
	Focusing,

	/** Spec op is rolling */
	Rolling
};

/** Base for the spec op character, should be derived in blueprints */
UCLASS()
class ASpecOpCharacter : public AEGCharacter
{
	GENERATED_BODY()

public:

	ASpecOpCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:

	// Begin EGCharacter Interface
	virtual void InitializeAttributeDefaults() override;
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags) override;
	virtual void OnRagdoll_Implementation() override;
	virtual void OnRagdollStop_Implementation() override;
	// End EGCharacter Interface

public:

	// Begin AEGCharacter Interface
	virtual bool CanBeStunned() const override { return false; /** For now */ }
	// End AEGCharacter Interface

	// Begin ACharacter Interface
	virtual bool CanCrouch() override; 
	virtual void Landed(const FHitResult& Hit) override;
	// End ACharacter Interface

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetActorHiddenInGame(bool bNewHidden) override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	virtual void FellOutOfWorld(const class UDamageType& DamageType) override;
	// End AActor Interface

	// Begin UObject Interface
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

public:

	/** Get camera boom */
	FORCEINLINE UFocusingSpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Get camera */
	FORCEINLINE UCameraComponent* GetCamera() const { return Camera; }

	/** Get spec op movement component */
	FORCEINLINE USpecOpMovementComponent* GetSpecOpMovementComponent() const { return SpecOpMovementComponent; }

	/** Get spec op ability system */
	FORCEINLINE USpecOpAbilitySystemComponent* GetSpecOpAbilitySystem() const { return SpecOpAbilitySystem; }

	/** Get interactive area */
	FORCEINLINE UInteractiveComponent* GetInteractiveArea() const { return InteractiveArea; }

	/** Get helmet torch */
	FORCEINLINE USpotLightComponent* GetHelmetTorch() const { return HelmetTorch; }

protected:

	/** Called via input to move forward */
	void MoveForward(float Value);

	/** Called via input to move right */
	void MoveRight(float Value);

	/** Called via input to turn at given rate */
	void TurnAtRate(float Rate);

	/** Called via input to look at given rate */
	void LookUpAtRate(float Rate);

protected:

	// Begin APawn Interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End APawn Interface

protected:

	/** Base turn rate in deg/sec. This will be overwritten by sensitivity settings on begin play  */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Camera)
	float TurnRate;

	/** Base turn rate in deg/sec when focused. This will be overwritten by sensitivity settings on begin play */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Camera)
	float FocusedTurnRate;

	/** If up-/down look input is being inverted. This will be overwritten by sensitivity settings on begin play */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Camera)
	uint32 bInvertYTurnInput : 1;

private:

	/** Updates the turn values based on current user settings */
	void UpdateSensitivityValues();

private:

	/** Focusing spring arm to allow different views from camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UFocusingSpringArmComponent* CameraBoom;

	/** Players viewport of the world */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* Camera;

	/** Characters movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	USpecOpMovementComponent* SpecOpMovementComponent;

	/** Players ability system */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Abilities, meta = (AllowPrivateAccess = "true"))
	USpecOpAbilitySystemComponent* SpecOpAbilitySystem;

	/** Players interact area */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Interactive, meta = (AllowPrivateAccess = "true"))
	UInteractiveComponent* InteractiveArea;

	/** Torch on players helmet (doesn't have to be on helmet) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USpotLightComponent* HelmetTorch;

public:

	#if EG_GAMEPLAY_DEBUGGER
	// Begin AEGCharacter Interface
	virtual void GetDebugString(FString& String) const override;
	// End AEGCharacter Interface
	#endif

public:

	/** Get the players breath */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetBreath() const;

	/** Get the max amount of breath */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxBreath() const;

	/** Get the players energy */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetEnergy() const;

	/** Get the max amount of energy */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxEnergy() const;

	/** Get the players sprint modifier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetSprintModifier() const;

	/** Get the players sneak modifier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetSneakModifier() const;

	/** Get the players jump modifier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetJumpModifier() const;

	/** Get the players sound modifier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetSoundModifier() const;

public:

	/** Get spec ops attributes */
	FORCEINLINE USpecOpAttributeSet* GetSpecOpAttributeSet() const { return SpecOpAttributeSet; }

public:

	/** Event signature for when spec ops breath has changed */
	UPROPERTY(BlueprintAssignable, Category = Character)
	FCharacterMeterChangedSignature OnCharacterBreathChanged;

	/** Event signature for when spec ops energy has changed */
	UPROPERTY(BlueprintAssignable, Category = Character)
	FCharacterMeterChangedSignature OnCharacterEnergyChanged;

protected:

	/** C++ functions for attribute events (called after blueprint functions) */

	virtual void OnBreathChanged_Implementation(float NewBreath, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnOutOfBreath_Implementation(float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnBreathRegenRateChanged_Implementation(float NewRate, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnBreathRegenerated_Implementation(float NewBreath, float Delta, bool bFullyRestored) { }
	virtual void OnEnergyChanged_Implementation(float NewEnergy, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnOutOfEnergy_Implementation(float Delta, const FGameplayTagContainer& GameplayTags);
	virtual void OnEnergyRegenRateChanged_Implementation(float NewRate, float Delta, bool bIsIdleRate, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnEnergyRegenerated_Implementation(float NewEnergy, float Delta, bool bFullyRestored) { }
	virtual void OnSprintModifierChanged_Implementation(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnSneakModifierChanged_Implementation(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnJumpModifierChanged_Implementation(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags) { }

protected:

	/** Blueprint functions for attribute events (called before C++ functions) */

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnBreathChanged(float NewBreath, float Delta, const FGameplayTagContainer& GameplayTags);

	/** When breath has been restored in some way, this either being from no breath or to max breath */
	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnBreathRestored(bool bFully);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnOutOfBreath(float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnBreathRegenRateChanged(float NewRate, float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnBreathRegenerated(float NewBreath, float Delta, bool bFullyRestored);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnEnergyChanged(float NewEnergy, float Delta, const FGameplayTagContainer& GameplayTags);

	/** When energy has been restored in some way, this either being from no energy or to max energy */
	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnEnergyRestored(bool bFully);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnOutOfEnergy(float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnEnergyRegenRateChanged(float NewRate, float Delta, bool bIsIdleRate, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnEnergyRegenerated(float NewEnergy, float Delta, bool bFullyRestored);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnSprintModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnSneakModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = SpecOp)
	void OnJumpModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

private:

	/** Allow attribute set to notify us on certain events */
	friend USpecOpAttributeSet;

	// TODO: possibly have some check functions here to check

	/** Notify for whenever spec ops breath has changed */
	void NotifyBreathChanged(float NewBreath, float Delta, bool bOutOfBreath, const FGameplayTagContainer& GameplayTags);

	/** Notify that breath regen rate has been changed */
	void NotifyBreathRegenRateChanged(float NewRate, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Notify that breath has regenerated a bit */
	void NotifyBreathRegenerated(float NewBreath, float Delta, bool bFullyRestored);

	/** Notify for whenever spec ops energy has changed */
	void NotifyEnergyChanged(float NewEnergy, float Delta, bool bOutOfEnergy, const FGameplayTagContainer& GameplayTags);

	/** Notify that energy regen rate has been changed */
	void NotifyEnergyRegenRateChanged(float NewRate, float Delta, bool bIsIdleRate, const FGameplayTagContainer& GameplayTags);

	/** Notify that energy has regenerated a bit */
	void NotifyEnergyRegenerated(float NewEnergy, float Delta, bool bFullyRestored);

	/** Notify when sprint modifier has been changed */
	void NotifySprintModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Notify when sneak modifier has been changed */
	void NotifySneakModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Notify when jump modifier has been changed */
	void NotifyJumpModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

private:

	/** Players attributes */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Abilities, meta = (AllowPrivateAccess = "true"))
	USpecOpAttributeSet* SpecOpAttributeSet;

public:

	/** Convenience function for updating closest interactive */
	UFUNCTION(BlueprintCallable, Category = Interactive)
	void UpdateClosestInteractive();

public:

	/** Get if spec op can interact with world in current state */
	virtual bool CanInteract() const;

public:

	/** Get if the player is running */
	UFUNCTION(BlueprintPure, Category = Character)
	bool IsSprinting(bool bConsiderVelocity = true) const;

	/** Get if the player is sneaking */
	UFUNCTION(BlueprintPure, Category = Character)
	bool IsSneaking() const;

	/** Get if the player is focusing */
	UFUNCTION(BlueprintPure, Category = Character)
	bool IsFocusing() const;

	/** Get if the player is rolling */
	UFUNCTION(BlueprintPure, Category = Character)
	bool IsRolling() const;

	/** Inform character to start sprinting */
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void StartSprinting();

	/** Inform character to stop sprinting */
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void StopSprinting();

	/** Inform character to start sneaking */
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void StartSneaking();

	/** Inform character to stop sneaking */
	UFUNCTION(BlueprintCallable, Category = Character)
	virtual void StopSneaking();

	/** Inform character to start focusing */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StartFocusing();

	/** Inform character to stop focusing */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StopFocusing();

	/** Informs character to rolling */
	UFUNCTION(BlueprintCallable, Category = Character)
	void Roll();

	/** Informs character to stop rolling */
	void StopRolling();

private:

	// These functions actually start / stop performing actions

	void InternalStartSprinting();
	void InternalStopSprinting();
	void InternalStartSneaking();
	void InternalStopSneaking();
	void InternalStartFocusing();
	void InternalStopFocusing();
	void InternalStartRolling(ESpecOpState FromState);
	void InternalStopRolling();

public:

	/** Check if character can perform any action */
	virtual bool CanPerformAnyAction(bool bJustLanded = false) const;

	/** Check if character can sprint in the current state */
	virtual bool CanSprint(bool bJustLanded = false) const;

	/** Check if character can sneak in the current state */
	virtual bool CanSneak(bool bJustLanded = false) const;

	/** Check if character can focus in the current state */
	virtual bool CanFocus(bool bJustLanded = false) const;

	/** Check if character can currently roll */
	virtual bool CanRoll(bool bJustLanded = false) const;

protected:

	/** Get if character can sprint with given gun equipped */
	bool CanSprintWithEquippedGun() const;

	/** Get if character can roll with given gun equipped */
	bool CanRollWithEquippedGun() const;
		 
public:

	/** If spec op should update orientation to face camera */
	bool ShouldFaceCamera() const;

private:

	/** Attempts to enter given state */
	void TryEnterState(ESpecOpState NewState);

	/** Attempts to enter any state wanting to be active */
	void TryEnterWantingStates(bool bFromLandedEvent = false);

	/** Determines characters orientation basedon current state */
	void DetermineOrientation();

protected:
	
	/** If character wants to sprint */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	uint32 bWantsToSprint : 1;

	/** If character wants to sneak */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	uint32 bWantsToSneak : 1;

	/** If character is sprinting */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	uint32 bIsSprinting : 1;

	/** If character is sneaking */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	uint32 bIsSneaking : 1;

	/** If we are wanting to aim with our weapon */
	UPROPERTY(BlueprintReadOnly, Category = SpecOp)
	uint32 bWantsToFocus : 1;

	/** If character is currently focusing */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = SpecOp)
	uint32 bIsFocusing : 1;

	/** If character is current rolling */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = SpecOp)
	uint32 bIsRolling : 1;

private:

	/** Cached state of the character */
	UPROPERTY(Transient)
	ESpecOpState State;

	/** Cached value for movement component when starting to roll. This is so we
	can roll off ledges, since the can walk of ledges while crouching might be false */
	uint32 bCachedCanWalkOffLedgesWhenCrouching : 1;

protected:

	/** If the spec op is out of breath */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Character)
	uint32 bIsOutOfBreath : 1;

	/** If the spec op is out of energy */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Character)
	uint32 bIsOutOfEnergy : 1;

protected:

	// Begin AEGCharacter Interface
	virtual void OnCharacterInitialHit(AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit) override;
	// End AEGCharacter Interface

protected:

	/** Binds default abilities to ability system */
	virtual void BindDefaultAbilities();

private:

	/** Default ability to activate with roll action */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding RollBinding;

	/** Default ability to activate with crouch action */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding CrouchBinding;

	/** Default ability to activate with sprint action */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding SprintBinding;

	/** Default ability to activate with sneak binding */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding SneakBinding;

	/** Default ability to activate with interact binding */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding InteractBinding;

	/** Default ability to activate with torch binding */
	UPROPERTY(EditDefaultsOnly, Category = Controls, meta = (ShowOnlyInnerProperties = "true"))
	FSpecOpAbilityInputBinding TorchBinding;

	/** Spec ops first power up */
	UPROPERTY(EditDefaultsOnly, Category = Powers)
	TSubclassOf<UGameplayAbility> PowerUp1;

	/** Spec ops second power up */
	UPROPERTY(EditDefaultsOnly, Category = Powers)
	TSubclassOf<UGameplayAbility> PowerUp2;

	/** Spec ops third power up */
	UPROPERTY(EditDefaultsOnly, Category = Powers)
	TSubclassOf<UGameplayAbility> PowerUp3;

	/** Spec ops fourth power up */
	UPROPERTY(EditDefaultsOnly, Category = Powers)
	TSubclassOf<UGameplayAbility> PowerUp4;

private:

	/** Callback for when the focusing tag has changed */
	void OnFocusTagUpdated(const FGameplayTag Tag, int32 Stack);

	/** Callback for when character should manually orientant to focus camera */
	void OnOrientationTagUpdated(const FGameplayTag Tag, int32 Stack);

public:

	uint32 bOrientationFocused : 1;

public:

	/** Applies 'recoil' to the spec op. This recoil is applied to the controller */
	UFUNCTION(BlueprintCallable, Category = SpecOp)
	void ApplyRecoil(float YawRecoil, float PitchRecoil);

	/** Applies 'recoil' to the spec op by calculating random value in range */
	UFUNCTION(BlueprintCallable, Category = SpecOp)
	void ApplyRandomRecoil(float YawMin, float YawMax, float PitchMin, float PitchMax);

protected:

	virtual void OnInstigatedDamage(AEGCharacter* Character, float Damage, const FGameplayTagContainer& GameplayTags) override;

public:

	/** Enables/Disables the torch spotlight */
	UFUNCTION(BlueprintCallable, Category = SpecOp)
	void SetHelmetTorchEnabled(bool bEnable);

	/** Conveniance function for toggling the helmet torch. Get if torch is now active */
	UFUNCTION(BlueprintCallable, Category = SpecOp)
	bool ToggleHelmetTorch();

public:

	/** Get if the helmet torch is currently active */
	UFUNCTION(BlueprintPure, Category = SpecOp)
	bool IsHelmetTorchActive() const;

public:

	/** Get spec ops animation instance (can be null) */
	UFUNCTION(BlueprintPure, Category = SpecOp)
	USpecOpAnimInstance* GetSpecOpAnimInstance() const;

public:

	/** Give the gun to this spec op, adding it to the inventory.
	If not forced, will return false if inventory is full */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	bool GiveGun(AGunBase* Gun, bool bForce = false);

	/** Give the gun to this spec op, adding it to the inventory.
	This function will not automatically equip the gun however */
	bool GiveGunDontEquip(AGunBase* Gun);

	/** Gives the gun to this spec op and automaticlly equipping it
	If not force, will return false if inventory is full */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	bool GiveAndEquipGun(AGunBase* Gun, bool bForce = false, bool bSkipEquip = false);

	/** Drops the given gun only if it is in spec ops inventory (unless
	dropping it would */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	bool DropGun(AGunBase* Gun);

	/** Drops the gun at the given index (unless dropping it would result in 
	inventory emptying) and will auto equip the next gun avaliable to equip */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	bool DropGunAtIndex(int32 Index);

	/** Drops all guns the spec op is currently carrying. Can
	potentially do nothing if not forced to drop all guns */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	void DropAllGuns(bool bForce = false);

	/** Destroys all the guns in spec ops inventory. This is
	used when re-loading the character from a save file */
	void DestroyAllGuns();

public:

	/** Get if the spec op can drop guns at this time */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	bool CanDropGuns(bool bDropAll = false) const;

	/** Get if the spec op is currently switching guns */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	bool IsSwitchingGuns() const;

private:

	/** Drops gun at index and equips next avaliable gun */
	bool DropAndReplaceGun(int32 Index);

private:

	/** Adds gun to inventory, get index in inventory (INDEX_NONE if failed to add) */
	int32 AddGunToInventory(AGunBase* Gun, bool bForce);

	/** Removes gun at index from inventory, get index needed to replace gun and keep order.
	If forced, gun will be dropped event if CanDropGuns returns false */
	int32 RemoveGunFromInventory(int32 Index, bool bForce = false);

public:

	/** Equips the given gun only if it is in spec ops inventory */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	void EquipGun(AGunBase* Gun, bool bSkipHolster = false);

	/** Equips the gun at the given index (anything outside of range is ignored) */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	void EquipGunAtIndex(int32 Index, bool bSkipHolster = false);

	/** Equips the gun at the given index (anything outside of range is ignored)
	Both the holster actions and the equip actions are skipped (instant switch) */
	void EquipGunImmediately(int32 Index);

	/** Equips the next gun in gun inventory (loops back to start of inventory) */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	void EquipNextGun(bool bSkipHolster = false);

	/** Equips the previous gun in gun inventory (loops backs to end of inventory) */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns")
	void EquipPreviousGun(bool bSkipHolster = false);

	/** Functions bound to input to allow mouse scroll between guns */
	void ScrollNextGun();
	void ScrollPreviousGun();

	/** Physically equips the equipped gun (gun will appear equipped with mesh) */
	void PhysicallyEquipWeapon();

	/** Physically holsters the equipped gun (gun will appear holstered with mesh) */
	void PhysicallyHolsterWeapon();

public:

	/** Get if the spec op can switch gun at this time */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	bool CanSwitchGuns() const;

private:

	/** Equips gun at index, returning if either gun was equipped or is pending equip */
	bool EquipGunInInventory(int32 Index, bool bSkipHolster);

private:

	/** Attempts to start equipping the given gun */
	void TryEquipGun(AGunBase* Gun, bool bSkipAction);

	/** Equips given gun, replacing the currently equipped gun.
	Will handle playing the equip ability and if the ability fails */
	void EquipGunWithAbility(AGunBase* Gun, bool bSkipAction = false);

	/** Equips the gun currently in queue, resetting it once done */
	void EquipQueuedGun();

	/** Holsters equipped gun and queues the given gun to be equipped.
	Will handle playing the holster ability and if the ability fails */
	void HolsterAndQueueGun(AGunBase* GunToQueue, bool bSkipAction);

	/** Cancels the pending equip of equipped gun, get if cancel was successfull */
	bool CancelPendingEquip();

	/** Cancels the pending holster of equipped gun, get if holster was successfull */
	bool CancelPendingHolster();

	/** Binds/Clears switch gun callback */
	void BindSwitchGunCallback();
	void ClearSwitchGunCallback();

	/** Callback for when an ability ends. This function checks if its the
	equip/holster ability of the equipped gun to notify its action end */
	UFUNCTION()
	void OnSwitchGunFinished(const FAbilityEndedData& EndedData);

private:

	/** Handle to the switch gun callback */
	FDelegateHandle SwitchGunHandle;

	/** If when holster switch ability has finished, should we actually equip queued gun.
	This prevents cancellation during holster from switching the guns */
	uint32 bShouldSwitchGun : 1;

public:

	/** Removes references to given weapon, does not call any functions
	on the weapon to notify it, so this should only be called by weapons */
	void RemoveWeapon(AWeaponBase* Weapon);

public:

	/** Attaches the given weapon to socket on spec ops mesh */
	void AttachWeaponToMesh(AWeaponBase* Weapon, const FName& Socket) const;

public:

	/** Get all weapons (both guns and melee weapon) in spec ops inventory */
	UFUNCTION(BlueprintPure, Category = "Weapons")
	TArray<AWeaponBase*> GetAllWeapons() const;

public:

	/** Event for guns being added or removed from inventory */
	FWeaponInventoryChangeSignature OnWeaponInventoryUpdated;

	/** Event for when the equipped gun has changed, gun can possibly be null */
	FEquippedGunChangeSignature OnEquippedGunUpdated;

public:

	/** Sets the max amount of guns the spec op can hold (clamped to 1 or greater) */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Guns", meta = (UnsafeDuringActorConstruction="true"))
	void SetMaxGunInventorySize(int32 MaxSize);

public:

	/** Get if the equipped gun is physically equipped */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	bool IsGunPhysicallyEquipped() const;

	/** Get if gun inventory is full */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	bool IsGunInventoryFull() const { return GunInventory.Num() >= MaxGunInventorySize; }

	/** Get currently equipped gun */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	AGunBase* GetEquippedGun() const { return EquippedGun; }

	/** Get the index of currently equipped gun */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	int32 GetEquippedGunIndex() const { return GunInventory.Find(EquippedGun); }

	/** Get the amount of guns in spec ops inventory */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	int32 GetNumOfGuns() const { return GunInventory.Num(); }

	/** Get the max amount of guns the spec op can hold */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	int32 GetMaxNumOfGuns() const { return MaxGunInventorySize; }

	/** Get all guns spec op is carrying */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	const TArray<AGunBase*>& GetAllGuns() const { return GunInventory; }

	/** Check if spec op is carrying gun of class and get it if so.
	Can optionally prioritize the equipped gun over first in inventory */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	AGunBase* GetGunOfClass(TSubclassOf<AGunBase> GunClass, bool bPrioritizeEquipped = true) const;

	/** Get all guns of class the spec op is carrying */
	UFUNCTION(BlueprintPure, Category = "Weapons|Guns")
	void GetAllGunsOfClass(TSubclassOf<AGunBase> GunClass, TArray<AGunBase*>& OutGuns) const;

private:

	/** Sets the new size of the of the gun inventory.
	Handles based on new size and current world (PIE, Editor) */
	void SetGunInventorySize(int32 NewSize, EWorldType::Type WorldType);

	/** Pops queued gun (nullifies queued gun) */
	AGunBase* PopQueuedGun();

private:

	/** The gun currently equipped by the spec op */
	UPROPERTY(VisibleInstanceOnly, Category = Weapons)
	AGunBase* EquippedGun;

	/** The gun queued to be equipped */
	UPROPERTY(VisibleInstanceOnly, Transient, DuplicateTransient, Category = Weapons)
	AGunBase* QueuedGun;

	/** All the guns in spec ops inventory */
	UPROPERTY(VisibleAnywhere, Category = Weapons)
	TArray<AGunBase*> GunInventory;

	/** The max amount of guns this spec op can hold at a time. This will
	get overriden by default spec op inventory when generating a new save */
	UPROPERTY(EditAnywhere, Category = Weapons, meta = (ClampMin=1))
	int32 MaxGunInventorySize;

	/** If switching guns by scrolling should skip holstering */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = Weapons)
	uint32 bGunScrollSkipsHolstering : 1;

public:

	/** Sets the melee weapon for this spec op to use */
	UFUNCTION(BlueprintCallable, Category = "Weapons|Melee")
	void SetMeleeWeapon(AMeleeBase* Melee);

	/** Destroys the melee weapon in spec ops inventory. This
	is used when re-loading the character from a save file */
	void DestroyMeleeWeapon();

public:

	/** Get if this spec op has a melee weapon */
	UFUNCTION(BlueprintPure, Category = "Weapons|Melee")
	bool HasMeleeWeapon() const { return MeleeWeapon != nullptr; }

	/** Get if spec op is currently using the melee weapon */
	UFUNCTION(BlueprintPure, Category = "Weapons|Melee")
	bool IsUsingMeleeWeapon() const;

	/** Get spec ops melee weapon */
	UFUNCTION(BlueprintPure, Category = "Weapons|Meshes")
	AMeleeBase* GetMeleeWeapon() const { return MeleeWeapon; }

private:

	/** The melee weapon this spec op is using */
	UPROPERTY(VisibleAnywhere, Category = Weapons)
	AMeleeBase* MeleeWeapon;

public:

	/** Notifies that melee weapon should appear in hand (and to hide equipped gun) */
	UFUNCTION(BlueprintCallable, Category = "Weapons")
	void SetShowMeleeInHand(bool bNewShow);

private:

	/** If melee weapon is being shown in hand */
	uint32 bIsMeleeInHand : 1;
};

