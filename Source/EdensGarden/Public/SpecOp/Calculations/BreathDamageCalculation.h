// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "BreathDamageCalculation.generated.h"

/** Calculates if damage should be applied to spec op based on current breath */
UCLASS()
class EDENSGARDEN_API UBreathDamageCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	UBreathDamageCalculation();
	
public:

	// Begin UGameplayEffectExecutionCalculation
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	// End UGameplayEffectExecutionCalculation Calculation
};
