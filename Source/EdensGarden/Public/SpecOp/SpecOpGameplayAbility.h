// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterGameplayAbility.h"
#include "SpecOpGameplayAbility.generated.h"

class AGunBase;
class AMeleeBase;
class ASpecOpCharacter;

/** Gameplay ability specialized to work with the spec op character */
UCLASS()
class EDENSGARDEN_API USpecOpGameplayAbility : public UEGCharacterGameplayAbility
{
	GENERATED_BODY()
	
public:

	USpecOpGameplayAbility();

public:

	// Begin UGameplayAbility Interface
	virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate) override;
	// End UGameplayAbility Interface

public:

	/** Applies a gameplay effect to our equipped weapon */
	UFUNCTION(BlueprintCallable, Category = Ability)
	FActiveGameplayEffectHandle ApplyGameplayEffectToEquippedGun(TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel = 1, int32 Stacks = 1, bool bOnlyIfEquipped = true);

	/** Removes a gameplay effect from our equipped weapon */
	UFUNCTION(BlueprintCallable, Category = Ability)
	void RemoveGameplayEffectFromEquippedGunWithHandle(FActiveGameplayEffectHandle Handle, int32 StacksToRemove = -1);

	/** Applies a gameplay effect to owner with equipped weapon as source */
	UFUNCTION(BlueprintCallable, Category = Ability)
	FActiveGameplayEffectHandle ApplyGameplayEffectFromGunToOwner(TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel = 1, int32 Stacks = 1, bool bOnlyIfEquipped = true);

	/** Applies a gameplay effects to target with equipped weapon as source */
	UFUNCTION(BlueprintCallable, Category = Ability)
	TArray<FActiveGameplayEffectHandle> ApplyGameplayEffectFromGunToTarget(FGameplayAbilityTargetDataHandle TargetData, TSubclassOf<UGameplayEffect> GameplayEffectClass, int32 GameplayEffectLevel = 1, int32 Stacks = 1, bool bOnlyIfEquipped = true);

	/** Makes an outgoing gameplay effect from equipped weapon */
	UFUNCTION(BlueprintCallable, Category = Ability, meta = (DisplayName = "MakeOutgoingGameplayEffectSpecFromGun"))
	FGameplayEffectSpecHandle BP_MakeOutgoingGameplayEffectSpecFromGun(TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level = 1.f) const;

	FGameplayEffectSpecHandle MakeOutgoingGameplayEffectSpecFromGun(AGunBase* Gun, TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level = 1.f) const;
	FGameplayEffectContextHandle MakeEffectContextFromGun(AGunBase* Gun, const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const;

	/** Makes a target location info using the currently equipped guns mesh */
	UFUNCTION(BlueprintPure, Category = Ability)
	FGameplayAbilityTargetingLocationInfo MakeTargetLocationInfoFromEquippedGun(FName SocketName);

	/** 
	* Makes a targeting location info from guns muzzle but with additional
	* spread applied to rotation based off the characters current velocity.
	* Min and max spread are in degrees 
	*/
	UFUNCTION(BlueprintPure, Category = Ability)
	FGameplayAbilityTargetingLocationInfo MakeTargetLocationInfoFromEquippedGunWithSpread(FName SocketName, float MinSpread, float MaxSpread, float MinVelocity, float MaxVelocity);

public:

	/** Get the spec op who owns this ability. This will only
	be valid if instance policy is not set to Non-Instanced */
	UFUNCTION(BlueprintPure, Category = Ability)
	ASpecOpCharacter* GetSpecOp() const;

	/** Get the spec ops current equipped gun. This can return null */
	UFUNCTION(BlueprintPure, Category = Ability)
	AGunBase* GetEquippedGun() const;

	/** Get the spec ops melee weapon. This can return null */
	UFUNCTION(BlueprintPure, Category = Ability)
	AMeleeBase* GetMeleeWeapon() const;
	
private:

	/** The spec op who owns this ability. Cached to avoid casting */
	UPROPERTY(Transient)
	ASpecOpCharacter* SpecOp;
};
