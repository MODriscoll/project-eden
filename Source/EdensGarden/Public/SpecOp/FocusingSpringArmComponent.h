// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "FocusingSpringArmComponent.generated.h"

class ASpecOpCharacter;

/** Struct containing data for blend */
USTRUCT(BlueprintType)
struct FFocusBlendData
{
	GENERATED_BODY()

public:

	FFocusBlendData()
	{
		TargetArmLength = 0.f;
		SocketOffset = FVector::ZeroVector;
		TargetOffset = FVector::ZeroVector;
	}

public:

	/** Natural length of spring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TargetArmLength;

	/** Offset at end of socket */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector SocketOffset;

	/** Offset at start of spring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector TargetOffset;
};

/** Struct containing easing functions to use */
USTRUCT(BlueprintType)
struct FFocusBlendFuncs
{
	GENERATED_BODY()

public:

	FFocusBlendFuncs()
	{
		TargetArmLengthFunc = EEasingFunc::Linear;
		SocketOffsetFunc = EEasingFunc::Linear;
		TargetOffsetFunc = EEasingFunc::Linear;
	}

public:

	/** Function for target arm length */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EEasingFunc::Type> TargetArmLengthFunc;

	/** Function for socket offset */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EEasingFunc::Type> SocketOffsetFunc;

	/** Function for target offset */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TEnumAsByte<EEasingFunc::Type> TargetOffsetFunc;
};

USTRUCT(BlueprintType)
struct FFocusSpringArmState
{
	GENERATED_BODY()

public:

	FFocusSpringArmState()
	{
		TargetArmLength = 300.f;
		SocketOffset = FVector::ZeroVector;
		TargetOffset = FVector::ZeroVector;
		bEnableCameraLag = false;
		BlendTime = 0.1f;
	}

public:

	/** Natural length of spring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TargetArmLength;

	/** Offset at end of socket */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector SocketOffset;

	/** Offset at start of spring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector TargetOffset;

	/** If camera lag should be enabled */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 bEnableCameraLag : 1;

	/** The time to fully blend to this state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (ClampMin = 0))
	float BlendTime;

public:

	/** Get blend time safe (does not return zero) */
	float GetBlendTimeSafe() const
	{
		return FMath::IsNearlyZero(BlendTime) ? 1.f : BlendTime;
	}
};

/** Spring arm that is designed to work with the spec ops ability to focus */
UCLASS(hidecategories = Camera)
class EDENSGARDEN_API UFocusingSpringArmComponent : public USpringArmComponent
{
	GENERATED_BODY()
	
public:

	/** The default state to use when no other state is set */
	static const FFocusSpringArmState DefaultState;

public:

	UFocusingSpringArmComponent();

public:

	// Begin UActorComponent Interface
	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void InitializeComponent() override;
	// End UActorComponent Interface

public:

	/** Performs new blend using given data from current data */
	UFUNCTION(BlueprintCallable, Category = Focusing)
	void SetNewFocusSpot(const FFocusBlendData& Data, float BlendTime, const FFocusBlendFuncs& Funcs, bool bIgnoreIfAlreadyBlending = false);

	/** Helper function for restoring blend to original data */
	UFUNCTION(BlueprintCallable, Category = Focusing)
	void RestoreDefaultFocusSpot(bool bBlendTo, float BlendTime, const FFocusBlendFuncs& Funcs, bool bIgnoreIfAlreadyBlending = false);

protected:

	/** Get this objects default focus blend target */
	FFocusBlendData GetDefaultBlendData() const;

protected:

	/** If we are currently blending */
	uint8 bIsBlending : 1;

	/** Blend point we started at */
	FFocusBlendData StartData;

	/** Target of blend we are going for */
	FFocusBlendData TargetData;

	/** Easing functions we are using */
	FFocusBlendFuncs BlendFuncs;

	/** The amount of time to blend for */
	float BlendDuration;
	
	/** The time we start blending */
	float BlendStartTime;

private:
	
	/** Our default data */
	FFocusBlendData DefaultData;

public:

	/** Sets the state to use, only if state exists */
	UFUNCTION(BlueprintCallable, Category = Focusing)
	bool SetState(const FName& NewState);

protected:

	/** Get the spring data from state. If state 
	doesn't exist, the default state is returned */
	UFUNCTION(BlueprintPure, Category = Focusing)
	const FFocusSpringArmState& GetState(const FName& StateName) const;

private:
	
	/**Get if component should auto update state */
	bool ShouldAutoUpdateFocusState() const;

	/** Updates the values of the spring */
	void UpdateSpringValues();

public:

	/** The states of this spring arm */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Focusing)
	TMap<FName, FFocusSpringArmState> FocusStates;

	/** The interpolation function to use for switching between states */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Focusing)
	TEnumAsByte<EEasingFunc::Type> FocusBlendFunction;

	/** If states are automatically updated by this component based on spec ops state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Focusing)
	uint8 bAutoUpdateFocusState : 1;

	/** The name of the state for when spec op is idle while standing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Focusing, meta = (EditCodition = bAutoUpdateFocusState))
	FName IdleStandStateName;

	/** The name of the state for when spec op is focusing while standing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Focusing, meta = (EditCodition = bAutoUpdateFocusState))
	FName FocusStandStateName;

	/** The name of the state for when spec op is idle while crouching */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Focusing, meta = (EditCodition = bAutoUpdateFocusState))
	FName IdleCrouchStateName;

	/** The name of the state for when spec op is focusing while crouching */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Focusing, meta = (EditCodition = bAutoUpdateFocusState))
	FName FocusCrouchStateName;

protected:

	/** The state currently in use. Default value is used as initial state */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = State)
	FName CurrentStateName;

	/** The state being left, is needed to interpolate to new state */
	UPROPERTY(BlueprintReadOnly, Category = State)
	FName PreviousStateName;

private:

	/** If spring should be blending */
	uint8 bBlendSpring : 1;

	/** The time the last state was set. Is used with
	current states blend time to determine springs values */
	float StateStartBlendTime;

	/** The spec op using this component */
	TWeakObjectPtr<ASpecOpCharacter> SpecOpOwner;
};
