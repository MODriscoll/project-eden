// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "EdensGardenTypes.h"
#include "SpecOpAbilitySystemComponent.generated.h"

class UInputComponent;

DECLARE_MULTICAST_DELEGATE_TwoParams(FHUDTagCountChangeSignature, const FGameplayTag&, int32);

/** The input IDs that can be used for power ups */
UENUM(BlueprintType)
enum class EPowerInputID : uint8
{
	Slot1 = (uint8)EEdensGardenInputID::CharacterPower1,
	Slot2 = (uint8)EEdensGardenInputID::CharacterPower2,
	Slot3 = (uint8)EEdensGardenInputID::CharacterPower3,
	Slot4 = (uint8)EEdensGardenInputID::CharacterPower4,

	None = 255
};

/** Ability system specialized to work with the spec op character */
UCLASS()
class EDENSGARDEN_API USpecOpAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:

	USpecOpAbilitySystemComponent();
	
public:

	// Begin UAbilitySystemComponent Interface
	virtual void OnRemoveAbility(FGameplayAbilitySpec& AbilitySpec) override;
	// End UAbilitySystemComponent Interface

	// Begin UActorComponent Interface
	virtual void InitializeComponent() override;
	virtual void OnComponentDestroyed(bool bDestroyingHierarchy) override;
	// End UActorComponent Interface

public:

	/** Binds ability activation to given input component */
	void BindAbilityActivationToEGInputs(UInputComponent* InputComponent);

public:

	/** Binds an ability to given edens garden input ID */
	UFUNCTION(BlueprintCallable, Category = Ability)
	FGameplayAbilitySpecHandle BindAbilityToInput(TSubclassOf<UGameplayAbility> Ability, EEdensGardenInputID InputID);

public:

	/** Gives a new power to spec op and attempts to bind to first available input */
	UFUNCTION(BlueprintCallable, Category = PowerUp)
	EPowerInputID GivePower(TSubclassOf<UGameplayAbility> PowerUp, bool bForce = false);

	/** Gives a new power to spec op and binds it to given slot */
	UFUNCTION(BlueprintCallable, Category = PowerUp)
	bool BindPowerToInput(TSubclassOf<UGameplayAbility> PowerUp, EPowerInputID InputID, bool bForce = false);

	/** Unbinds any power bound to given input */
	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void UnbindPowerAtInput(EPowerInputID InputID);

	/** Clears all power ups */
	UFUNCTION(BlueprintCallable, Category = PowerUp)
	void ClearPowers();

public:

	/** Get if any power up is currently bound to given input */
	UFUNCTION(BlueprintPure, Category = PowerUp)
	bool IsAnyPowerBoundToInput(EPowerInputID InputID, bool bMustBeValid = true) const;

private:

	/** All powers currently bound to spec op. Stores the index to their ability spec handle */
	TMap<int32, FGameplayAbilitySpecHandle> BoundPowers;

public:

	/** Adds a new shared attribute set with this component */
	void AddSharedAttributeSet(UAttributeSet* AttributeSet);

	/** Removes a shared attribute set from this component */
	void RemoveSharedAttribute(UAttributeSet* AttributeSet);

private:

	/** Attribute sets that have an external owner, but we share it with them.
	This is here so gameplay effects can be applied directly to us to modify them */
	UPROPERTY(Transient, DuplicateTransient)
	TSet<UAttributeSet*> ExternalAttributeSets;

public:

	/** Event for when a HUD tag count has changed */
	FHUDTagCountChangeSignature OnHUDTagCountChanged;

private:

	/** Callback for when any tag count has changed.
	This will check for tags with HUD as the root */
	UFUNCTION()
	void OnGameplayTagCountChanged(const FGameplayTag GameplayTag, int32 Count);
};
