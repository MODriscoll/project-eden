// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterAnimInstance.h"
#include "Guns/GunAnimationSet.h"
#include "SpecOpAnimInstance.generated.h"

class ASpecOpCharacter;
class UGunAnimationSet;

/** Anim instance specialized to work with the spec op character */
UCLASS()
class EDENSGARDEN_API USpecOpAnimInstance : public UEGCharacterAnimInstance
{
	GENERATED_BODY()
	
public:

	USpecOpAnimInstance();

public:

	// Begin UEGCharacterAnimInstance Interface
	virtual float CalculateSpeedRatio(const AEGCharacter* Character, float DeltaTime) override; 
	virtual void CalculateYawPitchOffset(const AEGCharacter* Character, float DeltaTime, float ResetAt) override;
	virtual void UpdateStateFlags(const AEGCharacter* Character, float DeltaTime) override;
	// End UEGCharacterAnimInstance Interface

	// Begin UAnimInstance Interface
	virtual void NativeInitializeAnimation() override;
	virtual void NativeUpdateAnimation(float DeltaTime) override;
	// End UAnimInstance Interface

	// Begin UObject Interface
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

public:

	/** Get if the rolling animation is animating */
	bool IsAnimatingRoll() const { return bIsRolling; }

	/** Set the gun animation set to use */
	void SetGunAnimationSet(UGunAnimationSet* AnimSet);

	/** Resets gun animation set to default set */
	void ResetGunAnimationSet();

protected:

	/** Get if character is currently in a valid state to perform a break animation.
	This should be called after updating all other variables for best results */
	UFUNCTION(BlueprintPure, Category = "Character|Animations")
	bool IsInValidBreakState() const;

protected:

	/** The animation set for the current equipped gun */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Character|Animations")
	UGunAnimationSet* GunSet;

	/** Copy of animation data from gun set to allow thread safe access */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = "Character|Animations")
	FGunAnimationData GunAnimations;

	/** The interval between playing break animations when idle */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Aesthetics", meta = (ClampMin = 1.f))
	float BreakInterval;

protected:

	/** If the spec op is currently sprinting */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsSprinting : 1;

	/** If the spec op is currently sneaking */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsSneaking : 1;

	/** If the spec op is currently rolling */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsRolling : 1;

	/** If the spec op is currently focusing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsFocusing : 1;	

	/** If the spec op is reloading current gun */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsReloading : 1;

	/** If the spec op is using their melee weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsUsingMelee : 1;

	/** If the spec op initiated a jump */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bStartedJumping : 1;

	/** If its valid to go into break state */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bCanTakeBreak : 1;

protected:

	/** The direction we were travelling when we started rolling */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Animations")
	float RollDirection;

	/** The time elapsed till since being in a valid state for taking a break */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|Animations")
	float BreakStateElapsed;

	/** The break animation to play when entering break state */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = "Character|Animations")
	UAnimSequenceBase* BreakAnimation;

public:

	/** Get the spec op who owns this component (can possibly be null) */
	UFUNCTION(BlueprintPure, Category = "Character|Animations")
	ASpecOpCharacter* TryGetSpecOp() const;

private:

	/** Cached pointer to spec op who owns this anim instance */
	UPROPERTY()
	ASpecOpCharacter* MySpecOp;
};
