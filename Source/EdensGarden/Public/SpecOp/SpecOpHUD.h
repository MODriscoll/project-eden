// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/HUD.h"
#include "GameplayTagContainer.h"
#include "SpecOpHUD.generated.h"

class AEGCharacter;
class AGunBase;
class ASpecOpCharacter;
class UAbilitySystemComponent;
class UGameOverWidget;
class UPauseMenuWidget;
class USpecOpHUDWidget;
class UUserWidget;

struct FActiveGameplayEffectHandle;
struct FGameplayEffectSpec;

/** HUD that handles the connection between the spec op
character and widget functionality to display to the user */
UCLASS(abstract)
class EDENSGARDEN_API ASpecOpHUD : public AHUD
{
	GENERATED_BODY()

public:

	// Begin AActor Interface
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface
	
public:

	/** Called by the controller who owns us to
	establish our link between the spec op character */
	void Initialize(ASpecOpCharacter* SpecOp);

	/** Clears any links we had with the spec op */
	void CleanUp();

public:

	/** Displays the main HUD */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void DisplayMainHUD(bool bHideRest = true);

	/** Displays the pause menu, will hide the main HUD */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void DisplayPauseMenu();

	/** Displays the game over screen, will hide the main HUD */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void DisplayGameOverScreen();

	/** Hides the main HUD */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void HideMainHUD();

	/** Hides the pause menu */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void HidePauseMenu();

	/** Hides the game over screen */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void HideGameOverScreen();

public:

	/** Get the instance of the spec op HUD, creating it if it doesn't exist */
	UFUNCTION(BlueprintPure, Category = HUD)
	USpecOpHUDWidget* GetMainHUDWidget();

	/** Get the instance of the pause menu, creating it if it doesn't exist */
	UFUNCTION(BlueprintPure, Category = HUD)
	UPauseMenuWidget* GetPauseMenuWidget();

	/** Get the instance of the game over screen, creating it if it doesn't exist */
	UFUNCTION(BlueprintPure, Category = HUD)
	UGameOverWidget* GetGameOverWidget();

	/** Get if main HUD is in viewport */
	bool IsMainHUDVisible() const;

	/** Get if pause menu is in viewport */
	bool IsPauseMenuVisible() const;

	/** Get if game over screen is in viewport */
	bool IsGameOverScreenVisible() const;

private:

	/** Checks if given widget is in viewport (checks if null) */
	bool IsWidgetInViewport(UUserWidget* Widget) const;

private:

	/** Callback from subtitle manager that new subtitles should be displayed */
	UFUNCTION()
	void OnSubtitlesSet(const FText& SubtitlesText);

	/** Callback for when spec ops health has changed */
	UFUNCTION()
	void OnHealthChanged(AEGCharacter* Character, float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Callback for when spec ops breath has changed */
	UFUNCTION()
	void OnBreathChanged(AEGCharacter* Character, float NewBreath, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Callback for when spec ops energy has changed */
	UFUNCTION()
	void OnEnergyChanged(AEGCharacter* Character, float NewEnergy, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Callback for when spec op has equipped a different gun */
	UFUNCTION()
	void OnNewGunEquipped(ASpecOpCharacter* Character, AGunBase* Gun);

	/** Callback for when the closest interactive to spec op has changed */
	UFUNCTION()
	void OnClosestInteractiveChanged(AActor* Interactive, const FText& DisplayTitle);

	/** Callback for when interaction has begun */
	UFUNCTION()
	void OnInteractionStart();

	/** Callback for when interaction has finished */
	UFUNCTION()
	void OnInteractionFinished(bool bWasCancelled);

	/** Callback for when a HUD gameplay tag count has changed for spec op */
	UFUNCTION()
	void OnHUDTagCountChanged(const FGameplayTag& HUDTag, int32 Count);

	/** Callback for when an effect or a periodic effect is executed.
	This can be executed along with attribute changes (health, breath) */
	UFUNCTION()
	void OnEffectApplied(UAbilitySystemComponent* AbilitySystem, const FGameplayEffectSpec& EffectSpec, FActiveGameplayEffectHandle ActiveEffect);

	/** Callback for when the owner fails to activate an ability */
	UFUNCTION()
	void OnAbilityActivationFailed(const UGameplayAbility* GameplayAbility, const FGameplayTagContainer& FailureReasons);

	/** Callback for when new checkpoint has been reached */
	UFUNCTION()
	void OnCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved);

private:

	/** Handle for when subtitles have been set */
	FDelegateHandle SubtitlesSetHandle;

	/** Handle to new equipped gun callback */
	FDelegateHandle NewGunEquippedHandle;

	/** Handle to interaction start callback */
	FDelegateHandle InteractionStartHandle;

	/** Handle to interaction end callback */
	FDelegateHandle InteractionFinishedHandle;

	/** Handle to the HUD tag count changed callback */
	FDelegateHandle TagCountChangeHandle;

	/** Handle to ability activation fail callback */
	FDelegateHandle AbilityFailedHandle;

	/** Handle to the effect applied callback */
	FDelegateHandle EffectAppliedHandle;

	/** Handle to the periodic execution of effect callback */
	FDelegateHandle PeriodicEffectAppliedHandle;

private:

	/** Helper function for hiding all widgets */
	void HideAllWidgets();

public:

	/** The template of the HUD widget to make */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	TSubclassOf<USpecOpHUDWidget> MainHUDWidgetTemplate;

	/** The template of the pause widget to make */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	TSubclassOf<UPauseMenuWidget> PauseMenuWidgetTemplate;

	/** The template of the game over screen to make */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
	TSubclassOf<UGameOverWidget> GameOverWidgetTemplate;

private:

	/** The spec op whos HUD we represent. Keeping this
	here so we clear the correct spec op on clean up */
	TWeakObjectPtr<ASpecOpCharacter> MySpecOp;

	/** Instance of our HUD widget */
	UPROPERTY(Transient)
	USpecOpHUDWidget* MainWidgetInstance;

	/** Instance of pause menu widget */
	UPROPERTY(Transient)
	UPauseMenuWidget* PauseWidgetInstance;

	/** Instance of the game over widget */
	UPROPERTY(Transient)
	UGameOverWidget* GameOverWidgetInstance;
};
