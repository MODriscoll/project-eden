// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterAttributeSet.h"
#include "TickableAttributeSetInterface.h"
#include "SpecOpAttributeSet.generated.h"

class ASpecOpCharacter;

/** Defines the attributes associated with the players spec op */
UCLASS()
class EDENSGARDEN_API USpecOpAttributeSet : public UEGCharacterAttributeSet, public ITickableAttributeSetInterface
{
	GENERATED_BODY()
	
public:

	USpecOpAttributeSet();

public:

	// Begin ITickableAttributeSet Inteface
	virtual void Tick(float DeltaTime) override;
	virtual bool ShouldTick() const override;
	// End ITickableAttributeSet Interface
	
	// Begin UAttributeSet Interface
	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;
	// End UAttributeSet Interface

private:

	/** Attempts to regenerate some breath based on give character */
	void RegenerateBreath(float DeltaTime, ASpecOpCharacter* SpecOp, const FGameplayAbilityActorInfo* ActorInfo);

	/** Attempts to regenerate some energy based on given character */
	void RegenerateEnergy(float DeltaTime, ASpecOpCharacter* SpecOp, const FGameplayAbilityActorInfo* ActorInfo);

public:

	/** Current breath of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Breath")
	FGameplayAttributeData Breath;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, Breath)

	/** Max breath of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Breath")
	FGameplayAttributeData MaxBreath;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, MaxBreath)

	/** Rate at which breath is regenerated (per second) */
	UPROPERTY(BlueprintReadOnly, Category = "Breath")
	FGameplayAttributeData BreathRegen;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, BreathRegen)

	/** Current energy of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	FGameplayAttributeData Energy;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, Energy)

	/** Max energy of the character */
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	FGameplayAttributeData MaxEnergy;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, MaxEnergy)

	/** Rate at which energy is regenerated when idle (per second) */
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	FGameplayAttributeData IdleEnergyRegen;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, IdleEnergyRegen)

	/** Rate at which to energy is regenerated when moving (per second) */
	UPROPERTY(BlueprintReadOnly, Category = "Energy")
	FGameplayAttributeData MovingEnergyRegen;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, MovingEnergyRegen)

	/** Sprint modifier */
	UPROPERTY(BlueprintReadOnly, Category = "Mobility")
	FGameplayAttributeData SprintModifier;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, SprintModifier)

	/** Sneak modifier */
	UPROPERTY(BlueprintReadOnly, Category = "Mobility")
	FGameplayAttributeData SneakModifier;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, SneakModifier)

	/** Jump modifier */
	UPROPERTY(BlueprintReadOnly, Category = "Mobility")
	FGameplayAttributeData JumpModifier;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, JumpModifier)

	/** Energy consumption rate of the torch */
	UPROPERTY(BlueprintReadOnly, Category = Misc, meta = (HideFromLevelInfos))
	FGameplayAttributeData TorchConsumptionRate;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, TorchConsumptionRate)

	// TODO: Move to base EGCharacter Set?
	/** The multiplier that should be used when spec op makes a noise */
	UPROPERTY(BlueprintReadOnly, Category = Misc)
	FGameplayAttributeData SoundModifier;
	ATTRIBUTE_ACCESSORS(USpecOpAttributeSet, SoundModifier)
};
