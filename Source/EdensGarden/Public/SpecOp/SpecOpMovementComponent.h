// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterMovementComponent.h"
#include "SpecOpMovementComponent.generated.h"

class ASpecOpCharacter;

/** Movement component specialized to work with the spec op character */
UCLASS()
class EDENSGARDEN_API USpecOpMovementComponent : public UEGCharacterMovementComponent
{
	GENERATED_BODY()
	
public:

	// Begin UCharacterMovementComponent Interface
	virtual bool DoJump(bool bReplayingMoves) override;
	// End UCharacterMovementComponent Interface

	// Begin UMovementComponent Interface
	virtual void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;
	virtual float GetMaxSpeed() const override;
	// End UMovementComponent Interface	

	// Begin UObject Interface
	virtual void PostLoad() override;
	// End UObject Interface

public:

	/** Get spec op owner */
	UFUNCTION(BlueprintPure, Category = "SpecOp")
	ASpecOpCharacter* GetSpecOpOwner() const { return SpecOpOwner; }

	/** Calculates max speed without using specified modifiers */
	UFUNCTION(BlueprintPure, Category = "SpecOp Movement")
	float GetMaxSpeedWithoutModifiers(bool bWithoutSprint, bool bWithoutSneak, bool bWithoutGeneral = false) const;

protected:

	/** Cached pointer to spec op owner */
	UPROPERTY(Transient, DuplicateTransient)
	ASpecOpCharacter* SpecOpOwner;
};
