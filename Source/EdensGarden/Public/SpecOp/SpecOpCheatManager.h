// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/CheatManager.h"
#include "SpecOpCheatManager.generated.h"

class AGunBase;
class AHologramCharacter;
class AMeleeBase;
class AMonsterCharacter;
class AWeaponBase;

/** Cheat manager for spec op. Provides simple commands for debugging purposes */
UCLASS()
class EDENSGARDEN_API USpecOpCheatManager : public UCheatManager
{
	GENERATED_BODY()
	
public:

	// Begin UCheatManager Interface
	virtual void God() override;
	// End UCheatManager Interface

public:

	/** Spawns a new weapon in front of spec op */
	UFUNCTION(BlueprintCallable, Exec, Category = "Cheat Manager")
	void SpawnWeapon(const FName& WeaponClass);
	
	/** Spawns a monster at location controller is looking at */
	UFUNCTION(BlueprintCallable, Exec, Category = "Cheat Manager")
	void SpawnMonster(const FName& MonsterClass);

	/** Sets the team the spec op is on (specified by Edens Garden Team ID) */
	UFUNCTION(BlueprintCallable, Exec, Category = "Cheat Manager")
	void SetTeam(EEdensGardenTeamID TeamID);

	/** Sets the team of the character the player is looking at */
	UFUNCTION(BlueprintCallable, Exec, Category = "Cheat Manager")
	void SetTeamTarget(EEdensGardenTeamID TeamID);

	/** Kills the character player has possessed */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void Kill();

	/** Kills the character player is currently looking at (can potentially kill players character) */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void KillTarget();

	/** Toggles ragdoll of players characters */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void Ragdoll();

	/** Ragdolls the character the player is currently looking at (can potentially toggle players character) */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void RagdollTarget();

	/** Ragdolls the character the player is currently looking at with impulse applied */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void RagdollTargetWithImpulse(float Power);

	/** Refills the ammo of any ammo guns the player is carrying */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void RefillAmmo();

	/** Immediately completes this level and travels to given level */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void TravelToLevel(const FName& LevelName);

	/** Enables/Disables saving, this does not apply settings.
	0 means disable, any other value means enable */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void SetSavingEnabled(int32 Value);

	/** Enables/Disables subtitles, this does not apply settings.
	0 means disable, any other value means enable */
	UFUNCTION(BlueprintCallable, Exec, Category = "CheatManager")
	void SetSubtitlesEnabled(int32 Value);
};
