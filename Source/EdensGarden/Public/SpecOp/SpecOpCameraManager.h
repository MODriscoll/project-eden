// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Camera/PlayerCameraManager.h"
#include "SpecOpCameraManager.generated.h"

/** Camera manager that works along side the spec op player controller */
UCLASS()
class EDENSGARDEN_API ASpecOpCameraManager : public APlayerCameraManager
{
	GENERATED_BODY()
	
public:

	ASpecOpCameraManager();
	
protected:

	// Begin APlayerCameraManager Interface
	virtual void UpdateViewTargetInternal(FTViewTarget& OutVT, float DeltaTime) override;
	// End APlayerCameraManager Interface

public:

	/** If the camera should focus on the players character.
	This will only update the cameras rotation, not location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpecOp)
	uint8 bFocusOnSpecOp : 1;

	/** The name of the bone or socket on the players character mesh to focus on */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpecOp)
	FName FocusSocketName;

	/** Interpolation speed for when focusing on the players character.
	A value of zero or less means instantly focusing on character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpecOp)
	float FocusInterpSpeed;
};
