// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/PlayerController.h"
#include "SpecOpPlayerController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogSpecOpController, Log, All);

class AEGCharacter;
class ASpecOpCharacter;
class ASpecOpHUD;

/** The player controller specialized to work with the spec op */
UCLASS()
class EDENSGARDEN_API ASpecOpPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	
	ASpecOpPlayerController();

public:

	// Begin APlayerController Interface
	virtual void SetupInputComponent() override;
	virtual bool SetPause(bool bPause, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	// End APlayerController Interface

	// Begin AController Interface
	virtual void Possess(APawn* Pawn) override;
	virtual void UnPossess() override;
	// End AController Interface

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

private:

	/** Updates player input mode to paused settings */
	void PauseGame();

	/** Updates player input mode to game settings */
	void UnpauseGame();

	/** Positions the mouse to the center of the screen */
	void CenterMouse();

private:

	/** Initializes the HUD, links callbacks */
	void InitializeHUD();

	/** Cleans up the HUD, removing callbacks */
	void CleanUpHUD();

public:

	/** Helper function for retrieving the spec op HUD */
	UFUNCTION(BlueprintPure)
	ASpecOpHUD* GetSpecOpHUD() const;

protected:

	/** Reference to the spec op we have possessed */
	UPROPERTY(BlueprintReadOnly)
	ASpecOpCharacter* SpecOp;

private:

	/** Our HUD, to avoid casting */
	TWeakObjectPtr<ASpecOpHUD> SpecOpHUD;

private:

	/** Displays the game over screen, setting up input and centering mouse before hand */
	void DisplayEndScreen();

private:

	/** Callback for when possessed spec op has died */
	UFUNCTION()
	void OnSpecOpDeath(AEGCharacter* DeadCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);

	/** Callback for when game over screen stall time has passed */
	UFUNCTION()
	void OnEndScreenStallTimeComplete();

public:

	/** The amount of time to stall before displaying the game over screen on death.
	A value of zero or less means instantly display the game over screen upon death */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float GameOverScreenStallTime;

private:

	/** Timer handle for end screen stall time */
	FTimerHandle TimerHandle_EndScreenStall;
};
