// Copyright of Sock Goblins

#pragma once

#include "EdensGardenSaveGameTypes.h"
#include "GameFramework/SaveGame.h"
#include "EdensGardenSaveGame.generated.h"

/** Save game for a playthrough of Edens Garden. Manages the state of a single level so
back-tracking to previous completed levels is impossible (reverting to old inventories) */
UCLASS()
class EDENSGARDEN_API UEdensGardenSaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UEdensGardenSaveGame();

public:

	/** The version the instance of the save game was made in */
	UPROPERTY(VisibleAnywhere, Category = Version)
	EEdensGardenSaveGameVersion Version;

	/** Is used to record how the active save game should be handled.
	Does not get written to disk, but is used to determine if this save
	game should be updated during normal gameplay or not */
	UPROPERTY(Transient)
	ESaveGamePolicy Policy;

public:

	/** The amount of times this save has been written to disk */
	UPROPERTY()
	int32 TimesSaved;

public:

	/** The name of the level */
	// TODO: Replace with PrimaryAssetID?
	UPROPERTY()
	FName LevelName;

	/** The last checkpoint the player is at. This takes priority
	 over checkpoint name when determining checkpoint to use */
	UPROPERTY()
	int32 CheckpointIndex;

	/** The name of the checkpoint start. This is here as multiple starts might
	might share the same index. Invalid name if no specific checkpoint is required */
	UPROPERTY()
	FName CheckpointName;

public:

	/** The players attributes and inventory */
	UPROPERTY()
	FSpecOpSaveData PlayerData;

	/** All actors that require serialization based on gameplay. This
	will only ever contain actors that implement the SaveGame interface */
	UPROPERTY()
	TArray<FActorSaveData> GameWorldData;

	/** Any actors that need to be destroyed when initializing the level */
	UPROPERTY()
	FDestroyedActorsSaveData DestroyedActorsData;

	/** Additional data saved from the current levels script actor */
	UPROPERTY()
	TArray<uint8> LevelScriptActorData;
};
