// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterAIController.h"
#include "HologramAIController.generated.h"

/** Simple AI controller for the hologram character.
Has a perception system but only utilizes the sight */
UCLASS()
class EDENSGARDEN_API AHologramAIController : public AEGCharacterAIController
{
	GENERATED_BODY()
	
public:

	AHologramAIController();

protected:

	// Begin AEGCharacterAIController Interface
	virtual void NotifyActorFirstSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus) override;
	virtual void NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus) override;
	// End AEGCharacterAIController Interface

public:

	// Begin AAIController Interface
	virtual FVector GetFocalPointOnActor(const AActor* Actor) const override;
	// End AAIController Interface

public:

	/** Get all the monsters within our sight */
	UFUNCTION(BlueprintPure, Category = Hologram)
	const TArray<AEGCharacter*> GetCharactersInSight() const { return CharactersToRespondTo; }

protected:

	/** Get if this character is something this hologram should respond to */
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = Hologram)
	bool CanHologramRespondTo(const AEGCharacter* EGCharacter) const;
	virtual bool CanHologramRespondTo_Implementation(const AEGCharacter* EGCharacter) const;

protected:

	/** All the characters currently within our sight (that we can respond to) */
	UPROPERTY(VisibleInstanceOnly, Transient, Category = Hologram)
	TArray<AEGCharacter*> CharactersToRespondTo;

public:

	/** If the hologram should ignore enemies that are almost dead (1 health or less).
	This is here as holograms might be able to damage enemies but not kill them outright */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hologram)
	uint32 bIgnoreAlmostDeadCharacters : 1;
};
