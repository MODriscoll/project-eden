// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Interactives/InteractiveActor.h"
#include "PowerUpActor.generated.h"

class UGameplayAbility;

/** Simple actor that will give spec op a power up upon iteraction */
UCLASS()
class EDENSGARDEN_API APowerUpActor : public AInteractiveActor
{
	GENERATED_BODY()
	
public:	
	
	APowerUpActor();

public:	

	// Begin IInteractive Interface
	virtual void OnInteractiveGainFocus_Implementation(ASpecOpCharacter* Character);
	virtual void OnInteractiveLoseFocus_Implementation(ASpecOpCharacter* Character);
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const;
	virtual void OnInteract_Implementation(ASpecOpCharacter* Interactor);
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled);
	// End IInteractive Interface

public:

	/** The power to give to the spec op */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PowerUp)
	TSoftClassPtr<UGameplayAbility> PowerAbility;

private:

	/** If we have been picked up */
	UPROPERTY()
	uint8 bHaveBeenPickedUp : 1;
};
