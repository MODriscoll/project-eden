// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameplayCueNotify_Actor.h"
#include "WraithCueNotify.generated.h"

class ASpecOpCharacter;
class AWeaponBase;

class UMeshComponent;

/** Gameplay cue for the wraith power ability (specialized to work witht the spec op).
Will override all materials on the spec op (including weapons) to use set material */
UCLASS()
class EDENSGARDEN_API AWraithCueNotify : public AGameplayCueNotify_Actor
{
	GENERATED_BODY()
	
public:
	
	// Begin AGameplayCueNotify_Actor Interface
	virtual bool OnActive_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters) override;
	virtual bool OnRemove_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters) override;
	// End AGameplayCueNotify_Actor Interface

public:

	/** Get the spec op we are altering */
	UFUNCTION(BlueprintPure, Category = "Wraith")
	ASpecOpCharacter* GetSpecOp() const;

private:

	/** Applies the wraith material to given actor */
	void ApplyWraithToActor(AActor* Actor);

	/** Removes the wraith material and restores original materials for given actor */
	void RestoreActorsMaterials(AActor* Actor);

private:

	/** Sets up the appropriate callbacks */
	void SetUpCallbacks();

	/** Cleans up the set callbacks */
	void CleanUpCallbacks();

	/** Creates new instance of material */
	void CreateMaterialInstance();

	/** Destroys the instanced material */
	void DestroyMaterialInstance();

protected:

	/** The material to override with */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wraith", meta = (DisplayName="Wraith Material"))
	UMaterialInterface* WraithMaterialTemplate;

	/** Dynamic instance of the wraith material */
	UPROPERTY(BlueprintReadOnly, Category = "Wraith")
	UMaterialInstanceDynamic* WraithMaterialInstance;

private:

	/** Weak reference to the spec op we are altering */
	TWeakObjectPtr<ASpecOpCharacter> SpecOp;

	/** Map of actors and their altered meshes */
	//UPROPERTY(Transient, DuplicateTransient)
	TMap<TWeakObjectPtr<AActor>, TArray<UMeshComponent*>> ChangedActors;

	/** Map containing each mesh we have changed with our material */
	//UPROPERTY(Transient, DuplicateTransient)
	TMap<UMeshComponent*, TArray<UMaterialInterface*>> ChangedMeshes;

private:

	/** Callback for when the spec ops weapon inventory has changed */
	UFUNCTION()
	void OnSpecOpInventoryChanged(ASpecOpCharacter* SpecOp, AWeaponBase* Weapon, bool bAdded);

private:

	/** Handle to weapon inventory change delegate */
	FDelegateHandle WeaponInventoryHandle;
};
