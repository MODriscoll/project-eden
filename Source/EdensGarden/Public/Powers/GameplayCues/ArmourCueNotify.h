// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameplayCueNotify_Actor.h"
#include "ArmourCueNotify.generated.h"

class ASpecOpCharacter;

class UArmourAnimInstance;
class USkeletalMeshComponent;

/** Gameplay cue for the armour power that spec op can utilize, re-animates
the players current mesh with a slightly scaled mesh with different materials */
UCLASS()
class EDENSGARDEN_API AArmourCueNotify : public AGameplayCueNotify_Actor
{
	GENERATED_BODY()

public:

	AArmourCueNotify();
	
public:

	// Begin AGameplayCueNotify_Actor Interface
	virtual bool OnActive_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters) override;
	virtual bool OnRemove_Implementation(AActor* MyTarget, const FGameplayCueParameters& Parameters) override;
	// End AGameplayCueNotify_Actor Interface
	
private:

	/** Get armour anim instance from mesh */
	UArmourAnimInstance* GetArmourAnimInstance() const;

	/** Creates new instance of material */
	void CreateMaterialInstance();

	/** Destroys the instanced material */
	void DestroyMaterialInstance();

private:

	/** The skeletal mesh re-animating the spec op */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh;

protected:

	/** The material to override with */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armour", meta = (DisplayName = "Armour Material"))
	UMaterialInterface* ArmourMaterialTemplate;

	/** Dynamic instance of the wraith material */
	UPROPERTY(BlueprintReadOnly, Category = "Armour")
	UMaterialInstanceDynamic* ArmourMaterialInstance;

private:

	/** Spec op who is wearing this armour, is required for callbacks */
	TWeakObjectPtr<ASpecOpCharacter> SpecOp;
};
