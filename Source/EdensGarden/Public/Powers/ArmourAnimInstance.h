// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "ArmourAnimInstance.generated.h"

class AEGCharacter;

/** Anim instance used by the armour power up */
UCLASS()
class EDENSGARDEN_API UArmourAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	/** Set the mesh to copy pose from */
	void SetMeshToCopy(USkeletalMeshComponent* Mesh);

	/** Get the skeletal mesh to copy pose from */
	UFUNCTION(BlueprintPure, Category = Armour, meta = (BlueprintThreadSafe))
	USkeletalMeshComponent* GetSkeletalMeshToCopy() const;
	
protected:

	/** The character to clone */
	UPROPERTY()
	USkeletalMeshComponent* ArmourMesh;
};
