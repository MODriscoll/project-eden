// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InspectorTraceActor.generated.h"

class ASpecOpCharacter;

class USphereComponent;

/** The base for the trace used by the inspector power. Only considers
actors relevant if they implement the GameplayTagsAsset Interface */
UCLASS()
class EDENSGARDEN_API AInspectorTraceActor : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AInspectorTraceActor();

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

private:

	/** Callback for when our collision has been overlapped */
	UFUNCTION()
	void TraceOverlappedActor(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:

	/** The spec op who casted this trace, gotten once at start */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Trace)
	ASpecOpCharacter* SpecOpOwner;

private:

	/** The sphere component used to detect for overlaps */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionSphere;

public:

	/** The max radius of this trace */
	UPROPERTY(EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = Inspector, meta = (ExposeOnSpawn = "true"))
	float TraceRadius;

	/** The time of this trace */
	UPROPERTY(EditAnywhere, AdvancedDisplay, BlueprintReadWrite, Category = Inspector, meta = (ExposeOnSpawn = "true"))
	float TraceTime;

private:

	/** The time we started tracing */
	float TraceStartTime;

private:

	/** Initializes the trace material */
	void InitializeTraceMaterial();

	/** Clears the trace material */
	void ClearTraceMaterial();

public:

	/** The trace material to use */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Inspector, meta = (DisplayName = "Material Template", ExposeOnSpawn = "true"))
	UMaterialInterface* TraceMaterialTemplate;

protected:

	/** Instanced trace material to update */
	UPROPERTY(VisibleInstanceOnly, Category = Inspector, meta = (DisplayName = "Material Instance"))
	UMaterialInstanceDynamic* TraceMaterialInstance;

protected:

	/** Name of the origin parameter in the trace material */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AdvancedDisplay, Category = Inspector, meta = (DisplayName = "Origin Parameter", ExponseOnSpawn = "true"))
	FName TraceMaterialOriginParameter;

	/** Name of the radius parameter in the trace material */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AdvancedDisplay, Category = Inspector, meta = (DisplayName = "Origin Parameter", ExponseOnSpawn = "true"))
	FName TraceMaterialRadiusParameter;
};
