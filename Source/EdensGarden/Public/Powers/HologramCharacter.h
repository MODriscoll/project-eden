// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Characters/EGCharacter.h"
#include "HologramCharacter.generated.h"

/** The base for the hologram character used by the hologram power.
How long this hologram lasts for depends on its life span */
UCLASS(abstract)
class EDENSGARDEN_API AHologramCharacter : public AEGCharacter
{
	GENERATED_BODY()
	
public:

	AHologramCharacter();

public:

	// Begin AEGCharacter Interface
	virtual bool CanBeStunned() const override { return false; }
	// End AEGCharacter Interface

	// Begin APawn Interface
	virtual FVector GetPawnViewLocation() const override; // Move in base character?
	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime) override;
	 // End APawn Interface

	// Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

public:

	/** Called when hologram starts shooting to enter the firing state */
	UFUNCTION(BlueprintCallable, Category = Hologram)
	void EnterFiringState();

	/** Called when hologram stops shooting to exit the firing state */
	UFUNCTION(BlueprintCallable, Category = Hologram)
	void ExitFiringState();

public:

	/** If the hologram is currently firing */
	UFUNCTION(BlueprintPure, Category = Hologram)
	bool IsHologramFiring() const { return bIsInFiringState; }

public:

	/** The yaw inherit rotation speed when firing */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn)
	float InheritYawRotationSpeedWhileFiring;

protected:

	/** If the hologram is in the firing state */
	UPROPERTY(VisibleInstanceOnly, Category = Hologram)
	uint8 bIsInFiringState : 1;

public:

	/** Get weapon mesh subobject */
	FORCEINLINE USkeletalMeshComponent* GetWeaponMesh() const { return WeaponMesh; }

private:

	/** The mesh for the weapon we are holding */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* WeaponMesh;
};
