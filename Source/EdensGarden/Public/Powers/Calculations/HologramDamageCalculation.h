// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "HologramDamageCalculation.generated.h"

/** Damage calculation for the hologram that will prevent damage 
from completely depleting the targets health (will clamp at 1).
Also applies team based damage calculations */
UCLASS()
class EDENSGARDEN_API UHologramDamageCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	UHologramDamageCalculation();

public:
	
	// Begin UGameplayEffectExecutionCalculation Interface
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	// End UGameplayEffectExecutionCalculation Interface
};
