// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "UObject/Object.h"
#include "GameplayTagContainer.h"
#include "EdensGardenGlobals.generated.h"

// TODO: Deprecate

/** Holds all data shared through edens garden */
UCLASS()
class EDENSGARDEN_API UEdensGardenGlobals : public UObject
{
	GENERATED_BODY()

public:

	UEdensGardenGlobals();
	
public:

	/** Get access to global data */
	static UEdensGardenGlobals& Get();

public:

	/** Initializes global data */
	virtual void Initialize();

public:

	/** 
	* The speed the spec op needs to be moving in order
	* for the moving energy regeneration rate to be used.
	* (See SpecOpAttributeSet.cpp) 
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EGGlobals)
	float MovingEnergyRegenThreshold;

public:

	/**
	* The global weapon attack multiplier, is used by weapon damage
	* to scale final calculated damage before applying to target 
	*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = EGGlobals)
	float WeaponDamageMultiplier;	// TODO: Move to gamestate

public:

	/** Gameplay tag for characters falling */
	UPROPERTY()
	FGameplayTag CharacterFallingTag;

	/** Gameplay tag for characters who are stunned */
	UPROPERTY()
	FGameplayTag CharacterStunnedTag;

	/** Gameplay tag for a dead character */
	UPROPERTY()
	FGameplayTag CharacterDeadTag;

public:

	/** Gameplay tag highlighting owner has invincibility */
	UPROPERTY()
	FGameplayTag InvincibilityTag;

protected:

	/** Loads all tags */
	virtual void LoadTags();
};
