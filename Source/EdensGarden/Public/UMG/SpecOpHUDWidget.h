// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Blueprint/UserWidget.h"
#include "GameplayTagContainer.h"
#include "SpecOpHUDWidget.generated.h"

class AGunBase;
class ASpecOpCharacter;
class ASpecOpHUD;

class UFadingProgressBar;
class UGunMasterWidget;
class UInteractiveWidget;
class UProgressBar;
class UVerticalBox;

/** The widget used to display the spec ops HUD. Acts as an interface
for allowing the SpecOpHUD actor to communicate with UMG. Will notify
blueprints of changes to attributes and when certain events have occured */
UCLASS(abstract)
class EDENSGARDEN_API USpecOpHUDWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	USpecOpHUDWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	/** The amount of time to display the energy and breath progress bars before fading them out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD, meta = (ClampMin=0))
	float ProgressShowTime;

	/** The amount of time to fade out the energy and breath progress bars */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD, meta = (ClampMin=0))
	float ProgressFadeTime;

public:

	/** Sets our owner and initializes values to match */
	void SetOwner(ASpecOpHUD* Owner, ASpecOpCharacter* Character);

	UFUNCTION(BlueprintImplementableEvent)
	void DisplayHitMarker();

public:

	/** Notify that subtitles should be displayed on screen */
	void NotifySubtitlesSet(const FText& SubtitlesText);

public:

	/** Notify from owner that health has changed */
	void NotifyHealthChanged(float NewHealth, float Alpha, const FGameplayTagContainer& GameplayTags);

	/** Notify from owner that breath has changed */
	void NotifyBreathChanged(float NewBreath, float Alpha, const FGameplayTagContainer& GameplayTags);

	/** Notify from owner that energy has changed */
	void NotifyEnergyChanged(float NewEnergy, float Alpha, const FGameplayTagContainer& GameplayTags);

	/** Notify from owner that has equipped new gun */
	void NotifyEquippedGunChanged(AGunBase* Gun);

public:

	/** Notify from owner that closest interactive has changed */
	void NotifyInteractiveChanged(AActor* Interactive, const FText& DisplayTitle);

	/** Notify that interaction has begun, but interaction is latent */
	void NotifyInteractionStart();

	/** Notify that iteraction has finished */
	void NotifyInteractionFinished(bool bWasCancelled);

	/** Notify that HUD tag count has changed */
	void NotifyHUDTagCountUpdated(const FGameplayTag& GameplayTag, int32 Count);

	/** Notify that HUD event tags have been recieved */
	void NotifyHUDTagEventRecieved(const FGameplayTagContainer& GameplayTags);

	/** Notify from a ability that it failed to activate */
	void NotifyHUDFailureTagRecieved(const FGameplayTag& GameplayTag);
	
	/** Notify that a new checkpoint has been reached (and same has been saved) */
	void NotifyCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved);

private:

	/** Helper function for calculating and setting the progress of a progress bar */
	void SetBarProgress(UProgressBar* Bar, float Current, float Max, const FString& BarString);

	/** Helper function for calculating and setting the progress of a fading progress bar */
	void SetBarProgressAndFade(UFadingProgressBar* Bar, float Current, float Max, const FString& BarString);

protected:

	/** Event for when subtitles is ready to display on screen */
	UFUNCTION(BlueprintImplementableEvent)
	void OnDisplaySubtitles(const FText& Subtitles);

protected:

	/** Event for when health has changed (for blueprints to inherit) */
	UFUNCTION(BlueprintImplementableEvent)
	void OnHealthChanged(float NewHealth, float Alpha,  const FGameplayTagContainer& GameplayTags);

	/** Event for when breath has changed (for blueprints to inherit) */
	UFUNCTION(BlueprintImplementableEvent)
	void OnBreathChanged(float NewBreath, float Alpha, const FGameplayTagContainer& GameplayTags);

	/** Event for when energy has changed (for blueprints to inherit) */
	UFUNCTION(BlueprintImplementableEvent)
	void OnEnergyChanged(float NewEnergy, float Alpha, const FGameplayTagContainer& GameplayTags);

protected:

	/** Event for when closest interactive has changed (for blueprints to inherit) */
	UFUNCTION(BlueprintImplementableEvent)
	void OnInteractiveChanged(AActor* Interactive, const FText& DisplayTitle);

	/** Event for a new checkpoint has been reached (for blueprints to inherit) */
	UFUNCTION(BlueprintImplementableEvent)
	void OnNewCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved);

protected:

	/** Event for when the count for given tag has changed. These will only ever be tags with root as HUD */
	UFUNCTION(BlueprintImplementableEvent)
	void OnTagCountChanged(const FGameplayTag& GameplayTag, int32 Count);

	/** Event for when a tag event has occured. These are instantaneous so be
	careful of how you use this. These will only ever be tags with root as HUD */
	UFUNCTION(BlueprintImplementableEvent)
	void OnTagEventRecieved(const FGameplayTagContainer& EventTags);

	/** Event for when a tag has been passed from an ability that
	failed to activate. These will only ever be tags with root as HUD */
	UFUNCTION(BlueprintImplementableEvent)
	void OnAbilityFailureTagRecieved(const FGameplayTag& EventTags);

protected:

	/** Reference to the HUD that owns us */
	UPROPERTY(BlueprintReadOnly, Category = HUD)
	ASpecOpHUD* MyOwner;

	/** Reference to the spec op we representing */
	UPROPERTY(BlueprintReadOnly, Category = HUD)
	ASpecOpCharacter* MySpecOp;

protected:

	/** The progress bar widget for health */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* HealthBar;

	/** The progress bar for energy */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UFadingProgressBar* EnergyBar;

	/** The progress bar for breath */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UFadingProgressBar* BreathBar;

private:

	/** Callback for when a progress bar has finished fading out */
	UFUNCTION()
	void OnBarFinishFade(class UFadingWidget* FadingWidget);

protected:

	/** The widget for display for interactives */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UInteractiveWidget* InteractivePrompt;

protected:

	/** The widget used to display info about the currently equipped weapon */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UGunMasterWidget* EquippedGunWidget;
};
