// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameOverWidget.generated.h"

/** Base for the game over menu to display when game is over */
UCLASS()
class EDENSGARDEN_API UGameOverWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	// TODO: Function for restarting at last checkpoint
	// TODO: Function for returning to main menu (check if save is needed)
	
	
};
