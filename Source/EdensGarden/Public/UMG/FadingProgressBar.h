// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "UMG/FadingWidget.h"
#include "Components/ProgressBar.h"
#include "FadingProgressBar.generated.h"

class UProgressBar;

/** A progress bar that is utilized by the fading widget */
UCLASS()
class EDENSGARDEN_API UFadingProgressBar : public UFadingWidget
{
	GENERATED_BODY()
	
public:

	/** Helper function for setting progress percent */
	UFUNCTION(BlueprintCallable)
	void SetPercent(float Percent);

public:

	/** The progress bar to update */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	UProgressBar* ProgressBar;
};
