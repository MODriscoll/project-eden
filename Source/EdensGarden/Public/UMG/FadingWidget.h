// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "FadingWidget.generated.h"

// TODO: Make a slate widget of this? simply doesn't draw child if faded out?
// Would allow us to use curve sequence

/** How the widget is currently fading */
UENUM(BlueprintType)
enum class EFadeMode : uint8
{
	FadeIn,
	FadeOut
};

/** Delegate for when fade out has finished */
DECLARE_DYNAMIC_DELEGATE_OneParam(FFadeFinishedSignature, class UFadingWidget*, FadeWidget);

/** User widget that allows for fades with optional delay */
UCLASS()
class EDENSGARDEN_API UFadingWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UFadingWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin UUserWidget Interface
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);
	// End UUserWidget Interface

public:

	/** Sets the widgets opacity */
	UFUNCTION(BlueprintCallable)
	void SetOpacity(float Alpha);

private:

	/** Resets fading values */
	void ResetFadeValues();

	/** Get the updated alpha based on fade mode. 
	This functions assumes default mode is fade in*/
	float GetUpdatedAlpha(float Alpha) const;

	/** Calculates the opacity to set before applying it */
	void CalculateAndSetOpacity(float StartTime, float CurrentTime, float EndTime);

public:

	/** Starts playing the fade out animation from the beginning. Also 
	provides an optional delay before we should actually start fading */
	UFUNCTION(BlueprintCallable, Category = "Fade Widget")
	void StartFading(float Time, EFadeMode Mode, float Delay = 0.f);

	/** Stops current fade (calls fade finished) */
	UFUNCTION(BlueprintCallable, Category = "Fade Widget")
	void StopFading();

public:

	/** If this widget is currently fading out */
	UFUNCTION(BlueprintPure, Category = "Fade Widget")
	bool IsFading(bool bConsiderDelay = true) const;

	/** The mode we are using for fading out */
	UFUNCTION(BlueprintPure, Category = "Fade Widget")
	EFadeMode GetFadeMode() const { return FadeMode; }
	
protected:

	/** If this widget should fade out */
	UFUNCTION(BlueprintNativeEvent)
	bool ShouldFadeOut() const;
	virtual bool ShouldFadeOut_Implementation() const { return true; }

public:

	/** Event for when widget has finished fading */
	FFadeFinishedSignature OnFadeFinished;

public:

	/** Optional dilation to apply */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Fade)
	float FadeDilation;

private:

	/** The time to fade for */
	float FadeTime;

	/** The time to delay for */
	float FadeDelay;

	/** Elapsed time since we started fading */
	float FadeElapsed;

	/** The time we started fading */
	float FadeStart;

	/** The mode we are using to fade */
	EFadeMode FadeMode;
};
