// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "InteractiveWidget.generated.h"

class ASpecOpCharacter;

class UProgressBar;
class UTextBlock;

/** A widget to be used for displaying a prompt to
the user when the closest interactive has changed */
UCLASS()
class EDENSGARDEN_API UInteractiveWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	/** Notify that the interactive has changed. (By defaults, sets text for title text) */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractiveChanged(AActor* Interactive, const FText& DisplayTitle);
	virtual void OnInteractiveChanged_Implementation(AActor* Interactive, const FText& DisplayTitle);

	/** Event for when interaction has started (duration interaction) */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractionStart();
	virtual void OnInteractionStart_Implementation();

	/** Event for when interaction has finished */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractionFinished(bool bWasCancalled);
	virtual void OnInteractionFinished_Implementation(bool bWasCancalled);

public:

	/** Get the progress for current interaction */
	UFUNCTION(BlueprintPure, Category = Interactive)
	float GetInteractionProgress();

public:

	/** Get the spec op that displays this widget */
	UFUNCTION(BlueprintPure, Category = Interactive)
	ASpecOpCharacter* GetSpecOp() const;

protected:

	/** The text block to modify (optional) */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidgetOptional))
	UTextBlock* TitleText;

	/** The progress bar to use when displaying interaction progress */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidgetOptional))
	UProgressBar* InteractionProgress;

public:

	/** Weak reference to the spec op we work with */
	TWeakObjectPtr<ASpecOpCharacter> SpecOp;
};
