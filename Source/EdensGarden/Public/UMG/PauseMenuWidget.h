// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PauseMenuWidget.generated.h"

/** Base for the pause menu to display when paused */
UCLASS()
class EDENSGARDEN_API UPauseMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/** Helper function for unpausing the game */
	UFUNCTION(BlueprintCallable, Category = Pause)
	void Unpause();
};
