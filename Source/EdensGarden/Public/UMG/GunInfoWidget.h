// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "UMG/FadingWidget.h"
#include "GunInfoWidget.generated.h"

class AAmmoGun;
class AFuelGun;
class AGunBase;
class UImage;

/** Widget that displays the current information for a gun.
Is designed to work with the gun master widget class */
UCLASS(abstract)
class EDENSGARDEN_API UGunInfoWidget : public UUserWidget
{
	GENERATED_BODY()

public:

	/** Initializes this widget with the given gun */
	virtual void InitGunWidget(AGunBase* Gun) { }

protected:

	/** Helper function for telling our owner to start fading out.
	This assumes our owner is the gun master widget class */
	void NotifyOwnerToFadeOut();

public:

	/** The master gun widget we belong to */
	TWeakObjectPtr<class UGunMasterWidget> MasterWidget;
};

/** Specialized gun info widget to work with the Ammo Gun */
UCLASS(abstract)
class EDENSGARDEN_API UAmmoGunInfoWidget : public UGunInfoWidget
{
	GENERATED_BODY()

public:

	// Begin UGunInfoWidget Interface
	virtual void InitGunWidget(AGunBase* Gun) override;
	// End UGunInfoWidget Interface

protected:

	/** Notification for when ammo count has changed */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun)
	void OnAmmoCountUpdated(float ClipCount, float ReserveAmmo);

private:

	/** Callback from ammo gun that its ammo count has changed */
	UFUNCTION()
	void InternalAmmoCountChanged(float ClipCount, float ReserveAmmo);

protected:

	/** The ammo gun we display info about */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gun)
	AAmmoGun* AmmoGun;
};

/** Specialized gun info widget to work with the Fuel Gun */
UCLASS(abstract)
class EDENSGARDEN_API UFuelGunInfoWidget : public UGunInfoWidget
{
	GENERATED_BODY()

public:

	// Begin UGunInfoWidget Interface
	virtual void InitGunWidget(AGunBase* Gun) override;
	// End UGunInfoWidget Interface

protected:

	/** Notification for when fuel count has changed */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun)
	void OnFuelCountUpdated(float FuelCount, float MaxFuelCount);

private:

	/** Callback from ammo gun that its ammo count has changed */
	UFUNCTION()
	void InternalFuelCountChanged(float FuelCount, float MaxFuelCount);

protected:

	/** The fuel gun we display info about */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Gun)
	AFuelGun* FuelGun;
};

/** Master widget for displaying on HUD elements for a gun */
UCLASS(abstract)
class EDENSGARDEN_API UGunMasterWidget : public UFadingWidget
{
	GENERATED_BODY()

public:

	UGunMasterWidget();

public:

	/** Updates the gun this widget represents */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void SetDisplayedGun(AGunBase* Gun);

protected:

	/** Called when a new gun info widget has been created. This should
	attach the new widget to the correct slot in the overall widget */
	UFUNCTION(BlueprintImplementableEvent)
	void AttachGunInfoWidgetToHUD(UGunInfoWidget* InfoWidget);

protected:

	/** The image that acts the current equipped guns icon */
	UPROPERTY(BlueprintReadWrite, meta = (BindWidgetOptional))
	UImage* GunIcon;

	/** The current gun info widget for current equipped gun (Can be null) */
	UPROPERTY(BlueprintReadOnly, Category = HUD)
	UGunInfoWidget* GunInfo;

public:

	/** Starts fading out this widget based on fade properties */
	UFUNCTION(BlueprintCallable, Category = HUD)
	void StartMasterFadeOut();

public:

	/** The amount of time to remain before fading out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Fade)
	float WidgetShowTime;

	/** The amount of time to fade out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Fade)
	float WidgetFadeTime;
};