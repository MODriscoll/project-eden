// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GameplayAbilityTargetTypes.h"
#include "EdensGardenFunctionLibrary.generated.h"

class AEdensGardenGameMode;
class AEdensGardenGameState;
class AMainMenuGameMode;
class UEdensGardenGameInstance;
class UEdensGardenUserSettings;
class UPathFollowingComponent;
class UUserWidget;

struct FHitResult;

/** Header full of helper functions relating to edens garden */
UCLASS()
class EDENSGARDEN_API UEdensGardenFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	/** Get if we are currently running in editor */
	UFUNCTION(BlueprintPure, Category = Development)
	static bool IsWithEditor();

public:

	/** Get edens garden game instance */
	UFUNCTION(BlueprintPure, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static UEdensGardenGameInstance* GetEdensGardenGameInstance(const UObject* WorldContextObject);

	/** Get current game mode as the edens garden game mode */
	UFUNCTION(BlueprintPure, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static AEdensGardenGameMode* GetEdensGardenGameMode(const UObject* WorldContextObject);

	/** Get current game state as the edens garden game state */
	UFUNCTION(BlueprintPure, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static AEdensGardenGameState* GetEdensGardenGameState(const UObject* WorldContextObject);

	/** Get current game mode as the main menu game mode */
	UFUNCTION(BlueprintPure, Category = Menu, meta = (WorldContext = "WorldContextObject"))
	static AMainMenuGameMode* GetMainMenuGameMode(const UObject* WorldContextObject);

public:

	/** Displays the loading screen. If not auto completing, the screen will only be displayed
	for time specified by display time, otherwise hide loading screen must be called instead */
	UFUNCTION(BlueprintCallable, Category = Loading)
	static void DisplayLoadingScreen(bool bAutoComplete, float DisplayTime);

	/** Hides the loading screen. This must be called if previous 
	call to display loading screen was not set to auto complete */
	UFUNCTION(BlueprintCallable, Category = Loading)
	static void HideLoadingScreen();

public:

	/** Notifies the game state that a checkpoint has been reached */
	UFUNCTION(BlueprintCallable, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static void NotifyGameCheckpointReached(const UObject* WorldContextObject, int32 CheckpointIndex, FName CheckpointName);

	/** Notifies the game state that the end of the level has been reached */
	UFUNCTION(BlueprintCallable, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static void NotifyGameLevelEndReached(const UObject* WorldContextObject, FName NextLevelName);

	/** Notifies the game state that the end of the game has been reached */
	UFUNCTION(BlueprintCallable, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static void NotifyGameCampaignEndReached(const UObject* WorldContextObject, FName MenuLevelName);

	/** Get the current checkpoint index and name */
	UFUNCTION(BlueprintPure, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static bool GetCurrentCheckpointDetails(const UObject* WorldContextObject, int32& Index, FName& Name);

public:

	/** Plays a narrative piece for the players spec op character */
	UFUNCTION(BlueprintCallable, Category = Game, meta = (WorldContext = "WorldContextObject"))
	static void PlayNarrativePiece(const UObject* WorldContextObject, const FCharacterSpeechRequest& SpeechRequest);

public:

	/** 
	* Sets the moving threshold that is used to determine
	* if a spec op is considered moving when regenerating energy
	*/
	UFUNCTION(BlueprintCallable, Category = EdensGarden)
	static void SetMovingEnergyRegenThreshold(float Threshold);

	/** Sets the global weapon damage multiplier */
	UFUNCTION(BlueprintCallable, Category = EdensGarden)
	static void SetGlobalWeaponDamageMultiplier(float Multiplier);

	/** Get the threshold for determining if moving energy regen rate should be used */
	UFUNCTION(BlueprintPure, Category = EdensGarden)
	static float GetMovingEnergyRegenThreshold();

	/** Get the global weapon damage multiplier */
	UFUNCTION(BlueprintPure, Category = EdensGarden)
	static float GetGlobalWeaponDamageMultiplier();

public:

	/** Returns the targeting transfrom from a target location info */
	UFUNCTION(BlueprintPure, Category = GameplayAbility)
	static FTransform GetTransformFromLocationInfo(const FGameplayAbilityTargetingLocationInfo& LocationInfo);

	/** Applies a gameplay effect spec to given target data */
	UFUNCTION(BlueprintCallable, Category = GameplayEffect)
	static TArray<FActiveGameplayEffectHandle> ApplyGameplayEffectSpecToTargets(const FGameplayAbilityTargetDataHandle& TargetData, const FGameplayEffectSpecHandle& SpecHandle, UAbilitySystemComponent* SourceASC);

public:

	/** Makes a team based target data filter */
	UFUNCTION(BlueprintPure, Category = TargetData)
	static FGameplayTargetDataFilterHandle MakeTeamBasedFilterHandle(FTeamBasedTargetDataFilter Filter, AActor* FilterActor);

	/** Makes a new target data handle from given hit results */
	UFUNCTION(BlueprintPure, Category = TargetData)
	static FGameplayAbilityTargetDataHandle AbilityTargetDataFromHitResults(const TArray<FHitResult>& HitResults);

	/** Makes a new target data handle from given origin and hit results */
	UFUNCTION(BlueprintPure, Category = TargetData)
	static FGameplayAbilityTargetDataHandle AbilityTargetDataFromOriginAndHitResults(const FVector& Origin, const TArray<FHitResult>& HitResults);

public:

	/** 
	* Performs a sphere trace at given location to check for overlaps. Will then
	* perform a line of sight trace to determine if hit primitives are visible to 
	* trace origin. An actor will only be reported once
	*/
	UFUNCTION(BlueprintCallable, Category = Gameplay, meta = (WorldContext = "WorldContextObject", AutoCreateRefTerm = "IgnoreActors"))
	static bool SphereLineOfSightTrace(const UObject* WorldContextObject, const FVector& Origin, float Radius, ECollisionChannel LOSChannel, const TArray<AActor*>& IgnoreActors, TArray<FHitResult>& HitResults);

public:

	/** Get the destination actor from the path following component */
	UFUNCTION(BlueprintPure, Category = AI, meta = (DisplayName="Get Destination Actor"))
	static AActor* GetPathFollowingComponentsDestinationActor(UPathFollowingComponent* PathFollowingComponent);

public:

	/** 
	* Applies random spread (in degrees) to given direction based on the given
	* velocity only if both min and max velocity are valid (greater than zero).
	* The spread applied is clamped between min and max, with min/max velocity
	* used to calculate the alpha for the spread. Min Spread will be used if
	* velocity length is less than min velocity, vice versa for max spread.
	* If either min or max velocity is invalid, a random spread between
	* min and max spread is applied instead (each axis gets a random spread).
	* This function doesn't check if min is greater than max.
	*/
	UFUNCTION(BlueprintPure, Category = Gameplay)
	static FVector ApplySpreadToDirection(FVector Direction, const FVector& Velocity, float MinSpread, float MaxSpread, float MinVelocity, float MaxVelocity);

public:

	/**
	* Adds widget to viewport only if it isn't currently in the viewport
	*/
	UFUNCTION(BlueprintCallable, Category = UI)
	static bool AddWidgetToViewport(UUserWidget* Widget, int32 ZOrder = 0);

	/**
	* Removes widget from parent only if it is currently in the viewport
	*/
	UFUNCTION(BlueprintCallable, Category = UI)
	static void RemoveWidgetFromParent(UUserWidget* Widget);

public:

	/** Highlights the given actor, only works if game mode is the Edens Garden Game Mode */
	UFUNCTION(BlueprintCallable, Category = Highlight, meta = (WorldContext = "WorldContextObject"))
	static void HighlightActor(const UObject* WorldContextObject, AActor* Actor, FLinearColor Color);

	/** Highlights the given actor for given duration, only works if game mode is the Edens Garden Game Mode */
	UFUNCTION(BlueprintCallable, Category = Highlight, meta = (WorldContext = "WorldContextObject"))
	static void PingActor(const UObject* WorldContextObject, AActor* Actor, EHighlightColor Color, float Duration);

public:

	/** Updates the widget being displayed for the menu (removes current widget), only works if game mode is Main Menu Game Mode */
	UFUNCTION(BlueprintCallable, Category = Menu, meta = (WorldContext = "WorldContextObject"))
	static void SetMainMenuWidget(const UObject* WorldContextObject, TSubclassOf<UUserWidget> Widget, int32 ZOrder = 0);

public:

	/** Get the tags for a gameplay cues from given table */
	UFUNCTION(BlueprintPure, Category = GameplayCue)
	static bool GetGameplayCueTagsFromTable(FGameplayTag Tag, const UDataTable* DataTable, FGameplayTagContainer& OutCueContainer);

	/** Retreives gameplay cues from data table and exeucting them immediately on the given ability system */
	UFUNCTION(BlueprintCallable, Category = GameplayCue)
	static void ExecuteGameplayCuesFromTable(UAbilitySystemComponent* AbilitySystem, FGameplayTag Tag, const UDataTable* DataTable, const FGameplayCueParameters& Parameters);

public:

	/** Get if component is moveable, static or stationary will return false */
	UFUNCTION(BlueprintPure, Category = Components)
	static bool IsComponentMoveable(USceneComponent* SceneComponent);

public:

	/** Get if a gamepad has been attached to current session */
	UFUNCTION(BlueprintPure, Category = Input)
	static bool IsGamepadAttached();

private:

	/** Determines if origin has a clear line of sight to given primitive */
	static bool HasClearLineOfSight(UWorld* World, const FVector& Origin, UPrimitiveComponent* Primitive, ECollisionChannel LOSChannel, FCollisionQueryParams& QParams, FHitResult& Hit);
};
