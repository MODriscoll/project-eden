// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "EdensGardenAssetManager.generated.h"

class AMonsterCharacter;
class AWeaponBase;
class USpecOpInventory;

/** Asset Manager that handles assets for Edens Garden */
UCLASS()
class EDENSGARDEN_API UEdensGardenAssetManager : public UAssetManager
{
	GENERATED_BODY()

public:

	/** Types of actors that should be dynamically spawned in */
	static const FPrimaryAssetType WeaponType;
	static const FPrimaryAssetType MonsterType;

	/** Spec Op Inventory used to generate new saves */
	static const FPrimaryAssetType SpecOpInventoryType;
	
public:

	UEdensGardenAssetManager();
	
public:

	/** Get direct access to asset manager */
	static UEdensGardenAssetManager& Get();

public:

	/** Loads the weapon synchronously */
	TSubclassOf<AWeaponBase> LoadWeapon(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure = true);

	/** Loads the monster synchronously */
	TSubclassOf<AMonsterCharacter> LoadMonster(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure = true);

	/** Loads the spec op inventory synchronously */
	USpecOpInventory* LoadSpecOpInventory(const FPrimaryAssetId& PrimaryAssetId, bool bEnsure = true);
};
