// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/AbilityMacros.h"
#include "GameplayTagContainer.h"
#include "EGAttributeSet.generated.h"

// TODO: replace these with just single (non-dynamic) delegates
DECLARE_DYNAMIC_MULTICAST_DELEGATE_SevenParams(FEGDamagedSignature, float, Damage, bool, bKillingBlow, APawn*, Instigator, AActor*, SourceActor, const FGameplayTagContainer&, SourceTags, bool, bValidHit, const FHitResult&, HitResult);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEGHealthSignature, float, Health, const FGameplayTagContainer&, SourceTags);

/** Base attribute set for all actors that work with the ability system.
Contains all properites that are shared amongst most attributes */
UCLASS()
class EDENSGARDEN_API UEGAttributeSet : public UAttributeSet
{
	GENERATED_BODY()

public:
	
	UEGAttributeSet();

public:

	// Begin UAttributeSet Interface
	virtual bool PreGameplayEffectExecute(FGameplayEffectModCallbackData& Data) override;
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	// End UAttributeSet Interface

public:

	/** Current health of our owner */
	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData Health;
	ATTRIBUTE_ACCESSORS(UEGAttributeSet, Health)

	/** Max health of our owner */
	UPROPERTY(BlueprintReadOnly, Category = "Health")
	FGameplayAttributeData MaxHealth;
	ATTRIBUTE_ACCESSORS(UEGAttributeSet, MaxHealth)

	/** Damage our owner is recieving. Cleared on application */
	UPROPERTY(BlueprintReadOnly, Category = "Damage", meta = (HideFromLevelInfos))
	FGameplayAttributeData Damage;
	ATTRIBUTE_ACCESSORS(UEGAttributeSet, Damage)

	/** Defense multiplier of our owner, this scales incoming damage */
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData DefenseMultiplier;
	ATTRIBUTE_ACCESSORS(UEGAttributeSet, DefenseMultiplier)

public:

	/** Event called when damaged */
	UPROPERTY(BlueprintAssignable, Category = EGAttribute, meta = (HideFromModifiers, HideFromLevelInfos))
	FEGDamagedSignature OnDamaged;

	/** Event called when health has changed (including when damaged) */
	UPROPERTY(BlueprintAssignable, Category = EGAttribute, meta = (HideFromModifiers, HideFromLevelInfos))
	FEGHealthSignature OnHealthChanged;

public:

	/** Get if the owner of this attribute set is invincible */
	bool IsInvincible() const;
};
