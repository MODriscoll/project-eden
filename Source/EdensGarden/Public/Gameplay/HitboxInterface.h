// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "UObject/Interface.h"
#include "GameplayTagContainer.h"
#include "HitboxInterface.generated.h"

UINTERFACE(BlueprintType)
class UHitboxInterface : public UInterface
{
	GENERATED_BODY()
};

/** Interface for actors who hitboxes to activate/de-activate during attacks */
class EDENSGARDEN_API IHitboxInterface
{
	GENERATED_BODY()

public:

	/** Activates the hitboxes, passing along the tag associated 
	with the hitbox activation) and tags for identifying hitboxes */
	UFUNCTION(BlueprintNativeEvent, Category = Gameplay)
	void EnableHitboxes(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags);
	virtual void EnableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) { }

	/** Deactivates the hitboxes. passing along the original associated
	tag (or invalid tag) and hitbox tags used when activated initially */
	UFUNCTION(BlueprintNativeEvent, Category = Gameplay)
	void DisableHitboxes(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags);
	virtual void DisableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) { }
};
