// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "GameplayEffect_DirectDamage.generated.h"

/** Gameplay effect that directly applies damage using simple modifier (no executions)
By default, the damaged is forced, meaning even invicible or immune holders will get hurt */
UCLASS()
class EDENSGARDEN_API UGameplayEffect_DirectDamage : public UGameplayEffect
{
	GENERATED_BODY()
	
public:

	/** The data name for the damage attribute we modify */
	static const FName DamageDataName;

public:

	UGameplayEffect_DirectDamage();		
};
