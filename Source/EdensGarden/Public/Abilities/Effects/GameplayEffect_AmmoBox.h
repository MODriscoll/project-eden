// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffect.h"
#include "GameplayEffect_AmmoBox.generated.h"

/** Gameplay effect used by the ammo box prop to allow setting of the ammo to give */
UCLASS()
class EDENSGARDEN_API UGameplayEffect_AmmoBox : public UGameplayEffect
{
	GENERATED_BODY()
	
public:

	/** The data name for the ammo attribute we modify */
	static const FName AmmoDataName;

public:
	
	UGameplayEffect_AmmoBox();
};
