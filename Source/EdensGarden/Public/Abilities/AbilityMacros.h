#pragma once

#include "AttributeSet.h"
#include "AbilitySystemComponent.h"

// Stripped from 4.20
/**
* This defines a set of helper functions for accessing and initializing attributes, to avoid having to manually write these functions.
* It would creates the following functions, for attribute Health
*
*	static FGameplayAttribute UMyHealthSet::GetHealthAttribute();
*	FORCEINLINE float UMyHealthSet::GetHealth() const;
*	FORCEINLINE void UMyHealthSet::SetHealth(float NewVal);
*	FORCEINLINE void UMyHealthSet::InitHealth(float NewVal);
*
* To use this in your game you can define something like this, and then add game-specific functions as necessary:
*
*	#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
*	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
*	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
*	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
*	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName)
*
*	ATTRIBUTE_ACCESSORS(UMyHealthSet, Health)
*/

#define GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	static FGameplayAttribute Get##PropertyName##Attribute() \
	{ \
		static UProperty* Prop = FindFieldChecked<UProperty>(ClassName::StaticClass(), GET_MEMBER_NAME_CHECKED(ClassName, PropertyName)); \
		return Prop; \
	} \

#define GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	FORCEINLINE float Get##PropertyName() const \
	{ \
		return PropertyName.GetCurrentValue(); \
	} \

#define GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	FORCEINLINE void Set##PropertyName(float NewVal) \
	{ \
		UAbilitySystemComponent* AbilityComp = GetOwningAbilitySystemComponent(); \
		if (ensure(AbilityComp)) \
		{ \
			AbilityComp->SetNumericAttributeBase(Get##PropertyName##Attribute(), NewVal); \
		}; \
	} \

#define GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName) \
	FORCEINLINE void Init##PropertyName(float NewVal) \
	{ \
		PropertyName.SetBaseValue(NewVal); \
		PropertyName.SetCurrentValue(NewVal); \
	} \

#define ATTRIBUTE_ACCESSORS(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_PROPERTY_GETTER(ClassName, PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_GETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_SETTER(PropertyName) \
	GAMEPLAYATTRIBUTE_VALUE_INITTER(PropertyName) 
