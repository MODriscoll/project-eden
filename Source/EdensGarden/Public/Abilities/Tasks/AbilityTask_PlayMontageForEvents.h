// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "Animation/AnimMontage.h"
#include "AbilityTask_PlayMontageForEvents.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayMontageForEventsTaskDelegate, FGameplayTag, EventTag, const FGameplayEventData&, Payload);

/** 
* Task that is a combination between the PlayMontageAndWait and WaitForEvent tasks into one (similar to ActionRPG example).
* Event tag and payload are only valid when OnEvent is called, all others are simply for playing the montage
*/
UCLASS()
class EDENSGARDEN_API UAbilityTask_PlayMontageForEvents : public UAbilityTask
{
	GENERATED_BODY()

public:

	UAbilityTask_PlayMontageForEvents();

public:

	UPROPERTY(BlueprintAssignable)
	FPlayMontageForEventsTaskDelegate OnCompleted;

	UPROPERTY(BlueprintAssignable)
	FPlayMontageForEventsTaskDelegate OnBlendOut;

	UPROPERTY(BlueprintAssignable)
	FPlayMontageForEventsTaskDelegate OnInteruppted;

	UPROPERTY(BlueprintAssignable)
	FPlayMontageForEventsTaskDelegate OnCancelled;

	UPROPERTY(BlueprintAssignable)
	FPlayMontageForEventsTaskDelegate OnEvent;

public:

	/** Callback for when the montage is starting to blend out */
	void OnStartBlendOut(UAnimMontage* Montage, bool bInterrupted);

	/** Callback for when montage has finished playing */
	void OnMontageFinished(UAnimMontage* Montage, bool bInterrupted);

	/** Callback for when our owning ability has been cancelled */
	void OnAbilityCancelled();

	/** Callback for when our owner has recieved a new event */
	void OnGameplayEventRecieved(const FGameplayEventData* Payload);

	/**
	* Plays a montage while listening for events from owner of ability. Only events that match
	* the passed in tag will be reported. All execs besides OnEvent are called based on the montage.
	* The external target is only used to listen for the event, the montage is always played on abilitys avatar
	*/
	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UAbilityTask_PlayMontageForEvents* PlayMontageWhileWaitingForEvents(
		UGameplayAbility* OwningAbility,
		FName TaskInstanceName,
		UAnimMontage* Montage,
		FGameplayTag TagToListenFor,
		bool bOnlyReportOnce = false,
		bool bStopOnAbilityEnd = true,
		float PlayRate = 1.f,
		FName StartSection = NAME_None,
		AActor* OptionalExternalTarget = nullptr,
		float RootMotionTranslationScale = 1.f);

protected:

	// Begin UGameplayTask Interface
	virtual void Activate() override;
	virtual void ExternalCancel() override;
	virtual void OnDestroy(bool bAbilityEnded) override;
	// End UGameplayTask Interface

private:

	/** Sets the external target if valid */
	void SetTargetASC(AActor* Target);

	/** Get the ASC to use based on if we have an external target */
	UAbilitySystemComponent* GetTargetASC() const;

	/** Stops playing the montage (if possible) and removes delegates */
	bool TryStopPlayingMontage();

protected:

	/** Handles for montage based events */
	FOnMontageBlendingOutStarted BlendOutHandle;
	FOnMontageEnded MontageEndHandle;
	FDelegateHandle CancelHandle;

	/** Handle to actual payload events */
	FDelegateHandle EventHandle;

private:

	/** The montage that is playing */
	UPROPERTY()
	UAnimMontage* PlayingMontage;

	/** Event tag to listen out for */
	UPROPERTY()
	FGameplayTag EventTag;

	/** Optional external target to use */
	UPROPERTY()
	UAbilitySystemComponent* ExternalTarget;

	/** If an event should only be reported once */
	UPROPERTY()
	uint8 bOnlyReportOnce : 1;

	/** If the montage should be cancelled when ability ends */
	UPROPERTY()
	uint8 bStopOnAbilityEnd : 1;

	/** Rate at which to play the montage */
	UPROPERTY()
	float PlayRate;

	/** Section of montage to start at */
	UPROPERTY()
	FName StartAtSection;

	/** Modifiers how root motion is applied */
	UPROPERTY()
	float RootMotionTranslationScale;

private:

	/** If we have already reported an event */
	uint8 bHaveReportedEvent : 1;
};
