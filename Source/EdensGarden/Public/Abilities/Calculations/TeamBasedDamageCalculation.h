// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "TeamBasedDamageCalculation.generated.h"

/** A damage calculation that will only apply the damage if the target ASC's owner is not friendly */
UCLASS()
class EDENSGARDEN_API UTeamBasedDamageCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	UTeamBasedDamageCalculation();

public:

	// Begin UGameplayEffectExecutionCalculation Interface
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	// End UGameplayEffectExecutionCalculation Interface
};
