// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Volume.h"
#include "GameplayEffectTypes.h"
#include "EffectVolume.generated.h"

// TODO: move into a more fitting folder?

class UGameplayEffect;

/** Simple volume that will apply a gameplay effect on overlap and remove it on end */
UCLASS()
class EDENSGARDEN_API AEffectVolume : public AVolume
{
	GENERATED_BODY()
	
public:

	AEffectVolume();

public:

	// Begin AActor Interface
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

private:

	/** Attempts to remove the effect from the given actor */
	void RemoveEffectFromActor(AActor* Actor);
	
protected:

	/** Callback for when an actor overlaps this volume */
	UFUNCTION()
	void StartOverlap(AActor* OverlappedActor, AActor* OtherActor);

	/** Callback for when an actor leaves this volume */
	UFUNCTION()
	void EndOverlap(AActor* OverlappedActor, AActor* OtherActor);

public:

	/** The gameplay effect to apply */ // TODO: replace with an array?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effect)
	TSubclassOf<UGameplayEffect> EffectToApply;

	/** If we should remove our effect when overlap ends */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effect)
	uint8 bRemoveEffectOnExit : 1;

private:

	/** All the effects that have been successfully applied */
	UPROPERTY()
	TMap<TWeakObjectPtr<AActor>, FActiveGameplayEffectHandle> ActiveEffects;
};
