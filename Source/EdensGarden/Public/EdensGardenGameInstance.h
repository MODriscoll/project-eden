// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Engine/GameInstance.h"
#include "EdensGardenSaveGame.h"
#include "EdensGardenGameInstance.generated.h"

class ASpecOpCharacter;
class UWorld;

struct FGenericTeamId;

/** Game instance used for Edens Garden. Manages the saving functionality and transition between levels*/
UCLASS(abstract)
class EDENSGARDEN_API UEdensGardenGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:

	UEdensGardenGameInstance();

public:

	// Begin UGameInstance Interface
	virtual void Init() override;
	// End UGameInstance Interface

public:

	/** Creates a new save game, setting it as the active save game. This function
	does not write the current active save game to disk, so any unsaved data will be lost */
	bool CreateSaveGame(const FString& Slot, ESaveGamePolicy Policy = ESaveGamePolicy::AllowSave);

	/** Loads an existing save game from slot, setting it as the active save game. This
	functon does not write the current active save game to disk, so any unsaved data will be lost */
	bool LoadSaveGame(const FString& Slot, ESaveGamePolicy Policy = ESaveGamePolicy::AllowSave);

	/** Loads a potential existing save game from slot. If save game doesn't exist, a new one is created instead */
	bool LoadOrCreateSaveGame(const FString& Slot, ESaveGamePolicy Policy = ESaveGamePolicy::AllowSave);

	/** Writes the active save game to disk. This will do nothing if saving is disabled */
	bool WriteSaveGame(bool bIncrementCount = true);

	/** Clears active save game (from memory only, not disk) */
	void ClearSaveGame();

	/** Deletes save game from disk (optionally from memory as well) */
	void DestroySaveGame(bool bClearMemory = true);

	/** Set if saving is allowed */
	UFUNCTION(BlueprintCallable, Category = Saving)
	void SetSavingEnabled(bool bEnable) { bIsSavingEnabled = bEnable; }

public:

	/** Creates a new persistant save file, deleting the original one
	and overwriting current active slot. This does not transition to game 
	world, only sets active game slot and writing new save to disk.
	Get if operation was successfull */
	UFUNCTION(BlueprintCallable, Category = Saving)
	bool CreateNewPersistantSave(const FName& DefaultLevelName, bool bUseEditorSaveIfPossible = false);

	/** Loads the original persistant save file, overwriting current active slot.
	This does not transition to game world, only sets active game slot.
	Get if operation was successfull */
	UFUNCTION(BlueprintCallable, Category = Saving)
	bool LoadPersistantSave(bool bUseEditorSaveIfPossible = false);

	/** Get if game already exists in persistant save slot */
	UFUNCTION(BlueprintPure, Category = Saving)
	bool DoesPersistantSaveExist(bool bUseEditorSaveIfPossible = false) const;

private:

	/** Get the persistant save slot string to use. If editor slot is true, the editor
	save slot name will be used only in editor, will get game save slot if not with editor */
	FString GetPersistantSaveSlotString(bool bEditorSlot) const;

public:

	/** Get the active save game */
	UEdensGardenSaveGame* GetActiveSameGame() const { return ActiveSaveGame; }

	/** Get if active save game is valid */
	bool IsActiveSaveGameValid() const { return ActiveSaveGame != nullptr; }

private:

	/** Get if active save game should be written to disk */
	bool ShouldWriteSaveGame() const;

public:

	/** Generates new inventory for spec op then overwritting current save data (inventory and attributes) */
	bool GenerateSpecOpSaveData(ASpecOpCharacter* SpecOp);

	/** Overwrites current spec op save data based on given spec op (inventory and attributes) */
	bool SaveSpecOpSaveData(ASpecOpCharacter* SpecOp);

	/** Initializes given spec op with currently saved player data. This should be
	called before load game world save data so picked up weapons re-serialized */
	bool LoadSpecOpSaveData(ASpecOpCharacter* SpecOp);

	/** Overwrites current game world save data based on given world. Only considers actor that implement 
	SaveGame Interface, with ShouldSave returning true. Any save game actors that name is in the destroyed
	actors set is recorded as a destroyed actor rather than a saved game world actor. Reset will remove
	all the current destroyed actors, but keep the destroyed actors being passed in */
	bool SaveGameWorldSaveData(UWorld* World, bool bResetDestroyed, const TArray<FName>* DestroyedActors = nullptr);

	/** Initializes given world with currently saved game world data. Iterates all actors
	of world, destroying required actors and loading saved states of save game actors */
	bool LoadGameWorldSaveData(UWorld* World);

private:

	/** Container for a spec ops inventory */
	struct FSpecOpInventory
	{
		TArray<AGunBase*> Guns;
		int32 GunInventorySize;
		int32 EquippedGunIndex;
		AMeleeBase* MeleeWeapon;
	};

	/** Writes spec ops attributes to active save data */
	void WriteSpecOpAttributesToSaveData(ASpecOpCharacter* SpecOp) const;

	/** Generates new inventory then writing it to active save data */
	bool GenerateAndWriteInventoryToSaveData(UWorld* World) const;

	/** Writes given weapons and powers to active save data */
	void WriteInventoryToSaveData(const FSpecOpInventory& Inventory) const;

	/** Overwrites spec ops attributes with active save data. 
	This should only ever be called at the start of a game */
	void CloneSpecOpAttributes(ASpecOpCharacter* SpecOp) const;

	/** Overwrites spec ops inventory with active save data.
	This should only ever be called at the start of a game */
	void CloneSpecOpInventory(ASpecOpCharacter* SpecOp) const;

private:

	/** The name of the save slot of active save game */
	UPROPERTY()
	FString ActiveSaveSlot;

	/** The active save game begin played (only available during gameplay */
	UPROPERTY()
	UEdensGardenSaveGame* ActiveSaveGame;

	/** If active same game should be saved to disk, can still be updated though */
	UPROPERTY(EditDefaultsOnly, Category = Saving)
	uint8 bIsSavingEnabled : 1;

	/** The default inventory to generate with a new save file */
	UPROPERTY(EditDefaultsOnly, Category = Saving)
	FPrimaryAssetId DefaultSpecOpInventory;

private:

	/** Determines attitude between teams, is used by perception system */
	static ETeamAttitude::Type DetermineTeamAttitude(FGenericTeamId Left, FGenericTeamId Right);	
};
