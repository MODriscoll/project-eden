// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Modules/ModuleInterface.h"

class UEdensGardenGlobals;

/** Primary module for edens garden */
class FEdensGardenModule : public IModuleInterface
{
public:

	FEdensGardenModule() = default;
	virtual ~FEdensGardenModule() = default;

public:

	// Begin IModule Interface
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	virtual bool IsGameModule() const override;
	// End IModule Interface

protected:

	/** Registers gameplay debugger */
	void RegisterGameplayDebugger();

	/** Unregisters gameplay debugger */
	void UnregisterGameplayDebugger();

public:

	/** Get the pointer to edens gardens global data.
	Will generate new data if not initialized */
	UEdensGardenGlobals* GetGlobalData();

private:

	/** Pointer to single instance of global data */
	UEdensGardenGlobals* GlobalData;
};