// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/ProjectileActor.h"
#include "FragProjectile.generated.h"

/** The frag projectile used by the thunderclap weapon */
UCLASS()
class EDENSGARDEN_API AFragProjectile : public AProjectileActor
{
	GENERATED_BODY()
	
public:

	AFragProjectile();

protected:

	// Begin AProjectileActor Interface
	virtual void OnProjectileHitActor_Implementation(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit) override;
	virtual void OnExpired_Implementation() override;
	// End AProjectileActor Interface

public:

	/** Detonates this projectile, applying explosion effect to any
	actors caught in radius. This will also destroy the projectile */
	UFUNCTION(BlueprintCallable, Category = Projectile)
	void Detonate();
	
public:

	/** Event for when this projectile detonates, passes the actors hit by explosion */
	UFUNCTION(BlueprintImplementableEvent)
	void OnDetonate(const FVector& Location, const TArray<FHitResult>& Hits);

public:

	/* If the explosion should also ignore our instigator */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Projectile)
	uint8 bExplosionIgnoresInstigator : 1;

	/** The radius of the explosion for when detonated */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Projectile, meta = (ExposeOnSpawn = true, ClampMin = 0))
	float ExplosionRadius;

	/** The gameplay effect to apply to anything caught with the detonation */
	UPROPERTY(BlueprintReadOnly, Category = Effects, meta = (ExposeOnSpawn = true))
	FGameplayEffectSpecHandle ExplosionEffect;
};
