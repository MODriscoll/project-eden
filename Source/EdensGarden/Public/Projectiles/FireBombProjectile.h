// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/ProjectileActor.h"
#include "FireBombProjectile.generated.h"

class USphereComponent;

/** Fire bomb projectile used by the nanite dissolver. On impact, leaves an area
of effect where an additional effect is applied to all those actors that enter it */
UCLASS()
class EDENSGARDEN_API AFireBombProjectile : public AProjectileActor
{
	GENERATED_BODY()
	
public:

	AFireBombProjectile();

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual bool IsComponentRelevantForNavigation(UActorComponent* Component) const override { return true; }
	// End AActor Interface
	
protected:

	// Begin AProjectileActor Interface
	virtual void OnProjectileHitActor_Implementation(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit) override;
	virtual void OnExpired_Implementation() override;
	// End AFragProjectile Interface

public:

	/** Explodes this fire bomb, leaving the area of effect behind */
	UFUNCTION(BlueprintCallable, Category = FireBomb)
	void Explode();

private:

	/** Finds a suitable location to place the area of effect after exploding
	Will only check for static places, dynamics aren't considered when placing */
	void FindAndMoveToFloor();

protected:

	/** Event for when firebomb explodes. This is called before activating area of effect */
	UFUNCTION(BlueprintImplementableEvent, Category = FireBomb)
	void OnExploded(const FVector& Location, const TArray<FHitResult>& Hits);

	/** Event for when firebombs area of effect has diminished (by default, destroys this projectile) */
	UFUNCTION(BlueprintNativeEvent, Category = FireBomb)
	void OnDiminished();
	virtual void OnDiminished_Implementation() { DestroyProjectile(); }

private:

	/** Event for when fire bomb has diminished  */
	UFUNCTION()
	void OnAreaOfEffectDiminished();

public:

	/** The radius of the fire bombs explosion */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FireBomb, meta = (ExposeOnSpawn = true, ClampMin = 0))
	float ExplosionRadius;

	/** The time the area of effect lasts before diminishing, zero or less means instantly diminish */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = FireBomb, meta = (ExposeOnSpawn = true, ClampMin = 0))
	float AreaOfEffectTime;

	/** The gameplay effect to apply to anything caught with the explosion */
	UPROPERTY(BlueprintReadOnly, Category = Effects, meta = (ExposeOnSpawn = true))
	FGameplayEffectSpecHandle ExplosionEffect;

	/** The gameplay effect to apply to anything that enters the area of effect */
	UPROPERTY(BlueprintReadOnly, Category = Effects, meta = (ExposeOnSpawn = true))
	TSubclassOf<UGameplayEffect> AreaEffect;

protected:

	/** If fire bomb has exploded and is waiting to expire */
	uint32 bIsAreaOfEffectActive : 1;

private:

	/** Timer handle for controlling diminish time */
	FTimerHandle TimerHandle_Diminish;

private:

	/** Removes the area effect from the given actor */	
	void RemoveAreaEffectFrom(AActor* Actor);

	/** Callback for when area of effect is overlapped */
	UFUNCTION()
	void AreaOfEffectStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Callback for when actor leaves area of effect */
	UFUNCTION()
	void AreaOfEffectEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:

	/** The sphere used as collision when still flying */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = true))
	USphereComponent* CollisionSphere;

	/** The area of effect which is active after bomb has exploded */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = true))
	USphereComponent* AreaOfEffect;
};
