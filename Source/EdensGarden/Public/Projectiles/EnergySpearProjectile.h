// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Projectiles/FragProjectile.h"
#include "EnergySpearProjectile.generated.h"

/** The energy spear projectile used by the thunderclap weapon */
UCLASS()
class EDENSGARDEN_API AEnergySpearProjectile : public AFragProjectile
{
	GENERATED_BODY()

public:	
	
	AEnergySpearProjectile();

protected:

	// Begin AProjectileActor Interface
	virtual void OnProjectileOverlappedActor_Implementation(AActor* Actor) override;
	// End AProjectileActor Interface

protected:

	/** Event for when this projectile phases an actor */
	UFUNCTION(BlueprintImplementableEvent)
	void OnPhase(AActor* PhasedActor);

public:

	/** The gameplay effect to apply to anything this projectile phases */
	UPROPERTY(BlueprintReadOnly, Category = Effects, meta = (ExposeOnSpawn = true))
	FGameplayEffectSpecHandle PhaseEffect;
};
