// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "GameplayAbilityTargetTypes.h"
#include "ProjectileActor.generated.h"

class UProjectileMovementComponent;
class USphereComponent;

/** How a projectile responds to certain hit events */
UENUM(meta=(Bitflags))
enum class EProjectileResponse
{
	/** Respond to overlaps */
	Overlap,

	/** Respond to hits */
	Hit
};

/** Base for all projectiles used by abilities. Can respond to overlaps and hits of the actor */
UCLASS(abstract, config=Game)
class EDENSGARDEN_API AProjectileActor : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AProjectileActor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void LifeSpanExpired() override;
	virtual bool IsComponentRelevantForNavigation(UActorComponent* Component) const override { return false; }
	// End AActor Interface

public:

	/** Get projectile movement */
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return MovementComponent; }

protected:

	/** Movement component used to move this projectile */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UProjectileMovementComponent* MovementComponent;

public:

	/** If we are responding to overlaps or hits */
	UPROPERTY(EditDefaultsOnly, Category = Projectile, meta = (Bitmask, BitmaskEnum = "EProjectileResponse"))
	int32 ProjectileResponse;

	/** If our instigator (one who shot this projectile) should be ignored */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Projectile)
	uint8 bIgnoreInstigator : 1;

	/** How long this projectile should live for before expiring. 0 means infinite */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Projectile)
	float ProjectileLifeSpan;

protected:

	/** Event for when this projectile has overlapped something.
	Only gets called if projectile response was flagged with overlap */
	UFUNCTION(BlueprintNativeEvent, Category = Projectile)
	void OnProjectileOverlappedActor(AActor* OverlappedActor);
	virtual void OnProjectileOverlappedActor_Implementation(AActor* Actor) { }

	/** Event for when this projectile has hit something.
	Only gets called if projectile was flagged with hit */
	UFUNCTION(BlueprintNativeEvent, Category = Projectile)
	void OnProjectileHitActor(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit);
	virtual void OnProjectileHitActor_Implementation(AActor* HitActor, const FVector& NormalImpulse, const FHitResult& Hit) { }

	/** Event for when this projectile has expired (life span has run out).
	By default this function will simply destroy the projectile */
	UFUNCTION(BlueprintNativeEvent, Category = Projectile)
	void OnExpired();
	virtual void OnExpired_Implementation();

private:

	/** Callback for when this projectile is overlapped */
	UFUNCTION()
	void OnProjectileOverlap(AActor* OverlappedActor, AActor* OtherActor);

	/** Callback for when this projectile is hit */
	UFUNCTION()
	void OnProjectileHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);

public:

	/** Destroys this projectile, this is the preffered method of destroying
	this actor as we will also notify the ability system of our destruction */
	UFUNCTION(BlueprintCallable, Category = Projectile)
	void DestroyProjectile();

protected:

	/** Helper function for applying a gameplay effect spec to actor */
	void ApplyEffectToActor(AActor* Actor, const FGameplayEffectSpecHandle& Handle) const;
};

