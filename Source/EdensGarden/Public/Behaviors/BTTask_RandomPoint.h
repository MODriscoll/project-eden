// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_RandomPoint.generated.h"

/** Task that will find and set a random point from the
navmesh within a radius from the origin of the test. By default
the owners pawn is used to get the origin, but can be overriden bia blackboard */
UCLASS()
class EDENSGARDEN_API UBTTask_RandomPoint : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_RandomPoint();
	
public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

private:

	/** Helper function for getting the origin of the test */
	FVector GetTestOrigin(const APawn& OwnerPawn, const UBlackboardComponent& OwnerBlackboard) const;

public:

	/** The radius to use when performing the test */
	UPROPERTY(EditAnywhere, Category = Test, meta = (ClampMin=0))
	float Radius;

	/** Navigation query filter to use when searching for point */
	UPROPERTY(EditAnywhere, Category = Test)
	TSubclassOf<UNavigationQueryFilter> QueryFilter;

	/** If the point must be reachable from the origin location */
	UPROPERTY(EditAnywhere, Category = Test)
	uint8 bMustBeReachable : 1;

	/** The key to use for setting the random point */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector RandomPointKey;

	/** The key to use to retrieve the origin (optional) */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector OriginKey;
};
