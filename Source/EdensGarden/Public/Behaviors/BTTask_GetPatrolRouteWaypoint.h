// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_GetPatrolRouteWaypoint.generated.h"

/** Task node that works with the patrol route component. This task
will request the next waypoint and set it in the blackboard */
UCLASS()
class EDENSGARDEN_API UBTTask_GetPatrolRouteWaypoint : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_GetPatrolRouteWaypoint();
	
public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

public:

	/** The key to use to retrieve the patrol route component */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector PatrolRouteComponentKey;

	/** The key to set the waypoint with */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector WaypointKey;
};
