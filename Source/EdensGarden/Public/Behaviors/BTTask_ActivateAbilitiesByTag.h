// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "GameplayAbilityTypes.h"
#include "GameplayTagContainer.h"
#include "BTTask_ActivateAbilitiesByTag.generated.h"

/** Simple blueprint task that will attempt to activate abilities on owners pawn.
Will always fail if no abilities were activated as a result of executing this task,
if at least one was activated, success will be returned if non-blocking, otherwise pending */
UCLASS()
class EDENSGARDEN_API UBTTask_ActivateAbilitiesByTag : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_ActivateAbilitiesByTag();

public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

	// Begin UBTNode Interface
	virtual void DescribeRuntimeValues(const UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, EBTDescriptionVerbosity::Type Verbosity, TArray<FString>& Values) const override;
	// End UBTNode Interface

protected:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type AbortTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

private:

	/** Cleans up this node */
	void CleanUp(UBehaviorTreeComponent& OwnerComp);

public:

	/** The tags to use when trying to activate abilities */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tags)
	FGameplayTagContainer AbilityTags;

	/** If the abilities should just be activated instead of waiting for them to finish */
	UPROPERTY(EditAnywhere, Category = Task)
	uint8 bNonBlocking : 1;

	/** If this task was aborted, should we cancel the activated abilities? */
	UPROPERTY(EditAnywhere, Category = Task)
	uint8 bCancelAbilitiesOnAbort : 1;

private:

	/** Owner of this task */
	UPROPERTY()
	UBehaviorTreeComponent* MyOwner;

	/** Cached ability system we used */
	UPROPERTY()
	UAbilitySystemComponent* CachedASC;

	/** The specs for every activated ability */
	UPROPERTY()
	TSet<FGameplayAbilitySpecHandle> ActivatedSpecs;

private:

	/** Handle to callback */
	FDelegateHandle AbilityFinishedHandle;

private:

	/** Callback for when an ability has finished */
	UFUNCTION()
	void OnAbilityFinished(const FAbilityEndedData& EndedData);
};
