// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTDecorator.h"
#include "GenericTeamAgentInterface.h"
#include "BTDecorator_TeamAttitude.generated.h"

/** Decorator node that checks if owner and actor at
given key have the set team attitude towards each other */
UCLASS()
class EDENSGARDEN_API UBTDecorator_TeamAttitude : public UBTDecorator
{
	GENERATED_BODY()
	
public:

	UBTDecorator_TeamAttitude();

protected:

	// Begin UBTDecorator Interface
	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const;
	// End UBTDecorator Interface
	
public:

	/** The attitude required to pass */
	UPROPERTY(EditAnywhere, Category = Team)
	TEnumAsByte<ETeamAttitude::Type> Attitude;

	/** Key to use for getting actor to compare */
	UPROPERTY(EditAnywhere, Category = Team)
	FBlackboardKeySelector BlackboardKey;
};
