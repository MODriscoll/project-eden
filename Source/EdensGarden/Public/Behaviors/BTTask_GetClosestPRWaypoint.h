// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_GetClosestPRWaypoint.generated.h"

/** Task node that works with the patrol route component. This task
will update the current waypoint to the waypoint closest to the pawn */
UCLASS()
class EDENSGARDEN_API UBTTask_GetClosestPRWaypoint : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_GetClosestPRWaypoint();
	
public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

public:

	/** The key to use to retrieve the patrol route component */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector PatrolRouteComponentKey;

	/** The key to set the waypoint with */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector WaypointKey;
	
};
