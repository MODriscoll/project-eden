// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayAbilitySpec.h"
#include "GameplayAbilityTargetDataFilter.h"
#include "GameplayEffect.h"
#include "GameplayEffectTypes.h"
#include "GameplayTagContainer.h"
#include "Engine/DataTable.h"
#include "Styling/SlateBrush.h"
#include "EdensGardenTypes.generated.h"

class AEGCharacter;
class UGameplayAbility;
class UGameplayEffect;
class UGunInfoWidget;
class USoundBase;

namespace ETeamAttitude
{
	enum Type;
}

/** Name of the input bindings to attach to abilities. These binding names
need to match the names in the actions section of defaultinput.ini */
UENUM(BlueprintType)
enum class EEdensGardenInputID : uint8
{
	/** Roll action */
	Roll,

	/** Sprint action */
	Sprint,

	/** Sneak action */
	Sneak,

	/** Crouch action */
	Crouch,

	/** Interact action */
	Interact,

	/** Torch ability */
	Torch,

	/** Melee ability for melee weapons */
	Melee,

	/** Primary ability for weapons */
	PrimaryAction,

	/** Secondary ability for weapons */
	SecondaryAction,

	/** Reload ability for weapons */
	Reload,

	// Character powers need to remain in order \\

	/** Activate first character ability */
	CharacterPower1,

	/** Activate second character ability */
	CharacterPower2,

	/** Activate third character ability */
	CharacterPower3,

	/** Activate fourth character ability */
	CharacterPower4,

	/** Amount of inputs */
	Count UMETA(Hidden)
};

/** Teams in Edens Garden with priority the enemies of said teams will treat them with */
UENUM(BlueprintType)
enum class EEdensGardenTeamID : uint8
{
	/** Human team (spec op team with lower priority) */
	Human			= 0,

	/** Hologram team (spec op team with higher priority) */
	Hologram		= 1,

	/** Monsters team */
	Monster			= 2,

	/** Amount of teams */
	Count UMETA(Hidden),

	/** Invalid team */
	NoTeam = 255 UMETA(Hidden)
};

/** The state of a weapon when in the players inventory */
UENUM(BlueprintType)
enum class ESwitchGunState : uint8
{
	/** Weapon is equipped can be used */
	Equipped,

	/** Weapon is being equipped and cannot be used by character */
	PendingEquip,

	/** Weapon is holstered, not in use */
	Holstered,

	/** Weapon is being holstered and cannot be used by character */
	PendingHolster
};

/** The state of a shambler */
UENUM(BlueprintType)
enum class EShamblerState : uint8
{
	/** Shambler is roaming around */
	Roam,

	/** Shambler is roaming around but agitated */
	Agitated,

	/** Shambler is investigating source */
	Investigating,

	/** Shambler is hunting its prey */
	Hunting	
};

/** The state of a slug */
UENUM(BlueprintType)
enum class ESlugState : uint8
{
	/** Slug is roaming around */
	Roam,

	/** Slug is investigating source */
	Investigating,

	/** Slug is retreating to safer spot */
	Retreating,

	/** Slug is hunting its prey */
	Hunting
};

/** Abilities to bind to spec op on play */
USTRUCT()
struct EDENSGARDEN_API FSpecOpAbilityInputBinding
{
	GENERATED_BODY()

public:

	/** Ability to bind */
	UPROPERTY(EditAnywhere, Category = Input)
	TSubclassOf<UGameplayAbility> Ability;

	/** Handle to the bound ability */
	FGameplayAbilitySpecHandle Handle;

public:

	/** If this binding is valid */
	bool IsValid() const;
};

/** Container for gameplay effects (wrapped in struct so it can be nested with maps) */
USTRUCT(BlueprintType)
struct FWrappedEffectsContainer
{
	GENERATED_BODY()

public:

	/** Container of effects */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<UGameplayEffect>> GameplayEffects;
};

// TODO: Make UItemAsset then derive it to UPowerItemAsset?
// This struct could then hold the actor instance to the power
/** A power up that the player now carries */
USTRUCT(BlueprintType)
struct FPowerItem
{
	GENERATED_BODY()

public:

	/** The ability this power grants to the player */
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TSubclassOf<UGameplayAbility> PowerAbility;
};

/** Table row for getting a gameplay cue tag based on another tag.
The row name should be the tag of the event being recieved */
USTRUCT(BlueprintType)
struct FGameplayCueTags : public FTableRowBase
{
	GENERATED_BODY()

public:

	/** Tags to use to activate gameplay gues */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tag, meta = (Categories="GameplayCue"))
	FGameplayTagContainer CueTags;
};

/** Target data filter for gameplay abilities that will filter out actors based on team */
USTRUCT(BlueprintType)
struct EDENSGARDEN_API FTeamBasedTargetDataFilter : public FGameplayTargetDataFilter
{
	GENERATED_BODY()

public:

	FTeamBasedTargetDataFilter();
	virtual ~FTeamBasedTargetDataFilter() = default;

public:

	// Begin FGameplayTargetDataFilter Interface
	virtual bool FilterPassesForActor(const AActor* ActorToBeFiltered) const override;
	// End FGameplayTargetDataFilter Interface

private:

	/** Get team attitude between self actor and given actor.
	Will return hostile by default if self actor is null */
	ETeamAttitude::Type GetAttitudeTowards(const AActor* Actor) const;

public:

	/** If friendlies should be filtered out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Filter, meta = (ExposeOnSpawn = true))
	uint8 bFilterOutFriendlies : 1;

	/** If neutrals should be filtered out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Filter, meta = (ExposeOnSpawn = true))
	uint8 bFilterOutNeutrals : 1;

	/** If hostiles should be filtered out */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Filter, meta = (ExposeOnSpawn = true))
	uint8 bFilterOutHostiles : 1;
};

/** Struct containing data for the sound a character is emitting.
Is used to determine if sound should be interuppted or continue playing */
USTRUCT(BlueprintType)
struct FCharacterSpeechRequest
{
	GENERATED_BODY()

public:

	FCharacterSpeechRequest()
	{
		Sound = nullptr;
		Priority = 0;
		bOverride = true;
	}

public:

	/** The sound to play */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USoundBase* Sound;

	/** The priority of this sound (higher value means higher priority).
	Higher values will override any requests with lower priority. Zero
	is the smallest value, anything lower gets clamped to zero */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Priority;

	/** If this request should override the existing request if both have same priority */
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bOverride;
};

/** Widget information for the HUD to use when displaying information for a gun */
USTRUCT(BlueprintType)
struct FGunWidgetData
{
	GENERATED_BODY()

public:

	/** The icon for this gun */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun|HUD")
	FSlateBrush Icon;

	/** The widget to use to display guns attributes */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gun|HUD")
	TSubclassOf<UGunInfoWidget> InfoWidget;
};
