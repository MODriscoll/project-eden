// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/PlayerStart.h"
#include "SaveGameInterface.h"
#include "CheckpointStart.generated.h"

class UBoxComponent;

/** Save data for serializing a DoorActor */
USTRUCT()
struct EDENSGARDEN_API FCheckpointStartSaveData
{
	GENERATED_BODY()

public:

	/** If checkpoint should trigger a save */
	UPROPERTY()
	bool bSaveOnTrigger;

public:

	friend FArchive& operator << (FArchive& Ar, FCheckpointStartSaveData& SaveData)
	{
		Ar << SaveData.bSaveOnTrigger;

		return Ar;
	}
};

/** Checkpoint actor used to locate respawn positions when loading a saved game.
Uses a box volume to trigger the checkpoint update when player enters it. The player
start tag is used when notifying the game state of a checkpoint being reached */
UCLASS()
class EDENSGARDEN_API ACheckpointStart : public APlayerStart, public ISaveGameInterface
{
	GENERATED_BODY()

public:

	/** The player tag used by default, is detected as the first checkpoint of the level */
	static const FName StartingCheckpointName;

public:

	ACheckpointStart(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override { return true; }
	// End ISaveGame Interface

	// Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	// End UObject Interface

public:

	/** The index of this checkpoint. Is used to prevent old checkpoints
	from becoming the latest checkpoint when player enters new area */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Object)
	int32 CheckpointIndex;

public:

	/** Sets if this checkpoint should try to update players checkpoint on trigger */
	UFUNCTION(BlueprintCallable, Category = Checkpoint)
	void SetSaveOnTrigger(bool bEnable);

protected:

	/** If this checkpoint should try to update the players current checkpoint on trigger enter.
	This can be usefull for checkpoints that are triggered by other events rather than a trigger volume */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Object)
	uint8 bSaveOnTrigger : 1;

private:

	/** Callback for when player actor enters this checkpoint volume */
	UFUNCTION()
	void OnCheckpointTriggerOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Notifies game state to update checkpoint to this checkpoint */
	void NotifyGameStateCheckpointReached() const;

private:

	/** Box used as checkpoint trigger */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* CheckpointTrigger;
};
