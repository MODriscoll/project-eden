// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "SpecOpInventory.generated.h"

class AGunBase;
class AMeleeBase;

/** A template used to generate the spec ops inventory when genrating a new save */
UCLASS()
class EDENSGARDEN_API USpecOpInventory : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:

	USpecOpInventory();

public:

	// Begin UObject Interface
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

public:

	/** Guns in inventory */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Guns)
	TArray<TSubclassOf<AGunBase>> Guns;

	/** Size of gun inventory */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Guns, meta = (ClampMin = 1))
	int32 GunInventorySize;

	/** Melee weapon in inventory */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Melee)
	TSubclassOf<AMeleeBase> MeleeWeapon;
};

