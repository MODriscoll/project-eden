// Copyright of Sock Goblins

#pragma once

#include "EdensGardenSaveGameTypes.h"
#include "UObject/Interface.h"
#include "SaveGameInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USaveGameInterface : public UInterface
{
	GENERATED_BODY()
};

/** Interface for actors to inherit from if save game functionality is required */
class EDENSGARDEN_API ISaveGameInterface
{
	GENERATED_BODY()

public:

	/** Called when performing a world save to check if actors state should be saved */
	virtual bool ShouldSaveForSaveGame() const PURE_VIRTUAL(ISaveGameInterface::ShouldSaveForSaveGame, return false;)

	/** How this actor should be handled when loading into a level which it is not a part of. This is
	saved out with all the other data saved about the actor to determine if to spawn it on level load */
	virtual ESaveGameActorSpawnPolicy GetSaveGameSpawnPolicy() const { return ESaveGameActorSpawnPolicy::Static; }

	/** If this actor wants its transform updated to match the transform from the save data.
	This allows certain actors to continue where they left off while others
	will always restart at where they were placed in editor by the level designer */
	virtual bool WantsSaveGameTransformSet() const { return false; }

	/** Called before serialization to notify actor its being saved */
	virtual void PreSaveGameSerialization() { }

	/** Called after serialization to notify actor its has been loaded */
	virtual void PostLoadGameSerialization() { }
};
