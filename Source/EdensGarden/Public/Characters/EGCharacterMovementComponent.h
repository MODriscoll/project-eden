// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "EGCharacterMovementComponent.generated.h"

class AEGCharacter;

/** Base movement component for edens garden characters */
UCLASS()
class EDENSGARDEN_API UEGCharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	
	// Begin UNavMovementComponent Interface
	virtual bool CanStartPathFollowing() const override;
	// End UNavMovementComponent Interface

	// Begin UMovementComponent Interface
	virtual void SetUpdatedComponent(USceneComponent* NewUpdatedComponent) override;
	virtual float GetMaxSpeed() const override;
	// End UMovementComponent Interface

	// Begin UObject Interface
	virtual void PostLoad() override;
	// End UObject Interface

public:

	/** Get EG character owner */
	UFUNCTION(BlueprintPure, Category = "EGCharacter")
	AEGCharacter* GetEGCharacterOwner() const { return EGCharacterOwner; }

protected:

	/** Helper function for applying a modifier to given speed value.
	Simply checks and logs a warning if modifier is nearly zero before applying */
	void ApplyModifierToSpeed_LogIfNearlyZero(float& Speed, float Modifier, const FString& ModifierString) const;

protected:

	/** The EG character who owns this component */
	UPROPERTY(Transient, DuplicateTransient)
	AEGCharacter* EGCharacterOwner;
};
