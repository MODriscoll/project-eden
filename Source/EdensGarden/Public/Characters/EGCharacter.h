// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagAssetInterface.h"
#include "GenericTeamAgentInterface.h"
#include "EGCharacter.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogEGCharacter, Log, All);

class AEGCharacter;

/** Event for attribute meters being changed (attributes could health, energy, etc) */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FCharacterMeterChangedSignature, AEGCharacter*, SpecOp, float, NewValue, float, Delta, const FGameplayTagContainer&, GameplayTags);

/** Event for when character has taken damage */ // TODO: Add hit result?
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FCharacterDamagedSignature, AEGCharacter*, Character, float, Damage, APawn*, DamageInstigator, AActor*, Source, const FGameplayTagContainer&, GameplayTags);

/** Event for when character enters or exits ragdoll */
DECLARE_MULTICAST_DELEGATE_TwoParams(FCharacterRagdolledSignature, AEGCharacter* /*Character*/, bool /*bIsRagdoll*/);

class UAIPerceptionStimuliSourceComponent;
class UAudioComponent;
class UEGCharacterAttributeSet;
class UGameplayAbility;
class UGameplayEffect;

/** Base for all 'characters' featured in Edens Garden. Provides
the base functionality shared amongst these characters */
UCLASS(config=Game, abstract)
class EDENSGARDEN_API AEGCharacter : public ACharacter, public IAbilitySystemInterface, public IGenericTeamAgentInterface, public IGameplayTagAssetInterface
{
	GENERATED_BODY()

public:

	/** Name of ability system subobject */
	static const FName AbilitySystemComponentName;

	/** Name of the attribute set subobject */
	static const FName AttributeSetName;

	/** Default team ID for characters */
	static const FGenericTeamId DefaultTeamId;

public:

	AEGCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin IAbilitySystem Interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const { return AbilitySystem; }
	// End IAbilitySystem Interface

	// Begin ACharacter Interface
	virtual void Falling() override;
	virtual void Landed(const FHitResult& Hit) override;
	// End ACharacter Interface

	// Begin APawn Interface
	virtual void PossessedBy(AController* NewController) override;
	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime) override;
	// End APawn Interface

	// Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaTime) override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	virtual void FellOutOfWorld(const class UDamageType& DamageType) override;
	// End AActor Interface

public:

	/** Rotation speed when inheriting controllers yaw (zero means instant) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Pawn, meta = (UIMin=0.f))
	float InheritYawRotationSpeed;

public:

	#if EG_GAMEPLAY_DEBUGGER
	/** Get formatted debug string for this character */
	virtual void GetDebugString(FString& String) const;
	#endif

public:

	// Begin IGameplayTagAssetInterface
	virtual void GetOwnedGameplayTags(FGameplayTagContainer& TagContainer) const override;
	// End IGameplayTagAssetInterface

public:

	/** ID tags of this character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	FGameplayTagContainer IdentityTags;

protected:

	/** Applies the default gameplay abilities and effects */
	void ApplyDefaultAbilitiesAndEffects();

	/** Removes the default gameplay abilities and effects */
	void RemoveDefaultAbilitiesAndEffects();

private:

	/** The abilities to give this character on creation, these
	abilities could be activated by tags or manually activated */
	UPROPERTY(EditDefaultsOnly, Category = Abilities)
	TArray<TSubclassOf<UGameplayAbility>> PassiveAbilities;

	/** The effects to give this character on creation, these effects
	could be used to initialize attributes or give certain tags */
	UPROPERTY(EditDefaultsOnly, Category = Abilities)
	TArray<TSubclassOf<UGameplayEffect>> PassiveEffects;

public:

	/** Kills this character (even if god mode is enabled) */
	UFUNCTION(BlueprintCallable, Category = Character)
	void Kill();

	/** Enables/Disables god mode for this character. This can be
	useful when a character shouldn't be able to recieve damage */
	UFUNCTION(BlueprintCallable, Category = Character)
	void SetGodEnabled(bool bEnable);

public:

	/** Get this characters health */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetHealth() const;

	/** Get this characters max health */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxHealth() const;

	/** Get this characters general speed modifier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetSpeedModifier() const;

	/** Get this characters defense multiplier */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetDefenseMultiplier() const;

public:

	/** Get characters attributes */
	FORCEINLINE UEGCharacterAttributeSet* GetAttributeSet() const { return AttributeSet; }

public:

	/** Event signature for when character health has changed (called along side damaged and death) */
	UPROPERTY(BlueprintAssignable, Category = Character)
	FCharacterMeterChangedSignature OnCharacterHealthChanged;

	/** Event signature for when character has taken damage but survived */
	UPROPERTY(BlueprintAssignable, Category = Character)
	FCharacterDamagedSignature OnCharacterDamaged;

	/** Event signature for when character has died (health is now zero or less) */
	UPROPERTY(BlueprintAssignable, Category = Character)
	FCharacterDamagedSignature OnCharacterDeath;

protected:

	/** C++ functions for attribute events (called after blueprint functions) */

	virtual void OnHealthChanged_Implementation(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnDamage_Implementation(float Damage, float NewHealth, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags) { }
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags);
	virtual void OnSpeedModifierChanged_Implementation(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags) { }

protected:

	/** Blueprint functions for attribute events (called before C++ functions) */

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnHealthChanged(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnHealthFullyRestored();

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnDamaged(float Damage, float NewHealth, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* SourceActor, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnDeath(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags);

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnSpeedModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

protected:

	virtual void OnInstigatedDamage(AEGCharacter* DamagedCharacter, float Damage, const FGameplayTagContainer& GameplayTags) { }

private:

	/** Allow attribute set to notify us on certain events */
	friend UEGCharacterAttributeSet;

	// TODO: possibly have some check functions here to check

	/** Notify for whenever characters health has changed */
	void NotifyHealthChanged(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Notify for whenever character has been damaged. This will be called before health changed notify */
	void NotifyDamaged(float Damage, float NewHealth, bool bKillingBlow, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags);

	/** Notify that are speed modifier has been changed */
	void NotifySpeedModifierChanged(float NewModifier, float Delta, const FGameplayTagContainer& GameplayTags);

protected:

	/** Called to intialize the defaults for the attributes */
	virtual void InitializeAttributeDefaults();

protected:

	/** The data table to use to initialize attributes */
	UPROPERTY(EditDefaultsOnly, Category = Attributes)
	UDataTable* AttributeDefaults;

private:

	/** Characters abilities */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UAbilitySystemComponent* AbilitySystem;
	
	/** Characters attributes */
	UPROPERTY()
	UEGCharacterAttributeSet* AttributeSet;

public:

	/** Get if this character want's to be notified of damage */
	bool WantsDamageNotification() const { return !bIsDead; }

public:

	/** Get if this character is dead */
	bool IsDead() const { return bIsDead; }

	/** Get if this character is ragdoll */
	bool IsRagdolling() const { return bIsRagdoll; }

	/** Get if this character is stunned */
	bool IsStunned() const { return bIsStunned; }

protected:

	/** If this character is dead */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, Category = Character)
	uint8 bIsDead : 1;

public:

	/** Puts this character into a state of ragdoll. Should really only be used if dead */
	UFUNCTION(BlueprintCallable, Category = Character)
	bool EnterRagdoll();

	/** Puts this character into a state of ragdoll and applies an additional impulse
	to all bones below the specified one. Should really only be used if dead */
	UFUNCTION(BlueprintCallable, Category = Character)
	bool EnterRagdollAndApplyImpulse(FVector Impulse, FName Bone = NAME_None);

	/** Exits ragdoll state if in it */
	UFUNCTION(BlueprintCallable, Category = Character)
	void ExitRagdoll();

protected:

	/** Event for when character enters ragdoll. This is called before entering ragdoll */
	UFUNCTION(BlueprintNativeEvent, Category = Character)
	void OnRagdoll();
	virtual void OnRagdoll_Implementation() { }

	/** Event for when character stops ragdoll. This is called after reverting ragdoll */
	UFUNCTION(BlueprintNativeEvent, Category = Character)
	void OnRagdollStop();
	virtual void OnRagdollStop_Implementation() { }

public:

	/** Event for listeners wanting to know when character enters/exits ragdoll */
	FCharacterRagdolledSignature OnRagdollStateChanged;

protected:

	/** If this character is ragdolling */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	uint8 bIsRagdoll : 1;

private:

	/** The relative transform the mesh had before it was ragdolling.
	Is used when exiting to restore it to the correct position */
	FTransform RelTransformBeforeRagdoll;

protected:

	/** Enables this character movement and movement collision.
	This is required for movement to work correctly while alive */
	UFUNCTION(BlueprintCallable, Category = "Character|Movement")
	void EnableMovementAndCollision();

	/** Disables this characters movement and movement collision.
	This is used for when the character has died so other 
	characters  don't walk into its capsule. */
	UFUNCTION(BlueprintCallable, Category = "Character|Movement")
	void DisableMovementAndCollision();

public:

	// Begin IGenericTeamAgent Interface
	virtual void SetGenericTeamId(const FGenericTeamId& TeamID) override;
	virtual FGenericTeamId GetGenericTeamId() const override;
	// End IGenericTeamAgent Interface

public:

	/** Get the enum team Id of this character */
	FORCEINLINE EEdensGardenTeamID GetTeamId() const { return TeamId; }

private:

	/** 1 frame delay before changing characters team */
	void DelayedTeamChange();

protected:

	/** The team ID for this character (is used for AI) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AI, meta = (ShowOnlyInnerProperties))
	EEdensGardenTeamID TeamId;

public:

	/** Get characters perception source */
	FORCEINLINE UAIPerceptionStimuliSourceComponent* GetPerceptionSource() const { return PerceptionSource; }

private:

	/** Perception stimuli that allows AI to sense this character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	UAIPerceptionStimuliSourceComponent* PerceptionSource;

protected:

	/** Callback for when the characters capsule hits something, simply reports a touch event */
	UFUNCTION()
	void OnCharacterHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	/** Event for the initial hit when the given actor */
	virtual void OnCharacterInitialHit(AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit) { }

private:

	/** These sets are used to prevent touch perception events from being sent every frame */

	/** The actors we have hit this tick */
	UPROPERTY(Transient, DuplicateTransient)
	TSet<AActor*> HitActorsThisTick;

	/** The actors we had hit the previous tick */
	UPROPERTY(Transient, DuplicateTransient)
	TSet<AActor*> HitActorsLastTick;

public:

	/** Stun this character, the power is used as push back from the source */
	UFUNCTION(BlueprintCallable, Category = Character)
	void Stun(const FVector& Source, float Power, FGameplayTag InstigatorTag);

	/** Stuns this character using an external impulse to apply */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StunWithImpulse(const FVector& Source, const FVector& Impulse, FGameplayTag InstigatorTag);

	/** Stop the current stun being applied to the character */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StopStun();

public:

	/** If this character can be stunned */
	UFUNCTION(BlueprintPure, Category = Character)
	virtual bool CanBeStunned() const;

protected:

	/** If this character should be stunned. By default, checks stun immunity tags */
	UFUNCTION(BlueprintNativeEvent, Category = Stun)
	bool ShouldBeStunnedBy(const FGameplayTag& InstigatorTag);
	virtual bool ShouldBeStunnedBy_Implementation(const FGameplayTag& InstigatorTag);

	/** Notify for when character has been stunned (initially, not called on repeat calls to stun) */
	UFUNCTION(BlueprintNativeEvent, Category = Stun)
	void OnCharacterStunned(const FVector& Source, FGameplayTag InstigatorTag);
	virtual void OnCharacterStunned_Implementation(const FVector& Source, FGameplayTag InstigatorTag) { }

	/** Notify for when character has exited stunned state */
	UFUNCTION(BlueprintNativeEvent, Category = Stun)
	void OnCharacterRecoveredFromStun();
	virtual void OnCharacterRecoveredFromStun_Implementation() { }

private:
	
	/** Stuns this character, applying impulse and notifying ability system */
	void Stun(const FVector& Source, const FVector& Impulse, FGameplayTag InstigatorTag);

protected:

	/** If this character is stunned */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, AdvancedDisplay, Category = Character)
	uint8 bIsStunned : 1;

	/** The stun instigator tags that this character is immune to. If
	the character will not be stunned by any events with these tags */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	FGameplayTagContainer StunImmunityTags;

private:

	#if WITH_EDITORONLY_DATA
	/** The time the character was stunned. Is used to warn if we have been stun locked */
	float StunStartTime;
	#endif

protected:

	/** Helper function for executing gameplay cues from cue data table */
	UFUNCTION(BlueprintCallable, Category = Character)
	void ExecuteGameplayCuesFromTable(const FGameplayTag& Tag, const FGameplayCueParameters& Parameters);

protected:

	/** Data table for executing gameplay effects based on gameplay tags */
	UPROPERTY(EditDefaultsOnly, Category = Attributes)
	UDataTable* TaggedGameplayCues;

public:

	/** Plays the speech according to the request. Get if request takes priority */
	UFUNCTION(BlueprintCallable, Category = Character)
	bool Talk(const FCharacterSpeechRequest& SpeechRequest);

	/** Plays the speech according to the request only if not already playing same sound */
	UFUNCTION(BlueprintCallable, Category = Character)
	bool TalkDontRepeat(const FCharacterSpeechRequest& SpeechRequest);

	/** Stops current speech being played. Negative values will cancel any speech, while
	a priority equal to zero or higher must be greater than current priority to cancel */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StopTalking(int32 CancelPriority = -1);

	/** Stops current speech only if given sound is currently playing */
	UFUNCTION(BlueprintCallable, Category = Character)
	void StopTalkingSound(USoundBase* Sound);

private:

	/** Callback for when last speech request has finished */
	UFUNCTION()
	void OnSpeechFinished();

public:

	/** If character voice should be auto cancelled on death */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	uint8 bStopVoiceOnDeath : 1;

private:

	/** The audio component that represents this characters 'voice'. Should be used for
	playing dialogue, voice clips, grunts, basically anything that can be interuppted */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* CharacterVoice;

	/** The priority of the sound currently playing (INDEX_NONE if no sound is playing) */
	int32 CurrentVoicePriority;
};
