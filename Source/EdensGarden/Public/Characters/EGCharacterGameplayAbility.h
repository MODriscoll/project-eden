// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "EGCharacterGameplayAbility.generated.h"

/** 
* Ability class designed for general use in edens garden. Is mostly
* designed to work with the EGCharacter as they use most abilities
*/
UCLASS()
class EDENSGARDEN_API UEGCharacterGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:

	UEGCharacterGameplayAbility();

public:

	// Begin UGameplayAbility Interface
	virtual FGameplayEffectContextHandle MakeEffectContext(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo) const override;
	virtual bool CheckCooldown(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags) const override;
	virtual void PreActivate(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, FOnGameplayAbilityEnded::FDelegate* OnGameplayAbilityEndedDelegate) override;
	virtual void EndAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, bool bReplicateEndAbility, bool bWasCancelled) override;
	// End UGameplayAbility Interface

public:

	/** Called when make a new effect context. Can be used
	to replace the source actor for the effect context */
	UFUNCTION(BlueprintImplementableEvent, Category = Ability)
	AActor* GetAdditionalEffectContextSource() const;

	/** Set the play rate of the current montage section */
	UFUNCTION(BlueprintCallable, Category = Ability)
	void SetMontagePlayRate(float PlayRate = 1.f);

	/** Get the name of the current montages current section */
	UFUNCTION(BlueprintPure, Category = Ability)
	FName GetMontageSectionName(int32 SectionIndex);

public:

	/** Event for when an event has occured with one of the listen tags */
	UFUNCTION(BlueprintNativeEvent)
	void OnTagEvent(const FGameplayTag& Tag, bool bWasAdded);
	virtual void OnTagEvent_Implementation(const FGameplayTag& Tag, bool bWasAdded) { }

protected:

	/** The tags we want to listen to. Will be reported when added or removed.
	Is only reported if instancing policy is NOT set to non-instanced */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Listen)
	FGameplayTagContainer ListenForTags;

private:

	/** Callback for when a tag event has occured. Checks
	if tag is any one we are listening to and reports its */
	void OnGenericTagEvent(const FGameplayTag Tag, int32 Count);

private:

	/** Handle for tag events */
	FDelegateHandle TagEventHandle;

protected:

	/** The optional tag to use if the commit ability cooldown fails */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Cooldowns)
	FGameplayTag CheckCooldownFailTag;
	
	/** The optional tag to use if the commit ability cost fails */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Costs)
	FGameplayTag CheckCostFailTag;
};
