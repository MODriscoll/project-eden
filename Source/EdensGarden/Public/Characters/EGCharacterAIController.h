// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "GameplayTagContainer.h"
#include "Perception/AIPerceptionTypes.h"
#include "EGCharacterAIController.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogEGCharacterAI, Log, All);

DECLARE_STATS_GROUP(TEXT("EGCharacterAI"), STATGROUP_EGCharacterAI, STATCAT_Advanced);

class AEGCharacter;

class UBehaviorTree;
class UBehaviorTreeComponent;

/** Base AI controller for AI characters. Utilizes the perception component 
and behavior trees to run logic. Provides helper functions for handling senses */
UCLASS()
class EDENSGARDEN_API AEGCharacterAIController : public AAIController
{
	GENERATED_BODY()

public:

	/** Name of perception component */
	static const FName PerceptionComponentName;
	
public:

	AEGCharacterAIController(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());
	
public:

	// Begin IGenericTeamAgent Interface
	virtual void SetGenericTeamId(const FGenericTeamId& TeamID);
	virtual FGenericTeamId GetGenericTeamId() const;
	// End IGenericTeamAgent Interface

	// Begin AController Interface
	virtual void Possess(APawn* Pawn) override;
	virtual void UnPossess() override;
	// End AController Interface

	// Begin AActor Interface
	virtual void Tick(float DeltaTime) override;
	// End AActor Interface

protected:

	/** The behavior tree to run on possesion of character */
	UPROPERTY(EditDefaultsOnly, Category = Behavior)
	UBehaviorTree* BehaviorTree;

private:

	/** This characters 'brain' component. Characters use behavior tree for AI logic */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Behavior, meta = (AllowPrivateAccess = "true"))
	UBehaviorTreeComponent* BehaviorTreeComponent;

	/** The character we have possessed */
	TWeakObjectPtr<AEGCharacter> PossessedCharacter;

protected:

	/** Callback for when the perception of an actor is updated */
	UFUNCTION()
	virtual void OnActorsPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus);

protected:

	/** Notify that the given actor has just come into sight */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyActorFirstSight(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyActorFirstSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that the given actor has just exited from sight range */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyActorLostSight(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that the given actor was heard making a sound */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyActorHeard(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyActorHeard_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that hearing stimulus for given actor has expired */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyActorHearingExpired(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyActorHearingExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

protected:

	/** Event for when possessed character has been damaged but survived */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyCharacterDamaged(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);
	virtual void NotifyCharacterDamaged_Implementation(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);

	/** Event for when possessed character has died */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyCharacterDeath(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);
	virtual void NotifyCharacterDeath_Implementation(float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);

private:

	/** Callback for character damage */
	UFUNCTION()
	void OnPossessedCharacterDamaged(AEGCharacter* OurCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);

	/** Callback for character death */
	UFUNCTION()
	void OnPossessedCharacterDeath(AEGCharacter* OurCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);
};
