// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "GameplayTagContainer.h"
#include "EGCharacterAnimInstance.generated.h"

// TODO: If adding IK, put it here
// TODO: rename some stuff

class AEGCharacter;

/** Anim instance containing commonly shared knowledge throughout most characters */
UCLASS()
class EDENSGARDEN_API UEGCharacterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:

	UEGCharacterAnimInstance();

public:
	
	// Begin UAnimInstance Interface
	virtual void NativeUpdateAnimation(float DeltaTime) override;
	// End UAnimInstance Interface

public:

	/** Updates base character variables using given character */
	UFUNCTION(BlueprintCallable, Category = "Character|Animations")
	void UpdateEGCharacter(const AEGCharacter* Character, float DeltaTime);

public:

	/** Calculates the characters speed ratio */
	UFUNCTION(BlueprintPure, Category = "Character|Animations")
	virtual float CalculateSpeedRatio(const AEGCharacter* Character, float DeltaTime);

	/** Calculates the angle character is travelling relative to its rotation */
	UFUNCTION(BlueprintPure, Category = "Character|Animations")
	virtual float CalculateDirectionAngle(const AEGCharacter* Character, float DeltaTime);

	/** Calculates the characters yaw and pitch control offset relative to rotation */
	UFUNCTION(BlueprintCallable, Category = "Character|Animations")
	virtual void CalculateYawPitchOffset(const AEGCharacter* Character, float DeltaTime, float ResetAt = 135.f);

	/** Updates the state flags based on given character */
	UFUNCTION(BlueprintCallable, Category = "Character|Animations")
	virtual void UpdateStateFlags(const AEGCharacter* Character, float DeltaTime);

protected:

	/** Helper function for clamping offset angles */
	UFUNCTION(BlueprintPure, Category = "Character|Animations")
	float ClampOffsetAngle(float Angle, float ClampAngle = 90.f) const;

public:

	/** Max value for the offsets (in degrees) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Offset", meta = (ClampMin = 0, ClampMax = 179))
	float MaxOffsetAngle;

	/** The angle at which to stop applying the offsets (in degrees) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Offset", meta = (ClampMin = 0, ClampMax = 179))
	float ResetOffsetsAngle;

	/** Interpolation speed for matching controller offset */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Offset", meta = (ClampMin = 0))
	float OffsetInterpSpeed;

	/** Interpolation speed for reverting back to no offset */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Offset", meta = (ClampMin = 0))
	float OffsetResetInterpSpeed;

protected:

	/** The characters speed ratio (current speed to max speed) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	float SpeedRatio;

	/** The direction the character is travelling in relation to their rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	float Direction;

	/** The characters controllers yaw offset relative to characters rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	float YawOffset;

	/** The characters controllers pitch offset relative to characters rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	float PitchOffset;

protected:

	/** If this character is currently falling */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsFalling : 1;

	/** If this character is currently crouching */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsCrouching : 1;

	/** If this character is currently dead */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsDead : 1;

	/** If this character is currently ragdolling */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character|State")
	uint32 bIsRagdoll : 1;

protected:

	/** If upper body montages should be blended */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character|Blending")
	uint32 bBlendUpperBodyMontages : 1;

	/** If offsets should be blended (if any) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Character|Blending")
	uint32 bBlendAimOffsets : 1;
};
