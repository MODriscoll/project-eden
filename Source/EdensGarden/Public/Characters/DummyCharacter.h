// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacter.h"
#include "DummyCharacter.generated.h"

/** Dummy character used purely for testing purposes */
UCLASS()
class EDENSGARDEN_API ADummyCharacter : public AEGCharacter
{
	GENERATED_BODY()
	
public:

	ADummyCharacter();
	
protected:

	// Begin AEGCharacter Interface
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& Tags) override;
	// End AEGCharacter Interface

protected:

	/** Restores dummies health and stature */
	void OnRespawn();

public:

	/** This dummies respawn interval */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Respawn, meta=(ClampMin=0.1))
	float RespawnTime;

private:

	/** Timer handle for managing respawn */
	FTimerHandle TimerHandle_Respawn;
};
