// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Gameplay/EGAttributeSet.h"
#include "EGCharacterAttributeSet.generated.h"

/** Defines the attributes shared amongst most characters */
UCLASS()
class EDENSGARDEN_API UEGCharacterAttributeSet : public UEGAttributeSet
{
	GENERATED_BODY()
	
public:

	UEGCharacterAttributeSet();

public:

	// Begin UAttributeSet Interface
	virtual void PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data) override;
	// End UAttributeSet Interface

public:
	
	/** General speed multiplier */
	UPROPERTY(BlueprintReadOnly, Category = "Mobility")
	FGameplayAttributeData SpeedModifier;
	ATTRIBUTE_ACCESSORS(UEGCharacterAttributeSet, SpeedModifier)
};
