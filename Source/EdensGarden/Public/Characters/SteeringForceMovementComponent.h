// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "EGCharacterMovementComponent.h"
#include "SteeringForceMovementComponent.generated.h"

DECLARE_STATS_GROUP(TEXT("SteeringForces"), STATGROUP_SteeringForces, STATCAT_Advanced);

/** All steering forces currently supported */
UENUM(BlueprintType, meta = (Bitflags))
enum class ESteeringForces : uint8
{
	/** Wander around in random directions */
	Wander,

	/** Avoid obstacles in front of me */
	Avoid,

	/** Seek the target */
	Seek,

	/** No steering force (literally does nothing) */
	None UMETA(Hidden)
};

// TODO: Allow adding of weights to forces (so some influence more than others)
// TODO: move to another folder?

/** 
* Character movement component that uses steering forces for 
* custom movement. Is designed to work with the monster characters 
*/
UCLASS()
class EDENSGARDEN_API USteeringForceMovementComponent : public UEGCharacterMovementComponent
{
	GENERATED_BODY()
	
public:

	/** ID for steering force movement */
	static const uint8 SteeringForceMode;

public:

	USteeringForceMovementComponent();

protected:

	// Begin UCharacterMovementComponent Interface
	virtual void PhysCustom(float DeltaTime, int32 Interations) override;
	// End UCharacterMovementComponent Interface

	// Begin UNavMovementComponent Interface
	virtual bool IsMovingOnGround() const override;
	// End UNavMovementComponent Interface

public:

	/** Enables/Disables given steering force */
	UFUNCTION(BlueprintCallable, Category = Steering)
	void SetSteeringForceEnabled(ESteeringForces Force, bool bEnable);

	/** Sets the steering forces to use */
	UFUNCTION(BlueprintCallable, Category = Steering)
	void SetSteeringForces(UPARAM(meta=(Bitmask, BitmaskEnum = "ESteeringForces"))int32 Forces);

	/** Set the seek target to follow, can automatically enabled seeking or disable if null */
	UFUNCTION(BlueprintCallable, Category = Steering)
	void SetSeekTarget(AActor* Actor, bool bEnable = true);

public:

	/** The steering forces that will be calculated and applied to movement */
	UPROPERTY(EditAnywhere, Category = Steering, meta = (DisplayName="Steering Types", Bitmask, BitmaskEnum = "ESteeringForces"))
	int32 SteeringFlags;

private:

	/** Helper function for testing if our flags have the given force */
	inline bool DoFlagsHaveForce(ESteeringForces Force) const
	{
		return (SteeringFlags & (1 << static_cast<int32>(Force))) != 0;
	}

protected:

	/** Wander behavior */

	/** Called to calculate new wander force */
	virtual FVector CalculateWanderForce(float DeltaTime);

public:

	/** Radius of circle to calculate displacement on */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Wander", meta = (ClampMin = 1))
	float WanderAreaRadius;

	/** Distance to wander area is from character */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Wander", meta = (ClampMin = 0))
	float WanderAreaDistance;

	/** Angle at which to update current walking direction */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Wander", meta = (ClampMin = 0))
	float WanderAngle;

	/** The weight wander force has compared to the rest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weights", meta = (UIMin = 0))
	float WanderSteerWeight;

protected:

	/** Previous angle we wandered towards */
	float PrevWanderAngle;

protected:

	/** Avoidance behavior */
	virtual FVector CalculateAvoidForce(float DeltaTime);

public:

	/** The max distance to start avoiding collision */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Avoidance", meta = (UIMin = 0))
	float MaxAvoidDistance;

	/** How much force we should use to move away from obstacles */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Avoidance", meta = (UIMin = 100))
	float MaxAvoidForce;

	/** Profile to use when raycasting */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Avoidance")
	FCollisionProfileName AvoidProfile;

	/** The weight avoidance force has compared to the rest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weights", meta = (UIMin = 0))
	float AvoidSteerWeight;

protected:

	/** Get the velocity of this character, which can be influenced by the floor they are standing on*/
	float GetVelocitySquaredIncludingFloor() const;

protected:

	/** Seek behavior */
	virtual FVector CalculateSeekForce(float DeltaTime);

public:

	/** The target to seek */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Seek")
	AActor* SeekTarget;

	/** If we should project seek force onto the floor we are standing on */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Steering Seek")
	uint32 bProjectSeekForceOntoFloor : 1;

	/** The weight seek force has compared to the rest */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weights", meta = (UIMin = 0))
	float SeekSteerWeight;
};
