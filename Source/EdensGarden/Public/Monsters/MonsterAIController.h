// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterAIController.h"
#include "MonsterAIController.generated.h"

class AMonsterCharacter;

/** Enum for getting if an actor should be targeted over another actor */
UENUM(BlueprintType)
enum class EMonsterTargetPriority : uint8
{
	/** Target has less of a priority */
	LessPriority,

	/** Target has same priority as current target */
	SamePriority,

	/** Target has more of a priority */
	MorePriority
};

/** AI controller specialized to work with the monster character */
UCLASS()
class EDENSGARDEN_API AMonsterAIController : public AEGCharacterAIController
{
	GENERATED_BODY()

public:

	AMonsterAIController();
	
public:

	// Begin AController Interface
	virtual void Possess(APawn* Pawn) override;
	virtual void UnPossess() override;
	// End AController Interface

protected:

	// Begin AEGCharacterAIController Interface
	virtual void OnActorsPerceptionUpdated(AActor* Actor, FAIStimulus Stimulus) override;
	virtual void NotifyActorLostSight_Implementation(AActor* Actor, const FAIStimulus& Stimulus) override;
	virtual void NotifyActorHeard_Implementation(AActor* Actor, const FAIStimulus& Stimulus) override;
	// End AEGCharacterAIController Interface

public:

	/** Get our possessed monster */
	UFUNCTION(BlueprintPure, Category = Monster)
	AMonsterCharacter* GetMonster() const;

protected:

	/** Cached pointer to our monster (for quick and easy access) */
	TWeakObjectPtr<AMonsterCharacter> PossessedMonster;

protected:

	// TODO: maybe have a delegate in monster that will report when its damaged, and we respond to that instead

	/** Notify that the given actor has just damaged the monster */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyDamagedByActor(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyDamagedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that damage stimulus for given actor has expired */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyDamagedByActorExpired(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyDamagedByActorExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that the given actor has come into context with the monster */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyTouchedByActor(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyTouchedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

	/** Notify that touch stimulus for given actor has expired */
	UFUNCTION(BlueprintNativeEvent, Category = AI)
	void NotifyTouchedByActorExpired(AActor* Actor, const FAIStimulus& Stimulus);
	virtual void NotifyTouchedByActorExpired_Implementation(AActor* Actor, const FAIStimulus& Stimulus);

protected:

	/** Called to determine if given target has a higher priority than the other */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = Monster)
	EMonsterTargetPriority GetTargetsPriorityOverOther(const AEGCharacter* Target, const AEGCharacter* Other) const;
	virtual EMonsterTargetPriority GetTargetsPriorityOverOther_Implementation(const AEGCharacter* Target, const AEGCharacter* Other) const;

public:

	/** Updates the current sight target for the monster to focus on */
	UFUNCTION(BlueprintCallable, Category = Monster)
	AActor* UpdateSightTarget();

public:

	/** Get if monster has target in its sight */
	bool HasTargetInSight() const { return SightTarget.IsValid(); }

	/** Get the actor this monster is focusing its sight on */
	UFUNCTION(BlueprintPure, Category = Monster)
	AActor* GetSightTarget() const;

	/** Get the actor this monster is focusing its sight on as a character */
	UFUNCTION(BlueprintPure, Category = Monster)
	AEGCharacter* GetSightTargetAsCharacter() const;

protected:

	/** The actor in the monsters sight it is focusing on */
	TWeakObjectPtr<AActor> SightTarget;

public:

	/** The key to use to set the location for the monster to investigate. For all monsters, 
	it is used to set the location of the last seen actor when they exit sight */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName InvestigateLocationKey;

	/** The key to use for setting the (offset) location we were touched at */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName TouchedAtKey;
};
