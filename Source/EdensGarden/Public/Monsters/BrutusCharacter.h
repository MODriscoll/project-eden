// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Monsters/ShamblerCharacter.h"
#include "BrutusCharacter.generated.h"

/** Base for the brutus monster */
UCLASS()
class EDENSGARDEN_API ABrutusCharacter : public AShamblerCharacter
{
	GENERATED_BODY()
	
public:

	ABrutusCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

protected:

	// Begin AShamblerCharacter Interface
	virtual void OnEnterNewState_Implementation(EShamblerState OldState, EShamblerState NewState) override;
	// End AShamblerCharacter Interface

	// Begin AActor Interface
	virtual void BeginPlay() override;
	// End AActor Interface

	// Begin AEGCharacter Interface
	//virtual bool CanBeStunned() const override { return false; }
	virtual void OnCharacterInitialHit(AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit) override;
	// End AEGCharacter Interface
	
protected:

	// Begin AEGCharacter Interface
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags) override;
	// End AEGCharacter Interface

protected:

	/** Produces a new spore cloud at current location.
	If spore stage multiplier is zero, nothing is spawned */
	UFUNCTION(BlueprintCallable, Category = Brutus)
	void ProduceSpores(bool bScaleBySporeStage = false);

private:

	/** Callback for producing spores (from timer) */
	void OnProduceSpores();

public:

	/** The type of spore cloud to passively drop */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	TSubclassOf<AMonsterEffectVolume> PassiveSporeCloudTemplate;

	/** The duration the passive spore clouds should last before expiring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	float PassiveSporeCloudDuration;

	/** If the passive spore clouds should be scaled by the current stages spore multiplier */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	uint32 bScalePassiveSporeCloudsByStage : 1;

private:

	/** The interval at which spore clouds are produced (lower values are more expensive) */
	UPROPERTY(EditAnywhere, Category = Spores, meta = (ClampMin = 0.1f))
	float PassiveSporeCloudProduceInterval;

	/** Handle for managing the spore cloud production */
	FTimerHandle TimerHandle_ProduceSpores;

protected:

	/** If the brutus should shove its friendlies out of the way on contact
	This will allow brutus to move other shamblers out of the way when hunting */
	uint32 bShoveFriendliesOnContact : 1;
};
