// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Monsters/SlugCharacter.h"
#include "GunkCharacter.generated.h"

/** Base for the gunk monster */
UCLASS()
class EDENSGARDEN_API AGunkCharacter : public ASlugCharacter
{
	GENERATED_BODY()
	
public:

	// Begin AEGCharacter Interface
	virtual bool CanBeStunned() const override { return false; /** For now */ }
	// End AEGCharacter Interface
};
