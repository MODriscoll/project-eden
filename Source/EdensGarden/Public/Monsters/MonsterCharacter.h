// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacter.h"
#include "SaveGameInterface.h"
#include "MonsterCharacter.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogMonster, Log, All);

DECLARE_STATS_GROUP(TEXT("Monster"), STATGROUP_Monster, STATCAT_Advanced);

class AMonsterEffectVolume;

/** Save data for serializing a MonsterCharacter */
USTRUCT()
struct EDENSGARDEN_API FMonsterCharacterSaveData
{
	GENERATED_BODY()

public:

	/** The health of monster */
	UPROPERTY()
	float Health;

public:

	friend FArchive& operator << (FArchive& Ar, FMonsterCharacterSaveData& SaveData)
	{
		Ar << SaveData.Health;

		return Ar;
	}
};


/** Base class used by all monsters featured in Edens Garden */
UCLASS(abstract)
class EDENSGARDEN_API AMonsterCharacter : public AEGCharacter, public ISaveGameInterface
{
	GENERATED_BODY()
	
public:

	AMonsterCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override;
	virtual ESaveGameActorSpawnPolicy GetSaveGameSpawnPolicy() const override { return ESaveGameActorSpawnPolicy::Spawn; }
	// End ISaveGame Interface

	// Begin AEGCharacter Interface
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags) override;
	virtual void OnCharacterStunned_Implementation(const FVector& Source, FGameplayTag InstigatorTag) override;
	virtual void OnCharacterRecoveredFromStun_Implementation() override;
	// End AEGCharacter Interface

	// Begin APawn Interface
	virtual FVector GetPawnViewLocation() const override; // Move to base character?
	// End APawn Interface

	// Begin UObject Interface
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;
	virtual void Serialize(FArchive& Ar) override;
	// End UObject Interface

public:

	/** Name of the socket (on monsters mesh) this monster sees from */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	FName EyesSocket;

private:

	/** If we were orientating to movement before being stunned */
	uint8 bCachedOrientRotationToMovement : 1;
};
