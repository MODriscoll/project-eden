// Copyright of Sock Goblins

#pragma once

#include "EdensGardenTypes.h"
#include "Monsters/MonsterCharacter.h"
#include "Gameplay/HitboxInterface.h"
#include "ShamblerCharacter.generated.h"

class UBoxComponent;
class USphereComponent;
class USteeringForceMovementComponent;

/** Struct containing data of the 'stages' for a shamblers spores */
USTRUCT(BlueprintType)
struct FShamblerSporeStage
{
	GENERATED_BODY()

public:

	FShamblerSporeStage()
	{
		HealthPercentage = 1.f;
		SporesMultiplier = 0.f;
	}

	FShamblerSporeStage(float Percentage, float Multiplier)
	{
		HealthPercentage = Percentage;
		SporesMultiplier = Multiplier;
	}

public:

	/** Percentage of health to report this event */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SporeStage, meta = (UIMin=0, UIMax=1))
	float HealthPercentage;

	/** The multiplier of the original spores. 
	A multiplier of zero means no spores are present */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SporeStage)
	float SporesMultiplier;

	/** Tags to include with report */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SporeStage)
	FGameplayTagContainer ReportTags;

public:

	/** Get if the two values encapsulate this stage */
	bool RangeEncapsulatesStage(float Min, float Max) const
	{
		return Min <= HealthPercentage && HealthPercentage <= Max;
	}

public:

	inline bool operator < (const FShamblerSporeStage& Lhs) const
	{
		return HealthPercentage < Lhs.HealthPercentage;
	}
};

/** Container for stages of a shamblers emmittion spores */
USTRUCT(BlueprintType)
struct FShamblerSporeStagesContainer
{
	GENERATED_BODY()

public:

	/** All stages of shamblers spores */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Spores, meta = (TitleProperty = "HealthPercentage"))
	TArray<FShamblerSporeStage> Stages;

public:

	/** Sorts the container */
	void Sort()
	{
		// We want the the higher health percentages to be at the top
		Stages.Sort([](const FShamblerSporeStage& Lhs, const FShamblerSporeStage& Rhs)->bool
		{
			return !(Lhs < Rhs);
		});
	}

public:

	/** Get if index is valid */
	bool IsValidIndex(int32 Index) const
	{
		return Stages.IsValidIndex(Index);
	}

	/** The number of stages */
	int32 Num() const
	{
		return Stages.Num();
	}

	/** Get state at index */
	FShamblerSporeStage& GetStage(int32 Index) 
	{ 
		check(IsValidIndex(Index));
		return Stages[Index]; 
	}

	/** Const-qualified get */
	const FShamblerSporeStage& GetStage(int32 Index) const 
	{ 
		check(IsValidIndex(Index));
		return Stages[Index];
	}
};

/** Base for the shambler monster */
UCLASS(abstract)
class EDENSGARDEN_API AShamblerCharacter : public AMonsterCharacter, public IHitboxInterface
{
	GENERATED_BODY()

public:

	/** Name of the melee collision shape component */
	static const FName MeleeCollisionName;
	
public:

	AShamblerCharacter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

public:

	/** Get melee collision */
	FORCEINLINE USphereComponent* GetMeleeCollision() const { return MeleeCollision; }

	/** Get steering force component */
	FORCEINLINE USteeringForceMovementComponent* GetSteeringMovement() const { return SteeringMovement; }

private:

	/** The steering force movement component used by shamblers */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USteeringForceMovementComponent* SteeringMovement;

public:

	#if EG_GAMEPLAY_DEBUGGER
	// Begin AEGCharacter Interface
	virtual void GetDebugString(FString& String) const override;
	// End AEGCharacter Interface
	#endif

protected:

	// Begin AEGCharacter Interface
	virtual void OnHealthChanged_Implementation(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags);
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags);
	// End AEGCharacter Interface
	
private:

	/** The area of spores surrounding the shambler */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* SporesVolume;

protected:

	/** Checks if we have entered a new spore stage.
	Will update and report change if change occurs */
	void CheckSporeStage(float NewHealth, float Delta, const FGameplayTagContainer& GameplayTags);

	/** Updates spore volume to match criteria of given stage */
	virtual void UpdateSporeStage(const FShamblerSporeStage& NewStage, int32 Index, bool bIsRevert);

	/** Notify for when a new stage of the spores has been entered */
	UFUNCTION(BlueprintImplementableEvent, Category = Spores)
	void OnNewSporeStagedEntered(const FShamblerSporeStage& NewStage, int32 Index, bool bIsRevert);

protected:

	/** If we should enter the next spore stage. 
	Additional tags are usually tags from taking damage*/
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = Spores)
	bool ShouldEnterNextSporeStage(const FShamblerSporeStage& NewStage, bool bIsRevert, const FGameplayTagContainer& AdditionalTags) const;
	virtual bool ShouldEnterNextSporeStage_Implementation(const FShamblerSporeStage& NewStage, bool bIsRevert, const FGameplayTagContainer& AdditionalTags) const { return true; }

	/** If the shamblers spore volume should 'explode'.
	Additional tags are usually tags from taking damage */
	UFUNCTION(BlueprintPure, BlueprintNativeEvent, Category = Spores)
	bool ShouldSporesExplode(const FGameplayTagContainer& AdditionalTags) const;
	virtual bool ShouldSporesExplode_Implementation(const FGameplayTagContainer& AdditionalTags) const { return true; }

public:

	/** Get the current spore stage of the shambler */
	UFUNCTION(BlueprintPure, Category = Spores)
	FShamblerSporeStage GetCurrentSporeStage() const;

public:

	/** The gameplay effect to apply when a character enters the spores volume */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	TSubclassOf<UGameplayEffect> SporesEffect;

	/**  The stages of the spores volume emmition. Is used to
	determine when to report changes and update spores volume  */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Spores)
	FShamblerSporeStagesContainer SporeStages;

	/** How long the spores last after the shambler has died.
	A value of zero or less means no spore cloud on death */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	float ExplosionCloudLifespan;

protected:

	/** If health is restored, should the spores
	volume revert back to previous stages */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Spores)
	uint8 bCanRevertSporeStages : 1;

	/** Index of the last spore stage we entered */
	int32 LastSporeStageIndex;

protected:

	/** Notify that shambler has died but spores have exploded */
	UFUNCTION(BlueprintImplementableEvent, Category = Spores)
	void OnSporesExplosion();

	/** Notify that the spore cloud has expired, this is when the shambler should be destroyed/deactivated. 
	This funciton is here for allow for cosmetic events but by default, it will simply destroy the shambler */
	UFUNCTION(BlueprintNativeEvent, Category = Spores)
	void OnSporesExpired(bool bAfterExplosion);
	virtual void OnSporesExpired_Implementation(bool bAfterExplosion);

private:

	/** Callback for when spore cloud has expired */
	void OnSporeCloudExpired();

private:

	/** The base scale of spores volume. Using scale instead of extent
	to allow child components to also grow/shrink during different stages */
	FVector OriginalSporeVolumeScale;

	/** Timer handle for tracking the spore explosion cloud lifespan */
	FTimerHandle TimerHandle_SporeCloud;

private:

	/** Callback for when the spores volume is overlapped. Will
	attempt to apply the spores effect to the overlapped actor */
	UFUNCTION()
	void SporeVolumeStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Callback for when an actor leaves the spores volume. Will remove the spores effect from them */
	UFUNCTION()
	void SporeVolumeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

public:

	// Begin IHitbox Interface
	virtual void EnableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) override;
	virtual void DisableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) override;
	// End IHitbox Interface

protected:

	/** Event called on hitbox activiation, should be used to ID tags to determine where to place the hitbox */
	UFUNCTION(BlueprintImplementableEvent, Category = Hitbox)
	void OnHitboxEnabled(const FGameplayTagContainer& HitboxTags);

private:

	/** Callback for when our hitbox has been overlapped */
	UFUNCTION()
	void OnHitboxOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:

	/** The associated tag that was given to us during last hitbox activation.
	This tag is recorded to later send as a gameplay event to our owners ASC on hitbox overlap */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Monster)
	FGameplayTag AttackTag;

private:

	/** The collision used to check for melee overlap */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Hitbox, meta = (AllowPrivateAccess = "true"))
	USphereComponent* MeleeCollision;

	/** The actors we have overlapped in current attack.
	This is to prevent reporting same actor multiple times */
	TSet<TWeakObjectPtr<AActor>> HitActors;

protected:

	/** Allow shambler controller to call events */
	friend class AShamblerAIController;

	/** Notify from our controller that our state has changed */
	void OnShamblerEnterState(EShamblerState OldState, EShamblerState NewState);

	/** Event called when shambler enters new state */
	UFUNCTION(BlueprintNativeEvent, Category = Shambler, meta = (DisplayName = "OnEnterNewState"))
	void OnEnterNewState(EShamblerState OldState, EShamblerState NewState);
	virtual void OnEnterNewState_Implementation(EShamblerState OldState, EShamblerState NewStater) { }

private:

	/** Removes current state effects (if any) and applies new state effects */
	void UpdateStateEffects(EShamblerState State);

protected:

	/** Effects mapped to specific states of the shambler */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shambler)
	TMap<EShamblerState, FWrappedEffectsContainer> StateEffects;

private:

	/** The effects applied during last state change */
	TArray<FActiveGameplayEffectHandle> ActiveStateEffects;
};
