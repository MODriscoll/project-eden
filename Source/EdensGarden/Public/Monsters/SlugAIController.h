// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Monsters/MonsterAIController.h"
#include "SlugAIController.generated.h"

class ASlugCharacter;

/** Base controller for the slug monster */
UCLASS()
class EDENSGARDEN_API ASlugAIController : public AMonsterAIController
{
	GENERATED_BODY()
	
public:

	ASlugAIController();

public:
	
	// Begin AController Interface
	virtual void Possess(APawn* Pawn) override;
	// End AController Interface

public:

	/** Set the slugs state, notifying possessed slug as well */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void SetSlugState(ESlugState NewState);

public:

	/** Get our possessed slug */
	UFUNCTION(BlueprintPure, Category = Slug)
	ASlugCharacter* GetSlug() const;

	/** Get the slugs state */
	UFUNCTION(BlueprintPure, Category = Slug)
	ESlugState GetSlugState() const { return SlugState; }

protected:

	// Begin AEGCharacterAIController
	virtual void NotifyDamagedByActor_Implementation(AActor* Actor, const FAIStimulus& Stimulus) override;
	// End AEGCharacterAIController

public:

	/** The key to use to set the state of the slug */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName SlugStateKey;

	/** The key to use for setting the slugs patrol route */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName PatrolRouteKey;

	/** The key to use to set where the slug was damaged from */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName DamagedFromKey;

private:

	/** The current state of the slug */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Slug, meta = (AllowPrivateAccess = "true"))
	ESlugState SlugState;
};
