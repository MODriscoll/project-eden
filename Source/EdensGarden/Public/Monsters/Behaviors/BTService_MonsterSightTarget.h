// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "BehaviorTree/BTService.h"
#include "BTService_MonsterSightTarget.generated.h"

/** Service to be used by monsters to update their target based on what they see */
UCLASS()
class EDENSGARDEN_API UBTService_MonsterSightTarget : public UBTService
{
	GENERATED_BODY()
	
public:

	UBTService_MonsterSightTarget();

protected:

	// Begin UBTService Interface
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	// End UBTService Interface
	
public:

	/** The blackboard key to use to set the sight target */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector SightTargetKey;
};
