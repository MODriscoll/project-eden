// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_SetShamblerState.generated.h"

/** Simple task that will set the state of a shambler in both
the blackboard and shambler AI Controller, this task will return
success if the blackboard value was set, false otherwise */
UCLASS()
class EDENSGARDEN_API UBTTask_SetShamblerState : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_SetShamblerState();
	
public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

	// Begin UBTNode Interface
	virtual FString GetStaticDescription() const override;
	// End UBTNode Interface

public:

	/** The shambler state to set */
	UPROPERTY(EditAnywhere, Category = Shambler)
	EShamblerState State;
};
