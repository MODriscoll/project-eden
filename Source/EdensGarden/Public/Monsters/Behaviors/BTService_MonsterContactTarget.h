// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_MonsterContactTarget.generated.h"

/** Service to be used by monsters to update the actor who they have come into contact with */
UCLASS()
class EDENSGARDEN_API UBTService_MonsterContactTarget : public UBTService
{
	GENERATED_BODY()
	
public:

	UBTService_MonsterContactTarget();

protected:

	// Begin UBTService Interface
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	// End UBTService Interface

public:

	/** The blackboard key to use to set the (displaced) contact location */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector ContactLocationKey;
};
