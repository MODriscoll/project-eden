// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_MonsterDamageTarget.generated.h"

/** Service to be used by monsters to update their target who damaged them */
UCLASS()
class EDENSGARDEN_API UBTService_MonsterDamageTarget : public UBTService
{
	GENERATED_BODY()
	
public:
	
	UBTService_MonsterDamageTarget();

protected:

	// Begin UBTService Interface
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	// End UBTService Interface

public:

	/** The blackboard key to use to set the origin of the damage */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector DamageLocationKey;
};
