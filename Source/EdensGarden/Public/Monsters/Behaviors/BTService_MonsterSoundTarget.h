// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BTService_MonsterSoundTarget.generated.h"

/** Service to be used by monsters to update the location of the last sound they heard */
UCLASS()
class EDENSGARDEN_API UBTService_MonsterSoundTarget : public UBTService
{
	GENERATED_BODY()

public:
	
	UBTService_MonsterSoundTarget();

protected:

	// Begin UBTService Interface
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	// End UBTService Interface

public:

	/** The blackboard key to use to set sound location */
	UPROPERTY(EditAnywhere, Category = Blackboard)
	FBlackboardKeySelector SoundLocationKey;
};
