// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_AgitateShambler.generated.h"

/** Task that agitates the shambler of owning shambler controller */
UCLASS()
class EDENSGARDEN_API UBTTask_AgitateShambler : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_AgitateShambler();

public:
	
	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

	// Begin UBTNode Interface
	virtual FString GetStaticDescription() const override;
	// End UBTNode Interface

public:

	/** The blackboard value to set with agitated value */
	//UPROPERTY(B)

	/** The amount of time the shambler should be agitated for */
	UPROPERTY(EditAnywhere, Category = Shambler, meta = (ClampMin = 0))
	float AgitateDuration;
};
