// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_SetSlugState.generated.h"

/** Simple task that will set the state of a slug in both
the blackboard and slug AI Controller, this task will return
success if the blackboard value was set, false otherwise */
UCLASS()
class EDENSGARDEN_API UBTTask_SetSlugState : public UBTTaskNode
{
	GENERATED_BODY()
	
public:

	UBTTask_SetSlugState();

public:

	// Begin UBTTaskNode Interface
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	// End UBTTaskNode Interface

	// Begin UBTNode Interface
	virtual FString GetStaticDescription() const override;
	// End UBTNode Interface

public:

	/** The slug state to set */
	UPROPERTY(EditAnywhere, Category = Slug)
	ESlugState State;
};
