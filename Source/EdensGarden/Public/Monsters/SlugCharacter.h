// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Monsters/MonsterCharacter.h"
#include "SlugCharacter.generated.h"

class APatrolRouteActor;
class ASlugCharacter;

class UBoxComponent;
class UGameplayEffect;
class UPatrolRouteComponent;

// TODO: try to use the procedural mesh component for the slime visuals?

/** Base for the slug character */
UCLASS()
class EDENSGARDEN_API ASlugCharacter : public AMonsterCharacter
{
	GENERATED_BODY()
	
public:

	ASlugCharacter();

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	// End AActor Interface
	
	// Begin UObject Interface
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

protected:

	// Begin AEGCharacter Interface
	virtual void OnDeath_Implementation(float Damage, bool bValidHit, const FHitResult& Hit, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags) override;
	// End AEGCharacter Interface

public:

	/** Get the patrol route this slug is following */
	FORCEINLINE UPatrolRouteComponent* GetPatrolRoute() const;

protected:

	/** The patrol route this slug follows */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Slug)
	APatrolRouteActor* PatrolRoute;

public:

	#if EG_GAMEPLAY_DEBUGGER
	// Begin AEGCharacter Interface
	virtual void GetDebugString(FString& String) const override;
	// End AEGCharacter Interface
	#endif

public:

	/** Produces a new passive slime at slugs current location */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = Slug)
	void ProduceSlime();

	/** Produces a new passive slime at given location */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void ProduceSlimeAtLocation(const FVector& Location, const FRotator& Rotation, bool bFindFloor = false);

	/** Produces a new passive slime based off a hit result */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void ProduceSlimeOffHit(const FHitResult& HitResult);

public:

	/** Produces slime of type at slugs current location with slug as instigator */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void ProduceSlimeOfType(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration);

	/** Produces slime of type at given location with slug as instigator */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void ProduceSlimeOfTypeAtLocation(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration, const FVector& Location, const FRotator& Rotation, bool bFindFloor = false);

	/** Produces slime of type based off a hit result with slug as instigator */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void ProduceSlimeOfTypeOffHit(TSubclassOf<AMonsterEffectVolume> SlimeTemplate, TSubclassOf<UGameplayEffect> SlimeEffect, float SlimeDuration, const FHitResult& HitResult);

public:

	/** If this slug can currently drop another slime volume */
	UFUNCTION(BlueprintPure, Category = Slug)
	virtual bool CanDropSlime() const;

protected:

	/** Event for when a new passive slime has been produced */
	UFUNCTION(BlueprintImplementableEvent, Category = Slug)
	void OnPassiveSlimeProduced(AMonsterEffectVolume* SlimeVolume);

private:

	/** Spawns a new slime based off a hit result. If hit (and hit component is static), the
	slime will be produced at hit location, if not, the slime will be produced on the floor */
	void SpawnNewSlimeFromHit(TSubclassOf<AMonsterEffectVolume> Template, TSubclassOf<UGameplayEffect> Effect, float Duration, const FHitResult& HitResult);

	/** Spawns a new slime volume and immediately activates it */
	void SpawnNewSlime(TSubclassOf<AMonsterEffectVolume> Template, TSubclassOf<UGameplayEffect> Effect, float Duration, const FVector& Location, const FRotator& Rotation, bool bFindFloor = true);

protected:

	/** The slime volume this slug passively produces */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Slime)
	TSubclassOf<AMonsterEffectVolume> PassiveSlimeTemplate;

	/** The duration the passive slime should last for before expiring */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slime)
	float PassiveSlimeDuration;

	/** The gameplay effect to give to passive slimes */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Slime)
	TSubclassOf<UGameplayEffect> PassiveSlimeEffect;

public:

	/** Sets the interval for when this slug will drop slime */
	UFUNCTION(BlueprintCallable, Category = Slug)
	void SetSlimeDropInterval(float NewInterval);

private:

	/** Callback for dropping slime (from timer) */
	void OnDropSlime();

	/** Will update and consume the interval for dropping slime */
	void UpdateDropSlimeTimer();

private:

	/** The interval at which the slug drops slime (lower values are more expensive) */
	UPROPERTY(EditAnywhere, Category = Slime, meta = (ClampMin=0.1f))
	float PassiveSlimeDropInterval;

private:

	/** Handle to the drop slime timer */
	FTimerHandle TimerHandle_DropSlime;

	/** The new interval to set when we drop a slime */
	float PendingSlimeDropInterval;

	/** Value if no interval is pending */
	static const float NoSlimeDropInterval;

protected:

	/** Allow slug controller to call events */
	friend class ASlugAIController;

	/** Notify from our controller that our state has changed */
	void OnSlugEnterState(ESlugState OldState, ESlugState NewState);

	/** Event called when slug enters new state */
	UFUNCTION(BlueprintNativeEvent, Category = Slug, meta = (DisplayName = "OnEnterNewState"))
	void OnEnterNewState(ESlugState OldState, ESlugState NewState);
	virtual void OnEnterNewState_Implementation(ESlugState OldState, ESlugState NewStater) { }
};
