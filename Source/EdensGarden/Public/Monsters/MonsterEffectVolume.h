// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "MonsterEffectVolume.generated.h"

class AMonsterCharacter;
class UBoxComponent;

// TODO: Re-incorporate pooling system (making pooling handler a component that is added to base monster?)

/** A simple damage volume that are used by monsters to drop either passively or manually. Simply
applies an effect to any actor who enters the volume, and only lasts for a set amount of time */
UCLASS()
class EDENSGARDEN_API AMonsterEffectVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	
	AMonsterEffectVolume();

public:

	/** Spawns a new monster effect volume at given transform with given monster as instigator.
	The volume will then be immediately activated with given effect and duration */
	UFUNCTION(BlueprintCallable, Category = Monster)
	static AMonsterEffectVolume* SpawnMonsterEffectVolume(
		AMonsterCharacter* Monster,
		TSubclassOf<AMonsterEffectVolume> Volume,
		const FVector& Location,
		const FRotator& Rotation,
		TSubclassOf<UGameplayEffect> Effect,
		float Duration);

public:
	
	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	virtual void SetActorHiddenInGame(bool bNewHidden) override;
	virtual void LifeSpanExpired() override;
	// End AActor Interface

public:

	/** Activates this volume for the set duration */
	void ActivateVolume(const FGameplayEffectSpecHandle& EffectHandle, float Duration);

public:

	/** Get if this volume is currently active */
	bool IsVolumeActive() const { return bVolumeActive; }

protected:

	/** Event for when this volume is activated */
	UFUNCTION(BlueprintNativeEvent, Category = Monster)
	void OnVolumeActivated();
	virtual void OnVolumeActivated_Implementation() { }

	/** Event for when this volume has expired, by default, this function calls destroy */
	UFUNCTION(BlueprintNativeEvent, Category = Slime)
	void OnVolumeExpired();
	virtual void OnVolumeExpired_Implementation() { Destroy(); }

private:

	/** Notifies this volume it has expired and should no longer apply effects */
	void ExpireVolume();

	/** Removes the effect from the given actor */
	void RemoveEffectFromActor(AActor* Actor);

	/** Removes the effect from all actors currently overlapping volume */
	void RemoveAllEffects();

protected:

	/** If this volume is active and applying effects */
	uint8 bVolumeActive : 1;

	/** The effect to apply to actors who enter this volume */
	FGameplayEffectSpecHandle VolumeEffectHandle;

private:

	/** Callback for when actor overlaps effect volume */
	UFUNCTION()
	void VolumeStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Callback for when actor leaves effect volume */
	UFUNCTION()
	void VolumeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

private:

	/** The volume to detect which actors should be affected */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* EffectVolume;
};
