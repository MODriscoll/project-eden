// Copyright of Sock Goblins

#pragma once

#include "EdensGardenTypes.h"
#include "Monsters/MonsterAIController.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "ShamblerAIController.generated.h"

class AShamblerCharacter;

/** Base controller for the shambler monster */
UCLASS()
class EDENSGARDEN_API AShamblerAIController : public AMonsterAIController
{
	GENERATED_BODY()
	
public:

	AShamblerAIController();

public:

	// Begin AController Interface
	virtual void Possess(APawn* Pawn) override;
	// End AController Interface

public:

	/** Set the shamblers state, notifying possessed shambler as well */
	UFUNCTION(BlueprintCallable, Category = Shambler)
	void SetShamblerState(EShamblerState NewState);

	/** Agitates the shambler for given amount of time before setting state to roam.
	Timer is cleared whenever SetShamblerState is called (even if state is Agitated!) */
	UFUNCTION(BlueprintCallable, Category = Shambler)
	void AgitateShambler(float Duration);

private:

	/** Sets shamblers state while optionally ignoring clearing the agitate timer */
	void SetShamblerState(EShamblerState NewState, bool bClearAgitateTimer);

	/** Sets shamblers state to roam after being agitated for certain duration */
	void OnAgitatedFinished();

public:

	/** Get our possessed shambler */
	UFUNCTION(BlueprintPure, Category = Shambler)
	AShamblerCharacter* GetShambler() const;

	/** Get this shamblers state */
	UFUNCTION(BlueprintPure, Category = Shambler)
	EShamblerState GetShamblerState() const { return ShamblerState; }

public:

	/** The key to use to set the state of the shambler */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Blackboard)
	FName ShamblerStateKey;

private:

	/** The current state of the shambler */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Shambler, meta = (AllowPrivateAccess = "true"))
	EShamblerState ShamblerState;

	/** Timer handle for entering roam state when agitated */
	FTimerHandle TimerHandle_Agitated;
};
