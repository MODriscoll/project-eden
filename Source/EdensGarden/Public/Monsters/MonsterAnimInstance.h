// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Characters/EGCharacterAnimInstance.h"
#include "MonsterAnimInstance.generated.h"

class AMonsterCharacter;

/** Anim instance for monsters that contians functionality shared by most monsters */
UCLASS()
class EDENSGARDEN_API UMonsterAnimInstance : public UEGCharacterAnimInstance
{
	GENERATED_BODY()
	
public:

	UMonsterAnimInstance();
	
public:

	//Begin UAnimInstance Interface
	virtual void NativeUpdateAnimation(float DeltaTime) override;
	// End UEGCharacterAnimInstance Interface

public:

	/** Calculates the amount to rotate the monsters spine by to be facing its target
	This is to allow for monsters to attack an enemy either below or above them */
	UFUNCTION(BlueprintPure, Category = "Monster|Animations")
	virtual FRotator CalculateSpineRotation(const AMonsterCharacter* Monster, float DeltaTime);

public:

	/** The range the monster must be in before applying spine rotations */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Monster", meta = (UIMin = 0))
	float SpineDistanceRange;

	/** The degress threshold for determining if spine rotation should be applied,
	the monsters target must be within this threshold before any rotation is applied */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Monster", meta = (ClampMin = 0))
	float SpineFOVRange;

	/** Interpolation speed to spine rotation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aesthetics|Monster")
	float SpineInterpSpeed;

protected:

	/** The rotation to apply to monsters spine to face either up or down */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Character)
	FRotator SpineRotation;
};
