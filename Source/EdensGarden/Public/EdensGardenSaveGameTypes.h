// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Serialization/ObjectAndNameAsStringProxyArchive.h"
#include "EdensGardenSaveGameTypes.generated.h"

class AActor;
class AGunBase;
class AMeleeBase;
class AWeaponBase;

/** The version of this save game. Is updated along side
the save game implementation to support older versions */
UENUM()
enum class EEdensGardenSaveGameVersion : uint8
{
	/** Initial implementation */
	Initial,

	/** Actors must request their transform be set when loading
	the game world. See ISaveGame::WantsSaveGameTransformSet */
	RequestSettingTransform,

	/** Name of the current checkpoint is recorded alongside index */
	CheckpointNameSaved,

	/** Level script actors are now supported for save game */
	LevelScriptSupport,

	// ----- Add New Versions Before This Line ----- \\
	
	/** Total amount of versions */
	Count,

	/** The current version implemented */
	CurrentVersion = Count - 1
};

/** The policy for saving the active save game. This is used
to check if save game should ultimately be written to disk */
UENUM()
enum class ESaveGamePolicy : uint8
{
	/** Can be updated and written to disk */
	AllowSave,

	/** Active save can be updated, but not written to disk */
	OnlyUpdate,

	/** Do not update or write this save to disk */
	NoSave
};

/** The policy to use when handling an unaccounted for saved actor in a level load */
UENUM()
enum class ESaveGameActorSpawnPolicy : uint8
{
	/** Actor should be left as is, no spawning or destroying */
	Static,

	/** Will spawn a new instance if instance does not exist already */
	Spawn

	/** Destroys instance if it exists, does not spawn new one if not */
	//Destroy
};

/** Archive used by Edens Garden to serialize save game data */
struct FEdensGardenArchive : public FObjectAndNameAsStringProxyArchive
{
	FEdensGardenArchive(FArchive& InInnerArchive)
		: FObjectAndNameAsStringProxyArchive(InInnerArchive, true)
	{
		ArIsSaveGame = true;
	}
};

/* Save data for any actor that implements save game interface */
USTRUCT()
struct EDENSGARDEN_API FActorSaveData
{
	GENERATED_BODY()

public:

	/** Name of the actor */
	UPROPERTY()
	FName Name;

	/** Transform of the actor, is only applied when spawning a new
	instance or save game actors root component mobility is movable */
	UPROPERTY()
	FTransform Transform;

	/** Class of the actor, required for verification and spawning */
	UPROPERTY()
	TSubclassOf<AActor> Class;

	/** Additional data actor needed to serialize */
	UPROPERTY()
	TArray<uint8> Data;

	/** Policy for when this save data is unaccounted for */
	UPROPERTY()
	ESaveGameActorSpawnPolicy Policy;

public:

	friend FArchive& operator << (FArchive& Ar, FActorSaveData& SaveData)
	{
		Ar << SaveData.Name;
		Ar << SaveData.Transform;
		Ar << SaveData.Class;
		Ar << SaveData.Data;
		Ar << SaveData.Policy;

		return Ar;
	}
};

/** Save data for an actor that needs to be destroyed when loading the game */
USTRUCT()
struct EDENSGARDEN_API FDestroyedActorsSaveData
{
	GENERATED_BODY()

public:

	/** Name of the actors destroyed */
	UPROPERTY()
	TSet<FName> Names;

public:

	friend FArchive& operator << (FArchive& Ar, FDestroyedActorsSaveData& SaveData)
	{
		Ar << SaveData.Names;

		return Ar;
	}
};

/** Save data for weapon in spec ops inventory (not in world) */
USTRUCT()
struct EDENSGARDEN_API FWeaponSaveData
{
	GENERATED_BODY()

public:

	/** Name of weapon. Is required to find
	potential weapon with same name to destroy */
	UPROPERTY()
	FName Name;

	/** Type of weapon. Is required to spawn
	in when loading generating inventory */
	UPROPERTY()
	TSubclassOf<AWeaponBase> Class;

	/** Additional data weapon needed to serialize (e.g. Ammo) */
	UPROPERTY()
	TArray<uint8> Data;

public:

	friend FArchive& operator << (FArchive& Ar, FWeaponSaveData& SaveData)
	{
		Ar << SaveData.Name;
		Ar << SaveData.Class;
		Ar << SaveData.Data;

		return Ar;
	}
};

/** Save data relating to the player, including the inventory */
USTRUCT()
struct EDENSGARDEN_API FSpecOpSaveData
{
	GENERATED_BODY()

public:

	/** Health of spec op */
	UPROPERTY()
	float Health;

	/** Energy of spec op */
	UPROPERTY()
	float Energy;

	/** Breath of spec op */
	UPROPERTY()
	float Breath;

	/** Additional data spec op might need to save */
	UPROPERTY()
	TArray<uint8> Data;

public:

	/** All guns spec op had in inventory */
	UPROPERTY()
	TArray<FWeaponSaveData> Guns;

	/** The max size of gun inventory */
	UPROPERTY()
	int32 GunInventorySize;

	/** Index of gun spec op had equipped */
	UPROPERTY()
	int32 EquippedGunIndex;

public:

	/** Melee weapon spec op had set */
	UPROPERTY()
	FWeaponSaveData MeleeWeapon;

public:

	friend FArchive& operator << (FArchive& Ar, FSpecOpSaveData& SaveData)
	{
		Ar << SaveData.Health;
		Ar << SaveData.Energy;
		Ar << SaveData.Breath;
		Ar << SaveData.Data;
		
		Ar << SaveData.Guns;
		Ar << SaveData.GunInventorySize;
		Ar << SaveData.EquippedGunIndex;

		Ar << SaveData.MeleeWeapon;

		return Ar;
	}
};
