// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/GameModeBase.h"
#include "EdensGardenGameMode.generated.h"

class AEGCharacter;
class UMaterialInterface;
class UMaterialInstanceDynamic;
class UMeshComponent;

// TODO: make a pingsystem object that manages this?

/** Wrapper for a highlighted actor */
USTRUCT()
struct FHighlightedActor
{
	GENERATED_BODY()

public:

	/** Actor that has been pinged */
	TWeakObjectPtr<AActor> Actor;

	/** The meshes that were pinged */
	TArray<TWeakObjectPtr<UMeshComponent>> Meshes;

public:

	/** Timer handle, required for cancelling ping */
	FTimerHandle TimerHandle_PingDuration;
};

/** Game mode that controls the core gameplay of Edens Garden */
UCLASS(minimalapi)
class AEdensGardenGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	AEdensGardenGameMode();

public:

	// Begin AGameModeBase Interface
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void StartPlay() override;
	virtual void SetPlayerDefaults(APawn* PlayerPawn) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	// End AGameModeBase Interface

	// Begin AActor Interface
	virtual void Reset() override;
	// End AActor Interface

public:

	/** Restarts at the last checkpoint in current active save data */
	UFUNCTION(BlueprintCallable, Category = EdensGarden)
	void RestartAtCheckpoint();

	/** Restarts at the levels starting values (reverting all progress made of current level) */
	//UFUNCTION(BlueprintCallable, Category = EdensGarden)
	//void RestartAtLevelStart();

	/** Transitions to the given map, updating save data to record it */
	UFUNCTION(BlueprintCallable, Category = EdensGarden)
	void TransitionToNextLevel(FName LevelName);

	/** Transitions back to the main menu */
	UFUNCTION(BlueprintCallable, Category = EdensGarden)
	void TransitionToMainMenu(FName LevelName);

private:

	/** Callback for when the players character has died */
	UFUNCTION()
	void OnPlayerCharacterDeath(AEGCharacter* DeadCharacter, float Damage, APawn* DamageInstigator, AActor* DamageSource, const FGameplayTagContainer& GameplayTags);

public:

	/** 
	* Will infinitely highlight the given actor (until asked to clear)
	* Will always clear the current highlighted actor, even if given actor is null.
	* The stencil values of actors meshes are overwritten and not recorded 
	*/
	UFUNCTION(BlueprintCallable, Category = Highlighting)
	void HighlightActor(AActor* Actor, const FLinearColor& Color);

	/** 
	* Will highlight the given actor for given duration. The color can not be changed.
	* This will overwrite existing ping (or make indef highlight duration based).
	* The stencil values of actors meshes are overwritten and not recorded 
	*/
	UFUNCTION(BlueprintCallable, Category = Highlighting)
	void PingActor(AActor* Actor, EHighlightColor Color, float Duration);

	/**
	* Clears given actor from ping (or indef highlight)
	*/
	UFUNCTION(BlueprintCallable, Category = Highlighting)
	void StopHighlightingActor(AActor* Actor);

public:

	/** Get the highlight material instance, will create it if invalid */
	UFUNCTION(BlueprintPure, Category = Highlighting)
	UMaterialInstanceDynamic* GetHighlightMaterial();

private:

	/** Initializes the highlight process for given actor */
	void HighlightActor(AActor* Actor, int32 StencilValue, float Duration, FHighlightedActor& OutHighlightActor);

	/** Restores given meshes to default values */
	void RestoreHighlightedMeshes(TArray<TWeakObjectPtr<UMeshComponent>>& Meshes);

protected:

	/** The highlight material for pinging actors */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Highlighting, meta = (DisplayName="Highlight Template"))
	UMaterialInterface* HighlightMaterialTemplate;

	/** The dynamic instance of the highlight material */
	UPROPERTY(VisibleInstanceOnly, Category = Highlighting, meta = (DisplayName="Highlight Instance"))
	UMaterialInstanceDynamic* HighlightMaterialInstance;

	/** The name of the custom color parameter in the highlight material */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Highlighting)
	FName HighlighCustomColorParameter;

private:

	/** Callback for when a duration based ping has expired */
	UFUNCTION()
	void OnActorPingExpired(uint32 UniqueID);

	/** Callback for when a pinged actor is destroyed */
	UFUNCTION()
	void OnPingedActorDestroyed(AActor* DestroyedActor);

private:

	/** An actor who is highlighted indefinitely (used by interactives).
	This is the only highlight actor that can use a custom color */
	FHighlightedActor FocusedPingedActor;

	/** All actors that were pinged temporarily, uses the actors unique ID as key */
	TMap<uint32, FHighlightedActor> DurationPingedActors;
};



