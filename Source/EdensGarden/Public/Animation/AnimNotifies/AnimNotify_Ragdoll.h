// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_Ragdoll.generated.h"

/** Notify that will ragdoll the character which is animating the mesh */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Ragdoll"))
class EDENSGARDEN_API UAnimNotify_Ragdoll : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_Ragdoll();

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
