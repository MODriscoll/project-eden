// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify_MakeNoise.h"
#include "AnimNotify_SpecOpMakeNoise.generated.h"

/** Emits a simple noise from the spec op, but will 
scale the loudness by the spec ops current sound modifier */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "SpecOp Make Noise"))
class EDENSGARDEN_API UAnimNotify_SpecOpMakeNoise : public UAnimNotify_MakeNoise
{
	GENERATED_BODY()

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
