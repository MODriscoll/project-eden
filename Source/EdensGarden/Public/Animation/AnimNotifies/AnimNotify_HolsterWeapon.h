// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_HolsterWeapon.generated.h"

/** Used to signal to the spec op character that they should holster their currently equipped weapon */
UCLASS(const, hidecategories = Object, meta = (DisplayName="Holster Weapon"))
class EDENSGARDEN_API UAnimNotify_HolsterWeapon : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_HolsterWeapon();
	
public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
