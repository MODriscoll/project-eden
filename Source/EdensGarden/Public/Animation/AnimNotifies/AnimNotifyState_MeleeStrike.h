// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState_EnableHitboxes.h"
#include "GameplayTagContainer.h"
#include "AnimNotifyState_MeleeStrike.generated.h"

/** Anim notify to work with the spec op character. Should be
used for animations working with a melee weapon. Will activate
strike on start and end it on finish */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Melee Strike"))
class EDENSGARDEN_API UAnimNotifyState_MeleeStrike : public UAnimNotifyState_EnableHitboxes
{
	GENERATED_BODY()
	
public:

	UAnimNotifyState_MeleeStrike();

public:

	// Begin UAnimNotifyState Interface
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotifyState Interface
};
