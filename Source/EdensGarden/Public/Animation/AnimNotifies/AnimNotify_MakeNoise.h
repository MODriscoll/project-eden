// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "GameplayTagContainer.h"
#include "AnimNotify_MakeNoise.generated.h"

/** Emits a simple noise from pawn owner */
UCLASS(const, hidecategories=Object, meta=(DisplayName="Make Noise"))
class EDENSGARDEN_API UAnimNotify_MakeNoise : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_MakeNoise();

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
	
public:

	/** How load the noise is */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Noise)
	float Loudness;

	/** The max range of this noise */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Noise)
	float MaxRange;

	/** Optional gameplay tag to send with noise. (Is converted to FName when sent) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Noise)
	FGameplayTag NoiseTag;
};
