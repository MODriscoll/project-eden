// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "GameplayTagContainer.h"
#include "AnimNotifyState_EnableHitboxes.generated.h"

/** Will activate hitboxes on start and deactivate them on end if owner implements the IHitboxInterface */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Enable Hitboxes"))
class EDENSGARDEN_API UAnimNotifyState_EnableHitboxes : public UAnimNotifyState
{
	GENERATED_BODY()
	
public:

	UAnimNotifyState_EnableHitboxes();

public:
	
	// Begin UAnimNotifyState Interface
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotifyState Interface

public:

	/** The tag associated with this activation (eg A damage tag) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Hitbox)
	FGameplayTag ActivationTag;

	/** Tags used to query the hitboxes to use */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Hitbox)
	FGameplayTagContainer HitboxTags;
};
