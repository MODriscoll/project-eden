// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "GameplayTagContainer.h"
#include "AnimNotify_GameplayEvent.generated.h"

/** Simple notify that sends a gameplay event with given tag to owner */
UCLASS(const, hidecategories=Object, meta=(DisplayName="Gameplay Tag Event"))
class EDENSGARDEN_API UAnimNotify_GameplayEvent : public UAnimNotify
{
	GENERATED_BODY()

public:

	UAnimNotify_GameplayEvent();
	
public:

	// Begin UAnimNotify Interface
	virtual FString GetNotifyName_Implementation() const override;
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
	
public:

	/** Tag of event */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Event)
	FGameplayTag EventTag;
};
