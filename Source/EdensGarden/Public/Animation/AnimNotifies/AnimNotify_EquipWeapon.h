// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_EquipWeapon.generated.h"

/** Used to signal to the spec op character that they should equip the currently queued weapon */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Equip Weapon"))
class EDENSGARDEN_API UAnimNotify_EquipWeapon : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_EquipWeapon();

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
