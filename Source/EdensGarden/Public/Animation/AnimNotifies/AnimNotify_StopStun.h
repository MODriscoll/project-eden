// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_StopStun.generated.h"

/** Notify class for stun animations used by character to inform that they should exit the stunned state */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Stop Stun"))
class EDENSGARDEN_API UAnimNotify_StopStun : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_StopStun();
	
public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
