// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_PlayWeaponSound.generated.h"

class USoundBase;

/** Plays a sound the originates from the spec ops equipped gun or melee weapon */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Play Weapon Sound"))
class EDENSGARDEN_API UAnimNotify_PlayWeaponSound : public UAnimNotify
{
	GENERATED_BODY()

public:

	UAnimNotify_PlayWeaponSound();

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
	
public:

	/** The sound to play */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Noise)
	USoundBase* Sound;

	/** If this sound is coming from the melee weapon.
	If not, the spec ops equipped gun is used by default */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Noise)
	uint8 bPlayFromMelee : 1;
};
