// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotify.h"
#include "AnimNotify_StopRolling.generated.h"

/** Notify class for rolling animations used by the spec op to inform rolling has finished */
UCLASS(const, hidecategories = Object, meta = (DisplayName = "Stop Rolling"))
class EDENSGARDEN_API UAnimNotify_StopRolling : public UAnimNotify
{
	GENERATED_BODY()
	
public:

	UAnimNotify_StopRolling();

public:

	// Begin UAnimNotify Interface
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;
	// End UAnimNotify Interface
};
