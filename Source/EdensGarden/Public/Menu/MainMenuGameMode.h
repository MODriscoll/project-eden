// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/GameModeBase.h"
#include "MainMenuGameMode.generated.h"

class UUserWidget;
class UWorld;

/** Handles the logic for the main menu functionality and transitioning into the game */
UCLASS()
class EDENSGARDEN_API AMainMenuGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	AMainMenuGameMode();

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	// End AActor Interface

public:

	/** Changes the current displayed widget, removing the previous one from viewport */
	UFUNCTION(BlueprintCallable, Category = Menu)
	void DisplayWidget(TSubclassOf<UUserWidget> Widget, int32 ZOrder = 0);

private:

	/** The default widget to open on start up */
	UPROPERTY(EditAnywhere, Category = Menu)
	TSubclassOf<UUserWidget> DefaultMenuWidget;

	/** The widget currently being displayed */
	UPROPERTY()
	UUserWidget* DisplayedWidget;

public:

	/** Creates a new save game, setting its default level to open before 
	transitioning to it. Get if level async loading has commenced */
	UFUNCTION(BlueprintCallable, Category = Menu)
	bool CreateSaveGameAndStart(bool bUseEditorSaveIfPossible = false);

	/** Loads the persistant save game and transitioning to it. 
	Get if level async loading has commenced */
	UFUNCTION(BlueprintCallable, Category = Menu)
	bool LoadSaveGameAndStart(bool bUseEditorSaveIfPossible = false);

private:

	/** Transitions to given map with given options. Will auto display the loading screen */
	void TransitionToMap(const FName& MapName, const FString& Options);

private:

	/** The name of default map. This is for testing purposes and will be replaced by default game level */
	UPROPERTY(EditDefaultsOnly, Category = Menu)
	FName DefaultGameLevelName;

	/** The default map to open when making a new save game */
	UPROPERTY(EditDefaultsOnly, Category = Menu)
	TSoftObjectPtr<UWorld> DefaultGameLevel;
};
