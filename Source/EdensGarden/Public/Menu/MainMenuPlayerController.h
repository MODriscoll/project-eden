// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainMenuPlayerController.generated.h"

/** Controller for players to use while in main menu */
UCLASS()
class EDENSGARDEN_API AMainMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AMainMenuPlayerController();

public:

	// Begin AActor Interface
	virtual void BeginPlay() override;
	// End AActor Interface
};
