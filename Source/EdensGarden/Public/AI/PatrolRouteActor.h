// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PatrolRouteComponent.h"
#include "PatrolRouteActor.generated.h"

class UBillboardComponent;

/** Actor that has the patrol route component */
UCLASS(ClassGroup = (AI))
class EDENSGARDEN_API APatrolRouteActor : public AActor
{
	GENERATED_BODY()
	
public:	
	
	APatrolRouteActor();

public:	
	
	/** Get patrol route component */
	FORCEINLINE UPatrolRouteComponent* GetPatrolRouteComponent() const { return PatrolRouteComponent; }

private:

	/** The patrol route component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UPatrolRouteComponent* PatrolRouteComponent;

	#if WITH_EDITORONLY_DATA
	/** The billboard component, only available with editor */
	UPROPERTY()
	UBillboardComponent* BillboardComponent;
	#endif
};
