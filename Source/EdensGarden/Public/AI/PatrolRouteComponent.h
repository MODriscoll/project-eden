// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Components/SceneComponent.h"
#include "PatrolRouteComponent.generated.h"

/** The direction to travel along a patrol route */
UENUM(BlueprintType)
enum class EPatrolRouteDirection : uint8
{
	/** Move forward (next iteration of point array) */
	Forward,

	/** Move backwards (previous iteration of point array) */
	Backward
};

/** Component which manages patrol points and status for AI that patrol a route */
UCLASS(ClassGroup=(AI), meta=(BlueprintSpawnableComponent))
class EDENSGARDEN_API UPatrolRouteComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	
	UPatrolRouteComponent();

public:

	// Begin UObject Interface
	#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

public:	
	
	/** Simple function for calculating, setting then getting the next waypoint in the route */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	virtual FVector GetNextWaypoint();
		
	/** Updates the current waypoint to the waypoint closest to given location */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	virtual FVector GetWaypointClosestTo(const FVector& Location);

public:

	/** Adds a new waypoint in local space */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	int32 AddWaypoint(const FVector& Location);

	/** Adds a new world space waypoint */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	int32 AddWorldspaceWaypoint(const FVector& Location);

	/** Inserts a new waypoint in local space at given index */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	int32 InsertWaypoint(const FVector& Location, int32 Index);

	/** Inserts a new worldspace waypoint at given index */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	int32 InsertWorldspaceWaypoint(const FVector& Location, int32 Index);

	/** Removes the waypoint at given index */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	void RemoveWaypoint(int32 Index);

	/** Overrides the current waypoint location with new location */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	void SetWaypointLocation(const FVector& Location, int32 Index);

	/** Offsets the waypoint at given index */
	UFUNCTION(BlueprintCallable, Category = "Patrol Route")
	void OffsetWaypointLocation(const FVector& Offset, int32 Index);

public:

	/** Get number of waypoints */
	UFUNCTION(BlueprintPure, Category = "Patrol Route")
	int32 GetNumWaypoints() const { return Waypoints.Num(); }

	/** Get the location of the current waypoint in world space */
	UFUNCTION(BlueprintPure, Category = "Patrol Route")
	FVector GetCurrentWaypoint() const;

	/** Get the location of waypoint at index in world space */
	UFUNCTION(BlueprintPure, Category = "Patrol Route")
	FVector GetWaypointAt(int32 Index) const;

	/** Get the index of the current waypoint */
	FORCEINLINE int32 GetCurrentWaypointIndex() const { return CurrentWaypoint; }

protected:
	
	/** Calculates the index of the next waypoint in the route. This function
	can be used to calculate changes to the route as well if desired */
	UFUNCTION(BlueprintNativeEvent, Category = "PatrolRoute")
	int32 CalculateNextWaypointIndex();
	virtual int32 CalculateNextWaypointIndex_Implementation();

private:

	/** Calculates and sets the index of the next waypoint in the route before returning it */
	int32 GetNextWaypointIndex();

public:

	/** The direction along the route to travel */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Patrol Route")
	EPatrolRouteDirection RouteDirection;

	/** If this patrol wraps back when at end of route */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Patrol Route")
	uint8 bRouteLoops : 1;

	#if WITH_EDITORONLY_DATA
	/** Color to draw debug visualization of waypoints */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Patrol Route")
	FLinearColor WaypointColor;

	/** Color to draw debug visualization of selected waypoint */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Patrol Route")
	FLinearColor SelectedWaypointColor;

	/** Color to draw debug visualization of routes between waypoints */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Patrol Route")
	FLinearColor RouteColor;

	/** Color to draw debug visualization of routes connecting to selected waypoint */
	UPROPERTY(EditAnywhere, AdvancedDisplay, Category = "Patrol Route")
	FLinearColor SelectedRouteColor;
	#endif

protected:

	/** The waypoints of this route (in local space) */
	UPROPERTY(EditAnywhere, NoClear, BlueprintReadOnly, Category = "Patrol Route")
	TArray<FVector> Waypoints;

	/** Index of the current waypoint. The default value is used as the start waypoint */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Patrol Route")
	int32 CurrentWaypoint;
};
