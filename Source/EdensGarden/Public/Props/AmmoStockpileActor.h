// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "InteractiveInterface.h"
#include "AmmoStockpileActor.generated.h"

class AAmmoGun;
class AGunBase;

struct FGameplayEffectContextHandle;

/** Enum for help identifying if an ammo stockpile can be interacted with */
UENUM(BlueprintType)
enum class EAmmoStockpileInteractResponse : uint8
{
	/** The spec op meets the interact requirements */
	CanGetAmmo,

	/** All the spec ops ammo guns are already full */
	AmmoAlreadyFull,

	/** The spec op is missing the required type of gun */
	MissingRequiredGun
};

/** Ammo stockpiles works similar to the ammo box, but will
share its ammo amonst all ammo guns the spec op is carrying */
UCLASS()
class EDENSGARDEN_API AAmmoStockpileActor : public AActor, public IInteractiveInterface
{
	GENERATED_BODY()
	
public:

	AAmmoStockpileActor();

public:

	// Begin IInteractive Interface
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const override;
	virtual float GetInteractDuration_Implementation() const override;
	virtual void GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const override;
	virtual void OnInteract_Implementation(ASpecOpCharacter* Interactor) override;
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled) override;
	// End IInteractive Interface

private:

	/** Visual mesh */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;

private:

	/** Gives ammo to given spec op */
	void GiveAmmoTo(ASpecOpCharacter* SpecOp);

	/** Calculates then gives ammo to specified gun */
	void GiveAmmoToGun(AAmmoGun* Gun, const FGameplayEffectContextHandle& ContextHandle);

protected:

	/** Event for when this stockpile has given ammo, by default, it destroys this actor */
	UFUNCTION(BlueprintNativeEvent, Category = AmmoBox)
	void OnAmmoGiven();
	virtual void OnAmmoGiven_Implementation();

public:

	/** The percentage of the max ammo of the gun we should refill */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stockpile, meta = (ClampMin = 0, ClampMax = 1))
	float RefillPercentage;

protected:

	/** Get how this ammo box should respond to spec ops interaction */
	UFUNCTION(BlueprintPure, Category = AmmoBox)
	EAmmoStockpileInteractResponse GetInteractResponse(const ASpecOpCharacter* Interactor) const;

private:

	/** Get the guns to refill with ammo */
	bool GetGunsToRefill(const ASpecOpCharacter* SpecOp, TArray<AGunBase*>& Guns) const;

public:

	/** Time required to pick up ammo. Zero or less means instant pick up */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AmmoBox)
	float InteractTime;
};
