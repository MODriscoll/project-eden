// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Interactives/InteractiveActor.h"
#include "EffectActor.generated.h"

class UGameplayEffect;

/** A simple interactive actor that applies a gameplay effect to the spec op on interaction */
UCLASS()
class EDENSGARDEN_API AEffectActor : public AInteractiveActor
{
	GENERATED_BODY()
	
public:

	AEffectActor();

public:
	
	// Begin IInteractive Interface
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const override;
	virtual void OnInteract_Implementation(ASpecOpCharacter* Interactor) override;
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled) override;
	// End IInteractive Interface

protected:

	/** Event for when the effect has been applied */
	UFUNCTION(BlueprintImplementableEvent)
	void OnEffectApplied(ASpecOpCharacter* SpecOp);

public:

	/** If this effect actor is waiting for delay to finish */
	bool IsInteractionDelayed() const;

private:

	/** Applies the gameplay effect to the spec op, will destroy self if desired */
	void ApplyGameplayEffect(ASpecOpCharacter* SpecOp);

public:

	/** The gameplay effect to apply */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Effects)
	TSubclassOf<UGameplayEffect> GameplayEffect;

	/** If we should destroy this actor on interact */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive)
	uint8 bDestroyOnInteract : 1;

	/** An optional delay to apply between interactions */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive, meta = (EditCondition="!bDestroyOnInteract"))
	float InteractionDelay;

private:

	/** Timer handle when using the optional delay */
	FTimerHandle TimerHandle_InteractDelay;
};
