// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "SaveGameInterface.h"
#include "Components/TimelineComponent.h"
#include "AI/Navigation/NavRelevantInterface.h"
#include "DoorActor.generated.h"

class UBoxComponent;
class UCurveFloat;
class UNavArea;

/** Simple event delegate for door switching states */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDoorOpenCloseSignature, bool, bNowOpen);

/** Simple event delegate for when door is unlocked */
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDoorUnlockSignature);

/** Save data for serializing a DoorActor */
USTRUCT()
struct EDENSGARDEN_API FDoorActorSaveData
{
	GENERATED_BODY()

public:

	/** If door is open */
	UPROPERTY()
	bool bIsOpen;

	/** If door is locked */
	UPROPERTY()
	bool bIsLocked;

public:

	friend FArchive& operator << (FArchive& Ar, FDoorActorSaveData& SaveData)
	{
		Ar << SaveData.bIsOpen;
		Ar << SaveData.bIsLocked;

		return Ar;
	}
};

/** Base for door actors that can open and close. Will call
the lerp doors function to allow for interpolation of the door */
UCLASS(abstract)
class EDENSGARDEN_API ADoorActor : public AActor, public ISaveGameInterface
{
	GENERATED_BODY()
	
public:	
	
	ADoorActor();

public:	

	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override { return true; }
	virtual void PostLoadGameSerialization() override;
	// End ISaveGame Interface

	// Begin AActor Interface
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	// End AActor Interface

	// Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	#if WITH_EDITOR
	virtual bool CanEditChange(const UProperty* InProperty) const override;
	virtual void PostEditUndo() override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
	#endif
	// End UObject Interface

public:

	/** Opens this door */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = Door)
	void Open();

	/** Closes the door */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = Door)
	void Close();

	/** Toggles the door, basically will 
	open if closed and close if open */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = Door)
	void ToggleDoor();

	/** Unlocks this door, returns if attempt was successfull */
	UFUNCTION(BlueprintCallable, CallInEditor, Category = Door)
	bool Unlock();

protected:

	/** Interpolates the doors between open and close.
	0 implies fully open while 1 implies fully closed */
	UFUNCTION(BlueprintNativeEvent, Category = Door)
	void LerpDoors(float Alpha);
	virtual void LerpDoors_Implementation(float Alpha) { }

private:

	/** Update the nav modifiers nav area based on doors state.
	Should be called during actual play, rather than in editor */
	void UpdateDoorNavModifier(bool bIsOpening, bool bFromLoad = false);

public:

	/** Get if this door is open */
	bool IsOpen() const { return bIsOpen; }

	/** Get if this door is locked */
	bool IsLocked() const { return bIsLocked; }

	/** If this door is currently opening or closing */
	UFUNCTION(BlueprintPure, Category = Door)
	bool IsOpeningOrClosing() const;

	/** If this door can be opened */
	UFUNCTION(BlueprintPure, Category = Door)
	virtual bool CanOpen() const;

	/** If this door can be closed */
	UFUNCTION(BlueprintPure, Category = Door)
	virtual bool CanClose() const;

	/** If this door can be unlocked */
	UFUNCTION(BlueprintPure, Category = Door)
	virtual bool CanUnlock() const;

	/** If this door will soon auto close */
	UFUNCTION(BlueprintPure, Category = Door)
	bool WillAutoClose() const;

protected:

	/** If this door is currently open. Can be 
	used to set the door to start open or closed */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Door)
	uint8 bIsOpen : 1;

	/** If this door is currently locked */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Door)
	uint8 bIsLocked : 1;

protected:

	/** Event for when door has started changing to new state */
	UFUNCTION(BlueprintImplementableEvent, Category = Door, meta = (DisplayName = "On Door Start Transition"))
	void DoorStartStateChange(bool bIsOpening);

	/** Event for when the door state has changed (on completion) */
	UFUNCTION(BlueprintImplementableEvent, Category = Door, meta = (DisplayName = "On Door End Transition"))
	void DoorStateChanged(bool bNowOpen);

	/** Event for when the door has been unlocked */
	UFUNCTION(BlueprintImplementableEvent, Category = Door, meta = (DisplayName = "On Door Unlocked"))
	void DoorUnlocked();

public:

	/** Event for when door has either opened or closed.
	Is broadcasted once opening/closing has finished */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorOpenCloseSignature OnDoorOpenOrClose;

	/** Event for when door has been unlocked */
	UPROPERTY(BlueprintAssignable, Category = Door)
	FDoorUnlockSignature OnDoorUnlocked;

private:

	/** Sets auto close timer only if set to auto close */
	void SetAutoCloseTimer();

public:

	/** The amount of time before the door should auto close. A
	value of zero or less means the door does not close automatically */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door, meta = (UIMin = -1))
	float TimeBeforeAutoClose;

private:

	/** Callback for when timeline has interpolated */
	UFUNCTION()
	void OnInterpUpdated(float Value);

	/** Callback for when timeline has finished */
	UFUNCTION()
	void OnInterpFinish();

protected:

	/** Float track to use for interpolation of the doors. Should 
	provide a curve that lerps from 0 to 1 (values are clamped anyways) */
	UPROPERTY(EditAnywhere, Category = Door)
	UCurveFloat* InterpolationTrack;

private:

	/** Timeline used for interpolation of doors */
	FTimeline DoorInterpTrack;

	/** Timer handle for managing auto closing */
	FTimerHandle TimerHandle_AutoClose;

protected:

	/** The nav area to use when door is closed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door)
	TSubclassOf<UNavArea> ClosedNavArea;

	/** The nav area to use when door is open */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Door)
	TSubclassOf<UNavArea> OpenNavArea;

private:

	/** The box volume used to determine this doors area relevance.
	This volume is not intended for collision purposes, rather as a method
	for easily laying out the area in which the door affects navigation*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* NavModifierVolume;
};
