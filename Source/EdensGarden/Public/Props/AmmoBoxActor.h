// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractiveInterface.h"
#include "SaveGameInterface.h"
#include "AmmoBoxActor.generated.h"

class AAmmoGun;

/** Enum for help identifying if an ammo box can be interacted with */
UENUM(BlueprintType)
enum class EAmmoBoxInteractResponse : uint8
{
	/** The spec op meets the interact requirements */
	CanGetAmmo,

	/** The spec op has the type of gun but already has full ammo */
	AmmoAlreadyFull,

	/** The spec op is missing the required type of gun */
	MissingRequiredGun,

	/** The ammo box is out of ammo */
	OutOfAmmo
};

/** Save data for serializing a DoorActor */
USTRUCT()
struct EDENSGARDEN_API FAmmoBoxActorSaveData
{
	GENERATED_BODY()

public:

	/** Ammo left in box */
	UPROPERTY()
	float Ammo;

public:

	friend FArchive& operator << (FArchive& Ar, FAmmoBoxActorSaveData& SaveData)
	{
		Ar << SaveData.Ammo;

		return Ar;
	}
};

/** A specialized prop that will give ammo to the spec op who interacts
with it. The term 'box' is generic, doesn't mean this has to be a box! */
UCLASS()
class EDENSGARDEN_API AAmmoBoxActor : public AActor, public IInteractiveInterface, public ISaveGameInterface
{
	GENERATED_BODY()
	
public:

	AAmmoBoxActor();
	
public:

	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override;
	// End ISaveGame Interface

	// Begin IInteractive Interface
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const override;
	virtual float GetInteractDuration_Implementation() const override;
	virtual void GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const override;
	virtual void OnInteract_Implementation(ASpecOpCharacter* Interactor) override;
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled) override;
	// End IInteractive Interface

	// Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	// End UObject Interface

private:

	/** Visual mesh */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;

private:

	/** Gives ammo to given spec op */
	void GiveAmmoTo(ASpecOpCharacter* SpecOp);

protected:

	/** Event for when ammo was given but ammo still remains */
	UFUNCTION(BlueprintImplementableEvent, Category = AmmoBox)
	void OnAmmoGiven();

	/** Event for when this box has run out of ammo, by default, it destroys this actor */
	UFUNCTION(BlueprintNativeEvent, Category = AmmoBox)
	void OnOutOfAmmo();
	virtual void OnOutOfAmmo_Implementation();

public:
	
	/** The type of gun we provide ammo for */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AmmoBox)
	TSubclassOf<AAmmoGun> GunAmmoType;

	/** The amount of ammo to give */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AmmoBox)
	float Ammo;

protected:

	/** Get how this ammo box should respond to spec ops interaction */
	UFUNCTION(BlueprintPure, Category = AmmoBox)
	EAmmoBoxInteractResponse GetInteractResponse(const ASpecOpCharacter* Interactor) const;

private:

	/** Get the gun to refill based on ammo type */
	AAmmoGun* GetGunToRefill(const ASpecOpCharacter* SpecOp) const;

public:

	/** Time required to pick up ammo. Zero or less means instant pick up */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = AmmoBox)
	float InteractTime;
};
