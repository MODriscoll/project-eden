// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "EGPhysicalMaterial.generated.h"

/** Physical material used in Edens Garden */
UCLASS(hidecategories=(Vehicles))
class EDENSGARDEN_API UEGPhysicalMaterial : public UPhysicalMaterial
{
	GENERATED_BODY()
};
