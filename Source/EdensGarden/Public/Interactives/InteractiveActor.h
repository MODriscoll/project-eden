// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "InteractiveInterface.h"
#include "InteractiveActor.generated.h"

/** Base class for creating a simple interactive actor */
UCLASS()
class EDENSGARDEN_API AInteractiveActor : public AActor, public IInteractiveInterface
{
	GENERATED_BODY()
	
public:	

	AInteractiveActor();

public:

	// Begin IInteractive Interface
	virtual void GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& DisplayColor) const override;
	virtual float GetInteractDuration_Implementation() const override;
	// End IInteractive Interface

	/** Get mesh */
	FORCEINLINE UStaticMeshComponent* GetMesh() const { return Mesh; }

public:

	/** The 'title' of this interactive */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive, meta=(DisplayName="Title"))
	FText InteractiveTitle;

	/** The color to highlight this interactive with */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive)
	FLinearColor HighlightColor;

	/** Time it takes to interact with this object. A value
	of 0 implies the interaction process is instant */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive, meta = (DisplayName = "Interact Duration", ClampMin = 0))
	float InteractiveDuration;

private:

	/** Visual mesh for interactive */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* Mesh;
};
