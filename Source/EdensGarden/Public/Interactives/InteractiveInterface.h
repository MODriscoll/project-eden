// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "InteractiveInterface.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogInteractive, Log, All);

UINTERFACE(Blueprintable)
class UInteractiveInterface : public UInterface
{
	GENERATED_BODY()
};

class ASpecOpCharacter;

/** Interface to be implemented by actors who can be interacted with by a spec op character */
class EDENSGARDEN_API IInteractiveInterface
{
	GENERATED_BODY()

public:

	/** Called when interactive becomes focus of player */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractiveGainFocus(ASpecOpCharacter* Character);
	virtual void OnInteractiveGainFocus_Implementation(ASpecOpCharacter* Character);

	/** Called when interactive loses focus of player */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractiveLoseFocus(ASpecOpCharacter* Character);
	virtual void OnInteractiveLoseFocus_Implementation(ASpecOpCharacter* Character);

public:

	/** Get the interactives highlight display info. This function is called either
	on focus gain or every arbitrary interval while also focused. */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void GetInteractiveDisplayInfo(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& HighlightColor) const;
	virtual void GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& HighlightColor) const;

public:

	/** Get if this interactive can currently be interacted with */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Interactive)
	bool CanInteractWith(const ASpecOpCharacter* Character) const;
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const { return true; }

	/** Get the amount of time required for the player to fully interact with this object.
	A time lower or equal to 0 implies the interaction is instant */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	float GetInteractDuration() const;
	virtual float GetInteractDuration_Implementation() const { return 0.f; }

public:
	
	/** Called when performing an instant interaction.
	Will only get called if GetInteractDuration() returned 0 or less */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteract(ASpecOpCharacter* Interactor);
	virtual void OnInteract_Implementation(ASpecOpCharacter* Interactor) { }

	/** Called when interaction has begun.
	Will only get called if GetInteractDuration returned a value greater than 0 */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractStart(ASpecOpCharacter* Interactor);
	virtual void OnInteractStart_Implementation(ASpecOpCharacter* Interactor) { }

	/** Called when interaction has finished.
	Will get called if GetInteractionDuration returned a value greater than 0.
	Can still get called with instant interactions if interaction
	was cancelled the same frame it was instigated */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractEnd(ASpecOpCharacter* Interactor, bool bWasCancelled);
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled) { }

	/** Called when interaction has failed due to can interact being false */
	UFUNCTION(BlueprintNativeEvent, Category = Interactive)
	void OnInteractFailed();
	virtual void OnInteractFailed_Implementation() { }
};
