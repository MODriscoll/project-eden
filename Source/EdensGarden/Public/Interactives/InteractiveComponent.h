// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Components/BoxComponent.h"
#include "InteractiveComponent.generated.h"

class IInteractiveInterface;

/** Delegate for when the closest interactive has changed */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractiveChangedSignature, AActor*, Interactive);

/** Delegate for when the closest interactive has been updated (not changed) */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInteractiveUpdatedSignature, AActor*, Interactive, const FText&, DisplayTitle);

/** Delegate for when interaction starts. This is only called on duration based interactions */
DECLARE_MULTICAST_DELEGATE(FInteractionStartSignature);

/** Delegate for when interaction has finished. Provides if iteraction finished due to cancellation */
DECLARE_MULTICAST_DELEGATE_OneParam(FInteractionFinishSignature, bool);

/** Simple collision component that also handles tracking of interactives */
UCLASS(ClassGroup="Interactive")
class EDENSGARDEN_API UInteractiveComponent : public UBoxComponent	
{
	GENERATED_BODY()
	
public:

	UInteractiveComponent();

public:

	// Begin UActorComponent Interface
	virtual void InitializeComponent() override;
	// End UActorComponent Interface

public:

	/** Start interacting with the closest interactive. There is at least a frame
	delay before interaction can fully complete, even if interactive is instant */
	UFUNCTION(BlueprintCallable, Category = Interactive)
	bool StartInteracting();
	
	/** Stop interacting with the closest interactive. (Will not cancel instant interactives) */
	UFUNCTION(BlueprintCallable, Category = Interactive)
	void StopInteracting();

private:

	/** Internal stop interacting for duration based interactives, handles actual ending of interaction */
	void StopInteraction(bool bHoldTimeComplete);

	/** Callback for instant interaction, should be called frame after interaction was initiated */
	void OnInstantComplete();

	/** Callback for when interaction duration has passed without cancellation */
	void OnDurationComplete();

public:

	/** If we should only acknowledge interactives that can 
	be interacted with when updating the closest interactive */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive)
	uint8 bOnlyAcknowledgeReadyInteractives : 1;

public:

	/** Finds the interactive closest to our spec op owner.
	Will return null if no object is within the interaction area */
	UFUNCTION(BlueprintPure, Category = Interactive)
	AActor* FindClosestInteractive() const;

	/** Re-calculates the interactive closest to our spec op owner */
	UFUNCTION(BlueprintPure, Category = Interactive)
	AActor* UpdateClosestInteractive(bool bStopInteraction = false);

	/** Get the amount of interactives in range */
	UFUNCTION(BlueprintPure, Category = Interactive)
	int32 GetNumInteractiveInRange() const { return Interactives.Num(); }

	/** Get the time remaining till interaction will 'complete'.
	This function will return 0 if not interacting */
	UFUNCTION(BlueprintPure, Category = Interactive)
	float GetTimeTillInteractComplete() const;

	/** Get the completion percentage of current interaction */
	UFUNCTION(BlueprintPure, Category = Interactive)
	float GetInteractionProgress() const;

	/** Get the last calculated closest interactive */
	AActor* GetClosestInteractive() const { return ClosestInteractive; }

	/** If interaction is in progress */
	bool IsInteracting() const { return bIsInteracting; }

private:

	/** Callback for when an object has entered the interactive range */
	UFUNCTION()
	void InteractAreaBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/** Callback for when an object has left the interactive range */
	UFUNCTION()
	void InteractAreaEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:

	/** The last checked closest interactive */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient, AdvancedDisplay, Category = "Interactive")
	AActor* ClosestInteractive;

	/** All interactives within interactive area */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient, AdvancedDisplay, Category = "Interactive")
	TArray<AActor*> Interactives;

	/** If we are currently in the process of interacting */
	UPROPERTY(BlueprintReadOnly, Transient, Category = "Interactive")
	uint8 bIsInteracting : 1;

	/** Cached duration time for current interaction */
	UPROPERTY(BlueprintReadOnly, Transient, Category = "Interactive")
	float CachedInteractDuration;

public:

	/** Delegate called when the closest interactive has changed.
	Will be null if no interactives are in range */
	UPROPERTY(BlueprintAssignable)
	FInteractiveChangedSignature OnClosestInteractiveChanged;
	
	/** Delegate called when the closest interactive has been updated (checked)
	Interactive pass will be null if no interactives are in range */
	UPROPERTY(BlueprintAssignable)
	FInteractiveUpdatedSignature OnClosestInteractiveUpdated;

	/** Delegate called when interaction process has started */
	FInteractionStartSignature OnInteractionStart;

	/** Delegate called when interaction process has ended */
	FInteractionFinishSignature OnInteractionFinished;

public:

	/** If the closest interactive should be automatically refreshed.
	Interval is only saved if bAuto is true */
	UFUNCTION(BlueprintCallable, Category = "Refresh")
	void SetAutoRefreshClosestInteractive(bool bAuto, float Interval = 0.2f);

protected:

	/** Activates the auto refresh timer if allowed */
	void ActivateRefreshTimer();

	/** Deactivates the auto refresh timer is set */
	void DeActivateRefreshTimer();

private:

	/** Callback for when to refresh the closest interactive */
	void OnAutoRefresh();

protected:

	/** If we should automatically refresh the closest interactive */
	UPROPERTY(EditDefaultsOnly, Category = Refreshing)
	uint8 bAutoRefresh : 1;

	/** Interval for refreshing the closest interactive */
	UPROPERTY(EditDefaultsOnly, Category = Refreshing, meta=(EditCondition=bAutoRefresh, ClampMin=0.05f))
	float RefreshInterval;

private:

	/** Spec op that owns this component */
	UPROPERTY(Transient)
	class ASpecOpCharacter* SpecOpOwner;

	/** If closest interactive is locked, meaning it shouldn't changed. This is
	used to prevent the interactive from changing while calling interface functions */
	uint8 bInteractiveLock : 1;

	/** Timer handle for tracking interact duration */
	FTimerHandle TimerHandle_Interaction;

	/** Timer handle for refreshing closest interactive */
	FTimerHandle TimerHandle_RefreshClosest;
};
