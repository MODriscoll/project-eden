// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "AbilityTask_Interact.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInteractTaskDelegate, bool, bWasCancelled);

/** Task that handles the interaction of the player */
UCLASS()
class EDENSGARDEN_API UAbilityTask_Interact : public UAbilityTask
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintAssignable)
	FInteractTaskDelegate NoInteraction;

	UPROPERTY(BlueprintAssignable)
	FInteractTaskDelegate InstantInteraction;

	UPROPERTY(BlueprintAssignable)
	FInteractTaskDelegate InteractionFinished;
	
public:

	/** Callback for when input was released. Simply stops interacting */
	UFUNCTION()
	void OnReleaseCallback();

	/** Callback for when interaction has concluded */
	UFUNCTION()
	void OnInteractionFinishCallback(bool bCancelled);

	/** Callback for when our owning ability has been cancelled */
	UFUNCTION()
	void OnAbilityCancelled();

	/** 
	* Interact with the closest interactive to the player. This task will 
	* initiate the interaction and handle cancellation if input has been released 
	*/
	UFUNCTION(BlueprintCallable, Category = "Ability|Tasks", meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "TRUE"))
	static UAbilityTask_Interact* Interact(UGameplayAbility* OwningAbility);

protected:

	// Begin UGameplayTask Interface
	virtual void Activate() override;
	virtual void ExternalCancel() override;
	virtual void OnDestroy(bool bAbilityEnded) override;
	// End UGameplayTask Interface

private:

	/** Helper function for unbinding callbacks */
	void UnbindCallbacks();

protected:

	FDelegateHandle ReleaseHandle;
	FDelegateHandle InteractHandle;
	FDelegateHandle CancelledHandle;

	TWeakObjectPtr<class ASpecOpCharacter> MySpecOp;

private:

	// Is used to track if interaction is instant or not.
	// Will be false if interaction was instant, true if over time
	uint8 bIsInteracting : 1;
};
