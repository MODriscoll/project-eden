// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbilitySystemInterface.h"
#include "GameplayTagContainer.h"
#include "EnergyShieldActor.generated.h"

class AEGCharacter;

class UEGAttributeSet;
class UGameplayEffect;
class USphereComponent;

/** Base for the energy shield used by the energy projector */
UCLASS()
class EDENSGARDEN_API AEnergyShieldActor : public AActor, public IAbilitySystemInterface
{
	GENERATED_BODY()
	
public:	
	
	AEnergyShieldActor();

public:

	// Begin IAbilitySystem Interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const { return AbilitySystem; }
	// End IAbilitySystem Interface

	// Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

public:

	/** The effect to apply to this shield upon creation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shield)
	TSubclassOf<UGameplayEffect> ShieldEffect;

protected:

	/** Notify that the shield has been damaged. 
	Character could be null if not damaged by one */
	UFUNCTION(BlueprintImplementableEvent, Category = Shield)
	void OnShieldDamaged(AEGCharacter* Character, float Damage, const FGameplayTagContainer& GameplayTags, bool bValidHit, const FHitResult& HitResult);

	/** Notify that the shield has been destroyed.
	Character could be null if not damaged by one */
	UFUNCTION(BlueprintNativeEvent, Category = Shield)
	void OnShieldDestroyed(AEGCharacter* Character, const FGameplayTagContainer& GameplayTags, bool bValidHit, const FHitResult& HitResult);
	virtual void OnShieldDestroyed_Implementation(AEGCharacter* Character, const FGameplayTagContainer& GameplayTags, bool bValidHit, const FHitResult& HitResult);

	/** Notify that the shield has expired */
	UFUNCTION(BlueprintNativeEvent, Category = Shield)
	void OnShieldExpired();
	virtual void OnShieldExpired_Implementation();

private:

	/** Callback for when we have taken damage */
	UFUNCTION()
	void OnShieldTakenDamage(float Damage, bool bKillingBlow, APawn* DamageInstigator, AActor* SourceActor, const FGameplayTagContainer& SourceTags, bool bValidHit, const FHitResult& HitResult);

public:

	/** Get Collision Sphere */
	FORCEINLINE USphereComponent* GetCollisionSphere() const { return CollisionSphere; }

	/** Get the shields health */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetHealth() const;

private:
	
	/** The shields area of effect */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CollisionSphere;

	/** Shields ability system, used to track attributes */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UAbilitySystemComponent* AbilitySystem;

	/** Shields attribute set, only supports health and being damaged */
	UPROPERTY()
	UEGAttributeSet* AttributeSet;

public:

	/** How long this shield should last for.0 or less means infinite */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Shield)
	float ShieldDuration;

private:

	/** Callback for when this shield expires */
	UFUNCTION()
	void OnShieldHasExpired();

private:

	/** Timer handle for tracking shield expiration */
	FTimerHandle TimerHandle_Expired;

protected:

	/** Callback for when the shield has been hit */
	UFUNCTION()
	virtual void OnShieldHit(AActor* SelfActor, AActor* OtherActor, FVector NormalImpulse, const FHitResult& Hit);
};
