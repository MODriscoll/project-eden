// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Weapons/Weapon.h"
#include "Gameplay/HitboxInterface.h"
#include "MeleeWeapon.generated.h"

class UBoxComponent;
class AEGCharacter;

/** Base for all melee weapons the spec op can utilize. The spec op can only
carry one melee weapon at a time, with its only ability being the melee strike */
UCLASS(abstract)
class AMeleeBase : public AWeaponBase, public IHitboxInterface
{
	GENERATED_BODY()

public:

	AMeleeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin IHitbox Interface
	virtual void EnableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) override;
	virtual void DisableHitboxes_Implementation(FGameplayTag AssociatedTag, const FGameplayTagContainer& HitboxTags) override;
	// End IHitbox Interface

	// Begin AWeaponBase Interface
	virtual EWeaponType GetWeaponType() const override final { return EWeaponType::Melee; }
	virtual void OnAddToInventory(ASpecOpCharacter* SpecOp) override;
	virtual void OnRemoveFromInventory() override;
	#if EG_GAMEPLAY_DEBUGGER
	virtual void GetDebugString(FString& String) const override;
	#endif
	// End AWeaponBase Interface

protected:

	// Begin AWeaponBase Interface
	virtual void CleanupWeapon() override;
	virtual void GiveSelfTo(ASpecOpCharacter* SpecOp) override final;
	virtual void RemoveSelfFrom(ASpecOpCharacter* SpecOp) override final;
	// End AWeaponBase Interface

protected:

	/** The melee ability to give to the spec op who owns us.
	This will be activated with the melee input binding */
	UPROPERTY(EditDefaultsOnly, Category = Melee)
	TSubclassOf<UGameplayAbility> MeleeAbility;

private:

	/** Handle to melee ability given to owner ASC */
	FGameplayAbilitySpecHandle MeleeAbilityHandle;

protected:

	/** Enables this melees attack collision */
	void EnableAttackCollision(FGameplayTag ActivationTag);

	/** Disables this melees attack collision */
	void DisableAttackCollision();

protected:

	/** Event for when melee is activated (has started to strike) */
	UFUNCTION(BlueprintImplementableEvent, Category = Melee)
	void OnMeleeActivated(FGameplayTag AttackTag);

	/** Event for when melee is deactivated (has stopped striking) */
	UFUNCTION(BlueprintImplementableEvent, Category = Melee)
	void OnMeleeDeactivated();

public:

	/** Get if this melee is active (striking) */
	UFUNCTION(BlueprintPure, Category = Melee)
	bool IsMeleeActive() const { return bIsMeleeActive; }

	/** Get if this melee's ability is active */
	UFUNCTION(BlueprintPure, Category = Melee)
	bool IsMeleeAbilityActive() const;

	/** Get this melees current activiation tag */
	UFUNCTION(BlueprintPure, Category = Melee)
	FGameplayTag GetActiviationTag() const { return AttackTag; }

protected:

	/** If the melee is active (currently registring hits) */
	uint8 bIsMeleeActive : 1;

	/** The gameplay tag associated with current activation. This gameplay
	tag is sent with the hit gameplay event when melee strikes something */
	FGameplayTag AttackTag;

private:

	/** All the actors that have been hit during current melee activation */
	TSet<AActor*> HitActors;

protected:

	/** Notify for when box collision was overlapped, will send gameplay event if strike is validated */
	UFUNCTION()
	void OnCollisionBoxOverlapped(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

private:

	/** Box used to detect collisions when melee is active */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* CollisionBox;

private:

	/** Notify that our owner has died, we disable melee if this happens */
	UFUNCTION()
	void OnOwnerDeath(AEGCharacter* Character, float Damage, APawn* DamageInstigator, AActor* Source, const FGameplayTagContainer& GameplayTags);
};

/** Null melee that does nothing, is used as default for when spec op has no set melee weapon */
UCLASS(NotBlueprintable, NotPlaceable)
class EDENSGARDEN_API ANullMelee final : public AMeleeBase
{
	GENERATED_BODY()
};