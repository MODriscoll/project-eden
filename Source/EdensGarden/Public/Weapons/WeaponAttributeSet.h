// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/AbilityMacros.h"
#include "WeaponAttributeSet.generated.h"

class AWeaponBase;

/** Base attribute set for all weapons */
UCLASS()
class EDENSGARDEN_API UWeaponAttributeSet : public UAttributeSet
{
	GENERATED_BODY()
	
public:

	UWeaponAttributeSet();

public:

	// Begin UAttributeSet Interface

	// End UAttributeSet Interface
	
public:

	/** Damage this weapon deals per hit */
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData WeaponDamage;
	ATTRIBUTE_ACCESSORS(UWeaponAttributeSet, WeaponDamage)

	/** Multiplier for dealing damage */
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
	FGameplayAttributeData WeaponDamageMultiplier;
	ATTRIBUTE_ACCESSORS(UWeaponAttributeSet, WeaponDamageMultiplier)
};
