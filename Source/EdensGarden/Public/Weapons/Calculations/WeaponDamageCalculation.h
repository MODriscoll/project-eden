// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "WeaponDamageCalculation.generated.h"

/** Calculates the damage to apply based on a weapons attribute */
UCLASS()
class EDENSGARDEN_API UWeaponDamageCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	UWeaponDamageCalculation();
	
public:

	// Begin UGameplayEffectExecutionCalculation Interface
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	// End UGameplayEffectExecutionCalculation Interface
};
