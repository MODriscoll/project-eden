// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/Actor.h"
#include "GameplayTagAssetInterface.h"
#include "AbilitySystemInterface.h"
#include "GameplayAbilitySpec.h"
#include "InteractiveInterface.h"
#include "SaveGameInterface.h"
#include "Weapon.generated.h"

DECLARE_LOG_CATEGORY_EXTERN(LogWeapon, Log, All);

class ASpecOpCharacter;
class UAbilitySystemComponent;
class UAudioComponent;
class UGameplayAbility;
class UGameplayEffect;
class USoundBase;

/** The type of weapon */
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	/** Weapon is a melee weapon */
	Melee,

	/** Weapon is a gun */
	Gun
};

/** The base for weapons the spec op can use. This class is simply here for common functionality
shared between melee weapons and gun, neither should be treated as replacement for the other */
UCLASS(abstract)
class EDENSGARDEN_API AWeaponBase : public AActor, public IInteractiveInterface, public ISaveGameInterface
{
	GENERATED_BODY()

public:

	AWeaponBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override;
	virtual ESaveGameActorSpawnPolicy GetSaveGameSpawnPolicy() const override { return ESaveGameActorSpawnPolicy::Spawn; }
	virtual bool WantsSaveGameTransformSet() const override { return true; }
	// End ISaveGame Interface

	// Begin IInteractive Interface
	virtual void GetInteractiveDisplayInfo_Implementation(const ASpecOpCharacter* Character, FText& DisplayTitle, FLinearColor& HighlightColor) const override;
	virtual bool CanInteractWith_Implementation(const ASpecOpCharacter* Character) const override;
	virtual float GetInteractDuration_Implementation() const override;
	virtual void OnInteractEnd_Implementation(ASpecOpCharacter* Interactor, bool bWasCancelled) override;
	// End IInteractive Interface

	// Begin AActor Interface
	virtual void EndPlay(EEndPlayReason::Type EndPlayReason) override;
	// End AActor Interface

	// Begin UObject Interface
	virtual FPrimaryAssetId GetPrimaryAssetId() const override;
	// End UObject Interface

protected:

	/** Any clean up that is required when being removed from
	owner. This is called during end play if being destroyed */
	virtual void CleanupWeapon() { }

public:

	/** Get weapon mesh */
	FORCEINLINE USkeletalMeshComponent* GetMesh() const { return Mesh; }

private:

	/** Weapons mesh, is skeletal to support animations */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* Mesh;

public:

	#if EG_GAMEPLAY_DEBUGGER
	/** Get debug string to display in gameplay debugger */
	virtual void GetDebugString(FString& String) const;
	#endif

public:

	/** The name of this weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive)
	FText WeaponName;

	/** Interaction time required to pick up this weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Interactive, meta = (ClampMin = 1))
	float InteractTime;

public:

	/** The equipment socket for this weapon when in use */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpecOp)
	FName EquipSocket;

	/** The holstering socket for this weapon when in inventory but not in use */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SpecOp)
	FName HolsterSocket;

public:

	/** Notifies this weapon that it has been added to the spec ops inventory.
	When deriving, super should be called first so SpecOpOwner is set */
	virtual void OnAddToInventory(ASpecOpCharacter* SpecOp);

	/** Notifies this weapon that it has been removed from the spec ops inventory.
	When deriving, super should be called last, as SpecOpOwner is cleared */
	virtual void OnRemoveFromInventory();

protected:

	/** Blueprint event for when being added to inventory */
	UFUNCTION(BlueprintImplementableEvent, Category = Weapon, meta = (DisplayName = "On Add To Inventory"))
	void BlueprintOnAddToInventory(ASpecOpCharacter* NewOwner);

	/** Blueprint event for when being removed from inventory */
	UFUNCTION(BlueprintImplementableEvent, Category = Weapon, meta = (DisplayName = "On Remove From Inventory"))
	void BlueprintOnRemoveFromInventory();

protected:

	/** Required for derived types to give themselves to the spec op */
	virtual void GiveSelfTo(ASpecOpCharacter* SpecOp);

	/** Required for derived types to remove themselves from the spec op */
	virtual void RemoveSelfFrom(ASpecOpCharacter* SpecOp);

public:

	/** Get this weapons type */
	UFUNCTION(BlueprintPure, Category = Weapon)
	virtual EWeaponType GetWeaponType() const { return EWeaponType::Gun; }

	/** Get if this weapon can be added to an inventory of given character */
	UFUNCTION(BlueprintPure, Category = Weapon)
	bool CanAddToInventory(const ASpecOpCharacter* SpecOp) const;

	/** Get if this weapon can be removed from inventory of given character */
	UFUNCTION(BlueprintPure, Category = Weapon)
	bool CanRemoveFromInventory(const ASpecOpCharacter* SpecOp) const;

	/** Get if this weapon is in a spec ops inventory */
	UFUNCTION(BlueprintPure, Category = Weapon)
	bool IsInInventory() const { return SpecOpOwner.IsValid(); }

protected:

	/** Small helper function for setting up abilities. Will add the ability
	to give system component if ability is valid and set us as the source */
	FGameplayAbilitySpecHandle GiveAbilityIfValid(UAbilitySystemComponent* ASC, TSubclassOf<UGameplayAbility> Ability, int32 InputID = -1);

	/** Overloaded to support soft class pointers (see GiveAbilityIfValid) */
	FGameplayAbilitySpecHandle GiveAbilityIfValid(UAbilitySystemComponent* ASC, TSoftClassPtr<UGameplayAbility> Ability, bool bForce, int32 InputID = -1);

protected:

	/** The spec op who owns this weapon */
	TWeakObjectPtr<ASpecOpCharacter> SpecOpOwner;

public:

	/** Detaches this weapon from our owner (doesn't remove from inventory) */
	void DetachWeaponFromOwnerOnly();

protected:

	/** Will attach this weapon to our owner at given socket */
	void AttachWeaponToOwner(const FName& Socket);

	/** Finds a suitable place around our owner before 'dropping' ourselves */
	void DropWeaponAroundOwner(float Offset = 100.f);

public:

	/** Plays an audio coming from the weapon itself, iterrupting the sound currently playing */
	UFUNCTION(BlueprintCallable, Category = Audio)
	void PlayWeaponSound(USoundBase* Sound);

	/** Stops the weapon sound that is playing */
	UFUNCTION(BlueprintCallable, Category = Audio)
	void StopWeaponSound();

	/** Only stops weapon sound if passed sound is playing */
	UFUNCTION(BlueprintCallable, Category = Audio)
	void StopWeaponSoundIfPlaying(USoundBase* Sound);

private:

	/** The audio component the represents the sound this weapon makes itself. Shouldn't
	be used for any sound that might linger or shouldn't be interrupted by other sounds */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components, meta = (AllowPrivateAccess = "true"))
	UAudioComponent* WeaponAudio;
};