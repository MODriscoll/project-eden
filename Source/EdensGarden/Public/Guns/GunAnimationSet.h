// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "GunAnimationSet.generated.h"

class UAnimMontage;
class UAnimSequenceBase;
class UBlendSpaceBase;

/** Struct that holds data used by animation instances for guns */
USTRUCT(BlueprintType)
struct EDENSGARDEN_API FGunAnimationData
{
	GENERATED_BODY()

public:

	FGunAnimationData()
	{
		StandIdleAnimation = nullptr;
		CrouchIdleAnimation = nullptr;
		IdleAimOffset = nullptr;
		StandWalkBlendspace = nullptr;
		CrouchWalkBlendspace = nullptr;
		FocusedStandWalkBlendspace = nullptr;
		FocusedCrouchWalkBlendspace = nullptr;
		SprintAnimation = nullptr;
		FallingAnimation = nullptr;
		LandingAnimation = nullptr;
		DeathMontage = nullptr;
	}

public:

	/** Animation for idle when standing while not focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* StandIdleAnimation;

	/** Animation for idle when crouched while not focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* CrouchIdleAnimation;

	/** Animation for idle when standing while focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* FocusedStandIdleAnimation;

	/** Animation for idle when crouched while focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* FocusedCrouchIdleAnimation;

	/** Offset to apply to idle animations while not focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* IdleAimOffset;

	/** Offset to apply to idle animations while focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* FocusedIdleAimOffset;

	/** Blendspace for walking while standing and not focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* StandWalkBlendspace;

	/** Blendspace for walking while crouched and not focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* CrouchWalkBlendspace;

	/** Blendspace for walking while standing and focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* FocusedStandWalkBlendspace;

	/** Blendspace for walking while crouched and focused */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* FocusedCrouchWalkBlendspace;

	/* Blendspace for sprinting */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UBlendSpaceBase* SprintAnimation;

	/** Animation for falling */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* FallingAnimation;

	/** Animation for landing */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimSequenceBase* LandingAnimation;

	/** Animation to play on death */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Animations)
	UAnimMontage* DeathMontage;

	/** Optional idle animations to play if not applying offsets and not moving */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, AdvancedDisplay, Category = Animations)
	TArray<UAnimSequenceBase*> IdleBreakAnimations;
};

/** Object containing the mobility animations to play when having a gun with this set equipped */
UCLASS(BlueprintType)
class EDENSGARDEN_API UGunAnimationSet : public UDataAsset
{
	GENERATED_BODY()
	
public:

	/** Animtion data to use with gun */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (ShowOnlyInnerProperties = "true"))
	FGunAnimationData AnimationData;
};
