// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Targets/GunTargetActor_LineTrace.h"
#include "GunTargetActor_TargetedLineTrace.generated.h"

/** Line trace that works by tracing from the origin directly to the target
location or by tracing a random point with the radius of the target location */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_TargetedLineTrace : public AGunTargetActor_LineTrace
{
	GENERATED_BODY()
	
public:

	AGunTargetActor_TargetedLineTrace();

protected:
	
	// Begin AGunTargetActor Interface
	virtual void InternalInitialize_Implementation() override;
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

protected:

	/** Gets the location to trace to (spread not applied) */
	FVector GetTargetPoint() const;

public:

	/** The location to target */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Target", meta = (ExposeOnSpawn = true))
	FVector TargetLocation;

	/** If a random point around the target location should be traced instead of directly tracing it */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Target", meta = (ExposeOnSpawn = true))
	uint32 bTraceTargetDirectly : 1;

	/** If not tracing target directly, the radius around the target location we should find a point to trace */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Target", meta = (ExposeOnSpawn = true, ClampMin = 0))
	float TargetingRadius;

	/** The odds of us hitting the targeting directly, 0 is more likely to hit target while 1 is less likely */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Target", meta = (ExposeOnSpawn = true, ClampMin = 0, ClampMax = 1))
	float TargetingPrecisionOdds;
};
