// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Targets/GunTargetActor.h"
#include "GunTargetActor_ProjectileTrace.generated.h"

/** Line trace that will try to predict the path of a projectile
then performing multiple linear traces along the predicted path */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_ProjectileTrace : public AGunTargetActor
{
	GENERATED_BODY()
	
public:

	AGunTargetActor_ProjectileTrace();

protected:
	
	// Begin AGunTargetActor Interface
	virtual bool RequiresSourceActor_Implementation() const override { return false; }
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

public:

	/** The speed of the projectile, origins rotation is scaled by this value to calculate launch velocity */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	float Speed;

	/** The radius of the projectile, zero or less means use lines instead */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	float ProjectileRadius;

	/** Max simulation time for the projectile */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	float MaxSimTime;

	/** Trace channel to use */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	TEnumAsByte<ECollisionChannel> TraceChannel;

	/** Determines size of each sub-step in the simulation (chopping up MaxSimTime). 
	Recommended between 10 to 30 depending on desired quality versus performance */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	float SimFrequency;

	/** Optional override of Gravity (if 0, uses WorldGravityZ) */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	float OverrideGravityZ;

	/** If to trace complex collision rather than simple collision */
	UPROPERTY(BlueprintReadOnly, Category = "Projectile", meta = (ExposeOnSpawn = true))
	uint8 bTraceComplex : 1;
};
