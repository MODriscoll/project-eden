// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Targets/GunTargetActor_LineTrace.h"
#include "GunTargetActor_PlayerLineTrace.generated.h"

UENUM(BlueprintType)
enum class EPlayerLineTraceMode : uint8
{
	/** Trace from player controller only */
	ControllerOnly,

	/** Trace from the origin only */
	OriginOnly,

	/** Trace from player controller first, then from
	the origin to the point the player trace hit */
	ControllerThenOrigin
};

/** Line trace that works by (optionally) first tracing from the player controller,
then using the hit location from the player controller to trace to from the origin set */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_PlayerLineTrace : public AGunTargetActor_LineTrace
{
	GENERATED_BODY()
	
public:

	AGunTargetActor_PlayerLineTrace();

protected:

	// Begin AGunTargetActor Interface
	virtual void InternalInitialize_Implementation() override;
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

public:

	/** If player controller should be traced */
	bool ShouldTraceController() const { return TraceMode != EPlayerLineTraceMode::OriginOnly; }

	/** If origin should be traced */
	bool ShouldTraceOrigin() const { return TraceMode != EPlayerLineTraceMode::ControllerOnly; }
	
public:

	/** How this trace will function */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Player", meta = (ExposeOnSpawn = true))
	EPlayerLineTraceMode TraceMode;

	/** Dot product tolerance for when tracing from origin to player traced location */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Player", meta = (ExposeOnSpawn = true))
	float PlayerTraceTolerance;

	/** Trace channel to use when performing player trace. The first blocking
	hit is used when updating the trace or end of player trace if no hit */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Player", meta = (ExposeOnSpawn = true))
	TEnumAsByte<ECollisionChannel> PlayerTraceChannel;
};
