// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Targets/GunTargetActor.h"
#include "Engine/CollisionProfile.h"
#include "GunTargetActor_ShapeTrace.generated.h"

// TODO: clean up

/** Target actor that uses a shape to query the world */
UCLASS(abstract)
class EDENSGARDEN_API AGunTargetActor_ShapeTrace : public AGunTargetActor
{
	GENERATED_BODY()
	
public:

	AGunTargetActor_ShapeTrace();

protected:

	// Begin AGunTargetActor Interface
	virtual bool RequiresSourceActor_Implementation() const override { return bIgnoreSourceActor; }
	// End AGunTargetActor Interface

protected:

	/** Performs the trace from start to finish */
	void PerformTrace(FGameplayAbilityTargetDataHandle& Handle, const FCollisionShape& Shape) const;

public:

	/** Translates the given targeting location
	info by given amount along desired axis */
	void OffsetTargetLocation(
		FGameplayAbilityTargetingLocationInfo& InOutLocationInfo,
		float Amount,
		EAxis::Type Axis = EAxis::X) const;

	/** Copies all actors from hit results into their own array */
	void CopyActorsToArray(
		const TArray<FHitResult>& Results,
		TArray<TWeakObjectPtr<AActor>>& OutActors) const;
	
public:

	/** Performs shape trace. Can pass in optional
	line of sight origin when generating fake hit results */
	bool ShapeTrace(
		TArray<FHitResult>& OutResults,
		UWorld* World,
		const FTransform& Transform,
		const FCollisionShape& Shape,
		const FName& ProfileName,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter,
		const FVector* LineOfSightOrigin = nullptr) const;

public:

	/** Generates a 'fake' hit result for the given actor.
	Can optionally perform a line of sight test to filter out this actor */
	bool GenerateFakeHitResult(
		FHitResult& OutResult, 
		UWorld* World, 
		const FVector& Start, 
		AActor* Actor,
		UPrimitiveComponent* Primitive,
		bool bTestLOS) const;

protected:

	/** Generates query params used by traces */
	FCollisionQueryParams GenerateQueryParams() const;

protected:

	/** Helper function for drawing debug shapes. Will
	only draw debug only if allowed and for requested time */
	void DrawDebugShape(const FTransform& Transform, const FCollisionShape& Shape, FColor Color, float Thickness = 2.f) const;

public:

	/** If our avatar should be ignored */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	uint8 bIgnoreSourceActor : 1;

	/** Profile to use for trace */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	FCollisionProfileName TraceProfile;

	/** The amount of offset to apply to the shapes origin.
	The shape will be pushed from the origin along the origins forward axis. */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	float Offset; 

	/** 
	* If a line-of-sight trace should also be performed. 
	* This will trace from the origin to the hit result and remove
	* it if the trace hit something other that the hit result 
	*/
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	uint8 bPerformLineOfSightTrace : 1;

	/**
	* When performing the line of sight trace, should we use the origin
	* of the trace (with no offset) or the center of the query shape 
	*/
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	uint8 bLineOfSightFromCenter : 1;

	/** The trace profile to use for the line of sight test if enabled */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Shape", meta = (ExposeOnSpawn = true))
	FCollisionProfileName LineOfSightProfile;
};

/** Target actor that uses a box to query the world */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_BoxTrace : public AGunTargetActor_ShapeTrace
{
	GENERATED_BODY()

public:

	AGunTargetActor_BoxTrace();

protected:

	// Begin AGunTargetActor Interface
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

public:

	/** The half extent of the box (half its size) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Box", meta = (ExposeOnSpawn = true))
	FVector HalfExtents;
};

/** Target actor that uses a sphere to query the world */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_SphereTrace : public AGunTargetActor_ShapeTrace
{
	GENERATED_BODY()

public:

	AGunTargetActor_SphereTrace();

protected:

	// Begin AGunTargetActor Interface
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

public:

	/** The radius of the sphere */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Sphere", meta = (ExposeOnSpawn = true))
	float Radius;
};

/** Target actor that uses a capsule to query the world */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_CapsuleTrace : public AGunTargetActor_ShapeTrace
{
	GENERATED_BODY()

public:

	AGunTargetActor_CapsuleTrace();

protected:

	// Begin AGunTargetActor Interface
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

public:

	/** The radius of the capsule */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Capsule", meta = (ExposeOnSpawn = true))
	float Radius;

	/** The half height of the capsule (half its height) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Capsule", meta = (ExposeOnSpawn = true))
	float HalfHeight;
};