// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Abilities/GameplayAbilityTargetTypes.h"
#include "Abilities/GameplayAbilityTargetDataFilter.h"
#include "GunTargetActor.generated.h"

// TODO: Make a pooling system (most likely built
// into the spec ops ability system component  or gun
// TODO: rename to GunTraceActor?

DECLARE_LOG_CATEGORY_EXTERN(LogGunTargetActor, Log, All);

DECLARE_STATS_GROUP(TEXT("GunTargetActor"), STATGROUP_GunTargetActor, STATCAT_Advanced);

/** 
* Actor that performs world traces from a given.
* Is designed to work with the GunTrace ability task
*/
UCLASS(abstract, NotPlaceable, Blueprintable)
class EDENSGARDEN_API AGunTargetActor : public AActor
{
	GENERATED_BODY()
	
public:	

	AGunTargetActor();

public:

	/** Initialize this target */
	void InitializeTargeting(UGameplayAbility* Ability);

	/** Starts performing the targeting */
	void StartTargeting();

	/** Finishes performing the targeting */
	void FinishTargeting(FGameplayAbilityTargetDataHandle Handle);

	/** Cancel the targeting process */
	void CancelTargeting();

protected:

	/** If this trace wants the source actor. If not and
	source actor is invalid, the targeting is cancelled */
	UFUNCTION(BlueprintNativeEvent, Category = GunTargetActor)
	bool RequiresSourceActor() const;
	virtual bool RequiresSourceActor_Implementation() const { return true; }

	/** Notify that the targeting has begun. This is
	when the traces should start querying the world */
	UFUNCTION(BlueprintNativeEvent, Category = GunTargetActor, meta=(DisplayName="Start Targeting", ToolTop="Called when targeting has commenced"))
	void InternalStart();
	virtual void InternalStart_Implementation() { }

	/** Notify that the targeting has finished. This is
	when traces should perform clean up of operations */
	UFUNCTION(BlueprintNativeEvent, Category = GunTargetActor, meta = (DisplayName = "Finish Targeting", ToolTop = "Called when targeting has concluded"))
	void InternalFinish();
	virtual void InternalFinish_Implementation() { }

	/** Internal initialize, can be used to prepare for targeting */
	UFUNCTION(BlueprintNativeEvent, Category = GunTargetActor, meta = (DisplayName = "Initialize Targeting", ToolTip = "Called to initialize targeting prior to starting"))
	void InternalInitialize();
	virtual void InternalInitialize_Implementation() { }

	/** Internal cancel, can be used to handle cancellation */
	UFUNCTION(BlueprintNativeEvent, Category = GunTargetActor, meta = (Display = "Cancel Targeting", ToolTip = "Called when targeting has been cancelled"))
	void InternalCancel();
	virtual void InternalCancel_Implementation() { }

public:

	/** The origin of this trace */
	UPROPERTY(BlueprintReadOnly, Category = Trace, meta = (ExposeOnSpawn = true))
	FGameplayAbilityTargetingLocationInfo Origin;

	/** Filter to use while tracing */
	UPROPERTY(BlueprintReadOnly, Category = Trace, meta = (ExposeOnSpawn = true))
	FGameplayTargetDataFilterHandle Filter;

	/** If debugging should be drawn (Do not use this to visualize your traces in the final game!) */
	UPROPERTY(BlueprintReadOnly, Category = Debug, meta = (ExposeOnSpawn = true))
	uint8 bDrawDebug : 1;

	/** How long the debug visuals should last */
	UPROPERTY(BlueprintReadOnly, Category = Debug, meta = (ExposeOnSpawn = true))
	float DebugDuration;

public:

	/** Callback we execute once the trace is complete */
	FAbilityTargetData OnTraceCompleteDelegate;

	/** Callback we execute if we were cancelled for any reason */
	FAbilityTargetData OnCancelledDelegate;

public:

	/** Ability that owns this target */
	UPROPERTY()
	UGameplayAbility* OwningAbility;

	/** The player that owns this (can possibly be null) */
	UPROPERTY(BlueprintReadOnly, Category = Player)
	APlayerController* OwningController;

protected:

	/** Source actor of trace (avatar of owning ASC). Only valid on start of targeting */
	UPROPERTY(BlueprintReadOnly, Category = Trace)
	AActor* SourceActor;

protected:

	/** Helper function for drawing debug lines. Will
	only draw debug only if allowed and for requested time */
	UFUNCTION(BlueprintCallable, Category = "Trace|Line")
	void DrawDebugTrace(const FVector& Start, const FVector& End, FColor Color, float Thickness = 2.f) const;
};
