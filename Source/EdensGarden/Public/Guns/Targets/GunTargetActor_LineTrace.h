// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Targets/GunTargetActor.h"
#include "Engine/CollisionProfile.h"
#include "GunTargetActor_LineTrace.generated.h"

// TODO: prob want to make a base line trace just in case
// has supporting funcs and variables

struct FGameplayAbilityTargetDataHandle;

/** Target actor that performs a simple line trace of the world */
UCLASS()
class EDENSGARDEN_API AGunTargetActor_LineTrace : public AGunTargetActor
{
	GENERATED_BODY()
	
public:

	AGunTargetActor_LineTrace();
	
protected:

	// Begin AGunTargetActor Interface
	virtual void InternalInitialize_Implementation() override;
	virtual void InternalStart_Implementation() override;
	// End AGunTargetActor Interface

protected:

	/** Performs trace into world using given profile, returning all target data within max num of hits */
	FGameplayAbilityTargetDataHandle TraceWorldByProfile(
		const FVector& Start,
		const FVector& End,
		const FName& ProfileName,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

	/** Performs trace into the world using given channel, returning all target data within max num of hits */
	FGameplayAbilityTargetDataHandle TraceWorldByChannel(
		const FVector& Start, 
		const FVector& End, 
		TEnumAsByte<ECollisionChannel> Channel, 
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

public:

	/** Performs a single line trace using profile */
	bool SingleLineTrace(
		FHitResult& OutResult,
		UWorld* World,
		const FVector& Start,
		const FVector& End,
		const FName& ProfileName,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

	/** Performs a multi-line trace using profile */
	bool MultiLineTrace(
		TArray<FHitResult>& OutResults,
		UWorld* World,
		const FVector& Start,
		const FVector& End,
		const FName& ProfileName,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

	/** Performs a single line trace using channel */
	bool SingleLineTrace(
		FHitResult& OutResult,
		UWorld* World,
		const FVector& Start,
		const FVector& End,
		TEnumAsByte<ECollisionChannel> Channel,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

	/** Performs a multi-line trace using channel */
	bool MultiLineTrace(
		TArray<FHitResult>& OutResults,
		UWorld* World,
		const FVector& Start,
		const FVector& End,
		TEnumAsByte<ECollisionChannel> Channel,
		const FCollisionQueryParams& Params,
		const FGameplayTargetDataFilterHandle& Filter) const;

protected:

	/** Helper function for applying spread base on the spread member variables */
	UFUNCTION(BlueprintPure, Category = "Trace|Line")
	FVector ApplySpread(FVector Direction) const;

public:

	/** The range of the trace */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	float TraceRange;

	/** Channel to use for trace */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	TEnumAsByte<ECollisionChannel> TraceChannel;

	/** How many overlaps we should allow till considered blocked. 0 means how ever many are traced.
	Note: The return target data can have less than the set value here even if
	actors were hit. This is because a trace is stopped when it encounters a
	blocking hit, so make sure the channel is working with overlaps for best results! */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true, ClampMin=0))
	int32 MaxNumOfHits;

	/** If spread should be applied when tracing from origin. This is
	useful for when using bTracePlayerFirst as rotation given from origin
	is ignored when set to true */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	uint8 bApplySpread : 1;

	/** Min spread to apply if applying spread (in degrees) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	float MinSpread;

	/** Max spread to apply if applying spread (in degrees) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	float MaxSpread;

	/** The min velocity length that used to calculate spread (see UEdensGardenFunctionLibrary::ApplySpreadToDirection) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	float MinVelocity;

	/** The max velocity length that used to calculate spread (see UEdensGardenFunctionLibrary::ApplySpreadToDirection) */
	UPROPERTY(BlueprintReadOnly, Category = "Trace|Line", meta = (ExposeOnSpawn = true))
	float MaxVelocity;
};

