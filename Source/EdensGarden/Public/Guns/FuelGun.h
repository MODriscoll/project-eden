// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Gun.h"
#include "FuelGun.generated.h"

/** Save data for serializing a FuelGun */
USTRUCT()
struct EDENSGARDEN_API FFuelGunSaveData
{
	GENERATED_BODY()

public:

	/** Fuel count of gun */
	UPROPERTY()
	float FuelCount;

public:

	friend FArchive& operator << (FArchive& Ar, FFuelGunSaveData& SaveData)
	{
		Ar << SaveData.FuelCount;

		return Ar;
	}
};

/** Base for a gun that utilizes a regenerating fuel structure */
UCLASS(abstract)
class EDENSGARDEN_API AFuelGun : public AGunBase
{
	GENERATED_BODY()
	
public:
	
	AFuelGun();

public:

	// Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	// End UObject Interface

protected:

	// Begin AGunBase Interface
	virtual void InitAttributeDefaults(const UDataTable* DataTable) override;
	// End AGunBase Interface

public:

	/** Get the fuel gun attribute set */
	UFuelGunAttributeSet* GetAttributeSet() const { return AttributeSet; }

private:

	/** Attribute set for this gun */
	UPROPERTY()
	UFuelGunAttributeSet* AttributeSet;

public:

	/** Get current fuel in tank */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetFuelCount() const;

	/** Get max fuel in tank */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxFuelCount() const;

public:

	// Begin AWeaponBase Interface
	#if EG_GAMEPLAY_DEBUGGER
	virtual void GetDebugString(FString& String) const override;
	#endif
	// End AWeaponBase Interface

protected:

	/** Event for when the fuel tank count has changed */
	UFUNCTION(BlueprintImplementableEvent)
	void OnFuelUpdated(float Fuel, float MaxFuel);

private:

	/** Callback from attribute set to notify on fuel tank count changing */
	UFUNCTION()
	void OnFuelCountChanged(float CurrentFuel, float MaxFuel);
};
