// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "ReloadCalculation.generated.h"

/** Performs a simple gun reload calculation */
UCLASS()
class EDENSGARDEN_API UReloadCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
	
public:

	UReloadCalculation();

public:

	// Begin UGameplayEffectExecutionCalculation Interface
	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams, FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const override;
	// End UGameplayEffectExecutionCalculation Interface
};
