// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Guns/Gun.h"
#include "AmmoGun.generated.h"

/** Save data for serializing an AmmoGun */
USTRUCT()
struct EDENSGARDEN_API FAmmoGunSaveData
{
	GENERATED_BODY()

public:

	/** Clip count of gun */
	UPROPERTY()
	float ClipCount;

	/** Reserve ammo of gun */
	UPROPERTY()
	float ReserveAmmo;

public:

	friend FArchive& operator << (FArchive& Ar, FAmmoGunSaveData& SaveData)
	{
		Ar << SaveData.ClipCount;
		Ar << SaveData.ReserveAmmo;

		return Ar;
	}
};

/** Base for a gun that utilizes basic ammo structure (clip and reserve) */
UCLASS(abstract)
class EDENSGARDEN_API AAmmoGun : public AGunBase
{
	GENERATED_BODY()
	
public:

	AAmmoGun();

public:

	// Begin UObject Interface
	virtual void Serialize(FArchive& Ar) override;
	// End UObject Interface

protected:

	// Begin AGunBase Interface
	virtual void InitAttributeDefaults(const UDataTable* DataTable) override;
	virtual void SetupActions(UAbilitySystemComponent* OwnerASC) override;
	virtual void ClearActions(UAbilitySystemComponent* OwnerASC) override;
	// End AGunBase Interface
	
public:

	/** Get ammo gun attribute set */
	UAmmoGunAttributeSet* GetAttributeSet() const { return AttributeSet; }

private:

	/** Attribute set for this gun */
	UPROPERTY()
	UAmmoGunAttributeSet* AttributeSet;

public:

	/** Get current ammo in clip */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetClipCount() const;

	/** Get max ammo in clip */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxClipCount() const;

	/** Get current reserve ammo */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetReserveAmmo() const;

	/** Get max ammo in reserve */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxReserveAmmo() const;

	/** Get the total ammo this gun holds (clip count + reserve ammo) */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetTotalAmmo() const;

	/** Get the max total ammo this gun can hold at a time (max clip count + max reserve ammo) */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetMaxTotalAmmo() const;

public:

	// Begin AWeaponBase Interface
	#if EG_GAMEPLAY_DEBUGGER
	virtual void GetDebugString(FString& String) const override;
	#endif
	// End AWeaponBase Interface

protected:

	/** This guns reload action, given to player
	on equipment and removed when holstered */
	UPROPERTY(EditDefaultsOnly, Category = Gun)
	TSubclassOf<UGameplayAbility> ReloadAction;

	/** Handle to reload action ability given to owner ASC */
	FGameplayAbilitySpecHandle ReloadActionHandle;

public:

	/** Given the amount of ammo, how much this gun would consume in order to refill ammo */
	UFUNCTION(BlueprintPure, Category = Attributes)
	float GetAmmoNeededToRefill(float MaxAmmoToRefill) const;

protected:

	/** Event for when the ammo count has changed */
	UFUNCTION(BlueprintImplementableEvent)
	void OnAmmoUpdated(float ClipCount, float ReserveAmmo);

private:

	/** Callback from attribute set to notify on ammo count changing */
	UFUNCTION()
	void OnAmmoCountChanged(float Clip, float Reserve);
};
