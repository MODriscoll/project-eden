// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Abilities/Tasks/AbilityTask.h"
#include "AbilityTask_GunTrace.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FGunTraceDelegate, const FGameplayAbilityTargetDataHandle&, Data);

class AGunTargetActor;

// TODO: Make this support object pooling!

/** Ability tasks for performing trace for 'guns'. This task
doesn't require the gun type to work, it's just designed for
traces that would work with certain types of guns */
UCLASS()
class EDENSGARDEN_API UAbilityTask_GunTrace : public UAbilityTask
{
	GENERATED_BODY()
	
public:

	/** Callback for when trace has finished */
	UPROPERTY(BlueprintAssignable)
	FGunTraceDelegate TraceFinished;
	
	/** Callback for when trace has been cancelled */
	UPROPERTY(BlueprintAssignable)
	FGunTraceDelegate Cancelled;

public:

	/** Callback for when the trace has finished */
	UFUNCTION()
	void OnTraceFinished(const FGameplayAbilityTargetDataHandle& Data);

	/** Callback for when the trace has been cancelled */
	UFUNCTION()
	void OnTraceCancelled(const FGameplayAbilityTargetDataHandle& Data);

public:

	/** 
	* Performs a query in the world using a gun targeting actor and waits for target information. 
	* These queries can be instant or hang over time, with no data being returned. The task doesn't
	* require the gun type to work, the traces are just designed for guns in mind.
	*/
	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true", HideSpawnParms = "Instigator"), Category = "Ability|Tasks")
	static UAbilityTask_GunTrace* GunTrace(UGameplayAbility* OwningAbility, FName TaskInstanceName, TSubclassOf<AGunTargetActor> Class);

	/** Activates this task */
	virtual void Activate() override;

public:

	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true"), Category = "Abilities")
	bool BeginSpawningActor(UGameplayAbility* OwningAbility, TSubclassOf<AGunTargetActor> Class, AGunTargetActor*& SpawnedActor);

	UFUNCTION(BlueprintCallable, meta = (HidePin = "OwningAbility", DefaultToSelf = "OwningAbility", BlueprintInternalUseOnly = "true"), Category = "Abilities")
	void FinishSpawningActor(UGameplayAbility* OwningAbility, AGunTargetActor* SpawnedActor);

protected:

	// Begin UGameplayTask Interface
	virtual void OnDestroy(bool AbilityEnded) override;
	// End UGameplayTask Interface

protected:

	/** The type of target actor to spawn */
	TSubclassOf<AGunTargetActor> TargetClass;

	/** The target actor we have spawned and are waiting on */
	UPROPERTY()
	AGunTargetActor* TargetActor;
};

/**
*	Requirements for using Begin/Finish SpawningActor functionality:
*		-Have a parameters named 'Class' in your Proxy factor function (E.g., WaitTargetdata)
*		-Have a function named BeginSpawningActor w/ the same Class parameter
*			-This function should spawn the actor with SpawnActorDeferred and return true/false if it spawned something.
*		-Have a function named FinishSpawningActor w/ an AActor* of the class you spawned
*			-This function *must* call ExecuteConstruction + PostActorConstruction
*/