// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Weapons/Weapon.h"
#include "GameplayAbilityTypes.h"
#include "GunAnimationSet.h"
#include "GunAttributeSet.h"
#include "Gun.generated.h"

/** Base class for all gun the spec op can equip. The spec op can carry more than one
gun at a time, but can only ever have one equipped at any point in time. Guns require
ammunition, thus have an attribute set which can be modified by gameplay effects */
UCLASS(abstract)
class AGunBase : public AWeaponBase, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:

	AGunBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get());

public:

	// Begin IAbilitySystem Interface
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override { return AbilitySystem; }
	// End IAbilitySystem Interface

	// Begin AWeaponBase Interface
	virtual EWeaponType GetWeaponType() const override final { return EWeaponType::Gun; }
	virtual void OnAddToInventory(ASpecOpCharacter* SpecOp) override;
	virtual void OnRemoveFromInventory() override;
	#if EG_GAMEPLAY_DEBUGGER
	virtual void GetDebugString(FString& String) const override;
	#endif
	// End AWeaponBase Interface

	// Begin AActor Interface
	virtual void PostInitializeComponents() override;
	virtual void BeginPlay() override;
	// End AActor Interface

public:

	/** Get this guns widget data */
	const FGunWidgetData& GetWidgetData() const { return WidgetData; }

protected:

	/** The HUD to use for this gun */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD, meta = (ShowOnlyInnerProperties=true))
	FGunWidgetData WidgetData;

private:

	/** Ability system used for attribute interaction */
	UPROPERTY(VisibleAnywhere, Category = Components)
	UAbilitySystemComponent* AbilitySystem;

protected:

	/** Gets called on begin play, should init stats for weapon using data table */
	virtual void InitAttributeDefaults(const UDataTable* DataTable) { }

private:

	/** The data table to initialize attributes with on BeginPlay */
	UPROPERTY(EditAnywhere, Category = Attributes)
	UDataTable* AttributeDefaults;

protected:

	// Begin AWeaponBase Interface
	virtual void CleanupWeapon() override;
	virtual void GiveSelfTo(ASpecOpCharacter* SpecOp) override final;
	virtual void RemoveSelfFrom(ASpecOpCharacter* SpecOp) override final;
	// End AWeaponBase Interface

public:

	/** Notifies this gun that owner is starting to equip it */
	void OnEquipStart();

	/** Notifies this gun that owner has finished equipping it */
	void OnEquipFinished();

	/** Notifies this gun that equip ability was skipped, this could be due to ability activation failure */
	void OnEquipSkipped();

	/** Notifies this gun that owner has cancelled equipping it */
	void OnEquipCancelled();

	/** Notifies this gun that owner is starting to holster it */
	void OnHolsterStart();

	/** Notifies this gun that owner has finished holstering it */
	void OnHolsterFinished();

	/** Notifies this gun that holster ability was skipped, this could be due to ability activation failure */
	void OnHolsterSkipped();

	/** Notifies this gun that owner has cancelled holstering it */
	void OnHolsterCancelled();

	/** Notifies this gun that it has been 'physically equipped', this
	is when the gun appears in our owners hands and can be used */
	void OnPhysicallyEquipped();

	/** Notifies this gun that it has been 'physically holstered', this
	is when the gun appears in the holstering slot on the owners body and
	guns actions should no longer be usable by the owner */
	void OnPhysicallyHolstered();

public:

	/** Get if this gun can be equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	virtual bool CanBeEquipped() const { return true; }

	/** Get if this gun can be holstered */
	UFUNCTION(BlueprintPure, Category = Gun)
	virtual bool CanBeHolstered() const { return true; }

	/** Get if this gun is equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool IsEquipped() const { return SwitchState == ESwitchGunState::Equipped; }

	/** Get if this gun is holstered */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool IsHolstered() const { return SwitchState == ESwitchGunState::Holstered; }

	/** If this gun is in the process of being equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool IsPendingEquip() const { return SwitchState == ESwitchGunState::PendingEquip; }

	/** If this gun is in the process of being holstered */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool IsPendingHolster() const { return SwitchState == ESwitchGunState::PendingHolster; }

	/** If guns is physically equipped (in owners hand) */
	bool IsPhysicallyEquipped() const { return bPhysicallyEquipped; }

protected:

	/** Event for when this gun is starting to be equipped */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DisplayName = "On Start Equipped"))
	void OnEquipStartByOwner();

	/** Event for when this gun has been equipped (actions can be used) */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DisplayName = "On Equipped"))
	void OnEquippedByOwner();

	/** Event for when this gun is starting to be holstered */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DisplayName = "On Start Holster"))
	void OnHolsterStartByOwner();

	/** Event for when this gun has been holstered (actions cant be used) */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DisplayName = "On Holstered"))
	void OnHolsteredByOwner();

	/** Event for when gun has been physically equipped (attached to owners equip hand) */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DisplayName = "On Physically Equipped"))
	void OnPhysicallyEquippedByOwner();

	/** Event for when gun has been physically holsterd (attached to owners holster slot) */
	UFUNCTION(BlueprintImplementableEvent, Category = Gun, meta = (DiplsyaName = "On Physically Holstered"))
	void OnPhysicallyHolsteredByOwner();

public:

	/** Get this weapons switch state (only valid if in inventory) */
	ESwitchGunState GetSwitchState() const { return SwitchState; }

private:

	/** The current switching gun of the weapon. This tracks our equipment logic */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	ESwitchGunState SwitchState;

	/** If this weapon is 'physically' equipped by our owner. This tracks our visual equipment */
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient, Category = Weapon, meta = (AllowPrivateAccess = "true"))
	uint32 bPhysicallyEquipped : 1;

public:

	/** Get the handle to equip ability */
	const FGameplayAbilitySpecHandle& GetEquipAbilityHandle() const { return EquipAbilityHandle; }

	/** Get the handle to holster ability */
	const FGameplayAbilitySpecHandle& GetHolsterAbilityHandle() const { return HolsterAbilityHandle; }

protected:

	/** The ability to use when the owner wants to equip this gun.
	This ability should notify the owner when equipment has finished */
	UPROPERTY(EditDefaultsOnly, Category = Gun)
	TSubclassOf<UGameplayAbility> EquipAbility;

	/** The ability to use when the owner wants to holster this gun.
	This ability should notify the owner when holstering has finished */
	UPROPERTY(EditDefaultsOnly, Category = Gun)
	TSubclassOf<UGameplayAbility> HolsterAbility;

	/** Handle to the equip ability */
	FGameplayAbilitySpecHandle EquipAbilityHandle;

	/** Handle to the holster ability */
	FGameplayAbilitySpecHandle HolsterAbilityHandle;

protected:

	/** Gives the owners ability system our actions. These actions
	should be bound to appropriate input binding, but not all abilities
	need to be bound to input */
	virtual void SetupActions(UAbilitySystemComponent* OwnerASC);

	/** Removes the abilities given to the owners ability system.
	This should remove any abilities given by the SetupActions function */
	virtual void ClearActions(UAbilitySystemComponent* OwnerASC);

protected:

	/** This guns primary action, given to player
	on equipment and removed when holstered */
	UPROPERTY(EditDefaultsOnly, Category = Gun)
	TSubclassOf<UGameplayAbility> PrimaryAction;

	/** This guns secondary action, given to player
	on equipment and removed when holstered */
	UPROPERTY(EditDefaultsOnly, Category = Gun)
	TSubclassOf<UGameplayAbility> SecondaryAction;

	/** Handle to primary action ability given to owner ASC */
	FGameplayAbilitySpecHandle PrimaryActionHandle;

	/** Handle to secondary action ability given to owner ASC */
	FGameplayAbilitySpecHandle SecondaryActionHandle;

public:

	/** If our owner can crouch with this weapon equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool CanCrouchWhileEquipped() const { return bCanCrouchWhileEquipped; }

	/** If our owner can sprint with this weapon equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool CanSprintWhileEquipped() const { return bCanSprintWhileEquipped; }

	/** If our owner can roll with this weapon equipped */
	UFUNCTION(BlueprintPure, Category = Gun)
	bool CanRollWhileEquipped() const { return bCanRollWhileEquipped; }

protected:

	/** If the owner of this weapon will be able to crouch with us equipped.
	This does not apply when in inventory, only when equipped */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mobility)
	uint32 bCanCrouchWhileEquipped : 1;

	/** If the owner of this weapon will be able to sprint with us equipped.
	This does not apply when in inventory, only when equipped */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mobility)
	uint32 bCanSprintWhileEquipped : 1;

	/** If the owner of this weapon will be able to roll with us equipped.
	This does not apply when in inventory, only when equipped */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mobility)
	uint32 bCanRollWhileEquipped : 1;

public:

	/** The gameplay effect to apply the guns owner when equipped.
	This effect will be removed when holstered or dropped by the owner */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, AdvancedDisplay, Category = Gun)
	TSubclassOf<UGameplayEffect> EquippedEffect;

private:

	/** Handle to equipped effect applied when equipped */
	FActiveGameplayEffectHandle EquippedEffectHandle;

public:

	/** Get animation set to use with this gun */
	UGunAnimationSet* GetAnimationSet() const { return AnimationSet; }

private:

	/** The animation set for owner to play with this gun equipped */
	UPROPERTY(EditDefaultsOnly, Category = Animations)
	UGunAnimationSet* AnimationSet;
};

/** Null gun that does nothing, is used as default for when spec op has no other guns */
UCLASS(NotBlueprintable, NotPlaceable)
class EDENSGARDEN_API ANullGun final : public AGunBase
{
	GENERATED_BODY()
};

