// Copyright of Sock Goblins

#pragma once

#include "CoreMinimal.h"
#include "Weapons/WeaponAttributeSet.h"
#include "TickableAttributeSetInterface.h"
#include "GunAttributeSet.generated.h"

/** Delegate for when either clip or reserve count has changed for an ammo gun */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGunAmmoCountChangeSignature, float, ClipCount, float, ReserveAmmo);

/** Attribute set to be used by guns that require ammunition */
UCLASS()
class EDENSGARDEN_API UAmmoGunAttributeSet : public UWeaponAttributeSet
{
	GENERATED_BODY()
	
public:

	UAmmoGunAttributeSet();

public:

	// Begin UAttributeSet Interface
	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;
	// End UAttributeSet Interface

public:

	/** Amount of ammo in clip */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData ClipCount;
	ATTRIBUTE_ACCESSORS(UAmmoGunAttributeSet, ClipCount)

	/** Max size of ammo in clip */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData MaxClipCount;
	ATTRIBUTE_ACCESSORS(UAmmoGunAttributeSet, MaxClipCount)

	/** Reserve ammo in stock */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData ReserveAmmo;
	ATTRIBUTE_ACCESSORS(UAmmoGunAttributeSet, ReserveAmmo)

	/** Max amount of reserve ammo to stock */
	UPROPERTY(BlueprintReadOnly, Category = "Ammo")
	FGameplayAttributeData MaxReserveAmmo;
	ATTRIBUTE_ACCESSORS(UAmmoGunAttributeSet, MaxReserveAmmo)

public:

	/** Get the total ammo this gun currently carries */
	float GetTotalAmmo() const;

	/** Get the max total ammo this gun can carry at a time (clip + reserve) */
	float GetMaxTotalAmmo() const;

public:

	/** Event for when either clip count or reserve ammo has changed */
	UPROPERTY(meta = (HideFromModifiers, HideFromLevelInfos))
	FGunAmmoCountChangeSignature OnAmmoCountChanged;
};

/** Delegate for when the guns fuel tank ratio has changed */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FGunFuelTankChangeSignature, float, CurrentFuel, float, MaxFuel);

/** Attribute set to be used by guns that work based on fuel that regenerates */
UCLASS()
class EDENSGARDEN_API UFuelGunAttributeSet : public UWeaponAttributeSet, public ITickableAttributeSetInterface
{
	GENERATED_BODY()

public:

	UFuelGunAttributeSet();

public:

	// Begin ITickableAttributeSet Interface
	virtual void Tick(float DeltaTime) override;
	virtual bool ShouldTick() const override;
	// End ITickableAttributeSet Interface

	// Begin UAttributeSet Interface
	virtual void PostGameplayEffectExecute(const struct FGameplayEffectModCallbackData& Data) override;
	// End UAttributeSet Interface

public:

	/** Amount of fuel currently in tank */
	UPROPERTY(BlueprintReadOnly, Category = "Fuel")
	FGameplayAttributeData FuelCount;
	ATTRIBUTE_ACCESSORS(UFuelGunAttributeSet, FuelCount)

	/** Max size of fuel in tank */
	UPROPERTY(BlueprintReadOnly, Category = "Fuel")
	FGameplayAttributeData MaxFuelCount;
	ATTRIBUTE_ACCESSORS(UFuelGunAttributeSet, MaxFuelCount)

	/** The fuel consumption rate for consuming fuel (per second) */
	UPROPERTY(BlueprintReadOnly, Category = "Fuel")
	FGameplayAttributeData FuelConsumptionRate;
	ATTRIBUTE_ACCESSORS(UFuelGunAttributeSet, FuelConsumptionRate)

	/** The fuel regeneration rate (per second), only
	applied if consumption rate is zero or less */
	UPROPERTY(BlueprintReadOnly, Category = "Fuel")
	FGameplayAttributeData FuelRegenRate;
	ATTRIBUTE_ACCESSORS(UFuelGunAttributeSet, FuelRegenRate)
	
public:

	/** Get the tanks full percentage */
	float GetTankFullPercentage() const;

public:

	/** Event for when the fuel tanks current count has changed */
	UPROPERTY(meta = (HideFromModifiers, HideFromLevelInfos))
	FGunFuelTankChangeSignature OnFuelCountChanged;
};