// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "GameFramework/GameStateBase.h"
#include "EdensGardenSaveGameTypes.h"
#include "EdensGardenGameState.generated.h"

class ACheckpointStart;

/** Delegate for when a checkpoint has been reached (and the game has been saved) */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FCheckpointReachSignature, int32, CheckpointIndex, const FName&, CheckpointName, bool, bProgressSaved);

/** Game state that manages the game of Edens Garden */
UCLASS()
class EDENSGARDEN_API AEdensGardenGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:

	// Begin AGameStateBase Interface
	virtual void HandleBeginPlay() override;
	// End AGameStateBase Interface

protected:

	/** Applies the active save game to actors of the world. Does not reset player to latest checkpoint */
	void InitializeSaveGame();

public:

	/** Notifies the game state that a checkpoint has been reached. The checkpoint
	will be compared to the current checkpoint to determine if this checkpoint is new.
	The game will always be saved in index is higher than */
	UFUNCTION(BlueprintCallable, Category = Checkpoints)
	void NotifyCheckpointReached(int32 Index, FName Name);

	/** Notifies the game state that the end of the level has been reached. This will
	write the players attributes and inventory to save data but not write anything to disk */
	UFUNCTION(BlueprintCallable, Category = Checkpoints)
	void NotifyLevelEndReached(FName NextLevelName);

	/** Notifies that the end of the game has been reached. This will destroy the
	current save file and return to the main menu (specified by the level name) */
	UFUNCTION(BlueprintCallable, Category = Checkpoints)
	void NotifyGameEndReached(FName MenuLevelName);

public:

	/** Event for when the checkpoint has been successfully
	reached and the state of the game has been saved */
	UPROPERTY(BlueprintAssignable, Category = Checkpoints)
	FCheckpointReachSignature OnNewCheckpointReached;

public:

	/** Registers that the given actor should be destroyed for current save. This
	change will be recored but not applied until the level itself has been saved */
	UFUNCTION(BlueprintCallable, Category = Saving)
	void SaveActorDestroyed(AActor* Actor);

private:

	/** The actors that should be saved as being destroyed on the next save game */
	UPROPERTY()
	TArray<FName> RecordedDestroyedActors;
};
