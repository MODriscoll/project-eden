// Copyright of Sock Goblins

#pragma once

#include "EdensGarden.h"
#include "Engine/LevelScriptActor.h"
#include "SaveGameInterface.h"
#include "EdensGardenLevelScriptActor.generated.h"

class ASoundBase;

struct FCharacterSpeechRequest;

/** Script actor used by Edens Garden. Supports serializing properties for save game.
Save game properites need to be marked as Save Game in order to be saved out */
UCLASS()
class EDENSGARDEN_API AEdensGardenLevelScriptActor : public ALevelScriptActor, public ISaveGameInterface
{
	GENERATED_BODY()
	
public:
	
	// Begin ISaveGame Interface
	virtual bool ShouldSaveForSaveGame() const override { return true; }
	// End ISaveGame Interface

	// Begin AActor Interface
	virtual void BeginPlay() override;
	// End AActor Interface

public:

	/** Notify from the game state that a checkpoint has 
	been reached. This is called after saving has occured */
	UFUNCTION(BlueprintImplementableEvent)
	void OnCheckpointReached(int32 CheckpointIndex, const FName& CheckpointName, bool bProgressSaved);
};
