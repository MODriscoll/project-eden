// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class EdensGarden : ModuleRules
{
	public EdensGarden(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[]
        {
            "Public",
            "Public/Abilities",
            "Public/AI",
            "Public/Animation",
            "Public/Behaviors",
            "Public/Characters",
            "Public/Gameplay",
            "Public/Guns",
            "Public/Interactives",
            "Public/Menu",
            "Public/Monsters",
            "Public/Physics",
            "Public/Powers",
            "Public/Projectiles",
            "Public/Props",
            "Public/Saving",
            "Public/SpecOp",
            "Public/UMG",
            "Public/Weapons"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "Private",
            "Private/Abilities",
            "Private/AI",
            "Private/Animation",
            "Private/Behaviors",
            "Private/Characters",
            "Private/Debug",
            "Private/Gameplay",
            "Private/Guns",
            "Private/Interactives",
            "Private/Menu",
            "Private/Monsters",
            "Private/Physics",
            "Private/Powers",
            "Private/Projectiles",
            "Private/Props",
            "Private/Saving",
            "Private/SpecOp",
            "Private/UMG",
            "Private/Weapons"
        });

        // General
        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "Engine",
        });

        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "EdensGardenLoadingScreen"
        });

        // Gameplay
        PublicDependencyModuleNames.AddRange(new string[]
        {
            "GameplayAbilities",
            "GameplayTags"
        });

        // AI
        PublicDependencyModuleNames.AddRange(new string[]
        {
            "AIModule"
        });

        // UI
        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Slate",
            "SlateCore",
            "UMG"
        });

        // Required
        PrivateDependencyModuleNames.AddRange(new string[]
        {
            "InputCore", 
            "GameplayTasks"     // Used by both Gameplay Abilities and AI Module
        });

        // Build specific requirements
        if (Target.bBuildDeveloperTools || (Target.Configuration != UnrealTargetConfiguration.Shipping && Target.Configuration != UnrealTargetConfiguration.Test))
        {
            PrivateDependencyModuleNames.Add("GameplayDebugger");

            PublicDefinitions.Add("EG_DRAW_DEBUG=1");
            PublicDefinitions.Add("EG_GAMEPLAY_DEBUGGER=1");
        }
        else
        {
            PublicDefinitions.Add("EG_DRAW_DEBUG=0");
            PublicDefinitions.Add("EG_GAMEPLAY_DEBUGGER=0");
        }
    }
}
