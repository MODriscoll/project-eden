// Copyright of Sock Goblins

#include "EdensGardenLoadingScreenModule.h"
#include "MoviePlayer.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "SThrobber.h"

// Paths are hardcoded since module loads very early during startup
#define EG_LOGO_TEXTURE_PATH	TEXT("/Game/Widgets/Textures/T_BloomLogo.T_BloomLogo")

struct FEdensGardenLoadingScreenBrush : public FSlateDynamicImageBrush, public FGCObject
{
public:

	FEdensGardenLoadingScreenBrush(const FName& InTextureName, const FVector2D& InImageSize)
		: FSlateDynamicImageBrush(InTextureName, InImageSize)
	{
		SetResourceObject(LoadObject<UObject>(nullptr, *InTextureName.ToString()));
	}

public:

	// Begin FGCObject Interface
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override
	{
		UObject* CachedResourceObject = GetResourceObject();
		if (CachedResourceObject)
		{
			Collector.AddReferencedObject(CachedResourceObject);
		}
	}
	// End FGCObject Interface
};

struct FEdensGardenLoadingScreenFont : public FSlateFontInfo, public FGCObject
{
public:

	FEdensGardenLoadingScreenFont(UObject* InFontObject, const int32 InSize)
		: FSlateFontInfo(InFontObject, InSize)
	{
		FontObject = InFontObject;
	}

public:

	// Begin FGCObject Interface
	virtual void AddReferencedObjects(FReferenceCollector& Collector) override
	{
		if (FontObject)
		{
			Collector.AddReferencedObject(FontObject);
		}
	}
	// End FGCObject Interface

private:

	/** Referenced font object */
	UPROPERTY()
	UObject* FontObject;
};

class SEdensGardenLoadingScreen : public SCompoundWidget
{
public:

	SLATE_BEGIN_ARGS(SEdensGardenLoadingScreen) { }
	SLATE_END_ARGS()

	void Construct(const FArguments& InArgs)
	{
		LogoBrush = MakeShareable(new FEdensGardenLoadingScreenBrush(EG_LOGO_TEXTURE_PATH, FVector2D(512.f, 512.f)));

		// Backdrop that surrounds logo (managed by a border)
		FSlateBrush* BackdropBrush = new FSlateBrush();
		BackdropBrush->TintColor = FLinearColor(0.025f, 0.1f, 0.075f, 1.f);

		/** Brush for the chuncks of the throbber */
		FSlateBrush* ThrobberChunk = new FSlateBrush();
		ThrobberChunk->ImageSize = FVector2D(50.f, 50.f);
		ThrobberChunk->TintColor = FLinearColor(0.65f, 0.3f, 0.715f, 1.f);

		ChildSlot
		.HAlign(HAlign_Fill)
		.VAlign(VAlign_Fill)
		.Padding(FMargin(0.f))
		[
			SNew(SBorder)
			.BorderImage(BackdropBrush)
			.HAlign(HAlign_Center)
			.VAlign(VAlign_Center)
			.Padding(FMargin(2.f))
			[
				SNew(SBox)
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				.MinDesiredHeight(800.f)
				[
					SNew(SOverlay)
					+ SOverlay::Slot()
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Center)
					//.Padding(FMargin(25.f))
					[
						SNew(SImage)
						.Image(LogoBrush.Get())
					]
					+ SOverlay::Slot()
					.HAlign(HAlign_Center)
					.VAlign(VAlign_Bottom)
					//.Padding(FMargin(0.f))
					[
						SNew(SThrobber)
						.Visibility(this, &SEdensGardenLoadingScreen::GetLoadingIndicatorVisibility)
						.NumPieces(8)
						.PieceImage(ThrobberChunk)
					]
				]
			]
		];
	}

private:

	/** When to display the loading text and throbber. We only want to display it if something is actually loading */
	EVisibility GetLoadingIndicatorVisibility() const
	{
		IGameMoviePlayer* MoviePlayer = GetMoviePlayer();
		return MoviePlayer->IsLoadingFinished() ? EVisibility::Collapsed : EVisibility::Visible;
	}

private:

	/** Brush used to display logo */
	TSharedPtr<FSlateDynamicImageBrush> LogoBrush;
};

class FEdensGardenLoadingScreenModule : public IEdensGardenLoadingScreenModule
{
public:

	// Begin IEdensGardenLoadingScreenModule Interface
	void DisplayLoadingScreen(bool bAutoComplete, float MinDisplayTime) override
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = bAutoComplete;
		LoadingScreen.bWaitForManualStop = !bAutoComplete;
		LoadingScreen.MinimumLoadingScreenDisplayTime = MinDisplayTime;
		LoadingScreen.WidgetLoadingScreen = SNew(SEdensGardenLoadingScreen);

		IGameMoviePlayer* MoviePlayer = GetMoviePlayer();
		MoviePlayer->SetupLoadingScreen(LoadingScreen);
	}

	void HideLoadingScreen() override
	{
		IGameMoviePlayer* MoviePlayer = GetMoviePlayer();
		MoviePlayer->StopMovie();
	}
	// End IEdensGardenLoadingScreenModule Interface

	// Begin IModule Interface
	virtual void StartupModule() override
	{
		// Force load so cooker references logo texture (used by loading screen widget)
		LoadObject<UObject>(nullptr, EG_LOGO_TEXTURE_PATH);

		if (IsMoviePlayerEnabled())
		{
			// Initial loading screen, while everything else gets ready
			InitializeLoadingScreen();
		}
	}

	virtual bool IsGameModule() const override
	{
		return true;
	}
	// End IModule Interface

private:

	/** Creates and initializes the loading screen */
	void InitializeLoadingScreen()
	{
		FLoadingScreenAttributes LoadingScreen;
		LoadingScreen.bAutoCompleteWhenLoadingCompletes = true;
		LoadingScreen.MinimumLoadingScreenDisplayTime = 5.f;
		LoadingScreen.WidgetLoadingScreen = SNew(SEdensGardenLoadingScreen);

		IGameMoviePlayer* MoviePlayer = GetMoviePlayer();
		MoviePlayer->SetupLoadingScreen(LoadingScreen);
	}
};

#undef EG_LOGO_TEXTURE_PATH

IMPLEMENT_GAME_MODULE(FEdensGardenLoadingScreenModule, EdensGardenLoadingScreen);