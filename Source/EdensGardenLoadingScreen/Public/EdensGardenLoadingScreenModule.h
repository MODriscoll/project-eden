// Copyright of Sock Goblins

#pragma once

#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"

/** Interface for loading screen modules to implement to be compatible with Edens Garden */
class IEdensGardenLoadingScreenModule : public IModuleInterface
{
public:

	/** Get the loading screen module in use */
	static inline IEdensGardenLoadingScreenModule& Get()
	{
		return FModuleManager::LoadModuleChecked<IEdensGardenLoadingScreenModule>("EdensGardenLoadingScreen");
	}

public:

	/** Displays the loading for either user custom time or passed time */
	virtual void DisplayLoadingScreen(bool bAutoComplete, float MinDisplayTime) = 0;

	/** Hides the loading screen */
	virtual void HideLoadingScreen() = 0;
};