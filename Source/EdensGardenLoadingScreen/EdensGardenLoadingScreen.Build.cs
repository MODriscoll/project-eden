// Copyright of Sock Goblins

using UnrealBuildTool;

public class EdensGardenLoadingScreen : ModuleRules
{
	public EdensGardenLoadingScreen(ReadOnlyTargetRules Target) : base(Target)
	{
        // Modules loading phase needs to be "PreLoadingScreen" in order to be hooked in time

        PublicIncludePaths.AddRange(new string[]
        {
            "Public"
        });

        PrivateIncludePaths.AddRange(new string[]
        {
            "Private"
        });

        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "Core",
            "CoreUObject",
            "Engine"
        });

        PrivateDependencyModuleNames.AddRange(new string[] 
        {
            "InputCore",
            "MoviePlayer",
            "Slate",
            "SlateCore"
        });          
	}
}
