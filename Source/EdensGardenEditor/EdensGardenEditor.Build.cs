// Copyright of Sock Goblins

using UnrealBuildTool;

public class EdensGardenEditor : ModuleRules
{
	public EdensGardenEditor(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicIncludePaths.AddRange(new string[] 
        {
            "Public"
        });

        PrivateIncludePaths.AddRange(new string[] 
        {
            "Private",
            "Private/Visualizers"
        });

        PublicDependencyModuleNames.AddRange(new string[] 
        {
            "EdensGarden",
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore"
        });

        PrivateDependencyModuleNames.AddRange(new string[] 
        {
            "ComponentVisualizers",
            "UnrealEd",
            "Slate",
            "SlateCore",
            "EditorStyle"
        });   
	}
}
