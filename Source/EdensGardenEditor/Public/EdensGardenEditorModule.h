// Copyright of Sock Goblins

#pragma once

#include "Modules/ModuleInterface.h"

class FEdensGardenEditorModule : public IModuleInterface
{
public:

	// Begin IModule Interface
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	// End IModule Interface

protected:

	/** Registers the component visualizers */
	void RegisterComponentVisualizers();

	/** UnRegisters the component visualizers */
	void UnregisterComponentVisualizers();
};