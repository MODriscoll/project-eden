// Copyright of Sock Goblins

#pragma once

#include "ComponentVisualizer.h"
#include "PatrolRouteComponent.h"
#include "Framework/Commands/UICommandList.h"

struct HPatrolRouteVisProxy : public HComponentVisProxy
{
	DECLARE_HIT_PROXY();

public:

	HPatrolRouteVisProxy(const UActorComponent* InComponent)
		: HComponentVisProxy(InComponent, HPP_Wireframe)
	{

	}
};

/** Single waypoint selection proxy */
struct HPatrolRouteWaypointProxy : public HPatrolRouteVisProxy
{
	DECLARE_HIT_PROXY();

public:

	HPatrolRouteWaypointProxy(const UActorComponent* InComponent, int32 InWaypointIndex)
		: HPatrolRouteVisProxy(InComponent)
		, WaypointIndex(InWaypointIndex)
	{

	}

public:

	/** Index of the waypoint we represent */
	int32 WaypointIndex;
};

/** A component visualizer that draws and allow editing of a patrol route component */
class EDENSGARDENEDITOR_API FPatrolRouteComponentVisualizer : public FComponentVisualizer
{
public:

	FPatrolRouteComponentVisualizer();
	virtual ~FPatrolRouteComponentVisualizer();

public:

	// Begin FComponent Visualizer Interface
	virtual void OnRegister() override;
	virtual void DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI) override;
	virtual bool VisProxyHandleClick(FEditorViewportClient* InViewportClient, HComponentVisProxy* VisProxy, const FViewportClick& Click) override;
	virtual void EndEditing() override;
	virtual bool GetWidgetLocation(const FEditorViewportClient* ViewportClient, FVector& OutLocation) const override;
	virtual bool HandleInputDelta(FEditorViewportClient* ViewportClient, FViewport* Viewport, FVector& DeltaTranslate, FRotator& DeltaRotate, FVector& DeltaScale) override;
	virtual bool HandleInputKey(FEditorViewportClient* ViewportClient, FViewport* Viewport, FKey Key, EInputEvent Event) override;
	virtual TSharedPtr<SWidget> GenerateContextMenu() const override;
	// End FComponentVisualizer Interface

public:

	/** Get the patrol route being visualized */
	UPatrolRouteComponent* GetEditedPatrolRouteComponent() const;

protected:

	/** Called when duplicating an existing waypoint */
	void DuplicateWaypoint();
	bool CanDuplicateWaypoint();

	/** Called when deleting an existing waypoint */
	void DeleteWaypoint();
	bool CanDeleteWaypoint();

protected:

	/** Index of selected waypoint of edited component */
	int32 SelectedWaypoint;

	/** Command list for editing patrol route */
	TSharedPtr<FUICommandList> PatrolRouteVisualizerActions;

	/** Actor that owns the patorl route component being edited */
	TWeakObjectPtr<AActor> RouteOwningActor;

	/** Name of the property on the actor that owns the edited patrol route */
	FPropertyNameAndIndex RoutePropertyName;
};