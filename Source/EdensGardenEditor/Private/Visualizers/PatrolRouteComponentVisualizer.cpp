// Copyright of Sock Goblins

#include "PatrolRouteComponentVisualizer.h"

#include "Editor.h"
#include "EditorStyleSet.h"
#include "Framework/Commands/Commands.h"
#include "Framework/MultiBox/MultiBoxBuilder.h"

IMPLEMENT_HIT_PROXY(HPatrolRouteVisProxy, HComponentVisProxy);
IMPLEMENT_HIT_PROXY(HPatrolRouteWaypointProxy, HPatrolRouteVisProxy);

#define LOCTEXT_NAMESPACE "PatrolRouteComponentVisualizer"

/** Command list for patrol route component */
class FPatrolRouteVisualizerCommands : public TCommands<FPatrolRouteVisualizerCommands>
{
public:

	FPatrolRouteVisualizerCommands()
		: TCommands<FPatrolRouteVisualizerCommands>
		(
			"PatrolRouteComponentVisualizer",
			LOCTEXT("PatrolRouteComponentVisualizerDesc", "Patrol Route Component Visualizer"),
			NAME_None,
			FEditorStyle::GetStyleSetName()
		)
	{

	}

public:

	// Begin TCommands Interface
	virtual void RegisterCommands() override
	{
		UI_COMMAND(DuplicateKey, "Duplicate Waypoint", "Duplicates the currently selected waypoint", EUserInterfaceActionType::Button, FInputGesture());
		UI_COMMAND(DeleteKey, "Delete Waypoint", "Deletes the currently selected waypoint", EUserInterfaceActionType::Button, FInputGesture());
	}
	// End TCommands Interface

public:

	/** Duplicate waypoint command */
	TSharedPtr<FUICommandInfo> DuplicateKey;

	/** Delete waypoint command */
	TSharedPtr<FUICommandInfo> DeleteKey;
};

FPatrolRouteComponentVisualizer::FPatrolRouteComponentVisualizer()
{
	FPatrolRouteVisualizerCommands::Register();
	SelectedWaypoint = INDEX_NONE;
}

FPatrolRouteComponentVisualizer::~FPatrolRouteComponentVisualizer()
{
	FPatrolRouteVisualizerCommands::Unregister();
}

void FPatrolRouteComponentVisualizer::OnRegister()
{
	// Bind callbacks for UI commands in context menu
	{
		const auto& Commands = FPatrolRouteVisualizerCommands::Get();

		PatrolRouteVisualizerActions = MakeShareable(new FUICommandList);
		PatrolRouteVisualizerActions->MapAction
		(
			Commands.DuplicateKey,
			FExecuteAction::CreateSP(this, &FPatrolRouteComponentVisualizer::DuplicateWaypoint),
			FCanExecuteAction::CreateSP(this, &FPatrolRouteComponentVisualizer::CanDuplicateWaypoint)
		);
		PatrolRouteVisualizerActions->MapAction
		(
			Commands.DeleteKey,
			FExecuteAction::CreateSP(this, &FPatrolRouteComponentVisualizer::DeleteWaypoint),
			FCanExecuteAction::CreateSP(this, &FPatrolRouteComponentVisualizer::CanDeleteWaypoint)
		);
	}
}

void FPatrolRouteComponentVisualizer::DrawVisualization(const UActorComponent* Component, const FSceneView* View, FPrimitiveDrawInterface* PDI)
{
	if (const UPatrolRouteComponent* RouteComponent = Cast<const UPatrolRouteComponent>(Component))
	{
		int32 Count = RouteComponent->GetNumWaypoints();

		const FLinearColor& WColor = RouteComponent->WaypointColor;
		const FLinearColor& WSColor = RouteComponent->SelectedWaypointColor;
		const FLinearColor& RColor = RouteComponent->RouteColor;
		const FLinearColor& RSColor = RouteComponent->SelectedRouteColor;

		// Checking this so we don't draw unnecessary lines
		if (Count == 1)
		{
			FVector Location = RouteComponent->GetWaypointAt(0);
			bool bIsSelected = SelectedWaypoint == 0;
			const FLinearColor& Color = bIsSelected ? WSColor : WColor;

			// Create hit proxy to track input
			PDI->SetHitProxy(new HPatrolRouteWaypointProxy(Component, 0));
			PDI->DrawPoint(Location, Color, 20.f, SDPG_Foreground);
			PDI->SetHitProxy(nullptr);
		}
		else
		{
			for (int32 i = 0; i < Count; ++i)
			{
				// Need to stay within range
				int32 j = (i + 1) % Count;

				// Waypoint locations
				FVector Start = RouteComponent->GetWaypointAt(i);
				FVector End = RouteComponent->GetWaypointAt(j);

				// Used to determine colors
				bool bIsSelected = i == SelectedWaypoint;
				bool bConnectsToSelected = (bIsSelected ? true : j == SelectedWaypoint);

				// Colors to draw points
				const FLinearColor& PColor = bIsSelected ? WSColor : WColor;
				const FLinearColor& LColor = bConnectsToSelected ? RSColor : RColor;

				// Create hit proxy to track input (for current waypoint)
				PDI->SetHitProxy(new HPatrolRouteWaypointProxy(Component, i));
				PDI->DrawPoint(Start, PColor, 20.f, SDPG_Foreground);
				PDI->SetHitProxy(nullptr);

				// TODO: Maybe support dragging lines?
				// Draw segment
				PDI->DrawLine(Start, End, LColor, SDPG_Foreground, 5.f);
			}
		}
	}
}

bool FPatrolRouteComponentVisualizer::VisProxyHandleClick(FEditorViewportClient* InViewportClient, HComponentVisProxy* VisProxy, const FViewportClick& Click)
{
	bool bEditing = false;

	if (VisProxy && VisProxy->Component.IsValid())
	{
		// Property should be valid
		const UPatrolRouteComponent* RouteComponent = Cast<const UPatrolRouteComponent>(VisProxy->Component.Get());
		RoutePropertyName = GetComponentPropertyName(RouteComponent);
		if (RoutePropertyName.IsValid())
		{
			bEditing = true;

			// Might now be editing different actor
			{
				AActor* OldRoute = RouteOwningActor.Get();
				RouteOwningActor = RouteComponent->GetOwner();

				if (OldRoute != RouteOwningActor)
				{
					SelectedWaypoint = INDEX_NONE;
				}
			}

			if (VisProxy->IsA(HPatrolRouteWaypointProxy::StaticGetType()))
			{
				HPatrolRouteWaypointProxy* Proxy = static_cast<HPatrolRouteWaypointProxy*>(VisProxy);
				SelectedWaypoint = Proxy->WaypointIndex;
			}
		}
		else
		{
			RouteOwningActor.Reset();
		}
	}

	return bEditing;
}

void FPatrolRouteComponentVisualizer::EndEditing()
{
	RouteOwningActor.Reset();
	SelectedWaypoint = INDEX_NONE;
}

bool FPatrolRouteComponentVisualizer::GetWidgetLocation(const FEditorViewportClient* ViewportClient, FVector& OutLocation) const
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	if (RouteComponent && SelectedWaypoint != INDEX_NONE)
	{
		OutLocation = RouteComponent->GetWaypointAt(SelectedWaypoint);
		return true;
	}

	return false;
}

bool FPatrolRouteComponentVisualizer::HandleInputDelta(FEditorViewportClient* ViewportClient, FViewport* Viewport, FVector& DeltaTranslate, FRotator& DeltaRotate, FVector& DeltaScale)
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	if (RouteComponent && SelectedWaypoint != INDEX_NONE)
	{
		RouteComponent->OffsetWaypointLocation(DeltaTranslate, SelectedWaypoint);
		return true;
	}

	return false;
}

bool FPatrolRouteComponentVisualizer::HandleInputKey(FEditorViewportClient* ViewportClient, FViewport* Viewport, FKey Key, EInputEvent Event)
{
	// Delete action using keys
	if (Key == EKeys::Delete && Event == EInputEvent::IE_Pressed)
	{
		UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
		if (RouteComponent && RouteComponent->GetNumWaypoints() > 1)
		{
			int32 Index = SelectedWaypoint;
			int32 Count = RouteComponent->GetNumWaypoints();
			
			if (Index >= Count - 1)
			{
				--SelectedWaypoint;
			}

			RouteComponent->RemoveWaypoint(Index);
			return true;
		}
	}

	return false;
}

TSharedPtr<SWidget> FPatrolRouteComponentVisualizer::GenerateContextMenu() const
{
	FMenuBuilder MenuBuilder(true, PatrolRouteVisualizerActions);
	{
		MenuBuilder.BeginSection("Patrol Route Actions", LOCTEXT("WaypointHeading", "Waypoint"));
		{
			if (SelectedWaypoint != INDEX_NONE)
			{
				MenuBuilder.AddMenuEntry(FPatrolRouteVisualizerCommands::Get().DuplicateKey);
				MenuBuilder.AddMenuEntry(FPatrolRouteVisualizerCommands::Get().DeleteKey);
			}
		}
		MenuBuilder.EndSection();
	}

	TSharedPtr<SWidget> MenuWidget = MenuBuilder.MakeWidget();
	return MenuWidget;
}

UPatrolRouteComponent* FPatrolRouteComponentVisualizer::GetEditedPatrolRouteComponent() const
{
	return Cast<UPatrolRouteComponent>(GetComponentFromPropertyName(RouteOwningActor.Get(), RoutePropertyName));
}

void FPatrolRouteComponentVisualizer::DuplicateWaypoint()
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	check(RouteComponent != nullptr);
	check(SelectedWaypoint != INDEX_NONE);

	FVector NewWaypoint = RouteComponent->GetWaypointAt(SelectedWaypoint);

	// Offset waypoint by a bit to prevent overlap
	NewWaypoint += FVector(10.f, 10.f, 0.f);

	SelectedWaypoint = RouteComponent->InsertWorldspaceWaypoint(NewWaypoint, SelectedWaypoint);
}

bool FPatrolRouteComponentVisualizer::CanDuplicateWaypoint()
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	if (RouteComponent)
	{
		return SelectedWaypoint >= 0 && SelectedWaypoint < RouteComponent->GetNumWaypoints();
	}

	return false;
}

void FPatrolRouteComponentVisualizer::DeleteWaypoint()
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	check(RouteComponent != nullptr);
	check(SelectedWaypoint != INDEX_NONE);

	int32 Index = SelectedWaypoint;
	int32 Count = RouteComponent->GetNumWaypoints();

	if (Index >= Count - 1)
	{
		--SelectedWaypoint;
	}

	RouteComponent->RemoveWaypoint(Index);
}

bool FPatrolRouteComponentVisualizer::CanDeleteWaypoint()
{
	UPatrolRouteComponent* RouteComponent = GetEditedPatrolRouteComponent();
	if (RouteComponent)
	{
		if (RouteComponent->GetNumWaypoints() > 1)
		{
			return SelectedWaypoint >= 0 && SelectedWaypoint < RouteComponent->GetNumWaypoints();
		}
	}

	return false;
}

#undef LOCTEXT_NAMESPACE