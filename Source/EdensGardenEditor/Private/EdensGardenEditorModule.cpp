// Copyright of Sock Goblins

#include "EdensGardenEditorModule.h"
#include "EdensGardenEditor.h"

#include "Modules/ModuleManager.h"
#include "ComponentVisualizers.h"
#include "UnrealEd.h"

#include "Visualizers/PatrolRouteComponentVisualizer.h"

#define LOCTEXT_NAMESPACE "EdensGardenEditor"

void FEdensGardenEditorModule::StartupModule()
{
	UE_LOG(LogEdensGardenEditor, Log, TEXT("Module Startup"));

	RegisterComponentVisualizers();

	if (GUnrealEd)
	{
		////TSharedPtr<FComponentVisualizer> Visualizer = MakeShareable(new FGuardPatrolRouteComponentVisualizer);

		//if (Visualizer.IsValid())
		//{
		//	GUnrealEd->RegisterComponentVisualizer(UGuardPatrolRouteComponent::StaticClass()->GetFName(), Visualizer);
		//	Visualizer->OnRegister();
		//}
	}
}

void FEdensGardenEditorModule::ShutdownModule()
{
	UE_LOG(LogEdensGardenEditor, Log, TEXT("Module Shutdown"));

	UnregisterComponentVisualizers();

	if (GUnrealEd)
	{
		//GUnrealEd->UnregisterComponentVisualizer(UGuardPatrolRouteComponent::StaticClass()->GetFName());
	}
}

void FEdensGardenEditorModule::RegisterComponentVisualizers()
{
	FComponentVisualizersModule& VisualizersModule = FModuleManager::LoadModuleChecked<FComponentVisualizersModule>(TEXT("ComponentVisualizers"));
	VisualizersModule.RegisterComponentVisualizer(UPatrolRouteComponent::StaticClass()->GetFName(), MakeShareable(new FPatrolRouteComponentVisualizer));
}

void FEdensGardenEditorModule::UnregisterComponentVisualizers()
{
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_GAME_MODULE(FEdensGardenEditorModule, EdensGardenEditor);