// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class EdensGardenEditorTarget : TargetRules
{
	public EdensGardenEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.AddRange(new string[]
        {
            "EdensGarden",
            "EdensGardenEditor"
        });
	}
}
