# Project Eden

Source code for Project Eden. Not all functionality can be found here due to some functionality being implemented in Blueprints.
<p>Project Eden is a third person shooter with sci-fi and horror elements. You play as a special operative hired to 
investigate a facility gone cold, only to learn the site has been contaminated and turned all the facility members 
into hideous plant monsters. You must either fight or stealth your way past these monsters as you scavenge the 
base for all of its research materials.</p>

<p><b>Watch trailer here: https://www.youtube.com/watch?v=kqwhHmqYKMg</b></p>
<p><b>Download game here: https://drive.google.com/uc?export=download&id=1mPjCoT-yRRuiZhwQeSfZYbVCC0haGHoX</b></p>